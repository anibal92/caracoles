@extends('Layout.app')
@section('titulo', 'Backups')
@section('contenido')
<div class="container-fluid">
    <br>
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="btn btn-primary" id="crearBackupButton"><i class="fa fa-spinner fa-spin"></i>Crear</button>
        </div>
    </div>
    <table id="backupTable" class="table table-bordered table-striped display nowrap" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<!-- Modal Eliminar Backup-->
<div class="modal fade" id="eliminarBackupModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CONFIRMAR</h4>
            </div>
            <div class="modal-body">
                <p><b>¿Está seguro/a que desea eliminar este backup?</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary hover" id="confirmarEliminarBackupButton">
                    <i class="fa fa-spinner fa-spin"></i>
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<script src="{{asset('js/backup/listado.js')}}"></script>
@endsection