<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{asset('img/misc/usuario.png')}}" class="img-circle" alt="User Image" width="35" height="35">
      </div>
      <div class="pull-left info">
        <p>{{session('nombre')}} {{session('apellido')}}</p>
        <a href="#">{{session('usuario')}}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
      <!--<li class="header">MENÚ</li>-->
      <hr>

      <!-- Menú Módulo Usuarios -->
      @if((bool)session('permiso')->modulo_usuarios)
      <li class="treeview">
        <a href="#"><i class="fa fa-gears"></i><span>Administración</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('usuarios/alta')}}">Nuevo Usuario</a></li>
          <li><a href="{{url('usuarios/listado')}}">Listado Usuarios</a></li>
          <li><a href="{{url('backup/listado')}}">Backups</a></li>
        </ul>
      </li>
      @endif

      <!-- Menú Módulo Participantes -->
      @if((bool)session('permiso')->modulo_participantes)
      <li class="treeview">
        <a href="#"><i class="fa fa-users"></i><span>Participantes</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('participantes/alta')}}">Cargar</a></li>
          <li><a href="{{url('participantes/listado')}}">Listado</a></li>
          <li><a href="{{url('participantes/graficos')}}">Gráficos</a></li>
        </ul>
      </li>
      @endif

      <!-- Menú Módulo Cursos -->
      @if((bool)session('permiso')->modulo_cursos )
      <li class="treeview">
        <a href="#"><i class="fa fa-graduation-cap"></i><span>Cursos</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('cursos/listado')}}">Listado</a></li>
          <li><a href="{{url('cursos/educadores/listado')}}">Educadores</a></li>
          <li><a href="{{url('cursos/graficos')}}">Gráficos</a></li>
        </ul>
      </li>
      @endif

      <!-- Menú Módulo Inventario -->
      @if((bool)session('permiso')->modulo_inventario)
      <li class="treeview">
        <a href="#"><i class="fa fa-list-alt"></i><span>Inventario</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('inventario/listado')}}">Listado</a></li>
          <li><a href="{{url('inventario/graficos')}}">Gráficos</a></li>
          <!--<li><a href="{{url('cursos/educadores/listado')}}">Educadores</a></li>-->
        </ul>
      </li>
      @endif

      <!-- Menú Módulo Biblioteca -->
      @if((bool)session('permiso')->modulo_biblioteca)
      <li class="treeview">
        <a href="#"><i class="fa fa-book"></i><span>Biblioteca</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('biblioteca/libro/listado')}}">Libros</a></li>
          <li><a href="{{url('biblioteca/prestamo/listado')}}">Préstamos y Devoluciones</a></li>
          <li><a href="{{url('biblioteca/graficos')}}">Gráficos</a></li>
        </ul>
      </li>
      @endif

      <!-- Menú Módulo Finanzas -->
      @if((bool)session('permiso')->modulo_finanzas)
      <li class="treeview">
        <a href="#"><i class="fa fa-dollar"></i><span>Finanzas</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{url('finanzas/listado')}}">Movimientos</a></li>
          <li><a href="{{url('finanzas/graficos')}}">Resumen</a></li>
          <!--<li><a href="{{url('cursos/educadores/listado')}}">Educadores</a></li>-->
        </ul>
      </li>
      @endif
    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>