<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{url('')}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="{{asset('img/logo-mini.png')}}" height="35px"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="{{asset('img/logo-header.png')}}" height="20px"><!--b>CARACOLES</b--></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Menú ayuda -->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
              <i class="fa fa-question-circle"></i>
            </a>
            <ul class="dropdown-menu">
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <h4>
                        Manual de Usuario
                      </h4>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="{{asset('lote.csv')}}">
                      <h4>
                        CSV Lote
                      </h4>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="{{asset('img/misc/usuario.png')}}" class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">{{session('usuario')}}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                <img src="{{asset('img/misc/usuario.png')}}" class="img-circle" alt="User Image">

                <p>{{session('nombre')}} {{session('apellido')}}</p>
              </li>
              <!-- Menu Body -->
              <!--<li class="user-body">
                
              </li>-->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{url('usuario/ajustes')}}" class="btn btn-default">
                    <i class="fa fa-cog"></i>
                  </a>
                </div>
                <div class="pull-right">
                  <a href="{{url('logout')}}" class="btn btn-danger">
                    <i class="fa fa-power-off"></i> 
                    <strong>Salir</strong> 
                  </a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>