@extends('Layout.app')
@section('titulo', 'Biblioteca | Libros')
@section('contenido')
<div class="container-fluid">
    <br>
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="btn btn-primary hover" data-toggle="modal" data-target="#libroModal"><i class="fa fa-plus"></i> Agregar</button>
        </div>
    </div>
    <br>
    <table id="librosTable" class="table table-bordered table-striped display nowrap" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                <th>Títutlo</th>
                <th>Autor</th>
                <th>Género</th>
                <th>Ejemplares</th>
                <th>Disponibles</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#autorModal">Nuevo Autor</button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editorialModal">Nueva Editorial</button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#literarioModal">Nuevo Género</button>
        </div>
    </div>
</div>

<!-- Modal Libro -->
<div class="modal fade" id="libroModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Libro</h4>
            </div>
            <form id="libroForm">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="titulo">Título:</label>
                                <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Ingrese el título">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="literario">Género:</label>
                                <select class="form-control" id="literario" name="literario">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <!--<label for="autor">Autor:</label>
                                <select class="form-control" id="autor" name="autor">
                                    <option value="" selected>Seleccione</option>
                                </select>-->

                                <div class="row">
                                    <button type="button" id="nuevaAsigAutor" class="btn btn-primary">Agregar Autor</button>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-12 col-sm-12">
                                        <label for="autorSelectFijo">Autor:</label>
                                        <select class="form-control" id="autorSelectFijo" name="autores">
                                            <option value="" selected>Seleccione</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="autoresWrapper">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" id="guardarLibroButton">
                            <i class="fa fa-spinner fa-spin"></i>
                            Guardar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Eliminar Libro-->
<div class="modal fade" id="eliminarLibroModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CONFIRMAR</h4>
            </div>
            <div class="modal-body">
                <p><b>¿Está seguro/a que desea eliminar este libro?</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="confirmarEliminarLibroButton">
                    <i class="fa fa-spinner fa-spin"></i>
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Ejemplares -->
<div class="modal fade" id="ejemplaresModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ejemplares</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <input type="hidden" id="ejemplarLibroId">
                    <table id="ejemplaresTable" class="table table-bordered table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Editorial</th>
                                <th>Estado</th>
                                <th>Prestamo</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Ejemplar -->
<div class="modal fade" id="ejemplarModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ejemplar</h4>
            </div>
            <form id="ejemplarForm">
                <input type="hidden" id="libroId" name="libroId">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="codigo">Código:</label>
                                <input type="text" class="form-control" id="codigo" name="codigo" placeholder="Ingrese el código">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="isbn">ISBN:</label>
                                <input type="text" class="form-control" id="isbn" name="isbn" placeholder="Ingrese el ISBN">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="editorial">Editorial:</label>
                                <select class="form-control" id="editorial" name="editorial">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-12">
                                <label for="edicion">Edición:</label>
                                <input type="text" class="form-control" id="edicion" name="edicion" placeholder="Ingrese la edición">
                            </div>
                            <div class="form-group col-lg-6 col-sm-12">
                                <label for="anio">Año:</label>
                                <input type="text" class="form-control" id="anio" name="anio" placeholder="Ingrese el año">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="estado">Estado:</label>
                                <select class="form-control" id="estado" name="estado">
                                    <option value="" selected>Seleccione</option>
                                    <option value="BUENO">BUENO</option>
                                    <option value="REGULAR">REGULAR</option>
                                    <option value="DAÑADO">DAÑADO</option>
                                    <option value="EXTRAVIADO">EXTRAVIADO</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" id="guardarEjemplarButton">
                            <i class="fa fa-spinner fa-spin"></i>
                            Guardar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Eliminar Ejemplar-->
<div class="modal fade" id="eliminarEjemplarModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CONFIRMAR</h4>
            </div>
            <div class="modal-body">
                <p><b>¿Está seguro/a que desea eliminar este ejemplar?</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="confirmarEliminarEjemplarButton">
                    <i class="fa fa-spinner fa-spin"></i>
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Prestamo -->
<div class="modal fade" id="prestamoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Prestamo</h4>
            </div>
            <form id="prestamoForm">
                <input type="hidden" id="ejemplarId" name="ejemplarId">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="fecha">Fecha:</label>
                                <input type="date" class="form-control" id="fecha" name="fecha" value="{{Date('Y-m-d')}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="participante">Participante:</label>
                                <select class="form-control" id="participante" name="participante">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" id="guardarPrestamoButton">
                                <i class="fa fa-spinner fa-spin"></i>
                                Guardar
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Autor -->
<div class="modal fade" id="autorModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Autor</h4>
            </div>
            <form id="autorForm">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="autorNombre">Nombre:</label>
                                <input type="text" class="form-control" id="autorNombre" name="autorNombre" placeholder="Ingrese el nombre">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="autorApellido">Apellido:</label>
                                <input type="text" class="form-control" id="autorApellido" name="autorApellido" placeholder="Ingrese el apellido">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" id="guardarAutorButton">
                            <i class="fa fa-spinner fa-spin"></i>
                            Guardar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal editorial -->
<div class="modal fade" id="editorialModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Editorial</h4>
            </div>
            <form id="editorialForm">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="editorialDescripcion">Nombre:</label>
                                <input type="text" class="form-control" id="editorialDescripcion" name="editorialDescripcion" placeholder="Ingrese el nombre">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="editorialPais">País:</label>
                                <input type="text" class="form-control" id="editorialPais" name="editorialPais" placeholder="Ingrese el país">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" id="guardarEditorialButton">
                            <i class="fa fa-spinner fa-spin"></i>
                            Guardar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal literario -->
<div class="modal fade" id="literarioModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Género</h4>
            </div>
            <form id="literarioForm">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="literarioDescripcion">Nombre:</label>
                                <input type="text" class="form-control" id="literarioDescripcion" name="literarioDescripcion" placeholder="Ingrese el género">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary" id="guardarLiterarioButton">
                            <i class="fa fa-spinner fa-spin"></i>
                            Guardar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{asset('js/biblioteca/libros.js')}}"></script>
@endsection