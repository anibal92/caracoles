@extends('Layout.app')
@section('titulo', 'Gráficos biblioteca')
@section('contenido')
<div class="loader centrado"></div>
<div id="contenido" class="container-fluid oculto">
  <div class="row">
    <div class="col-lg-6 col-sm-12">
      <h3>Cantidad por género</h3>
      <canvas id="generoCanvas"></canvas>
      <br>
      <p class="text-muted text-center">El gráfico muestra la cantidad de libros por género</p>
    </div>
    <div class="col-lg-6 col-sm-12">
      <h3>Popularidad por género</h3>
      <canvas id="popularidadCanvas"></canvas>
      <br>
      <p class="text-muted text-center">El gráfico muestra la cantidad de libros <u>alquilados</u> por género</p>
    </div>
  </div>
  <br>
  <br>
  <div class="row">
    <div class="col-lg-6 col-sm-12">
      <h3>Distribución por estado</h3>
      <canvas id="estadoCanvas"></canvas>
      <br>
      <p class="text-muted text-center">El gráfico muestra la cantidad de libros que se encuentran en determinado estado</p>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{asset('js/biblioteca/graficos.js')}}"></script>
@endsection