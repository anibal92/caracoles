@extends('Layout.app')
@section('titulo', 'Biblioteca | Préstamos y devoluciones')
@section('contenido')
<div class="container-fluid">
    <br>
    <table id="prestamosTable" class="table table-bordered table-striped" style="width:100%">
        <thead>
            <tr>
                <th>Código</th>
                <th>Libro</th>
                <th>Participante</th>
                <th>Préstamo</th>
                <th>Devolución</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<!-- Modal Prestamo -->
<div class="modal fade" id="prestamoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Prestamo</h4>
            </div>
            <form id="prestamoForm">
                <input type="hidden" id="ejemplarId" name="ejemplarId">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="ejemplar">Ejemplar:</label>
                                <input type="text" class="form-control" id="ejemplar" name="ejemplar" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="fecha">Fecha:</label>
                                <input type="date" class="form-control" id="fecha" name="fecha">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="participante">Participante:</label>
                                <select class="form-control" id="participante" name="participante">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" id="guardarPrestamoButton">
                                <i class="fa fa-spinner fa-spin"></i>
                                Guardar
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Devolucion -->
<div class="modal fade" id="devolucionModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Prestamo</h4>
            </div>
            <form id="devolucionForm">

                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="ejemplarDevolucion">Ejemplar:</label>
                                <input type="text" class="form-control" id="ejemplarDevolucion" name="ejemplarDevolucion" disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="participanteDevolucion">Participante:</label>
                                <input type="text" class="form-control" id="participanteDevolucion" name="participanteDevolucion" disabled>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="fechaDevolucion">Fecha devolución:</label>
                                <input type="date" class="form-control" id="fechaDevolucion" name="fechaDevolucion" value="{{Date('Y-m-d')}}">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="estadoDevolucion">Estado:</label>
                                <select class="form-control" id="estadoDevolucion" name="estadoDevolucion">
                                    <option value="" selected>Seleccione</option>
                                    <option value="BUENO">BUENO</option>
                                    <option value="REGULAR">REGULAR</option>
                                    <option value="DAÑADO">DAÑADO</option>
                                    <option value="EXTRAVIADO">EXTRAVIADO</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 form-group">
                                <label for="observacionDevolucion">Observación</label>
                                <textarea class="form-control" rows="7" id="observacionDevolucion" name="observacionDevolucion" placeholder="Ingrese la observación"></textarea>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary" id="guardarDevolucionButton">
                                <i class="fa fa-spinner fa-spin"></i>
                                Guardar
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Ver Prestamo -->
<div class="modal fade" id="verPrestamoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Prestamo</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="form-group col-lg-12 col-sm-12">
                            <label for="ejemplarVer">Ejemplar:</label>
                            <input type="text" class="form-control" id="ejemplarVer" name="ejemplarVer" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-12 col-sm-12">
                            <label for="participanteVer">Participante:</label>
                            <input type="text" class="form-control" id="participanteVer" name="participanteVer" readonly>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-lg-6 col-sm-12">
                            <label for="fechaPrestamoVer">Fecha prestamo:</label>
                            <input type="date" class="form-control" id="fechaPrestamoVer" name="fechaPrestamoVer" readonly>
                        </div>
                        <div class="form-group col-lg-6 col-sm-12">
                            <label for="fechaDevolucionVer">Fecha devolución:</label>
                            <input type="date" class="form-control" id="fechaDevolucionVer" name="fechaDevolucionVer" readonly>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-lg-12 col-sm-12">
                            <label for="estadoVer">Estado:</label>
                            <select class="form-control" id="estadoVer" name="estadoVer" readonly>
                                <option value="" selected>Seleccione</option>
                                <option value="BUENO">BUENO</option>
                                <option value="REGULAR">REGULAR</option>
                                <option value="DAÑADO">DAÑADO</option>
                                <option value="EXTRAVIADO">EXTRAVIADO</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 form-group">
                            <label for="observacionVer">Observación</label>
                            <textarea class="form-control" rows="7" id="observacionVer" name="observacionVer" readonly></textarea>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Eliminar Prestamo-->
<div class="modal fade" id="eliminarPrestamoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CONFIRMAR</h4>
            </div>
            <div class="modal-body">
                <p><b>¿Está seguro/a que desea eliminar este prestamo?</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="confirmarEliminarPrestamoButton">
                    <i class="fa fa-spinner fa-spin"></i>
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{asset('js/biblioteca/prestamos.js')}}"></script>
@endsection