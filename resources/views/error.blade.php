@extends('Layout.app')
@section('titulo', 'Error')
@section('contenido')
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" disabled>×</button>
    <h4><i class="icon fa fa-ban"></i> ERROR!</h4>
    @if(session('error'))
        {{session('error')}}
    @endif
</div>
@endsection