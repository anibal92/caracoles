@extends('Layout.app')
@section('titulo', 'Participantes')
@section('contenido')
<table id="participantesTable" class="table table-bordered table-striped display nowrap" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Documento</th>
            <th>Nombres</th>
            <th>Apellido</th>
            <th>Nacimiento</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="eliminarModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CONFIRMAR</h4>
            </div>
            <div class="modal-body">
                <p><b>¿Está seguro/a que desea eliminar este participante?</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary hover" id="confirmarRecuperarButton">
                    <i class="fa fa-spinner fa-spin"></i>
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{asset('js/participantes/listado.js')}}"></script>
@endsection