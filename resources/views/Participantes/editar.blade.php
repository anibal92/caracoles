@extends('Layout.app')
@section('titulo', 'Editar Participante')
@section('contenido')
<div class="loader centrado"></div>
<div class="container-fluid oculto" id="contenido">
    <h2>Datos Básicos</h2>
    <br>

    <form id="participanteForm">
        <input type="hidden" id="id" name="id">
        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="documento">Nº de Documento:</label>
                <input type="text" class="form-control" id="documento" name="documento" placeholder="Ingrese número de documento">
                <!--<div class="input-group">
                    <input type="text" class="form-control" id="documento" name="documento" placeholder="Ingrese número de documento">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-primary" id="buscarParticipanteButton"><i class="fa fa-search"></i></button>
                    </div>
                    
                </div>-->
            </div>
            <!--<div class="col-lg-6 col-md-6 col-sm-12">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="sinDocumento" id="sinDocumento"> Sin documento
                    </label>
                </div>
            </div>-->
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="nombres">Nombre<sup class="text-danger">(*)</sup>:</label>
                <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Ingrese el nombre">
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="apellido">Apellido<sup class="text-danger">(*)</sup>:</label>
                <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Ingrese el apellido">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="genero">Genero<sup class="text-danger">(*)</sup>:</label>
                <select class="form-control" id="genero" name="genero">
                    <option value="" selected>Seleccione</option>
                </select>
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="nacimiento">Fecha de nacimiento:</label>
                <input type="date" class="form-control" id="nacimiento" name="nacimiento" placeholder="Ingrese la fecha de nacimiento">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="calle">Calle:</label>
                <input type="text" class="form-control" id="calle" name="calle" placeholder="Ingrese la calle">
            </div>
            <div class="form-group col-lg-2 col-md-3 col-sm-6">
                <label for="numero">Número:</label>
                <input type="number" class="form-control" id="numero" name="numero" placeholder="Ingrese el Nº">
            </div>
            <div class="form-group col-lg-2 col-md-3 col-sm-6">
                <label for="piso">Piso:</label>
                <input type="number" class="form-control" id="piso" name="piso" placeholder="Ingrese el piso">
            </div>
            <div class="form-group col-lg-2 col-md-3 col-sm-6">
                <label for="departamento">Departamento:</label>
                <input type="text" class="form-control" id="departamento" name="departamento" placeholder="Ingrese el dpto">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="barrio">Barrio:</label>
                <select class="form-control" id="barrio" name="barrio">
                    <option value="" selected>Seleccione</option>
                </select>
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="localidad">Localidad:</label>
                <select class="form-control" id="localidad" name="localidad">
                    <option value="" selected>Seleccione</option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="escuela">Escuela:</label>
                <select class="form-control" id="escuela" name="escuela">
                    <option value="" selected>Seleccione</option>
                    <option value="" selected>Ninguna</option>
                </select>
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="grado">Grado:</label>
                <select class="form-control" id="grado" name="grado">
                    <option value="" selected>Seleccione</option>
                    <option value="" selected>Ninguno</option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="programaBeneficiario">Programa Beneficiario:</label>
                <select class="form-control" id="programaBeneficiario" name="programaBeneficiario">
                    <option value="" selected>Seleccione</option>
                    <option value="" selected>Ninguno</option>
                </select>
            </div>
        </div>

        <h2>Datos Salud</h2>
        <br>

        <div class="row">
            <div class="form-group col-lg-3 col-md-6 col-sm-6">
                <label for="sanguineo">Gupo Sanguíneo:</label>
                <select class="form-control" id="sanguineo" name="sanguineo">
                    <option value="" selected>Seleccione</option>
                </select>
            </div>
            <div class="form-group col-lg-3 col-md-6 col-sm-6">
                <label for="enfermedad">Enfermedad:</label>
                <input type="text" class="form-control" id="enfermedad" name="enfermedad" placeholder="Ingrese enfermedad">
            </div>
            <div class="form-group col-lg-3 col-md-6 col-sm-6">
                <label for="alergia">Alérgia:</label>
                <input type="text" class="form-control" id="alergia" name="alergia" placeholder="Ingrese alergia">
            </div>
            <div class="form-group col-lg-3 col-md-6 col-sm-6">
                <label for="medicacion">Medicación:</label>
                <input type="text" class="form-control" id="medicacion" name="medicacion" placeholder="Ingrese medicación">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="discapacidad">Discapacidad:</label>
                <select class="form-control" id="discapacidad" name="discapacidad">
                    <option value="" selected>Seleccione</option>
                </select>
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="cud">Cédula Única de Discapacidad:</label>
                <input type="text" class="form-control" id="cud" name="cud" placeholder="Ingrese el CUD">
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 form-group">
                <label for="observacionSalud">Observación</label>
                <textarea class="form-control" rows="3" id="observacionSalud" name="observacionSalud" placeholder="Ingrese la observación"></textarea>
            </div>
        </div>

        <h2>Grupo Familiar</h2>
        <br>

        <div class="row">
            <div class="col-lg-12">
                <button type="button" id="nuevoFamiliarButton" class="btn btn-primary hover pull-right" data-toggle="modal" data-target="#familiarModal"><i class="fa fa-edit"></i> Agregar</button>
                <table id="familiaresTable" class="table table-striped display nowrap" cellspacing="0" style="width:100%">
                    <thead>
                        <tr>
                            <th>Documento</th>
                            <th>Apellido</th>
                            <th>Nombre</th>
                            <th>Vínculo</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>

        </div>

        <h2>Contacto de Emergencia</h2>
        <br>

        <input type="hidden" id="contactoPersonaId" name="contactoPersonaId">
        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contactoNombres">Nombre<sup class="text-danger">(*)</sup>:</label>
                <input type="text" class="form-control" id="contactoNombres" name="contactoNombres" placeholder="Ingrese el nombre">
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contactoApellido">Apellido<sup class="text-danger">(*)</sup>:</label>
                <input type="text" class="form-control" id="contactoApellido" name="contactoApellido" placeholder="Ingrese el apellido">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contactoTelefono">Teléfono<sup class="text-danger">(*)</sup>:</label>
                <input type="text" class="form-control" id="contactoTelefono" name="contactoTelefono" placeholder="Ingrese el teléfono">
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contactoTelefonoAlternativo">Teléfono Alternativo:</label>
                <input type="text" class="form-control" id="contactoTelefonoAlternativo" name="contactoTelefonoAlternativo" placeholder="Ingrese el teléfono alternativo">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contactoCalle">Calle:</label>
                <input type="text" class="form-control" id="contactoCalle" name="contactoCalle" placeholder="Ingrese la calle">
            </div>
            <div class="form-group col-lg-2 col-md-3 col-sm-6">
                <label for="contactoNumero">Número:</label>
                <input type="number" class="form-control" id="contactoNumero" name="contactoNumero" placeholder="Ingrese el Nº">
            </div>
            <div class="form-group col-lg-2 col-md-3 col-sm-6">
                <label for="contactoPiso">Piso:</label>
                <input type="number" class="form-control" id="contactoPiso" name="contactoPiso" placeholder="Ingrese el piso">
            </div>
            <div class="form-group col-lg-2 col-md-3 col-sm-6">
                <label for="contactoDepartamento">Departamento:</label>
                <input type="text" class="form-control" id="contactoDepartamento" name="contactoDepartamento" placeholder="Ingrese el dpto">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contactoBarrio">Barrio:</label>
                <select class="form-control" id="contactoBarrio" name="contactoBarrio">
                    <option value="" selected>Seleccione</option>
                </select>
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contactoLocalidad">Localidad:</label>
                <select class="form-control" id="contactoLocalidad" name="contactoLocalidad">
                    <option value="" selected>Seleccione</option>
                </select>
            </div>
        </div>

        <div class="row">
            <hr id="separarEditarButton" class="oculto">
            <div class="col-lg-2 pull-right">
                <button type="submit" id="guardarParticipanteButton" class="btn btn-primary hover btn-lg pull-right">
                    <i class="fa fa-spinner fa-spin"></i>
                    Guardar
                </button>
            </div>
        </div>

    </form>

</div>

<!-- Modal Familiar-->
<div class="modal fade" id="familiarModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo Familiar</h4>
            </div>
            <form id="familiarForm">
                <input type="hidden" id="familiarPersonaId">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarDocumento">Nº de Documento:</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="familiarDocumento" name="familiarDocumento" placeholder="Ingrese número de documento">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary" id="buscarFamiliarButton"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-1">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="sinDocumentoFamiliar" id="sinDocumentoFamiliar"> Sin documento
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarNombres">Nombre<sup class="text-danger">(*)</sup>:</label>
                                <input type="text" class="form-control" id="familiarNombres" name="familiarNombres" placeholder="Ingrese el nombre">
                            </div>
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarApellido">Apellido<sup class="text-danger">(*)</sup>:</label>
                                <input type="text" class="form-control" id="familiarApellido" name="familiarApellido" placeholder="Ingrese el apellido">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-4 col-sm-1">
                                <label for="familiarNacimiento">Fecha de nacimiento:</label>
                                <input type="date" class="form-control" id="familiarNacimiento" name="familiarNacimiento" placeholder="Ingrese la fecha de nacimiento">
                            </div>
                            <div class="form-group col-lg-4 col-sm-1">
                                <label for="familiarGenero">Genero<sup class="text-danger">(*)</sup>:</label>
                                <select class="form-control" id="familiarGenero" name="familiarGenero">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-sm-1">
                                <label for="familiarCivil">Estado Civil:</label>
                                <select class="form-control" id="familiarCivil" name="familiarCivil">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarVinculo">Vínculo<sup class="text-danger">(*)</sup>:</label>
                                <select class="form-control" id="familiarVinculo" name="familiarVinculo">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarEmail">E-mail:</label>
                                <input type="email" class="form-control" id="familiarEmail" name="familiarEmail" placeholder="Ingrese el e-mail">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarTelefono">Teléfono:</label>
                                <input type="text" class="form-control" id="familiarTelefono" name="familiarTelefono" placeholder="Ingrese el teléfono">
                            </div>

                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarTelefonoAlternativo">Teléfono alternativo:</label>
                                <input type="text" class="form-control" id="familiarTelefonoAlternativo" name="familiarTelefonoAlternativo" placeholder="Ingrese el teléfono">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarCalle">Calle:</label>
                                <input type="text" class="form-control" id="familiarCalle" name="familiarCalle" placeholder="Ingrese la calle">
                            </div>
                            <div class="form-group col-lg-2 col-sm-1">
                                <label for="familiarNumero">Número:</label>
                                <input type="number" class="form-control" id="familiarNumero" name="familiarNumero">
                            </div>
                            <div class="form-group col-lg-2 col-sm-1">
                                <label for="familiarPiso">Piso:</label>
                                <input type="number" class="form-control" id="familiarPiso" name="familiarPiso">
                            </div>
                            <div class="form-group col-lg-2 col-sm-1">
                                <label for="familiarDepartamento">Dpto:</label>
                                <input type="text" class="form-control" id="familiarDepartamento" name="familiarDepartamento">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarBarrio">Barrio:</label>
                                <select class="form-control" id="familiarBarrio" name="familiarBarrio">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarLocalidad">Localidad:</label>
                                <select class="form-control" id="familiarLocalidad" name="familiarLocalidad">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarOficio">Oficio:</label>
                                <input type="text" class="form-control" id="familiarOficio" name="familiarOficio">
                            </div>
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarEscolaridad">Escolaridad:</label>
                                <select class="form-control" id="familiarEscolaridad" name="familiarEscolaridad">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-4 col-sm-1">
                                <label>
                                    <input id="familiarInteresaParticipar" name="familiarInteresaParticipar" type="checkbox"> Le interesa participar las actividades
                                </label>
                            </div>
                            <div class="form-group col-lg-4 col-sm-1">
                                <label>
                                    <input id="familiarInteresaEstudios" name="familiarInteresaEstudios" type="checkbox"> Le interesa terminar sus estudios
                                </label>
                            </div>
                            <div class="form-group col-lg-4 col-sm-1">
                                <label>
                                    <input id="familiarEmergencia" name="familiarEmergencia" type="checkbox"> Es
                                    contacto de emergencia
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary hover" id="guardarFamiliarButton">
                        <i class="fa fa-spinner fa-spin"></i>
                        Guardar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Eliminar -->
<div class="modal fade" id="eliminarFamiliarModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CONFIRMAR</h4>
            </div>
            <div class="modal-body">
                <p><b>¿Está seguro/a que desea eliminar este familiar?</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary hover" id="confirmarEliminarFamiliarButton">
                    <i class="fa fa-spinner fa-spin"></i>
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    var participanteId = {{$id}};
</script>
<script src="{{asset('js/participantes/editar.js')}}"></script>
@endsection