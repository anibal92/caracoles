<div class="container-fluid">
    <br>
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="btn btn-primary hover" data-toggle="modal" data-target="#seguimientoModal"><i class="fa fa-plus"></i> Agregar</button>
        </div>
    </div>
    <table id="seguimientosTable" class="table table-bordered table-striped display nowrap" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                <th>Fecha</th>
                <th>Autor/a</th>
                <th>Observación</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<!-- Modal Seguimiento -->
<div class="modal fade" id="seguimientoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo</h4>
            </div>
            <form id="seguimientoForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12 form-group">
                            <label for="observacionSeguimiento">Observación</label>
                            <textarea class="form-control" rows="10" id="observacionSeguimiento" name="observacionSeguimiento" placeholder="Ingrese la observación"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary hover" id="guardarSeguimientoButton">
                        <i class="fa fa-spinner fa-spin"></i>
                        Guardar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Ver Seguimiento-->
<div class="modal fade" id="verSeguimientoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Seguimiento</h4>
            </div>
            <div class="modal-body">
                <p><strong>Autor/a:</strong> <span id="verSeguimientoUsuario"></span></p>
                <p><strong>Fecha:</strong> <span id="verSeguimientoFecha"></span></p>
                <p><strong>Observación:</strong></p>
                <div id="verSeguimientoObservacion" class="justificado">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Eliminar Seguimiento-->
<div class="modal fade" id="eliminarSeguimientoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CONFIRMAR</h4>
            </div>
            <div class="modal-body">
                <p><b>¿Está seguro/a que desea eliminar este registro?</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary hover" id="confirmarEliminarSeguimientoButton">
                    <i class="fa fa-spinner fa-spin"></i>
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>
