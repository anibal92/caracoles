@extends('Layout.app')
@section('titulo', 'Participante')
@section('contenido')
<div class="loader centrado"></div>
<div id="contenido" class="oculto">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#fichaTab" data-toggle="tab">FICHA</a></li>
            <li><a href="#seguimientoTab" data-toggle="tab">SEGUIMIENTO</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="fichaTab">
                @include('Participantes.ficha')
            </div>
            <div class="tab-pane" id="seguimientoTab">
                @include('Participantes.seguimiento')
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    var participanteId = {{$id}};
</script>
<script src="{{asset('js/participantes/ficha.js')}}"></script>
<script src="{{asset('js/participantes/seguimiento.js')}}"></script>
@endsection