<div class="container-fluid">
    <h3>Datos Básicos</h3>
    <br>

    <form method="post" id="fichaForm">
        <input type="hidden" id="id" name="id">
        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="documento">Nº de Documento:</label>
                <input type="text" class="form-control" id="documento" name="documento" value="">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="nombres">Nombre:</label>
                <input type="text" class="form-control" id="nombres" name="nombres" value="">
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="apellido">Apellido:</label>
                <input type="text" class="form-control" id="apellido" name="apellido" value="">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="genero">Genero:</label>
                <select class="form-control" id="genero" name="genero" value="">
                    <option value="" selected></option>
                </select>
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="nacimiento">Fecha de nacimiento:</label>
                <input type="date" class="form-control" id="nacimiento" name="nacimiento" value="">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="calle">Calle:</label>
                <input type="text" class="form-control" id="calle" name="calle" value="">
            </div>
            <div class="form-group col-lg-2 col-md-3 col-sm-6">
                <label for="numero">Número:</label>
                <input type="number" class="form-control" id="numero" name="numero" value="">
            </div>
            <div class="form-group col-lg-2 col-md-3 col-sm-6">
                <label for="piso">Piso:</label>
                <input type="number" class="form-control" id="piso" name="piso" value="">
            </div>
            <div class="form-group col-lg-2 col-md-3 col-sm-6">
                <label for="departamento">Departamento:</label>
                <input type="text" class="form-control" id="departamento" name="departamento" value="">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="barrio">Barrio:</label>
                <select class="form-control" id="barrio" name="barrio" value="">
                    <option value="" selected></option>
                </select>
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="localidad">Localidad:</label>
                <select class="form-control" id="localidad" name="localidad" value="">
                    <option value="" selected></option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12>
                <label for=" escuela">Escuela:</label>
                <select class="form-control" id="escuela" name="escuela" value="">
                    <option value="" selected></option>
                </select>
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="grado">Grado:</label>
                <select class="form-control" id="grado" name="grado" value="">
                    <option value="" selected></option>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="programa">Programa Beneficiario:</label>
                <select class="form-control" id="programa" name="programa" value="">
                    <option value="" selected></option>
                </select>
            </div>
        </div>

        <h3>Datos Salud</h3>
        <br>

        <div class="row">
            <div class="form-group col-lg-3 col-md-6 col-sm-6">
                <label for="sanguineo">Gupo Sanguíneo:</label>
                <select class="form-control" id="sanguineo" name="sanguineo" value="">
                    <option value="" selected></option>
                </select>
            </div>
            <div class="form-group col-lg-3 col-md-6 col-sm-6">
                <label for="enfermedad">Enfermedad:</label>
                <input type="text" class="form-control" id="enfermedad" name="enfermedad" value="">
            </div>
            <div class="form-group col-lg-3 col-md-6 col-sm-6">
                <label for="alergia">Alérgia:</label>
                <input type="text" class="form-control" id="alergia" name="alergia" value="">
            </div>
            <div class="form-group col-lg-3 col-md-6 col-sm-6">
                <label for="medicacion">Medicación:</label>
                <input type="text" class="form-control" id="medicacion" name="medicacion" value="">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="discapacidad">Discapacidad:</label>
                <select class="form-control" id="discapacidad" name="discapacidad" value="">
                    <option value="" selected></option>
                </select>
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="cud">Cédula Única de Discapacidad:</label>
                <input type="text" class="form-control" id="cud" name="cud" value="">
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                <label for="observacionSalud">Observación</label>
                <textarea class="form-control" rows="3" id="observacionSalud" name="observacionSalud" value=""></textarea>
            </div>
        </div>

        <h3>Grupo Familiar</h3>
        <br>

        <div class="row">
            <div class="col-lg-12">
                <table id="familiaresTable" class="table table-striped display nowrap" cellspacing="0" style="width:100%">
                    <thead>
                        <tr>
                            <th>Documento</th>
                            <th>Apellido</th>
                            <th>Nombre</th>
                            <th>Vínculo</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <!--<button type="button" id="nuevoFamiliarButton" class="btn btn-primary pull-right" data-toggle="modal" data-target="#familiarModal"><i class="fa fa-edit"></i> Agregar</button>-->

        </div>

        <h3>Contacto de Emergencia</h3>
        <br>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contactoNombres">Nombre:</label>
                <input type="text" class="form-control" id="contactoNombres" name="contactoNombres" value="">
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contactoApellido">Apellido:</label>
                <input type="text" class="form-control" id="contactoApellido" name="contactoApellido" value="">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contactoTelefono">Teléfono:</label>
                <input type="text" class="form-control" id="contactoTelefono" name="contactoTelefono" value="">
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contactoTelefonoAlternativo">Teléfono Alternativo:</label>
                <input type="text" class="form-control" id="contactoTelefonoAlternativo" name="contactoTelefonoAlternativo" value="">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contactoCalle">Calle:</label>
                <input type="text" class="form-control" id="contactoCalle" name="contactoCalle" value="">
            </div>
            <div class="form-group col-lg-2 col-md-3 col-sm-6">
                <label for="contactoNumero">Número:</label>
                <input type="number" class="form-control" id="contactoNumero" name="contactoNumero" value="">
            </div>
            <div class="form-group col-lg-2 col-md-3 col-sm-6">
                <label for="contactoPiso">Piso:</label>
                <input type="number" class="form-control" id="contactoPiso" name="contactoPiso" value="">
            </div>
            <div class="form-group col-lg-2 col-md-3 col-sm-6">
                <label for="contactoDepartamento">Departamento:</label>
                <input type="text" class="form-control" id="contactoDepartamento" name="contactoDepartamento" value="">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contactoBarrio">Barrio:</label>
                <select class="form-control" id="contactoBarrio" name="contactoBarrio" value="">
                    <option value="" selected></option>
                </select>
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contactoLocalidad">Localidad:</label>
                <select class="form-control" id="contactoLocalidad" name="contactoLocalidad" value="">
                    <option value="" selected></option>
                </select>
            </div>
        </div>

        <!--<div class="row">
            <div class="col-lg-2 pull-right">
                <button type="submit" id="guardarFichaButton" class="btn btn-primary btn-lg pull-right">
                    <i class="fa fa-spinner fa-spin"></i>
                    Registrar
                </button>
            </div>
        </div>-->
    </form>
</div>

<!-- Modal Familiar-->
<div class="modal fade" id="familiarModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo Familiar</h4>
            </div>
            <form id="familiarForm">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarDocumento">Nº de Documento:</label>
                                <input type="text" class="form-control" id="familiarDocumento" name="familiarDocumento" placeholder="Ingrese número de documento">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarNombres">Nombre:</label>
                                <input type="text" class="form-control" id="familiarNombres" name="familiarNombres" placeholder="Ingrese el nombre">
                            </div>
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarApellido">Apellido:</label>
                                <input type="text" class="form-control" id="familiarApellido" name="familiarApellido" placeholder="Ingrese el apellido">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-4 col-sm-1">
                                <label for="familiarNacimiento">Fecha de nacimiento:</label>
                                <input type="date" class="form-control" id="familiarNacimiento" name="familiarNacimiento" placeholder="Ingrese la fecha de nacimiento">
                            </div>
                            <div class="form-group col-lg-4 col-sm-1">
                                <label for="familiarGenero">Genero:</label>
                                <select class="form-control" id="familiarGenero" name="familiarGenero">
                                    <option value="" selected></option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-sm-1">
                                <label for="familiarCivil">Estado Civil:</label>
                                <select class="form-control" id="familiarCivil" name="familiarCivil">
                                    <option value="" selected></option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarVinculo">Vínculo:</label>
                                <select class="form-control" id="familiarVinculo" name="familiarVinculo">
                                    <option value="" selected></option>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarEmail">E-mail:</label>
                                <input type="email" class="form-control" id="familiarEmail" name="familiarEmail" placeholder="Ingrese el e-mail">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarTelefono">Teléfono:</label>
                                <input type="text" class="form-control" id="familiarTelefono" name="familiarTelefono" placeholder="Ingrese el teléfono">
                            </div>

                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarTelefonoAlternativo">Teléfono alternativo:</label>
                                <input type="text" class="form-control" id="familiarTelefonoAlternativo" name="familiarTelefonoAlternativo" placeholder="Ingrese el teléfono">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarCalle">Calle:</label>
                                <input type="text" class="form-control" id="familiarCalle" name="familiarCalle" placeholder="Ingrese la calle">
                            </div>
                            <div class="form-group col-lg-2 col-sm-1">
                                <label for="familiarNumero">Número:</label>
                                <input type="number" class="form-control" id="familiarNumero" name="familiarNumero">
                            </div>
                            <div class="form-group col-lg-2 col-sm-1">
                                <label for="familiarPiso">Piso:</label>
                                <input type="number" class="form-control" id="familiarPiso" name="familiarPiso">
                            </div>
                            <div class="form-group col-lg-2 col-sm-1">
                                <label for="familiarDepartamento">Dpto:</label>
                                <input type="text" class="form-control" id="familiarDepartamento" name="familiarDepartamento">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarBarrio">Barrio:</label>
                                <select class="form-control" id="familiarBarrio" name="familiarBarrio">
                                    <option value="" selected></option>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarLocalidad">Localidad:</label>
                                <select class="form-control" id="familiarLocalidad" name="familiarLocalidad">
                                    <option value="" selected></option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarOficio">Oficio:</label>
                                <input type="text" class="form-control" id="familiarOficio" name="familiarOficio">
                            </div>
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="familiarEscolaridad">Escolaridad:</label>
                                <select class="form-control" id="familiarEscolaridad" name="familiarEscolaridad">
                                    <option value="" selected></option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-4 col-sm-1">
                                <label>
                                    <input id="familiarInteresaParticipar" name="familiarInteresaParticipar" type="checkbox"> Le interesa participar las actividades
                                </label>
                            </div>
                            <div class="form-group col-lg-4 col-sm-1">
                                <label>
                                    <input id="familiarInteresaEstudios" name="familiarInteresaEstudios" type="checkbox"> Le interesa terminar sus estudios
                                </label>
                            </div>
                            <div class="form-group col-lg-4 col-sm-1">
                                <label>
                                    <input id="familiarEmergencia" name="familiarEmergencia" type="checkbox"> Es
                                    contacto de emergencia
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!--<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary" id="guardarFamiliarButton">
                        <i class="fa fa-spinner fa-spin"></i>
                        Guardar
                    </button>-->
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Eliminar Familiar -->
<div class="modal fade" id="eliminarFamiliarModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CONFIRMAR</h4>
            </div>
            <div class="modal-body">
                <p><b>¿Está seguro/a que desea eliminar este familiar?</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="confirmarEliminarFamiliarButton">
                    <i class="fa fa-spinner fa-spin"></i>
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>