@extends('Layout.app')
@section('titulo', 'Gráficos participantes')
@section('contenido')
<div class="loader centrado"></div>
<div id="contenido" class="container-fluid oculto">
  <div class="row">
    <div class="col-lg-6 col-sm-12">
      <h3>Distribución por género</h3>
      <canvas id="generoCanvas"></canvas>
      <br>
      <p class="text-muted text-center">El gráfico muestra la cantidad de participantes, <br> según el género al que pertenecen</p>
    </div>
    <div class="col-lg-6 col-sm-12">
      <h3>Distribución por escolaridad</h3>
      <canvas id="escolaridadCanvas"></canvas>
      <br>
      <p class="text-muted text-center">El gráfico muestra la cantidad de participantes <br> según el nivel de escolaridad</p>
    </div>
  </div>
  <br>
  <br>
  <div class="row">
    <div class="col-lg-12 col-sm-12">
      <h3>Distribución por edades</h3>
      <canvas id="edadCanvas" height="100px"></canvas>
      <br>
      <p class="text-muted text-center">El gráfico muestra la cantidad de participantes en determinado rango de edades</p>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{asset('js/participantes/graficos.js')}}"></script>
@endsection