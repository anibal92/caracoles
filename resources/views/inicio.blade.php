@extends('Layout.app')
@section('titulo', 'Inicio')
@section('contenido')
@if(!session('emailVerificado'))
<!--<div class="alert alert-warning alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-warning"></i> Dirección de e-mail no verificada!</h4>
    <p>Su e-mail <b>{{session('email')}}</b> no esta verificada. <a href="#">Verificar aquí</a></p>
</div>-->
@endif

<div class="container-fluid">
 
    <div class="custom-card-user">
        <div class="custom-card-user__title">
            <h3>Bienvenido {{session('nombre')}}</h3>
        </div>
        <div class="custom-card-user__img">
            <img src="{{asset('img/misc/work-time-animate.svg')}}" alt="Bienvenido usuario">
        </div>
         
    </div>
        
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Accesos directos</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                
                </div>
                <div class="box-body">
                    Puede elegir un acceso directo para comenzar
                </div>
            </div>
            </div>
    </div>

    <div class="row">
        <div class="custom-arrow">
            <i class="fa fa-sort-desc"></i>
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <div class="custom-card card-purple" id="nuevoParticipante">
                <img src="{{asset('img/misc/Add User-amico.svg')}}" alt="nuevo participante" width="150" height="150">
                <h4>Nuevo Participante</h3>
                <p>Seleccione para cargar un nuevo participante</p> 
                <button class="custom-card__button" id="nuevoParticipanteButton">
                    <i class="fa fa-spinner fa-spin"></i> 
                </button> 
            </div>
        </div>
        
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <div class="custom-card card-orange" id="nuevoLibro">
                <img src="{{asset('img/misc/Book lover-amico.svg')}}" alt="nuevo libro" width="150" height="150">
                <h4>Nuevo libro</h3>
                <p>Seleccione para cargar un nuevo libro</p>  
                <button class="custom-card__button" id="nuevoLibroButton">
                    <i class="fa fa-spinner fa-spin"></i> 
                </button>
            </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <div class="custom-card card-lightblue" id="nuevoCurso">
                <img src="{{asset('img/misc/Teaching-pana.svg')}}" alt="nuevo curso" width="150" height="150">
                <h4>Nuevo curso</h3>
                <p>Seleccione para cargar un nuevo curso</p>  
                <button class="custom-card__button" id="nuevoCursoButton">
                    <i class="fa fa-spinner fa-spin"></i> 
                </button>
            </div>
        </div>
    </div>
    

    
</div>

@endsection

@section('scripts')
<script src="{{asset('js/inicio.js')}}"></script>
@endsection