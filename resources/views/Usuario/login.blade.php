<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Caracoles | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('bower_components/admin-lte/dist/css/AdminLTE.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('bower_components/admin-lte/plugins/iCheck/square/blue.css')}}">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <!-- Toastr -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<body>

  <div class="wave-container wave-container-a"></div>
  <div class="wave-container wave-container-b"></div>


  <div class="login-logo">
    <img src="{{asset('img/logo.png')}}" alt="Logo Caracoles">
  </div>

  <div class="container">
    <div class="left">
      <img src="{{asset('img/misc/computer.svg')}}" alt="Computadora">
    </div>
    <div class="right">
      <div class="right__avatar">
        <img src="{{asset('img/misc/avatar.svg')}}" alt="avatar">
      </div>
      <div class="right__title">
        <h1>Iniciar sesión</h1>
      </div>

      <div class="login-form">
        <form method="POST" id="loginForm">
          <div class="login-form__input">
            <input type="text" name="usuario" id="usuario" placeholder="Usuario" required>
          </div>
          <div class="login-form__input">
            <input type="password" name="contrasenia" id="contrasenia" placeholder="Contraseña" required>
          </div>
          <div class="login-form__input">
            <a href="{{url('consulta')}}">Acceder a consulta de libros</a>
          </div>
          <div class="login-form__input">
            <button type="submit" value="Ingresar" class="login-btn" id="loginButton">
              <span>ingresar</span>
              <i class="fa fa-spinner fa-spin"></i>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <footer class="login-footer">
    <h6 class="login-footer__title">Caracoles Centro Educativo Autogestionario
      <small class="login-footer__small">Sistema de Gestión</small>
    </h6>
  </footer>



  <!--
  <link rel="stylesheet" href="{{asset('css/estilos.css')}}">
  <style type="text/css">
    body {
      background: #e2b303 !important;
      background: linear-gradient(to right, #c99703, #e6b400) !important;
    }

    form {
      background-color: #FFFFFF;
      padding: 50px;
      text-align: center;
      border-radius: 5px;
      box-shadow: 0 0.5rem 1rem 0 rgba(0, 0, 0, 0.1);
      margin-bottom: 2em;

    }

    .logo {
      margin-bottom: 1em;
      padding: 40px;
      filter: opacity(80%);
    }

    h5 {
      margin-bottom: 2em;
    }

    a {
      margin-bottom: 2em;
      text-decoration: none;
      color: #ff0080;
    }

    p {
      margin-top: 2em;
      margin-bottom: 1em;
    }

    .form-group {
      padding: 0;
      margin-bottom: 10px;
    }

    .btn {
      margin-top: 1em;
      transition: all 0.5s;
    }

    .btn span {
      display: inline-block;
      position: relative;
      transition: 0.5s;
    }

    .btn span:after {
      content: '\00bb';
      position: absolute;
      opacity: 0;
      top: 0;
      right: -20px;
      transition: 0.5s;
    }

    .btn:hover span {
      padding-right: 25px;
    }

    .btn:hover span:after {
      opacity: 1;
      right: 0;
    }
  </style>
</head>

<body>
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-4 col-md-offset-4">
        <div class="logo">
          <img class="img-responsive" src="{{asset('img/logo.png')}}">
        </div>
        <form method="post" id="loginForm">
          <h5 class="text-center">Inicie sesión para acceder</h5>
          <div class="form-group">
            <input type="text" class="form-control font-weight-light" id="usuario" name="usuario"
              placeholder="Usuario" required>
          </div>
          <div class="form-group">
            <input type="password" class="form-control font-weight-light" id="contrasenia" name="contrasenia"
              placeholder="Contraseña" required>
          </div>
          <button type="submit" class="btn btn-success btn-block font-weight-bold" id="loginButton">
            <i class="fa fa-spinner fa-spin"></i>
            <span>Ingresar</span>
          </button>
        </form>

        <div class="text-center">
          <a href="#">¿Olvidó su contraseña?</a>
        </div>

        <p class="small text-center text-white font-weight-light">
          Caracoles Centro Educativo Autogestionario
          <small class="text-muted">Sistema de Gestión</small>
        </p>
      </div>
    </div>
  </div>
  -->

  <!-- jQuery 3 -->
  <script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
  <!-- iCheck -->
  <script src="{{asset('bower_components/admin-lte/plugins/iCheck/icheck.min.js')}}"></script>
  <!-- Toastr -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <!-- JQuery Validation -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
  <!-- DataTables -->
  <script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>

  <!-- Scritps propios -->
  <script src="{{asset('js/general.js')}}"></script>
  <script src="{{asset('js/usuario/login.js')}}"></script>
</body>

</html>