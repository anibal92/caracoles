@extends('Layout.app')
@section('titulo', 'Ajustes')
@section('contenido')
<div id="contenido" class="container-fluid">
    <form method="post" id="contraseniaForm">
        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="actual">Contraseña actual<sup class="text-danger">(*)</sup>:</label>
                <input type="password" class="form-control input-lg" id="actual" name="actual" placeholder="Ingrese la contraseña actual">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contrasenia">Nueva contraseña<sup class="text-danger">(*)</sup>:</label>
                <input type="password" class="form-control input-lg" id="contrasenia" name="contrasenia" placeholder="Ingrese la nueva contraseña">
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contraseniaConfirmar">Confirmar contraseña<sup class="text-danger">(*)</sup>:</label>
                <input type="password" class="form-control input-lg" id="contraseniaConfirmar" name="contraseniaConfirmar"
                    placeholder="Repita la contraseña">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 pull-right">
                <button type="submit" id="cambiarButton" class="btn btn-primary hover btn-lg pull-right">
                    <i class="fa fa-spinner fa-spin"></i>
                    Cambiar
                </button>
            </div>
        </div>
    </form>
</div>
@endsection

@section('scripts')
<script src="{{asset('js/usuario/ajustes.js')}}"></script>
@endsection