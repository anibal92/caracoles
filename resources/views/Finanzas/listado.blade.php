@extends('Layout.app')
@section('titulo', 'Finanzas | Movimientos')
@section('contenido')
<div class="container-fluid">
    <br>

    
        <form id="movimientoForm" class="formulario-group">
            <div class="form-group amount">
                <label for="monto">Monto:</label>
                <input type="number" class="form-control" id="monto" name="monto" placeholder="$">
            </div>
            
            <div class="form-group date">
                <label for="fecha">Fecha:</label>
                <input type="date" class="form-control" id="fecha" name="fecha" placeholder="Ingrese la fecha">
            </div> 

        
            <div class="form-group desc">
                <label for="descripcion">Descripción:</label>
                <textarea class="form-control" rows="3" id="descripcion" name="descripcion" placeholder="Ingrese la descripción"></textarea>
            </div>

            <div class="buttons">
                <button type="button" class="btn btn-success btn-rounded btn--add" title="Ingreso" id="btnIn">
                    <i class="fa fa-fw fa-plus" id="plus" style="pointer-events: none"></i>
                    <i class="fa fa-spinner fa-spin"></i>
                </button>
                <button type="button" class="btn btn-danger btn-rounded btn--sub" title="Egreso" id="btnEg">
                    <i class="fa fa-fw fa-minus" style="pointer-events: none" id="minus"></i>
                    <i class="fa fa-spinner fa-spin"></i>
                </button>
            </div>
            
        </form>

        
    <br>
    <table id="movimientosTable" class="table table-bordered table-striped display nowrap" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                <th>Tipo</th>
                <th>Monto</th>
                <th>Descripción</th>
                <th>Fecha</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>


<!-- Modal Eliminar Movimiento-->
<div class="modal fade" id="eliminarMovimientoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CONFIRMAR</h4>
            </div>
            <div class="modal-body">
                <p><b>¿Está seguro/a que desea eliminar este registro?</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary hover" id="confirmarEliminarMovimientoButton">
                    <i class="fa fa-spinner fa-spin"></i>
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<script src="{{asset('js/finanzas/listado.js')}}"></script>
@endsection