@extends('Layout.app')
@section('titulo', 'Resumen finanzas')
@section('contenido')

<div id="contenido" class="container-fluid">
    <div class="row">
        <form class="form-horizontal">
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="selectAnio">Seleccione el año</label>
                    <select class="form-control" id="selectAnio">
                        <option value="2020">2020</option>
                        <option value="2021" selected>2021</option>
                    </select>
                </div>
                <!--<div class="loader centrado" id="loaderAnio" style="height: 25px; width: 25px;"></div> -->
            </div>
        </form>
    </div>
    <div class="row">
    <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="box small-box bg-green widget2" id="ingresosAnual">
                <div class="inner">
                    <h3>-</h3>
                    <h4>-</h4>
                </div>
                <div class="icon">
                    <i class="fa fa-fw fa-usd"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="box small-box bg-red widget2" id="egresosAnual">
                <div class="inner">
                    <h3>-</h3>
                    <h4>-</h4>
                </div>
                <div class="icon">
                    <i class="fa fa-fw fa-usd"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12">
            <div class="box small-box bg-green widget2" id="balanceAnual">
                <div class="inner">
                    <h3>-</h3>
                    <h4>-</h4>
                </div>
                <div class="icon">
                    <i class="fa fa-fw fa-usd"></i>
                </div>
            </div>
        </div>
        <!-- <div class="col-lg-6 col-sm-12">
            <div class="box small-box bg-green widget2" id="balanceMensual">
                <div class="inner">
                    <h3>-</h3>
                    <h4>-</h4>
                </div>
                <div class="icon">
                    <i class="fa fa-fw fa-usd"></i>
                </div>
            </div>
        </div> -->
    </div>

    <br>

    <div class="row">
        <div class="col-lg-12 col-sm-12">
            <!--div id="balanceMensualChart" class="chartJS" style="width: 100%; height: 300px;"></div -->
            <canvas id="balanceMensualChart" class="chartJS" style="height: 250px; width: 787px;" height="250" width="787"></canvas>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{asset('js/finanzas/graficos.js')}}"></script>
@endsection