@extends('Layout.app')
@section('titulo', 'Gráficos cursos')
@section('contenido')
<div class="loader centrado"></div>
<div id="contenido" class="container-fluid oculto">
  <div class="row">
    <div class="col-lg-12 col-sm-12">
    <h3>Participantes por taller</h3>
      <canvas id="participantesCanvas" height="100px"></canvas>
      <br>
      <p class="text-muted text-center">El gráfico muestra la cantidad de participantes en cada taller</p>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="{{asset('js/cursos/graficos.js')}}"></script>
@endsection