@extends('Layout.app')
@section('titulo', 'Cursos')
@section('contenido')
<div class="container-fluid">
    <br>
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="btn btn-primary hover" data-toggle="modal" data-target="#cursoModal"><i class="fa fa-plus"></i> Agregar</button>
        </div>
    </div>
    <br>
    <table id="cursosTable" class="table table-bordered table-striped display nowrap" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Horario</th>
                <th>Responsable</th>
                <th>Inscriptos</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<!-- Modal Curso -->
<div class="modal fade" id="cursoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Curso</h4>
            </div>
            <form id="cursoForm">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-1">
                                <label for="nombre">Nombre del curso:</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingrese el nombre">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-4 col-sm-1">
                                <label for="dia">Día:</label>
                                <select class="form-control" id="dia" name="dia">
                                    <option value="L" selected>Lunes</option>
                                    <option value="M">Martes</option>
                                    <option value="X">Miércoles</option>
                                    <option value="J">Jueves</option>
                                    <option value="V">Viernes</option>
                                    <option value="S">Sábado</option>
                                    <option value="D">Domingo</option>
                                </select>
                            </div>
                            <div class="form-group  col-lg-4 col-sm-1">
                                <label for="horaInicio">Hora inicio:</label>
                                <div class="input-group clockpicker">
                                    <input type="text" class="form-control" value="" id="horaInicio" name="horaInicio" placeholder="HH:MM">
                                    <span class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </span>
                                </div>

                                <!-- div class="input-group">
                                    <input type="text" class="form-control" id="horaInicio" name="horaInicio" placeholder="HH:MM">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div -->
                            </div>                                  
                            <div class="form-group  col-lg-4 col-sm-1">
                                <label for="horaFin">Hora fin:</label>
                                <div class="input-group clockpicker">
                                    <input type="text" class="form-control" value="" id="horaFin" name="horaFin" placeholder="HH:MM">
                                    <span class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </span>
                                </div>
                                <!-- div class="input-group">
                                    <input type="text" class="form-control" id="horaFin" name="horaFin" placeholder="HH:MM">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-1">
                                <label for="educador">Educador:</label>
                                <select class="form-control" id="educador" name="educador">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary hover" id="guardarCursoButton">
                            <i class="fa fa-spinner fa-spin"></i>
                            Guardar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Participantes -->
<div class="modal fade" id="participantesModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Participantes del curso</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="form-group col-sm-12 col-lg-12 col-md-12">
                            <div class="input-group">
                                <input type="hidden" id="cursoId" name="cursoId">
                                <input type="text" class="form-control" id="participante" name="participante" placeholder="Ingrese nombre o documento">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary" id="agregarParticipanteButton"><!--<i class="fa fa-search"></i>-->Agregar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <table id="participantesTable" class="table table-bordered table-striped display nowrap" cellspacing="0" style="width:100%">
                        <thead>
                            <tr>
                                <th>Documento</th>
                                <th>Apellido</th>
                                <th>Nombre</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Eliminar Curso-->
<div class="modal fade" id="eliminarCursoModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CONFIRMAR</h4>
            </div>
            <div class="modal-body">
                <p><b>¿Está seguro/a que desea eliminar este curso?</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary hover" id="confirmarEliminarCursoButton">
                    <i class="fa fa-spinner fa-spin"></i>
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Quitar Participante-->
<div class="modal fade" id="quitarParticipanteModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CONFIRMAR</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="quitarCursoId">
                <input type="hidden" id="quitarParticipanteId">
                <p><b>¿Está seguro/a que desea quitar este participante?</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary hover" id="confirmarQuitarParticipanteButton">
                    <i class="fa fa-spinner fa-spin"></i>
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{asset('js/cursos/listado.js')}}"></script>
@endsection