@extends('Layout.app')
@section('titulo', 'Cursos | Educadores')
@section('contenido')
<div class="container-fluid">
    <br>
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="btn btn-primary hover" data-toggle="modal" data-target="#educadorModal"><i class="fa fa-plus"></i> Agregar</button>
        </div>
    </div>
    <br>
    <table id="educadoresTable" class="table table-bordered table-striped display nowrap" cellspacing="0" style="width:100%">
        <thead>
            <tr>
                <th>Documento</th>
                <th>Nombre</th>
                <th>Teléfono</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<!-- Modal Educador -->
<div class="modal fade" id="educadorModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Educador</h4>
            </div>
            <form id="educadorForm">
                <input type="hidden" id="personaId" name="personaId">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="documento">Nº de Documento:</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="documento" name="documento" placeholder="Ingrese número de documento">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary" id="buscarEducadorButton"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-1">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="sinDocumento" id="sinDocumento"> Sin documento
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="nombres">Nombre<sup class="text-danger">(*)</sup>:</label>
                                <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Ingrese el nombre">
                            </div>
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="apellido">Apellido<sup class="text-danger">(*)</sup>:</label>
                                <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Ingrese el apellido">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-4 col-sm-1">
                                <label for="nacimiento">Fecha de nacimiento:</label>
                                <input type="date" class="form-control" id="nacimiento" name="nacimiento" placeholder="Ingrese la fecha de nacimiento">
                            </div>
                            <div class="form-group col-lg-4 col-sm-1">
                                <label for="genero">Genero<sup class="text-danger">(*)</sup>:</label>
                                <select class="form-control" id="genero" name="genero">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-sm-1">
                                <label for="civil">Estado Civil:</label>
                                <select class="form-control" id="civil" name="civil">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="email">E-mail:</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Ingrese el e-mail">
                            </div>
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="telefono">Teléfono:</label>
                                <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Ingrese el teléfono">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="calle">Calle:</label>
                                <input type="text" class="form-control" id="calle" name="calle" placeholder="Ingrese la calle">
                            </div>
                            <div class="form-group col-lg-2 col-sm-1">
                                <label for="numero">Número:</label>
                                <input type="number" class="form-control" id="numero" name="numero">
                            </div>
                            <div class="form-group col-lg-2 col-sm-1">
                                <label for="piso">Piso:</label>
                                <input type="number" class="form-control" id="piso" name="piso">
                            </div>
                            <div class="form-group col-lg-2 col-sm-1">
                                <label for="departamento">Dpto:</label>
                                <input type="text" class="form-control" id="departamento" name="departamento">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="barrio">Barrio:</label>
                                <select class="form-control" id="barrio" name="barrio">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-sm-1">
                                <label for="localidad">Localidad:</label>
                                <select class="form-control" id="localidad" name="localidad">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary hover" id="guardarEducadorButton">
                            <i class="fa fa-spinner fa-spin"></i>
                            Guardar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Eliminar Educador-->
<div class="modal fade" id="eliminarEducadorModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CONFIRMAR</h4>
            </div>
            <div class="modal-body">
                <p><b>¿Está seguro/a que desea eliminar este educador?</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary hover" id="confirmarEliminarEducadorButton">
                    <i class="fa fa-spinner fa-spin"></i>
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{asset('js/cursos/educadores.js')}}"></script>
@endsection