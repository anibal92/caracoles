@extends('Layout.app')
@section('titulo', 'Inventario')
@section('contenido')
<div class="container-fluid">
    <br>
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="btn btn-primary hover" data-toggle="modal" data-target="#materialModal"><i class="fa fa-plus"></i> Agregar</button>
            <button type="button" class="btn btn-primary hover" data-toggle="modal" data-target="#csvModal"><i class="fa fa-plus"></i> Lote</button>
        </div>
    </div>
    <br>
    <table id="materialesTable" class="table table-bordered table-striped" style="width:100%">
        <thead>
            <tr>
                <th>Código</th>
                <th>Nombre</th>
                <th>Categoría</th>
                <th>Estado</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>

<!-- Modal Material -->
<div class="modal fade" id="materialModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Material</h4>
            </div>
            <form id="materialForm">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-12">
                                <label for="codigo">Código:</label>
                                <input type="text" class="form-control" id="codigo" name="codigo" placeholder="Ingrese el código">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="nombre">Nombre:</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingrese el nombre">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-12">
                                <label for="estado">Estado:</label>
                                <select class="form-control" id="estado" name="estado">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-sm-12">
                                <label for="categoria">Categoría:</label>
                                <select class="form-control" id="categoria" name="categoria">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-sm-12">
                                <label for="procedencia">Procedencia:</label>
                                <select class="form-control" id="procedencia" name="procedencia">
                                    <option value="COMPRA" selected>Compra</option>
                                    <option value="DONACION">Donación</option>
                                    <option value="OTRO">Otro</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-6 col-sm-12">
                                <label for="proyecto">Proyecto:</label>
                                <select class="form-control" id="proyecto" name="proyecto">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 form-group">
                                <label for="descripcion">Descripción</label>
                                <textarea class="form-control" rows="3" id="descripcion" name="descripcion" placeholder="Ingrese la descripción"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary hover" id="guardarMaterialButton">
                            <i class="fa fa-spinner fa-spin"></i>
                            Guardar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Material-Curso -->
<div class="modal fade" id="cursosModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Materiales por curso</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                <div class="row">
                            <div class="form-group col-lg-10 col-sm-10">
                                <label for="curso">Curso:</label>
                                <select class="form-control" id="curso" name="curso">
                                    <option value="" selected>Seleccione</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-sm-2">
                                <button type="button" class="btn btn-default" id='asignarCursoButton'>Asignar</button>
                            </div>
                        </div>
                    <table id="cursosTable" class="table table-bordered table-striped display nowrap" cellspacing="0" style="width:100%">
                        <thead>
                            <tr>
                                <th>Curso</th>
                                <th>Horarios</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Eliminar Material-->
<div class="modal fade" id="eliminarMaterialModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CONFIRMAR</h4>
            </div>
            <div class="modal-body">
                <p><b>¿Está seguro/a que desea eliminar este material?</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary hover" id="confirmarEliminarMaterialButton">
                    <i class="fa fa-spinner fa-spin"></i>
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal CSV-->
<div class="modal fade" id="csvModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CARGAR</h4>
            </div>
            <div class="modal-body">
                <input type="file" id="csv" name="csv">
                <br>
                <div id="textoCSV"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary hover" id="cargaCsvButton">
                    <i class="fa fa-spinner fa-spin"></i>
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<script src="{{asset('js/papaparse.min.js')}}"></script>
<script src="{{asset('js/inventario/listado.js')}}"></script>
@endsection