@extends('Layout.app')
@section('titulo', 'Gráficos inventario')
@section('contenido')
<div class="loader centrado"></div>
<div id="contenido" class="container-fluid oculto">
  <div class="row">
    <div class="col-lg-6 col-sm-12">
      <h3>Distribución por procedencia</h3>
      <canvas id="procedenciaCanvas"></canvas>
      <br>
      <p class="text-muted text-center">El gráfico muestra la cantidad de items <br>por procedencia</p>
    </div>
    <div class="col-lg-6 col-sm-12">
      <h3>Distribución por estado</h3>
      <canvas id="estadoCanvas"></canvas>
      <br>
      <p class="text-muted text-center">El gráfico muestra la cantidad de items <br>que se encuentran en determinado estado</p>
    </div>
  </div>
  <!--<div class="row">
    <div class="col-lg-12 col-sm-12">
    <h3>Participantes por taller</h3>
      <canvas id="participantesCanvas" height="100px"></canvas>
    </div>
  </div>-->
</div>
@endsection

@section('scripts')
<script src="{{asset('js/inventario/graficos.js')}}"></script>
@endsection