@extends('Layout.app')
@section('titulo', 'Administración | Usuarios')
@section('contenido')
<table id="usuariosTable" class="table table-bordered table-striped display nowrap" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Usuario</th>
            <th>Nombre y Apellido</th>
            <th>E-mail</th>
            <th>Último Acceso</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="eliminarModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">CONFIRMAR</h4>
            </div>
            <div class="modal-body">
                <p><b>¿Está seguro/a que desea desactivar este usuario?</b></p>
                <p>El nombre de usuario no podrá volver a ser utilizado</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary hover" id="confirmarEliminarButton">
                    <i class="fa fa-spinner fa-spin"></i>
                    Confirmar
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Contraseña -->
<div class="modal fade" id="contraseniaModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Recuperar contraseña</h4>
            </div>
            <form id="contraseniaForm">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="contrasenia">Contraseña<sup class="text-danger">(*)</sup>:</label>
                                <input type="password" class="form-control input-lg" id="contrasenia" name="contrasenia" placeholder="Ingrese la nueva contraseña">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12 col-sm-12">
                                <label for="contraseniaConfirmar">Confirmar Contraseña<sup class="text-danger">(*)</sup>:</label>
                                <input type="password" class="form-control input-lg" id="contraseniaConfirmar" name="contraseniaConfirmar" placeholder="Repita la contraseña">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary" id="confirmarRecuperarButton">
                        <i class="fa fa-spinner fa-spin"></i>
                        Recuperar
                    </button>
                </div>
        </div>
        </form>
    </div>
</div>
</div>
@endsection

@section('scripts')
<script src="{{asset('js/usuarios/listado.js')}}"></script>
@endsection