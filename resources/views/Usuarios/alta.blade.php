@extends('Layout.app')
@section('titulo', 'Administración | Alta de Usuario')
@section('contenido')
<div class="container-fluid">
    <h3>Datos del Usuario</h3>
    <br>

    <form method="post" id="altaForm">
        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="usuario">Nombre de usuario<sup class="text-danger">(*)</sup>:</label>
                <input type="text" class="form-control input-lg" id="usuario" name="usuario" placeholder="Ingrese nombre de usuario">
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="email">E-mail<sup class="text-danger">(*)</sup>:</label>
                <input type="email" class="form-control input-lg" id="email" name="email" placeholder="Ingrese el e-mail">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="nombre">Nombre:</label>
                <input type="text" class="form-control input-lg" id="nombre" name="nombre" placeholder="Ingrese el nombre">
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="apellido">Apellido:</label>
                <input type="text" class="form-control input-lg" id="apellido" name="apellido" placeholder="Ingrese el apellido">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contrasenia">Contraseña<sup class="text-danger">(*)</sup>:</label>
                <input type="password" class="form-control input-lg" id="contrasenia" name="contrasenia" placeholder="Ingrese la contraseña">
            </div>
            <div class="form-group col-lg-6 col-md-6 col-sm-12">
                <label for="contraseniaConfirmar">Confirmar Contraseña<sup class="text-danger">(*)</sup>:</label>
                <input type="password" class="form-control input-lg" id="contraseniaConfirmar" name="contraseniaConfirmar"
                    placeholder="Confirme la contraseña">
            </div>
        </div>

        <h3>Permisos <small class="text-uppercase"> | Modulos</small></h3>
        <br>

        <div class="row">
            <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-2">
                <h4>Adminsitración</h4>
            </div>
            <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-3">
                <label class="switch">
                    <input type="checkbox" id="moduloUsuarios" name="moduloUsuarios">
                    <span class="slider round"></span>
                </label>
            </div>
            <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-2">
                <h4>Participantes</h4>
            </div>
            <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-3">
                <label class="switch">
                    <input type="checkbox" id="moduloParticipantes" name="moduloParticipantes">
                    <span class="slider round"></span>
                </label>
            </div>
            <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-2">
                <h4>Cursos</h4>
            </div>
            <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-3">
                <label class="switch">
                    <input type="checkbox" id="moduloCursos" name="moduloCursos">
                    <span class="slider round"></span>
                </label>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-2">
                <h4>Inventario</h4>
            </div>
            <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-3">
                <label class="switch">
                    <input type="checkbox" id="moduloInventario" name="moduloInventario">
                    <span class="slider round"></span>
                </label>
            </div>
            <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-2">
                <h4>Biblioteca</h4>
            </div>
            <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-3">
                <label class="switch">
                    <input type="checkbox" id="moduloBiblioteca" name="moduloBiblioteca">
                    <span class="slider round"></span>
                </label>
            </div>
            <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-2">
                <h4>Finanzas</h4>
            </div>
            <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-3">
                <label class="switch">
                    <input type="checkbox" id="moduloFinanzas" name="moduloFinanzas">
                    <span class="slider round"></span>
                </label>
            </div>
        </div>

        <div class="row">
            <hr id="separarEditarButton" class="oculto">
            <div class="col-lg-2 pull-right">
                <button type="submit" id="altaButton" class="btn btn-primary hover btn-lg pull-right">
                    <i class="fa fa-spinner fa-spin"></i>
                    Registrar
                </button>
            </div>
        </div>
    </form>
</div>
@endsection

@section('scripts')
<script src="{{asset('js/usuarios/alta.js')}}"></script>
@endsection