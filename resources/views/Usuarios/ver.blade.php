@extends('Layout.app')
@section('titulo', 'Ver Usuario')
@section('contenido')
<div class="container-fluid">
    <div class="loader centrado"></div>
    <div id="contenido" class="oculto">
        <div class="row">
            <div class="col-lg-10">
                <h3>Datos del Usuario</h3>
            </div>
            <div class="col-lg-2">
                <h3>
                    <button type="button" id="activarEditarBtn" class="btn btn-primary hover pull-right"><i class="fa fa-edit"></i> Editar</button>
                    <button type="button" id="cancelarEditarBtn" class="btn btn-default pull-right"><i class="fa fa-close"></i> Cancelar</button>
                </h3>
            </div>
        </div>
        <br>

        <form method="post" id="editarForm">
            <input type="hidden" id="id" name="id">
            <div class="row">
                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                    <label for="usuario">Nombre de usuario:</label>
                    <input type="text" class="form-control input-lg" id="usuario" name="usuario" disabled>
                </div>
                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                    <label for="email">E-mail:</label>
                    <input type="email" class="form-control input-lg" id="email" name="email" disabled>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                    <label for="nombre">Nombre:</label>
                    <input type="text" class="form-control input-lg" id="nombre" name="nombre" disabled>
                </div>
                <div class="form-group col-lg-6 col-md-6 col-sm-12">
                    <label for="apellido">Apellido:</label>
                    <input type="text" class="form-control input-lg" id="apellido" name="apellido" disabled>
                </div>
            </div>

            <h3>Permisos <small class="text-uppercase"> | Modulos</small> </h3>
            <br>

            <div class="row">
                <div class=" col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-2">
                    <h4>Adminsitración</h4>
                </div>
                <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-3">
                    <label class="switch">
                        <input type="checkbox" id="moduloUsuarios" name="moduloUsuarios" disabled>
                        <span class="slider round"></span>
                    </label>
                </div>
                <div class=" col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-2">
                    <h4>Participantes</h4>
                </div>
                <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-3">
                    <label class="switch">
                        <input type="checkbox" id="moduloParticipantes" name="moduloParticipantes" disabled>
                        <span class="slider round"></span>
                    </label>
                </div>
                <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-2">
                    <h4>Cursos</h4>
                </div>
                <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-3">
                    <label class="switch">
                        <input type="checkbox" id="moduloCursos" name="moduloCursos" disabled>
                        <span class="slider round"></span>
                    </label>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-2">
                    <h4>Inventario</h4>
                </div>
                <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-3">
                    <label class="switch">
                        <input type="checkbox" id="moduloInventario" name="moduloInventario" disabled>
                        <span class="slider round"></span>
                    </label>
                </div>
                <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-2">
                    <h4>Biblioteca</h4>
                </div>
                <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-3">
                    <label class="switch">
                        <input type="checkbox" id="moduloBiblioteca" name="moduloBiblioteca" disabled>
                        <span class="slider round"></span>
                    </label>
                </div>
                <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-2">
                    <h4>Finanzas</h4>
                </div>
                <div class="col-lg-2 col-lg-offset-0 col-sm-3 col-sm-offset-3">
                    <label class="switch">
                        <input type="checkbox" id="moduloFinanzas" name="moduloFinanzas" disabled>
                        <span class="slider round"></span>
                    </label>
                </div>
            </div>

            

            <div class="row">
                <hr id="separarEditarButton" class="oculto">
                <div class="col-lg-2 pull-right">
                    <button type="submit" id="editarButton" class="btn btn-primary hover btn-lg pull-right">
                        <i class="fa fa-spinner fa-spin"></i>
                        Guardar
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>var usuarioId = {{$id}}</script>
<script src="{{asset('js/usuarios/ver.js')}}"></script>
@endsection