//===============================================
//					Variables
//===============================================
var backups;

var tablaBackup = $('#backupTable').DataTable({
	bLengthChange: false,
	info: false,
	language: {
		loadingRecords: '<div class="loader centrado"></div>',
		emptyTable: 'No hay resultados.',
		paginate: {
			next: 'Siguiente',
			previous: 'Anterior',
		},
		search: 'Buscar:'
	},
	responsive: true, 
	ordering: true,
	searching: true,
	order: [[0, "desc"]],
	ajax: {
		type: 'post',
		url: path + 'backup/listado',
		dataType: 'json',
		dataSrc: function (resultado) {
			if (resultado.exito) {
				backups = resultado.backups;
				$.each(resultado.backups, function (indice, backup) {
					backup.btn = '<div class="btn-group">';
					//backup.btn += '<a href="descargar/' + backup.archivo + '"><button type="button" class="btn btn-primary verMaterialButton"><i class="fa fa-eye"></i></button></a>';
					backup.btn += '<button type="button" class="btn btn-primary descargarBackupButton" title="Descargar" value = "' + backup.archivo + '"><i class="fa  fa-download"></i></button>';
					backup.btn += '<button type="button" class="btn btn-danger eliminarBackupButton" value="' + backup.archivo + '"><i class="fa fa-trash"></i></button>';
					backup.btn += '</div>';
				});

				return resultado.backups;
			}
			else {
				errorServidor(resultado.error);
			}
		},
		error: function (jqXHR, textStatus, thrownError) {
			console.log(thrownError);
			toastr.error('Error ' + jqXHR.status, 'Error al listar');
		},
	},
	columns: [
		{ data: 'fecha' },
		{ data: 'hora' },
		{ data: 'btn' }
	],
});


//===============================================
//					Funciones
//===============================================

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {

});

//===============================================
//					Eventos
//===============================================
$('#backupTable').on('click', '.descargarBackupButton', function () {
	window.location = path + 'backup/descargar/' + $(this).val();
});

$('#backupTable').on('click', '.eliminarBackupButton', function () {
	$('#confirmarEliminarBackupButton').val($(this).val());
	$('#eliminarBackupModal').modal('show');
});

$('#confirmarEliminarBackupButton').click(function () {
	activarBotonCargando($(this));
	$.ajax({
		type: 'post',
		url: path + 'backup/eliminar',
		dataType: 'json',
		data: {
			archivo: $(this).val()
		},
		success: function (resultado) {
			desactivarBotonCargando($('#confirmarEliminarBackupButton'));
			$(this).val('');
			$('#eliminarBackupModal').modal('hide');

			if (resultado.exito) {
				toastr.success('Elemento eliminado.');
				tablaBackup.ajax.reload();
			}
			else {
				errorServidor(resultado.error);
			}
		},
		error: function (jqXHR, textStatus, thrownError) {
			$(this).val('');
			$('#eliminarBackupModal').modal('hide');
			desactivarBotonCargando($('#confirmarEliminarBackupButton'));
			console.log(thrownError);
			toastr.error('Error ' + jqXHR.status, 'Error al eliminar');
		},
	});
});

$('#crearBackupButton').click(function () {
	activarBotonCargando($(this));
	$.ajax({
		type: 'post',
		url: path + 'backup/crear',
		success: function (resultado) {
			desactivarBotonCargando($('#crearBackupButton'));

			if (resultado == 0) {
				toastr.success('Backup creado.');
				tablaBackup.ajax.reload();
			}
			else {
				errorServidor(resultado.error);
			}
		},
		error: function (jqXHR, textStatus, thrownError) {
			$(this).val('');
			desactivarBotonCargando($('#crearBackupButton'));
			console.log(thrownError);
			toastr.error('Error ' + jqXHR.status, 'Error al crear backup');
		},
	});
});

