//===============================================
//					Variables
//===============================================

const nuevoParticipante = document.querySelector('#nuevoParticipante');
const nuevoLibro = document.querySelector('#nuevoLibro');
const nuevoCurso = document.querySelector('#nuevoCurso');

//===============================================
//					Funciones
//===============================================


//===============================================
//					Eventos
//===============================================

$(document).ready( function(){

    $(nuevoParticipante).click( function(e){
        let url = path + 'participantes/alta';
        activarBotonCargando($('#nuevoParticipanteButton'));
        if (e.target === nuevoParticipante){
            window.location.replace(url);
        } else {
            if (e.target.parentElement === nuevoParticipante){
                window.location.replace(url);
            }
        }
    })

   $(nuevoCurso).click( function(e){
        let url = path + 'cursos/listado';
        activarBotonCargando($('#nuevoCursoButton'));
        if (e.target === nuevoCurso){
            window.location.replace(url);
        } else {
            if (e.target.parentElement === nuevoCurso){
                window.location.replace(url);
            }
        }
    })

    $(nuevoLibro).click( function(e){
        let url = path + 'biblioteca/libro/listado';
        activarBotonCargando($('#nuevoLibroButton'));
        if (e.target === nuevoLibro){
            window.location.replace(url);
        } else {
            if (e.target.parentElement === nuevoLibro){
                window.location.replace(url);
            }
        }
    })

})