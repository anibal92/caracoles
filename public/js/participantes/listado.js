//===============================================
//					Variables
//===============================================
const date = new Date();
let fechaActual =
  date.getDate() +
  "-" +
  (date.getMonth() + 1) +
  "-" +
  date.getFullYear() +
  " " +
  date.getHours() +
  ":" +
  date.getMinutes() +
  ":" +
  date.getSeconds();

var tablaParticipantes = $("#participantesTable").DataTable({
  bLengthChange: false,
  info: true,
  language: {
    loadingRecords: '<div class="loader centrado"></div>',
    emptyTable: "No hay resultados.",
    paginate: {
      next: "Siguiente",
      previous: "Anterior",
    },
    search: "Buscar:",
    info: "Total: _TOTAL_",
  },
  responsive: true,
  dom: "Blfrtip",
  buttons: [
    {
      extend: "excelHtml5",
      exportOptions: {
        columns: [0, 1, 2],
      },
      text: '<i class="fa fa-file-excel-o"></i>',
      titleAttr: "Exportar a Excel",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2, 3],
      },
      title: "Listado de participantes - PARTICIPANTES",
      filename: "Listado de participantes - PARTICIPANTES" + " " + fechaActual,
    },
    {
      extend: "pdfHtml5",
      exportOptions: {
        columns: [0, 1, 2],
      },
      text: '<i class="fa fa-file-pdf-o"></i> ',
      titleAttr: "Exportar a Pdf",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2, 3],
      },
      title: "  ",
      filename: "Listado de participantes" + " " + fechaActual,
      customize: function (doc) {
        doc.content[1].table.widths = ["25%", "25%", "25%", "25%"];
        (doc.download = "open"), (doc.pageMargins = [30, 80, 20, 30]);
        doc.styles.tableHeader = {
          fontSize: "12",
          alignment: "left",
          fillColor: "#545454",
          color: "#FFFFFF",
        };

        doc.content.splice(1, 0, {
          alignment: "left",
          text: [
            "\n",
            "\n",
            "\n",
            {
              text: "Fecha: " + fechaActual,
              fontSize: 12,
              bold: false,
            },
            "\n",
            "\n",
          ],
        });

        doc["header"] = function () {
          return {
            columns: [
              {
                margin: [30, 10, 2, 5],
                alignment: "left",
                image: logoBase64,
                width: 100,
              },
              {
                margin: [0, 30, 100, 0],
                alignment: "right",
                text: "Listado de participantes", //TITULO DEL INFORME
                fontSize: 20,
                bold: false,
              },
            ],
          };
        };

        doc["footer"] = function (page, pages) {
          return [
            //para vertical x2: 580 ---- para horizontal x2:820
            {
              canvas: [
                { type: "line", x1: 20, y1: 6, x2: 580, y2: 6, lineWidth: 1 },
              ],
            },
            {
              columns: [
                {
                  text: "Centro Educativo Caracoles",
                  fontSize: 9,
                  bold: true,
                },
                {
                  alignment: "right",
                  text: [
                    {
                      text:
                        "Página " + page.toString() + " de " + pages.toString(),
                      fontSize: 9,
                      bold: true,
                    },
                  ],
                },
              ],
              margin: [20, 0],
            },
          ];
        };
      },
    },
  ],
  ordering: true,
  searching: true,
  ajax: {
    type: "post",
    url: path + "participantes/listado",
    dataType: "json",
    dataSrc: function (resultado) {
      if (resultado.exito) {
        $.each(resultado.participantes, function (indice, participante) {
          participante.btn = '<div class="btn-group">';
          participante.btn +=
            '<button type="button" class="btn btn-primary verButton" title="Ver participante" value = "' +
            participante.id +
            '"><i class="fa fa-eye"></i></button>';
          participante.btn +=
            '<button type="button" class="btn btn-primary editarButton" title="Editar participante" value = "' +
            participante.id +
            '"><i class="fa fa-pencil"></i></button>';
          participante.btn +=
            '<button type="button" class="btn btn-danger eliminarButton" value="' +
            participante.id +
            '"><i class="fa fa-trash"></i></button>';
          participante.btn += "</div>";
        });

        return resultado.participantes;
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al listar");
    },
  },
  columnDefs: [{ width: "12%", targets: [3, 4] }],
  columns: [
    { data: "documento" },
    { data: "nombres" },
    { data: "apellido" },
    { data: "nacimiento" },
    { data: "btn" },
  ],
});

//===============================================
//					Funciones
//===============================================

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {});
//===============================================
//					Eventos
//===============================================
$("#participantesTable").on("click", ".verButton", function () {
  window.location = path + "participantes/ver/" + $(this).val();
});

$("#participantesTable").on("click", ".editarButton", function () {
  window.location = path + "participantes/editar/" + $(this).val();
});

$("#participantesTable").on("click", ".eliminarButton", function () {
  $("#confirmarEliminarButton").val($(this).val());
  $("#eliminarModal").modal("show");
});

$("#confirmarEliminarButton").click(function () {
  activarBotonCargando($(this));
  $.ajax({
    type: "post",
    url: path + "participantes/eliminar",
    dataType: "json",
    data: {
      id: $(this).val(),
    },
    success: function (resultado) {
      desactivarBotonCargando($("#confirmarBtnModal"));
      $(this).val("");
      $("#eliminarModal").modal("hide");

      if (resultado.exito) {
        toastr.success("Participante desactivado.");
        tablaParticipantes.ajax.reload();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      $(this).val("");
      $("#eliminarModal").modal("hide");
      desactivarBotonCargando($("#confirmarEliminarButton"));
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al desactivar");
    },
  });
});
