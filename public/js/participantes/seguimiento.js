//===============================================
//					Variables
//===============================================
var seguimientos;

var tablaSeguimientos = $('#seguimientosTable').DataTable({
	bLengthChange: false,
	info: false,
	language: {
		loadingRecords: '<div class="loader centrado"></div>',
		emptyTable: 'No hay resultados.',
		paginate: {
			next: 'Siguiente',
			previous: 'Anterior',
		},
		search: 'Buscar:'
	},
	responsive: true,
	ordering: true,
	searching: true,
	order: [[0, "desc"]],
	ajax: {
		type: 'post',
		url: path + 'participantes/seguimiento/listado',
		dataType: 'json',
		data: {
			participanteId: participanteId
		},
		dataSrc: function (resultado) {
			if (resultado.exito) {
				seguimientos = resultado.seguimientos;
				$.each(seguimientos, function (indice, seguimiento) {
					seguimiento.btn = '<div class="btn-group">';
					seguimiento.btn += '<button type="button" class="btn btn-primary verSeguimientoButton" value = "' + indice + '"><i class="fa fa-eye"></i></button>';
					seguimiento.btn += '<button type="button" class="btn btn-danger eliminarSeguimientoButton" value="' + seguimiento.id + '"><i class="fa fa-trash"></i></button>';
					seguimiento.btn += '</div>';
				});
				
				return seguimientos;
			}
			else {
				errorServidor(resultado.error);
			}
		},
		error: function (jqXHR, textStatus, thrownError) {
			console.log(thrownError);
			toastr.error('Error ' + jqXHR.status, 'Error al listar');
		},
	},
	columns: [
		{ data: 'fecha' },
		{ data: 'usuario' },
		{ data: 'observacion', render: $.fn.dataTable.render.ellipsis(100) },
		{ data: 'btn' }
	],
});
//===============================================
//					Funciones
//===============================================

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
	//Instancia de la validación y envío del seguimiento;
	$('#seguimientoForm').validate({
		submitHandler: function (form) {
			activarBotonCargando($('#guardarSeguimientoButton'));
			$.ajax({
				type: 'post',
				url: path + 'participantes/seguimiento/alta',
				dataType: 'json',
				data: {
					observacion: $('#observacionSeguimiento').val(),
					participanteId: participanteId
				},
				success: function (resultado) {
					desactivarBotonCargando($('#guardarSeguimientoButton'));
					if (resultado.exito) {
						tablaSeguimientos.ajax.reload();
						toastr.success('Seguimiento guardado con éxito.');
						$('#seguimientoModal').modal('hide');
					}
					else {
						errorServidor(resultado.error);
					}
				},
				error: function (jqXHR, textStatus, thrownError) {
					desactivarBotonCargando($('#guardarSeguimientoButton'));
					console.log(thrownError);
					toastr.error('Error ' + jqXHR.status, 'Error guardar el seguimiento');
				},
			});
		},
		rules: {
			observacionSeguimiento: "required",
		},
		messages: {
			observacionSeguimiento: "Campo requerido",
		},
		errorElement: 'span',
		errorClass: 'help-block',
		highlight: function (element) {
			$(element).parent().addClass('has-error');
		},
		unhighlight: function (element) {
			$(element).parent().removeClass('has-error');
		},
		invalidHandler: function (event, validator) {
			toastr.error('Compruebe los campos');
		},
	});
});

//===============================================
//					Eventos
//===============================================

$('#seguimientosTable').on('click', '.verSeguimientoButton', function () {
	var seguimiento = seguimientos[$(this).val()];
	$('#verSeguimientoUsuario').text(seguimiento.usuario);
	$('#verSeguimientoFecha').text(seguimiento.fecha);
	$('#verSeguimientoObservacion').html(nl2br(seguimiento.observacion));
	$('#verSeguimientoModal').modal('show');
});

$('#seguimientoModal').on('hidden.bs.modal', function () {
	limpiarForm($('#seguimientoForm'));
});

$('#seguimientosTable').on('click', '.eliminarSeguimientoButton', function () {
	$('#confirmarEliminarSeguimientoButton').val($(this).val());
	$('#eliminarSeguimientoModal').modal('show');
});

$('#confirmarEliminarSeguimientoButton').click(function () {
	activarBotonCargando($(this));
	$.ajax({
		type: 'post',
		url: path + 'participantes/seguimiento/eliminar',
		dataType: 'json',
		data: {
			id: $(this).val()
		},
		success: function (resultado) {
			desactivarBotonCargando($('#confirmarEliminarSeguimientoButton'));
			$(this).val('');
			$('#eliminarSeguimientoModal').modal('hide');

			if (resultado.exito) {
				toastr.success('Elemento eliminado.');
				tablaSeguimientos.ajax.reload();
			}
			else {
				errorServidor(resultado.error);
			}
		},
		error: function (jqXHR, textStatus, thrownError) {
			$(this).val('');
			$('#eliminarModal').modal('hide');
			desactivarBotonCargando($('#confirmarEliminarSeguimientoButton'));
			console.log(thrownError);
			toastr.error('Error ' + jqXHR.status, 'Error al eliminar');
		},
	});
});