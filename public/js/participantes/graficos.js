//===============================================
//					Variables
//===============================================
var generoData = {
  labels: ["HOMBRES", "MUJERES", "OTROS"],
  datasets: [
    {
      label: "Género",
      data: [],
      backgroundColor: ["#00BFFF", "#FFB6C1", "#9370DB"],
      hoverOffset: 4,
    },
  ],
};

var escolaridadData = {
  labels: ["JARDÍN", "PRIMARIA", "SECUNDARIA", "SIN INFORMACIÓN"],
  datasets: [
    {
      label: "Escolaridad",
      data: [],
      backgroundColor: ["#32CD32", "#4682B4", "#DC143C"],
      hoverOffset: 4,
    },
  ],
};

var edadData = {
  labels: [
    "0-4 AÑOS",
    "5-9 AÑOS",
    "10-14 AÑOS",
    "15-17 AÑOS",
    "+18 AÑOS",
    "DESCONOCIDA",
  ],
  datasets: [
    {
      label: "Total",
      backgroundColor: "rgb(255, 205, 86)",
      borderColor: "rgb(255, 205, 86)",
      borderWidth: 1,
      data: [],
    },
  ],
};

var generoGrafico = null;
var edadGrafico = null;
var escolaridadGrafico = null;

//===============================================
//					Funciones
//===============================================

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
  generoGrafico = new Chart($("#generoCanvas"), {
    type: "pie",
    data: generoData,
    options: {
      responsive: true,
      legend: {
        position: "left",
      },
      title: {
        display: false,
        text: "Distribución por género",
      },
    },
  });

  escolaridadGrafico = new Chart($("#escolaridadCanvas"), {
    type: "pie",
    data: escolaridadData,
    options: {
      responsive: true,
      legend: {
        position: "left",
      },
      title: {
        display: false,
        text: "Distribución por escolaridad",
      },
    },
  });

  edadGrafico = new Chart($("#edadCanvas"), {
    type: "bar",
    data: edadData,
    options: {
      responsive: true,
      legend: {
        position: "left",
        display: false,
      },
      title: {
        display: false,
        text: "Distribución por edad",
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
            },
          },
        ],
      },
    },
  });

  $.ajax({
    type: "post",
    url: path + "participantes/graficos",
    dataType: "json",
    data: {},
    success: function (resultado) {
      if (resultado.exito) {
        $(".loader").hide();
        $("#contenido").show();
        generoData.datasets[0].data = resultado.generos;
        var totalGeneros =
          generoData.datasets[0].data[0] +
          generoData.datasets[0].data[1] +
          generoData.datasets[0].data[2];
        var porcentajeHombres =
          (generoData.datasets[0].data[0] * 100) / totalGeneros;
        var porcentajeMujeres =
          (generoData.datasets[0].data[1] * 100) / totalGeneros;
        var porcentajeOtros =
          (generoData.datasets[0].data[2] * 100) / totalGeneros;
        generoData.labels[0] =
          generoData.labels[0] + " " + porcentajeHombres.toFixed(2) + "%";
        generoData.labels[1] =
          generoData.labels[1] + " " + porcentajeMujeres.toFixed(2) + "%";
        generoData.labels[2] =
          generoData.labels[2] + " " + porcentajeOtros.toFixed(2) + "%";
        generoGrafico.update();

        escolaridadData.datasets[0].data = resultado.escolaridades;
        var totalEscolaridad =
          escolaridadData.datasets[0].data[0] +
          escolaridadData.datasets[0].data[1] +
          escolaridadData.datasets[0].data[2] +
          escolaridadData.datasets[0].data[3];
        var porcentajeJardin =
          (escolaridadData.datasets[0].data[0] * 100) / totalEscolaridad;
        var porcentajePrimaria =
          (escolaridadData.datasets[0].data[1] * 100) / totalEscolaridad;
        var porcentajeSecundaria =
          (escolaridadData.datasets[0].data[2] * 100) / totalEscolaridad;
        var porcentajeDesconocido =
          (escolaridadData.datasets[0].data[3] * 100) / totalEscolaridad;
        escolaridadData.labels[0] =
          escolaridadData.labels[0] + " " + porcentajeJardin.toFixed(2) + "%";
        escolaridadData.labels[1] =
          escolaridadData.labels[1] + " " + porcentajePrimaria.toFixed(2) + "%";
        escolaridadData.labels[2] =
          escolaridadData.labels[2] +
          " " +
          porcentajeSecundaria.toFixed(2) +
          "%";
        escolaridadData.labels[3] =
          escolaridadData.labels[3] +
          " " +
          porcentajeDesconocido.toFixed(2) +
          "%";
        generoGrafico.update();
        escolaridadGrafico.update();

        edadData.datasets[0].data = resultado.edades;
        edadGrafico.update();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al obtener los datos.");
    },
  });
});

//===============================================
//					Eventos
//===============================================
