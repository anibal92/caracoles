//===============================================
//					Variables
//===============================================
var participanteOriginal;
var participanteEditado;

//===============================================
//					Funciones
//===============================================
function obtenerParticipante(id) {
  $.ajax({
    type: "post",
    url: path + "participantes/ver",
    dataType: "json",
    data: {
      id: id,
    },
    success: function (resultado) {
      if (resultado.exito) {
        participanteOriginal = resultado.participante;
        cargarFormParticipante(participanteOriginal);
        $(".loader").hide();
        $("#contenido").show();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al cargar");
    },
  });
}

function cargarFormParticipante(participante) {
  //Carga los datos del participante
  $("#id").val(participante.id);
  $("#documento").val(participante.documento);
  $("#nacimiento").val(participante.nacimiento);
  $("#genero").val(participante.genero);
  $("#nombres").val(participante.nombres);
  $("#apellido").val(participante.apellido);
  $("#escuela").val(participante.escuela);
  $("#grado").val(participante.grado);
  $("#programa").val(participante.programa);
  $("#sanguineo").val(participante.salud.sanguineo);
  $("#enfermedad").val(participante.salud.enfermedad);
  $("#alergia").val(participante.salud.alergia);
  $("#medicacion").val(participante.salud.medicacion);
  $("#discapacidad").val(participante.salud.discapacidad);
  $("#cud").val(participante.salud.cud);
  $("#observacionSalud").val(participante.salud.observacion);

  if (participante.domicilio != null) {
    $("#calle").val(participante.domicilio.calle);
    $("#numero").val(participante.domicilio.numero);
    $("#piso").val(participante.domicilio.piso);
    $("#departamento").val(participante.domicilio.departamento);
    $("#barrio").val(participante.domicilio.barrio);
    $("#localidad").val(participante.domicilio.localidad);
  }

  cargarContactoEmergencia(participante.emergencia);

  listarFamiliares(participante.familiares);

  //Si ningún familiar es contacto de emergencia, carga el contacto
  /*if (!existeContactoFamiliar(familiares)) {
		cargarContactoEmergencia(participante.contacto_emergencia.persona);
	}*/
}

function listarFamiliares(familiares) {
  tablaFamiliares.clear().draw();

  $.each(familiares, function (indice, familiar) {
    var btn = '<div class="btn-group">';
    btn +=
      '<button type="button" class="btn btn-primary verFamiliarButton" title="Ver familiar" value = "' +
      indice +
      '"><i class="fa fa-eye"></i></button>';
    btn +=
      '<button type="button" class="btn btn-danger eliminarFamiliarButton" value="' +
      indice +
      '" disabled><i class="fa fa-trash"></i></button>';
    btn += "</div>";
    tablaFamiliares.row.add([
      familiar.documento,
      familiar.apellido,
      familiar.nombres,
      familiar.vinculoTexto,
      btn,
    ]);
  });
  tablaFamiliares.draw();
}

function cargarContactoEmergencia(contactoEmergencia) {
  //TODO: PONER EL ID DE LA PERSONA
  $("#contactoNombres").val(contactoEmergencia.nombres);
  $("#contactoApellido").val(contactoEmergencia.apellido);
  $("#contactoTelefono").val(contactoEmergencia.telefono);
  $("#contactoTelefonoAlternativo").val(contactoEmergencia.telefonoAlternativo);

  if (contactoEmergencia.domicilio != null) {
    $("#contactoCalle").val(contactoEmergencia.domicilio.calle);
    $("#contactoNumero").val(contactoEmergencia.domicilio.numero);
    $("#contactoPiso").val(contactoEmergencia.domicilio.piso);
    $("#contactoDepartamento").val(contactoEmergencia.domicilio.departamento);
    $("#contactoBarrio").val(contactoEmergencia.domicilio.barrio);
    $("#contactoLocalidad").val(contactoEmergencia.domicilio.localidad);
  }

  $("#contactoNombres").attr("disabled", true);
  $("#contactoApellido").attr("disabled", true);
  $("#contactoTelefono").attr("disabled", true);
  $("#contactoTelefonoAlternativo").attr("disabled", true);
  $("#contactoCalle").attr("disabled", true);
  $("#contactoNumero").attr("disabled", true);
  $("#contactoPiso").attr("disabled", true);
  $("#contactoDepartamento").attr("disabled", true);
  $("#contactoBarrio").attr("disabled", true);
  $("#contactoLocalidad").attr("disabled", true);
}

function limpiarContactoEmergencia() {
  //TODO: LIMPIAR EL ID DE LA PERSONA O QCYO
  $("#contactoNombres").val("");
  $("#contactoApellido").val("");
  $("#contactoTelefono").val("");
  $("#contactoTelefonoAlternativo").val("");
  $("#contactoCalle").val("");
  $("#contactoNumero").val("");
  $("#contactoPiso").val("");
  $("#contactoDepartamento").val("");
  $("#contactoBarrio").val("");
  $("#contactoLocalidad").val("");

  $("#contactoNombres").attr("disabled", false);
  $("#contactoApellido").attr("disabled", false);
  $("#contactoTelefono").attr("disabled", false);
  $("#contactoTelefono_alternativo").attr("disabled", false);
  $("#contactoCalle").attr("disabled", false);
  $("#contactoNumero").attr("disabled", false);
  $("#contactoPiso").attr("disabled", false);
  $("#contactoDepartamento").attr("disabled", false);
  $("#contactoBarrio").attr("disabled", false);
  $("#contactoLocalidad").attr("disabled", false);
}

/*function quitarContactos(familiares) {
	$.each(familiares, function (indice, familiar) {
		if (familiar.contacto_emergencia) {
			familiar.contacto_emergencia = 0;
		}
	});
}*/

/*function existeContactoFamiliar(familiares) {
	r = false;
	$.each(familiares, function (indice, familiar) {
		if (familiar.contacto_emergencia) {
			r = true;
		}
	});

	return r;
}*/

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
  deshabilitarForm($("#fichaForm"));
  deshabilitarForm($("#familiarForm"));
  //Instancia la Datatable
  tablaFamiliares = $("#familiaresTable").DataTable({
    bLengthChange: false,
    info: false,
    language: {
      emptyTable: "No hay resultados.",
      paginate: {
        next: "Siguiente",
        previous: "Anterior",
      },
    },
    ordering: false,
    searching: false,
  });

  $(".loader").show();
  $("#contenido").hide();

  //Carga de los combos
  cargarComboGET($("#familiarCivil"), "base/civil");
  cargarComboGET($("#escuela"), "base/escuelas");
  cargarComboGET($("#grado"), "base/grados");
  cargarComboGET($("#programa"), "base/programas");
  cargarComboGET($("#sanguineo"), "base/sanguineo");
  cargarComboGET($("#discapacidad"), "base/discapacidades");
  cargarComboGET($("#familiarEscolaridad"), "base/escolaridades");
  cargarComboGET($("#familiarVinculo"), "base/vinculos");
  cargarCombosGET([$("#genero"), $("#familiarGenero")], "base/generos");
  cargarCombosGET(
    [$("#barrio"), $("#familiarBarrio"), $("#contactoBarrio")],
    "base/barrios"
  );
  cargarCombosGET(
    [$("#localidad"), $("#familiarLocalidad"), $("#contactoLocalidad")],
    "base/localidades/1"
  );

  obtenerParticipante(participanteId);

  //Instancia y validación del formulario de familiares
  $("#familiarForm").validate({
    submitHandler: function (form, event) {
      event.preventDefault();
      var familiar = {
        vinculo: $("#familiarVinculo").val(),
        vinculoTexto: $("#familiarVinculo option:selected").text(),
        documento: $("#familiarDocumento").val(),
        nombres: $("#familiarNombres").val(),
        apellido: $("#familiarApellido").val(),
        genero: $("#familiarGenero").val(),
        civil: $("#familiarCivil").val(),
        nacimiento: $("#familiarNacimiento").val(),
        telefono: $("#familiarTelefono").val(),
        telefono_alternativo: $("#familiarTelefonoAlternativo").val(),
        email: $("#familiarEmail").val(),
        domicilio: {
          calle: $("#familiarCalle").val(),
          piso: $("#familiarPiso").val(),
          numero: $("#familiarNumero").val(),
          departamento: $("#familiarDepartamento").val(),
          barrio: $("#familiarBarrio").val(),
          localidad: $("#familiarLocalidad").val(),
        },
        oficio: $("#familiarOficio").val(),
        escolaridad: $("#familiarEscolaridad").val(),
        interesa_participar: $("#familiarInteresaParticipar").is(":checked")
          ? 1
          : 0,
        interesa_estudios: $("#familiarInteresaEstudios").is(":checked")
          ? 1
          : 0,
        contacto_emergencia: $("#familiarEmergencia").is(":checked") ? 1 : 0,
      };

      if (familiar.contacto_emergencia) {
        quitarContactos(familiares);
        cargarContactoEmergencia(familiar);
      }

      if ($("#guardarFamiliarButton").val() == "") {
        familiares.push(familiar);
      } else {
        familiares[$("#guardarFamiliarButton").val()] = familiar;
        $("#guardarFamiliarButton").val("");
      }

      if (!existeContactoFamiliar(familiares)) {
        limpiarContactoEmergencia();
      }

      listarFamiliares(familiares);
      $("#familiarModal").modal("hide");
    },
    rules: {
      familiarNombres: "required",
      familiarApellido: "required",
      familiarVinculo: "required",
      familiarGenero: "required",
      familiarEmail: "email",
      familiarCalle: {
        required: {
          depends: function (element) {
            return (
              $("#familiarNumero").val() != "" ||
              $("#familiarLocalidad").val() != ""
            );
          },
        },
      },
      familiarNumero: {
        required: {
          depends: function (element) {
            return (
              $("#familiarCalle").val() != "" ||
              $("#familiarLocalidad").val() != ""
            );
          },
        },
      },
      familiarLocalidad: {
        required: {
          depends: function (element) {
            return (
              $("#familiarCalle").val() != "" ||
              $("#familiarNumero").val() != ""
            );
          },
        },
      },
      familiarTelefono: {
        required: {
          depends: function (element) {
            return $("#familiarEmergencia").is(":checked");
          },
        },
      },
    },
    messages: {
      familiarNombres: "Campo requerido",
      familiarApellido: "Campo requerido",
      familiarVinculo: "Campo requerido",
      familiarGenero: "Campo requerido",
      familiarEmail: "E-mail inválido",
      familiarCalle: { required: "Campo requerido" },
      familiarNumero: { required: "Campo requerido" },
      familiarLocalidad: { required: "Campo requerido" },
      familiarTelefono: { required: "Campo requerido" },
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });

  //Instancia de la validación y envío del form;
  $("#fichaForm").validate({
    submitHandler: function (form) {
      activarBotonCargando($("#guardarFichaButton"));
      $.ajax({
        type: "post",
        url: path + "participantes/alta",
        dataType: "json",
        data: {
          documento: $("#documento").val(),
          nacimiento: $("#nacimiento").val(),
          genero: $("#genero").val(),
          nombres: $("#nombres").val(),
          apellido: $("#apellido").val(),
          domicilio: {
            calle: $("#calle").val(),
            numero: $("#numero").val(),
            piso: $("#piso").val(),
            departamento: $("#departamento").val(),
            barrio: $("#barrio").val(),
            localidad: $("#localidad").val(),
          },
          escuela: $("#escuela").val(),
          grado: $("#grado").val(),
          programa: $("#programaBeneficiario").val(),
          salud: {
            sanguineo: $("#grupoSanguineo").val(),
            enfermedad: $("#enfermedad").val(),
            alergia: $("#alergia").val(),
            medicacion: $("#medicacion").val(),
            discapacidad: $("#discapacidad").val(),
            cud: $("#cud").val(),
            observacionSalud: $("#observacionSalud").val(),
          },
          emergencia: {
            nombres: $("#contactoNombres").val(),
            apellido: $("#contactoApellido").val(),
            domicilio: {
              calle: $("#contactoCalle").val(),
              numero: $("#contactoNumero").val(),
              piso: $("#contactoPiso").val(),
              departamento: $("#contactoDepartamento").val(),
              barrio: $("#contactoBarrio").val(),
              localidad: $("#contactoLocalidad").val(),
            },
            telefono: $("#contactoTelefono").val(),
            telefonoAlternativo: $("#contactoTelefonoAlternativo").val(),
          },
          familiares: familiares,
        },
        success: function (resultado) {
          desactivarBotonCargando($("#altaButton"));
          if (resultado.exito) {
            crearToastSession("Participante creado con éxito.");
            location = path + "participantes/listado";
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($("#altaButton"));
          console.log(thrownError);
          toastr.error("Error " + jqXHR.status, "Error al dar de alta");
        },
      });
    },
    rules: {
      nombres: "required",
      apellido: "required",
      genero: "required",
      calle: {
        required: {
          depends: function (element) {
            return $("#numero").val() != "" || $("#localidad").val() != "";
          },
        },
      },
      numero: {
        required: {
          depends: function (element) {
            return $("#calle").val() != "" || $("#localidad").val() != "";
          },
        },
      },
      localidad: {
        required: {
          depends: function (element) {
            return $("#calle").val() != "" || $("#numero").val() != "";
          },
        },
      },
      contactoNombres: "required",
      contactoApellido: "required",
      contactoTelefono: "required",
      contactoCalle: {
        required: {
          depends: function (element) {
            return (
              $("#contactoNumero").val() != "" ||
              $("#contactoLocalidad").val() != ""
            );
          },
        },
      },
      contactoNumero: {
        required: {
          depends: function (element) {
            return (
              $("#contactoCalle").val() != "" ||
              $("#contactoLocalidad").val() != ""
            );
          },
        },
      },
      contactoLocalidad: {
        required: {
          depends: function (element) {
            return (
              $("#contactoCalle").val() != "" ||
              $("#contactoNumero").val() != ""
            );
          },
        },
      },
    },
    messages: {
      nombres: "Campo requerido",
      apellido: "Campo requerido",
      genero: "Campo requerido",
      calle: { required: "Campo requerido" },
      numero: { required: "Campo requerido" },
      localidad: { required: "Campo requerido" },
      contactoNombres: "Campo requerido",
      contactoApellido: "Campo requerido",
      contactoTelefono: "Campo requerido",
      contactoCalle: { required: "Campo requerido" },
      contactoNumero: { required: "Campo requerido" },
      contactoLocalidad: { required: "Campo requerido" },
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });
});

//===============================================
//					Eventos
//===============================================
/*$('#familiaresTable').on('click', '.eliminarFamiliarButton', function () {
	$('#confirmarEliminarBtnModal').val($(this).val());
	$('#eliminarFamiliarModal').modal('show');
});*/

$("#familiaresTable").on("click", ".verFamiliarButton", function () {
  var familiar = participanteOriginal.familiares[$(this).val()];

  $("#familiarVinculo").val(familiar.vinculo);
  $("#familiarDocumento").val(familiar.documento);
  $("#familiarNombres").val(familiar.nombres);
  $("#familiarApellido").val(familiar.apellido);
  $("#familiarNacimiento").val(familiar.nacimiento);
  $("#familiarTelefono").val(familiar.telefono);
  $("#familiarTelefonoAlternativo").val(familiar.telefonoAlternativo);
  $("#familiarEmail").val(familiar.email);
  $("#familiarOficio").val(familiar.oficio);
  $("#familiarInteresaParticipar").prop("checked", familiar.interesaParticipar);
  $("#familiarInteresaEstudios").prop("checked", familiar.interesaEstudios);
  $("#familiarEmergencia").prop("checked", familiar.emergencia);

  if (familiar.domicilio != null) {
    $("#familiarCalle").val(familiar.domicilio.calle);
    $("#familiarPiso").val(familiar.domicilio.piso);
    $("#familiarNumero").val(familiar.domicilio.numero);
    $("#familiarDepartamento").val(familiar.domicilio.departamento);

    if (familiar.domicilio.barrio) {
      $("#familiarBarrio").val(familiar.domicilio.barrio);
    }

    if (familiar.domicilio.localidad) {
      $("#familiarLocalidad").val(familiar.domicilio.localidad);
    }
  }

  if (familiar.genero) {
    $("#familiarGenero").val(familiar.genero);
  }
  if (familiar.civil) {
    $("#familiarCivil").val(familiar.civil);
  }
  if (familiar.escolaridad) {
    $("#familiarEscolaridad").val(familiar.escolaridad);
  }

  //$('#guardarFamiliarButton').val($(this).val());
  $("#familiarModal").modal("show");
});

/*$('#confirmarEliminarFamiliarButton').click(function () {
	familiares.splice($(this).val(), 1);

	listarFamiliares(familiares);

	if (!existeContactoFamiliar(familiares)) {
		limpiarContactoEmergencia();
	}

	$('#eliminarFamiliarModal').modal('hide');
});*/

$("#familiarModal").on("hidden.bs.modal", function () {
  limpiarForm($("#familiarForm"));
});

//TODO: en los select si viene vacio al valor no deberia poner nada
