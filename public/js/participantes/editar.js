//===============================================
//					Variables
//===============================================
var participante = null;
var familiares = [];
var eraContacto = null;
var eliminados = [];

//Instancia la Datatable
tablaFamiliares = $("#familiaresTable").DataTable({
  bLengthChange: false,
  info: false,
  language: {
    emptyTable: "No hay resultados.",
    paginate: {
      next: "Siguiente",
      previous: "Anterior",
    },
    search: "Buscar:",
  },
  responsive: true,
  ordering: false,
  searching: false,
});

//===============================================
//					Funciones
//===============================================
function obtenerParticipante(id) {
  $.ajax({
    type: "post",
    url: path + "participantes/ver",
    dataType: "json",
    data: {
      id: id,
    },
    success: function (resultado) {
      if (resultado.exito) {
        participante = resultado.participante;
        familiares = participante.familiares;
        cargarFormParticipante(participante);
        $(".loader").hide();
        $("#contenido").show();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al cargar");
    },
  });
}

function cargarFormParticipante(participante) {
  //Carga los datos del participante
  $("#id").val(participante.id);
  $("#documento").val(participante.documento);
  $("#nacimiento").val(participante.nacimiento);
  $("#genero").val(participante.genero);
  $("#nombres").val(participante.nombres);
  $("#apellido").val(participante.apellido);
  $("#escuela").val(participante.escuela);
  $("#grado").val(participante.grado);
  $("#programaBeneficiario").val(participante.programa);
  $("#sanguineo").val(participante.salud.sanguineo);
  $("#enfermedad").val(participante.salud.enfermedad);
  $("#alergia").val(participante.salud.alergia);
  $("#medicacion").val(participante.salud.medicacion);
  $("#discapacidad").val(participante.salud.discapacidad);
  $("#cud").val(participante.salud.cud);
  $("#observacionSalud").val(participante.salud.observacion);

  if (participante.domicilio != null) {
    $("#calle").val(participante.domicilio.calle);
    $("#numero").val(participante.domicilio.numero);
    $("#piso").val(participante.domicilio.piso);
    $("#departamento").val(participante.domicilio.departamento);
    $("#barrio").val(participante.domicilio.barrio);
    $("#localidad").val(participante.domicilio.localidad);
  }

  cargarContactoEmergenciaInicial(participante.emergencia);

  listarFamiliares(participante.familiares);

  //Si ningún familiar es contacto de emergencia, carga el contacto
  /*if (!existeContactoFamiliar(familiares)) {
		cargarContactoEmergencia(participante.contacto_emergencia.persona);
	}*/
}

function cargarContactoEmergenciaInicial(contactoEmergencia) {
  $("#contactoPersonaId").val(contactoEmergencia.personaId);
  $("#contactoNombres").val(contactoEmergencia.nombres);
  $("#contactoApellido").val(contactoEmergencia.apellido);
  $("#contactoTelefono").val(contactoEmergencia.telefono);
  $("#contactoTelefonoAlternativo").val(contactoEmergencia.telefonoAlternativo);

  if (contactoEmergencia.domicilio != null) {
    $("#contactoCalle").val(contactoEmergencia.domicilio.calle);
    $("#contactoNumero").val(contactoEmergencia.domicilio.numero);
    $("#contactoPiso").val(contactoEmergencia.domicilio.piso);
    $("#contactoDepartamento").val(contactoEmergencia.domicilio.departamento);
    $("#contactoBarrio").val(contactoEmergencia.domicilio.barrio);
    $("#contactoLocalidad").val(contactoEmergencia.domicilio.localidad);
  }

  var activar = existeContactoFamiliar(familiares);
  $("#contactoNombres").prop("readonly", activar);
  $("#contactoApellido").prop("readonly", activar);
  $("#contactoTelefono").prop("readonly", activar);
  $("#contactoTelefonoAlternativo").prop("readonly", activar);
  $("#contactoCalle").prop("readonly", activar);
  $("#contactoNumero").prop("readonly", activar);
  $("#contactoPiso").prop("readonly", activar);
  $("#contactoDepartamento").prop("readonly", activar);
  $("#contactoBarrio").prop("disabled", activar);
  $("#contactoLocalidad").prop("disabled", activar);
}

function cargarContactoEmergencia(contacto) {
  $("#contactoPersonaId").val(contacto.personaId);
  $("#contactoNombres").val(contacto.nombres);
  $("#contactoApellido").val(contacto.apellido);
  $("#contactoTelefono").val(contacto.telefono);
  $("#contactoTelefonoAlternativo").val(contacto.telefonoAlternativo);
  $("#contactoCalle").val(contacto.domicilio.calle);
  $("#contactoNumero").val(contacto.domicilio.numero);
  $("#contactoPiso").val(contacto.domicilio.piso);
  $("#contactoDepartamento").val(contacto.domicilio.departamento);
  $("#contactoBarrio").val(contacto.domicilio.barrio);
  $("#contactoLocalidad").val(contacto.domicilio.localidad);

  var activar = !existeContactoFamiliar(familiares);
  $("#contactoNombres").prop("readonly", activar);
  $("#contactoApellido").prop("readonly", activar);
  $("#contactoTelefono").prop("readonly", activar);
  $("#contactoTelefonoAlternativo").prop("readonly", activar);
  $("#contactoCalle").prop("readonly", activar);
  $("#contactoNumero").prop("readonly", activar);
  $("#contactoPiso").prop("readonly", activar);
  $("#contactoDepartamento").prop("readonly", activar);
  $("#contactoBarrio").prop("disabled", activar);
  $("#contactoLocalidad").prop("disabled", activar);
}

function quitarContactos(familiares) {
  $.each(familiares, function (indice, familiar) {
    if (familiar.emergencia) {
      familiar.emergencia = 0;
    }
  });
}

function listarFamiliares(familiares) {
  tablaFamiliares.clear().draw();

  $.each(familiares, function (indice, familiar) {
    var btn = '<div class="btn-group">';
    btn +=
      '<button type="button" class="btn btn-primary editarFamiliarButton" title="Editar familiar" value = "' +
      indice +
      '"><i class="fa fa-edit"></i></button>';
    btn +=
      '<button type="button" class="btn btn-danger eliminarFamiliarButton" value="' +
      indice +
      '"><i class="fa fa-trash"></i></button>';
    btn += "</div>";
    tablaFamiliares.row.add([
      familiar.documento,
      familiar.apellido,
      familiar.nombres,
      familiar.vinculoTexto,
      btn,
    ]);
  });
  tablaFamiliares.draw();
}

function existeContactoFamiliar(familiares) {
  r = false;
  $.each(familiares, function (indice, familiar) {
    if (familiar.emergencia) {
      r = true;
    }
  });

  return r;
}

function reiniciarTodo() {
  limpiarForm($("#participanteForm"));
  deshabilitarForm($("#participanteForm"));
  $("#nuevoFamiliarButton").prop("disabled", true);
  $("#documento").prop("disabled", false);
  $("#sinDocumento").prop("disabled", false);
}

function reiniciarFamiliar() {
  eraContacto = null;
  limpiarForm($("#familiarForm"));
  deshabilitarForm($("#familiarForm"));
  $("#familiarDocumento").prop("disabled", false);
  $("#sinDocumentoFamiliar").prop("disabled", false);
  $("#buscarFamiliarButton").prop("disabled", false);
}

function reiniciarContacto() {
  $("#contactoPersonaId").val("");
  $("#contactoNombres").val("");
  $("#contactoApellido").val("");
  $("#contactoTelefono").val("");
  $("#contactoTelefonoAlternativo").val("");
  $("#contactoCalle").val("");
  $("#contactoNumero").val("");
  $("#contactoPiso").val("");
  $("#contactoDepartamento").val("");
  $("#contactoBarrio").val("");
  $("#contactoLocalidad").val("");

  $("#contactoNombres").prop("readonly", false);
  $("#contactoApellido").prop("readonly", false);
  $("#contactoTelefono").prop("readonly", false);
  $("#contactoTelefonoAlternativo").prop("readonly", false);
  $("#contactoCalle").prop("readonly", false);
  $("#contactoNumero").prop("readonly", false);
  $("#contactoPiso").prop("readonly", false);
  $("#contactoDepartamento").prop("readonly", false);
  $("#contactoBarrio").prop("disabled", false);
  $("#contactoLocalidad").prop("disabled", false);
}

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
  //reiniciarTodo();
  reiniciarFamiliar();

  //Carga de los combos
  cargarComboGET($("#familiarCivil"), "base/civil");
  cargarComboGET($("#escuela"), "base/escuelas");
  cargarComboGET($("#grado"), "base/grados");
  cargarComboGET($("#programaBeneficiario"), "base/programas");
  cargarComboGET($("#sanguineo"), "base/sanguineo");
  cargarComboGET($("#discapacidad"), "base/discapacidades");
  cargarComboGET($("#familiarEscolaridad"), "base/escolaridades");
  cargarComboGET($("#familiarVinculo"), "base/vinculos");
  cargarCombosGET([$("#genero"), $("#familiarGenero")], "base/generos");
  cargarCombosGET(
    [$("#barrio"), $("#familiarBarrio"), $("#contactoBarrio")],
    "base/barrios"
  );
  cargarCombosGET(
    [$("#localidad"), $("#familiarLocalidad"), $("#contactoLocalidad")],
    "base/localidades/1"
  );

  obtenerParticipante(participanteId);

  $("#separarEditarButton").show(); //??

  //Instancia y validación del formulario de familiares
  $("#familiarForm").validate({
    submitHandler: function (form, event) {
      event.preventDefault();
      var familiar = {
        personaId: $("#familiarPersonaId").val(),
        vinculo: $("#familiarVinculo").val(),
        vinculoTexto: $("#familiarVinculo option:selected").text(),
        documento: $("#familiarDocumento").val(),
        nombres: $("#familiarNombres").val(),
        apellido: $("#familiarApellido").val(),
        genero: $("#familiarGenero").val(),
        civil: $("#familiarCivil").val(),
        nacimiento: $("#familiarNacimiento").val(),
        telefono: $("#familiarTelefono").val(),
        telefonoAlternativo: $("#familiarTelefonoAlternativo").val(),
        email: $("#familiarEmail").val(),
        domicilio: {
          calle: $("#familiarCalle").val(),
          piso: $("#familiarPiso").val(),
          numero: $("#familiarNumero").val(),
          departamento: $("#familiarDepartamento").val(),
          barrio: $("#familiarBarrio").val(),
          localidad: $("#familiarLocalidad").val(),
        },
        oficio: $("#familiarOficio").val(),
        escolaridad: $("#familiarEscolaridad").val(),
        interesaParticipar: $("#familiarInteresaParticipar").is(":checked")
          ? 1
          : 0,
        interesaEstudios: $("#familiarInteresaEstudios").is(":checked") ? 1 : 0,
        emergencia: $("#familiarEmergencia").is(":checked") ? 1 : 0,
      };

      //Si está marcado como contacto de emergencia reemplaza el que ya estaba
      if (familiar.emergencia) {
        quitarContactos(familiares);
        cargarContactoEmergencia(familiar);
      }

      //Si no tiene valor es un nuevo familiar, sino usa el indice
      if ($("#guardarFamiliarButton").val() == "") {
        familiares.push(familiar);
      } else {
        familiares[$("#guardarFamiliarButton").val()] = familiar;
        $("#guardarFamiliarButton").val("");
      }

      //Si no hay ningun familiar marcado como contacto limpia el form
      if (!existeContactoFamiliar(familiares)) {
        if (eraContacto != null && eraContacto) {
          reiniciarContacto();
        }
      }

      listarFamiliares(familiares);
      $("#familiarModal").modal("hide");
    },
    rules: {
      familiarNombres: "required",
      familiarApellido: "required",
      familiarVinculo: "required",
      familiarGenero: "required",
      familiarEmail: "email",
      familiarCalle: {
        required: {
          depends: function (element) {
            return (
              $("#familiarNumero").val() != "" ||
              $("#familiarLocalidad").val() != ""
            );
          },
        },
      },
      familiarNumero: {
        required: {
          depends: function (element) {
            return (
              $("#familiarCalle").val() != "" ||
              $("#familiarLocalidad").val() != ""
            );
          },
        },
      },
      familiarLocalidad: {
        required: {
          depends: function (element) {
            return (
              $("#familiarCalle").val() != "" ||
              $("#familiarNumero").val() != ""
            );
          },
        },
      },
      familiarTelefono: {
        required: {
          depends: function (element) {
            return $("#familiarEmergencia").is(":checked");
          },
        },
      },
    },
    messages: {
      familiarNombres: "Campo requerido",
      familiarApellido: "Campo requerido",
      familiarVinculo: "Campo requerido",
      familiarGenero: "Campo requerido",
      familiarEmail: "E-mail inválido",
      familiarCalle: { required: "Campo requerido" },
      familiarNumero: { required: "Campo requerido" },
      familiarLocalidad: { required: "Campo requerido" },
      familiarTelefono: { required: "Campo requerido" },
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });

  //Instancia de la validación y envío del form;
  $("#participanteForm").validate({
    submitHandler: function (form) {
      activarBotonCargando($("#guardarParticipanteButton"));
      $.ajax({
        type: "post",
        url: path + "participantes/editar",
        dataType: "json",
        data: {
          id: $("#id").val(),
          personaId: participante.personaId,
          documento: $("#documento").val(),
          nacimiento: $("#nacimiento").val(),
          genero: $("#genero").val(),
          nombres: $("#nombres").val(),
          apellido: $("#apellido").val(),
          domicilio: {
            calle: $("#calle").val(),
            numero: $("#numero").val(),
            piso: $("#piso").val(),
            departamento: $("#departamento").val(),
            barrio: $("#barrio").val(),
            localidad: $("#localidad").val(),
          },
          escuela: $("#escuela").val(),
          grado: $("#grado").val(),
          programa: $("#programaBeneficiario").val(),
          salud: {
            sanguineo: $("#sanguineo").val(),
            enfermedad: $("#enfermedad").val(),
            alergia: $("#alergia").val(),
            medicacion: $("#medicacion").val(),
            discapacidad: $("#discapacidad").val(),
            cud: $("#cud").val(),
            observacion: $("#observacionSalud").val(),
          },
          contactoEmergencia: {
            personaId: $("#contactoPersonaId").val(),
            documento: $("#contactoDocumento").val(),
            nombres: $("#contactoNombres").val(),
            apellido: $("#contactoApellido").val(),
            calle: $("#contactoCalle").val(),
            numero: $("#contactoNumero").val(),
            piso: $("#contactoPiso").val(),
            departamento: $("#contactoDepartamento").val(),
            barrio: $("#contactoBarrio").val(),
            localidad: $("#contactoLocalidad").val(),
            telefono: $("#contactoTelefono").val(),
            telefonoAlternativo: $("#contactoTelefonoAlternativo").val(),
          },
          familiares: familiares,
          eliminados: eliminados
        },
        success: function (resultado) {
          desactivarBotonCargando($("#guardarParticipanteButton"));
          if (resultado.exito) {
            crearToastSession("Participante modificado con éxito.");
            location = path + "participantes/listado";
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($("#guardarParticipanteButton"));
          console.log(thrownError);
          toastr.error("Error " + jqXHR.status, "Error al editar");
        },
      });
    },
    rules: {
      nombres: "required",
      apellido: "required",
      genero: "required",
      calle: {
        required: {
          depends: function (element) {
            return $("#numero").val() != "" || $("#localidad").val() != "";
          },
        },
      },
      numero: {
        required: {
          depends: function (element) {
            return $("#calle").val() != "" || $("#localidad").val() != "";
          },
        },
      },
      localidad: {
        required: {
          depends: function (element) {
            return $("#calle").val() != "" || $("#numero").val() != "";
          },
        },
      },
      contactoNombres: "required",
      contactoApellido: "required",
      contactoTelefono: "required",
      contactoCalle: {
        required: {
          depends: function (element) {
            return (
              $("#contactoNumero").val() != "" ||
              $("#contactoLocalidad").val() != ""
            );
          },
        },
      },
      contactoNumero: {
        required: {
          depends: function (element) {
            return (
              $("#contactoCalle").val() != "" ||
              $("#contactoLocalidad").val() != ""
            );
          },
        },
      },
      contactoLocalidad: {
        required: {
          depends: function (element) {
            return (
              $("#contactoCalle").val() != "" ||
              $("#contactoNumero").val() != ""
            );
          },
        },
      },
    },
    messages: {
      nombres: "Campo requerido",
      apellido: "Campo requerido",
      genero: "Campo requerido",
      calle: { required: "Campo requerido" },
      numero: { required: "Campo requerido" },
      localidad: { required: "Campo requerido" },
      contactoNombres: "Campo requerido",
      contactoApellido: "Campo requerido",
      contactoTelefono: "Campo requerido",
      contactoCalle: { required: "Campo requerido" },
      contactoNumero: { required: "Campo requerido" },
      contactoLocalidad: { required: "Campo requerido" },
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });
});

//===============================================
//					Eventos
//===============================================
$("#familiaresTable").on("click", ".eliminarFamiliarButton", function () {
  $("#confirmarEliminarFamiliarButton").val($(this).val());
  $("#eliminarFamiliarModal").modal("show");
});

$("#familiaresTable").on("click", ".editarFamiliarButton", function () {
  var familiar = familiares[$(this).val()];

  $("#familiarPersonaId").val(familiar.personaId);
  $("#familiarVinculo").val(familiar.vinculo);
  $("#familiarDocumento").val(familiar.documento);
  $("#familiarNombres").val(familiar.nombres);
  $("#familiarApellido").val(familiar.apellido);
  $("#familiarNacimiento").val(familiar.nacimiento);
  $("#familiarTelefono").val(familiar.telefono);
  $("#familiarTelefonoAlternativo").val(familiar.telefonoAlternativo);
  $("#familiarEmail").val(familiar.email);
  $("#familiarOficio").val(familiar.oficio);
  $("#familiarInteresaParticipar").prop("checked", familiar.interesaParticipar);
  $("#familiarInteresaEstudios").prop("checked", familiar.interesaEstudios);
  $("#familiarEmergencia").prop("checked", familiar.emergencia);
  eraContacto = familiar.emergencia;

  if (familiar.genero) {
    $("#familiarGenero").val(familiar.genero);
  }
  if (familiar.civil) {
    $("#familiarCivil").val(familiar.civil);
  }
  if (familiar.domicilio) {
    $("#familiarCalle").val(familiar.domicilio.calle);
    $("#familiarPiso").val(familiar.domicilio.piso);
    $("#familiarNumero").val(familiar.domicilio.numero);
    $("#familiarDepartamento").val(familiar.domicilio.departamento);

    if (familiar.domicilio.barrio) {
      $("#familiarBarrio").val(familiar.domicilio.barrio);
    }
    if (familiar.domicilio.localidad) {
      $("#familiarLocalidad").val(familiar.domicilio.localidad);
    }
  }
  if (familiar.escolaridad) {
    $("#familiarEscolaridad").val(familiar.escolaridad);
  }

  habilitarForm($("#familiarForm"));
  $("#buscarFamiliarButton").prop("disabled", false);
  $("#sinDocumentoFamiliar").prop("disabled", false);
  $("#guardarFamiliarButton").val($(this).val());
  $("#familiarModal").modal("show");
});

$("#confirmarEliminarFamiliarButton").click(function () {
  //guarda el contacto eliminado
  eliminados.push(familiares[$(this).val()]);

  //si es contacto de emergenica
  if (familiares[$(this).val()].emergencia) {
    reiniciarContacto();
  }

  familiares.splice($(this).val(), 1);

  listarFamiliares(familiares);

  if (!existeContactoFamiliar(familiares)) {
    reiniciarContacto();
  }

  $(this).val("");
  $("#eliminarFamiliarModal").modal("hide");
});

$("#familiarModal").on("hidden.bs.modal", function () {
  reiniciarFamiliar();
});

$("#buscarFamiliarButton").click(function () {
  $.ajax({
    type: "post",
    url: path + "participantes/persona/existe",
    dataType: "json",
    data: {
      documento: $("#familiarDocumento").val(),
    },
    success: function (resultado) {
      if (resultado.exito) {
        if (resultado.existe) {
          var persona = resultado.persona;
          $("#familiarPersonaId").val(persona.id);
          $("#familiarApellido").val(persona.apellido).prop("disabled", true);
          $("#familiarNombres").val(persona.nombres).prop("disabled", true);
          $("#familiarNacimiento")
            .val(persona.nacimiento)
            .prop("disabled", true);
          $("#familiarGenero").val(persona.genero).prop("disabled", true);
          $("#familiarCivil").val(persona.civil).prop("disabled", true);
          $("#familiarEscolaridad")
            .val(persona.escolaridad)
            .prop("disabled", true);
          $("#familiarTelefono").val(persona.telefono).prop("disabled", true);
          $("#familiarTelefonoAlternativo")
            .val(persona.telefonoAlternativo)
            .prop("disabled", true);
          $("#familiarEscolaridad")
            .val(persona.escolaridad)
            .prop("disabled", true);

          $("#familiarCalle")
            .val(persona.domicilio.calle)
            .prop("disabled", true);
          $("#familiarNumero")
            .val(persona.domicilio.numero)
            .prop("disabled", true);
          $("#familiarPiso").val(persona.domicilio.piso).prop("disabled", true);
          $("#familiarDepartamento")
            .val(persona.domicilio.departamento)
            .prop("disabled", true);
          $("#familiarBarrio")
            .val(persona.domicilio.barrio)
            .prop("disabled", true);
          $("#familiarLocalidad")
            .val(persona.domicilio.localidad)
            .prop("disabled", true);

          $("#familiarVinculo").prop("disabled", false);
          $("#familiarOficio").prop("disabled", false);
          $("#familiarInteresaParticipar").prop("disabled", false);
          $("#familiarInteresaEstudios").prop("disabled", false);
          $("#familiarEmergencia").prop("disabled", false);
        } else {
          habilitarForm($("#familiarForm"));
        }
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al buscar");
    },
  });
});

$("#familiarDocumento").keyup(function () {
  if (
    $("#familiarPersonaId").val() != "" &&
    !$("#sinDocumentoFamiliar").prop("disabled")
  ) {
    reiniciarFamiliar();
  }
});

$("#sinDocumento").change(function () {
  let sinDocEstado = $("#sinDocumento").prop("checked");
  reiniciarTodo();
  $("#sinDocumento").prop("checked", sinDocEstado);
  if ($("#sinDocumento").prop("checked")) {
    habilitarForm($("#participanteForm"));
    $("#nuevoFamiliarButton").prop("disabled", false);
    reiniciarContacto();
    $("#documento").prop("disabled", true);
  }
});

$("#sinDocumentoFamiliar").change(function () {
  let sinDocEstado = $("#sinDocumentoFamiliar").prop("checked");
  reiniciarFamiliar();
  $("#sinDocumentoFamiliar").prop("checked", sinDocEstado);
  if ($("#sinDocumentoFamiliar").prop("checked")) {
    habilitarForm($("#familiarForm"));
    $("#familiarDocumento").prop("disabled", true);
  }
});
