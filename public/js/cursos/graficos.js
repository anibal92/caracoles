//===============================================
//					Variables
//===============================================
var participantesData = {
	labels: [],
	datasets: [{
		label: 'Total',
		backgroundColor: 'rgb(255, 205, 86)',
		borderColor: 'rgb(255, 205, 86)',
		borderWidth: 1,
		data: []
	},
	]
};

var participantesGrafico = null;

//===============================================
//					Funciones
//===============================================

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
	
	participantesGrafico = new Chart($('#participantesCanvas'), {
		type: 'horizontalBar',
		data: participantesData,
		options: {
			responsive: true,
			legend: {
				display: false,
			},
			title: {
				display: false,
				text: 'Cantidad por curso'
			},
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});

	$.ajax({
		type: 'post',
		url: path + 'cursos/graficos',
		dataType: 'json',
		data: {
		},
		success: function (resultado) {
			if (resultado.exito) {
				$('.loader').hide();
				$('#contenido').show();
				participantesData.labels = resultado.cursos;
				participantesData.datasets[0].data = resultado.totales;
				participantesGrafico.update();
			}
			else {
				errorServidor(resultado.error);
			}
		},
		error: function (jqXHR, textStatus, thrownError) {
			console.log(thrownError);
			toastr.error('Error ' + jqXHR.status, 'Error al obtener los datos.');
		},
	});
});

//===============================================
//					Eventos
//===============================================
