//===============================================
//					Variables
//===============================================
var cursos;
var participantes;
var participanteAgregar;
const date = new Date();
let fechaActual =
  date.getDate() +
  "-" +
  (date.getMonth() + 1) +
  "-" +
  date.getFullYear() +
  " " +
  date.getHours() +
  ":" +
  date.getMinutes() +
  ":" +
  date.getSeconds();

var tablaCursos = $("#cursosTable").DataTable({
  bLengthChange: false,
  info: false,
  language: {
    loadingRecords: '<div class="loader centrado"></div>',
    emptyTable: "No hay resultados.",
    paginate: {
      next: "Siguiente",
      previous: "Anterior",
    },
    search: "Buscar:",
  },
  responsive: true,
  dom: "Bftp",
  buttons: [
    {
      extend: "excelHtml5",
      text: '<i class="fa fa-file-excel-o"></i>',
      titleAttr: "Exportar a Excel",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2, 3],
      },
      title: "Listado de cursos - CURSOS",
      filename: "Listado de cursos - CURSOS" + " " + fechaActual,
    },
    {
      extend: "pdfHtml5",
      text: '<i class="fa fa-file-pdf-o"></i> ',
      titleAttr: "Exportar a Pdf",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2, 3],
      },
      title: "   ",
      filename: "Listado de cursos" + " " + fechaActual,
      customize: function (doc) {
        doc.content[1].table.widths = ["25%", "25%", "25%", "25%"];
        (doc.download = "open"), (doc.pageMargins = [30, 80, 20, 30]);
        doc.styles.tableHeader = {
          fontSize: "12",
          alignment: "left",
          fillColor: "#545454",
          color: "#FFFFFF",
        };

        doc.content.splice(1, 0, {
          alignment: "left",
          text: [
            "\n",
            "\n",
            "\n",
            {
              text: "Fecha: " + fechaActual,
              fontSize: 12,
              bold: false,
            },
            "\n",
            "\n",
          ],
        });

        doc["header"] = function () {
          return {
            columns: [
              {
                margin: [30, 10, 2, 5],
                alignment: "left",
                image: logoBase64,
                width: 100,
              },
              {
                margin: [0, 30, 100, 0],
                alignment: "right",
                text: "Listado de cursos", //TITULO DEL INFORME
                fontSize: 20,
                bold: false,
              },
            ],
          };
        };

        doc["footer"] = function (page, pages) {
          return [
            //para vertical x2: 580 ---- para horizontal x2:820
            {
              canvas: [
                { type: "line", x1: 20, y1: 6, x2: 580, y2: 6, lineWidth: 1 },
              ],
            },
            {
              columns: [
                {
                  text: "Centro Educativo Caracoles",
                  fontSize: 9,
                  bold: true,
                },
                {
                  alignment: "right",
                  text: [
                    {
                      text:
                        "Página " + page.toString() + " de " + pages.toString(),
                      fontSize: 9,
                      bold: true,
                    },
                  ],
                },
              ],
              margin: [20, 0],
            },
          ];
        };
      },
    },
  ],
  ordering: true,
  searching: true,
  order: [[0, "desc"]],
  ajax: {
    type: "post",
    url: path + "cursos/listado",
    dataType: "json",
    dataSrc: function (resultado) {
      if (resultado.exito) {
        cursos = resultado.cursos;
        $.each(resultado.cursos, function (indice, curso) {
          curso.btn = '<div class="btn-group">';
          curso.btn +=
            '<button type="button" class="btn btn-primary verCursoButton" title="Ver curso" value = "' +
            indice +
            '"><i class="fa fa-eye"></i></button>';
          curso.btn +=
            '<button type="button" class="btn btn-primary asignarParticipantesButton" title="Asignar participantes" value = "' +
            curso.id +
            '"><i class="fa fa-users"></i></button>';
          curso.btn +=
            '<button type="button" class="btn btn-danger eliminarCursoButton" value="' +
            curso.id +
            '"><i class="fa fa-trash"></i></button>';
          curso.btn += "</div>";
        });

        return resultado.cursos;
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al listar");
    },
  },
  columnDefs: [{ width: "12%", targets: [4] }],
  columns: [
    { data: "nombre" },
    { data: "horarioStr" },
    { data: "educador" },
    { data: "inscriptos" },
    { data: "btn" },
  ],
});

var tablaParticipantes = $("#participantesTable").DataTable({
  bLengthChange: false,
  info: false,
  language: {
    loadingRecords: '<div class="loader centrado"></div>',
    emptyTable: "No hay resultados.",
    paginate: {
      next: "Siguiente",
      previous: "Anterior",
    },
    search: "Buscar:",
  },
  responsive: true,
  dom: "Bftp",
  buttons: [
    {
      extend: "excelHtml5",
      text: '<i class="fa fa-file-excel-o"></i>',
      titleAttr: "Exportar a Excel",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2],
      },
      title: "Listado de integrantes del curso - CURSOS",
      filename:
        "Listado de integrantes del curso - CURSOS" + " " + fechaActual,
    },
    {
      extend: "pdfHtml5",
      text: '<i class="fa fa-file-pdf-o"></i> ',
      titleAttr: "Exportar a Pdf",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2],
      },
      title: "   ",
      filename:
        "Listado de integrantes del curso" + " " + fechaActual,
        customize: function (doc) {
          doc.content[1].table.widths = ["33%", "33%", "33%", "33%"];
          (doc.download = "open"), (doc.pageMargins = [30, 80, 20, 30]);
          doc.styles.tableHeader = {
            fontSize: "12",
            alignment: "left",
            fillColor: "#545454",
            color: "#FFFFFF",
          };
  
          doc.content.splice(1, 0, {
            alignment: "left",
            text: [
              "\n",
              "\n",
              "\n",
              {
                text: "Fecha: " + fechaActual,
                fontSize: 12,
                bold: false,
              },
              "\n",
              "\n",
            ],
          });
  
          doc["header"] = function () {
            return {
              columns: [
                {
                  margin: [30, 10, 2, 5],
                  alignment: "left",
                  image: logoBase64,
                  width: 100,
                },
                {
                  margin: [0, 30, 100, 0],
                  alignment: "right",
                  text: 'Listado de participantes', //TITULO DEL INFORME
                  fontSize: 20,
                  bold: false,
                },
              ],
            };
          };
  
          doc["footer"] = function (page, pages) {
            return [
              //para vertical x2: 580 ---- para horizontal x2:820
              {
                canvas: [
                  { type: "line", x1: 20, y1: 6, x2: 580, y2: 6, lineWidth: 1 },
                ],
              },
              {
                columns: [
                  {
                    text: "Centro Educativo Caracoles",
                    fontSize: 9,
                    bold: true,
                  },
                  {
                    alignment: "right",
                    text: [
                      {
                        text:
                          "Página " + page.toString() + " de " + pages.toString(),
                        fontSize: 9,
                        bold: true,
                      },
                    ],
                  },
                ],
                margin: [20, 0],
              },
            ];
          };
        },
    },
  ],
  ordering: true,
  searching: false,
  order: [[0, "desc"]],
});

$.ajax({
  type: "post",
  url: "../participantes/autocomplete",
  dataType: "json",
  success: function (resultado) {
    if (resultado.exito) {
      $("#participante").autocomplete({
        minLength: 3,
        source: resultado.participantes,
        select: function (event, ui) {
          $("#participante").val(ui.item.label);
          participanteAgregar = ui.item;
          return false;
        },
        change: function (event, ui) {
          //$("#usuarioId").val(ui.item ? ui.item.value : ''); --> esto creo que no va mas
          participanteAgregar = ui.item ? ui.item : null;
        },
        appendTo: "#participantesModal",
      });
    } else {
      console.log(respuesta.error.descripcion);
      toastr.error(respuesta.error.mensaje);
    }
  },
});

//===============================================
//					Funciones
//===============================================
$.validator.addMethod(
  "time24",
  function (value, element) {
    if (value == "") {
      return true;
    }
    return /^([01]\d|2[0-3]):?([0-5]\d)$/.test(value);
  },
  "Hora inválida."
);

function participantesPorCurso(cursoId) {
  $.ajax({
    type: "post",
    url: path + "cursos/participantes",
    dataType: "json",
    data: {
      cursoId: cursoId,
    },
    success: function (resultado) {
      if (resultado.exito) {
        tablaParticipantes.clear().draw();
        participantes = resultado.participantes;

        $.each(resultado.participantes, function (indice, participante) {
          var btn = '<div class="btn-group">';
          btn +=
            '<button type="button" class="btn btn-danger quitarParticipanteButton" value="' +
            participante.id +
            '"><i class="fa fa-trash"></i></button>';
          btn += "</div>";

          tablaParticipantes.row.add([
            participante.documento,
            participante.apellido,
            participante.nombres,
            btn,
          ]);
        }); 

        tablaParticipantes.draw();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al listar");
    },
  });
}

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
  $(".clockpicker").clockpicker({
    donetext: "Hecho",
    default: "now",
    autoclose: "true",
  });

  //Carga de los combos
  cargarComboGET($("#educador"), "base/educadores");

  //Instancia de la validación y envío del seguimiento;
  $("#cursoForm").validate({
    submitHandler: function (form) {
      activarBotonCargando($("#guardarCursoButton"));

      var url = path + "cursos/alta";
      var data = {
        nombre: $("#nombre").val(),
        dia: $("#dia").val(),
        horaInicio: $("#horaInicio").val(),
        horaFin: $("#horaFin").val(),
        educador: $("#educador").val(),
      };

      if ($("#guardarCursoButton").val() != "") {
        url = path + "cursos/editar";
        data.id = $("#guardarCursoButton").val();
      }

      $.ajax({
        type: "post",
        url: url,
        dataType: "json",
        data: data,
        success: function (resultado) {
          desactivarBotonCargando($("#guardarCursoButton"));
          if (resultado.exito) {
            tablaCursos.ajax.reload();
            toastr.success("Curso guardado con éxito.");
            $("#cursoModal").modal("hide");
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($("#guardarCursoButton"));
          console.log(thrownError);
          toastr.error("Error " + jqXHR.status, "Error guardar el curso");
        },
      });
    },
    rules: {
      nombre: "required",
      horaInicio: {
        required: true,
        time24: true,
      },
      horaFin: "time24",
      educador: "required",
    },
    messages: {
      nombre: "Campo requerido",
      horaInicio: {
        required: "Campo requerido",
        time24: "Hora inválida",
      },
      horaFin: "Hora inválida",
      educador: "Campo requerido",
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });
});

//===============================================
//					Eventos
//===============================================
$("#cursosTable").on("click", ".verCursoButton", function () {
  var curso = cursos[$(this).val()];

  $("#nombre").val(curso.nombre); //.prop('disabled', true);
  $("#dia").val(curso.horarios[0].dia); //.prop('disabled', true);
  $("#horaInicio").val(curso.horarios[0].hora_inicio); //.prop('disabled', true);
  $("#horaFin").val(curso.horarios[0].hora_fin); //.prop('disabled', true);
  $("#educador").val(curso.educadorId); //.prop('disabled', true);
  $("#guardarCursoButton").val(curso.id); //.prop('disabled', true);

  $("#cursoModal").modal("show");
});

$("#cursosTable").on("click", ".asignarParticipantesButton", function () {
  participantesPorCurso($(this).val());
  $("#cursoId").val($(this).val());
  $("#participantesModal").modal("show");
});

$("#cursosTable").on("click", ".eliminarCursoButton", function () {
  $("#confirmarEliminarCursoButton").val($(this).val());
  $("#eliminarCursoModal").modal("show");
});

$("#participantesTable").on("click", ".quitarParticipanteButton", function () {
  $("#confirmarQuitarParticipanteButton").val($(this).val());
  $("#quitarParticipanteModal").modal("show");
});

$("#confirmarEliminarCursoButton").click(function () {
  activarBotonCargando($(this));
  $.ajax({
    type: "post",
    url: path + "cursos/eliminar",
    dataType: "json",
    data: {
      id: $(this).val(),
    },
    success: function (resultado) {
      desactivarBotonCargando($("#confirmarEliminarCursoButton"));
      $(this).val("");
      $("#eliminarCursoModal").modal("hide");

      if (resultado.exito) {
        toastr.success("Elemento eliminado.");
        tablaCursos.ajax.reload();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      $(this).val("");
      $("#eliminarCursoModal").modal("hide");
      desactivarBotonCargando($("#confirmarEliminarCursoButton"));
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al eliminar");
    },
  });
});

$("#confirmarQuitarParticipanteButton").click(function () {
  activarBotonCargando($(this));
  $.ajax({
    type: "post",
    url: path + "cursos/participantes/quitar",
    dataType: "json",
    data: {
      participanteId: $(this).val(),
      cursoId: $("#cursoId").val(),
    },
    success: function (resultado) {
      desactivarBotonCargando($("#confirmarQuitarParticipanteButton"));
      $(this).val("");
      $("#quitarParticipanteModal").modal("hide");

      if (resultado.exito) {
        toastr.success("Elemento eliminado.");
        participantesPorCurso($("#cursoId").val());
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      $(this).val("");
      $("#quitarParticipanteModal").modal("hide");
      desactivarBotonCargando($("#confirmarQuitarParticipanteButton"));
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al eliminar");
    },
  });
});

$("#agregarParticipanteButton").click(function () {
  if (participanteAgregar == null) {
    toastr.error("Seleccione un participante");
    return;
  }

  $("#agregarParticipanteButton").prop("disabled", true);

  $.ajax({
    type: "post",
    url: path + "cursos/participantes/agregar",
    dataType: "json",
    data: {
      participanteId: participanteAgregar.value,
      cursoId: $("#cursoId").val(),
    },
    success: function (respuesta) {
      if (respuesta.exito) {
        participantesPorCurso($("#cursoId").val());
        toastr.success("Participante agregado");
      } else {
        console.log(respuesta.error.descripcion);
        toastr.error(respuesta.error.mensaje);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al agregar");
    },
    complete: function (jqXHR, textStatus, thrownError) {
      $("#participante").val("");
      $("#agregarParticipanteButton").prop("disabled", false);
      participanteAgregar = null;
    },
  });
});

$("#cursoModal").on("hidden.bs.modal", function () {
  $("#nombre").val("").prop("disabled", false);
  $("#dia").val("L").prop("disabled", false);
  $("#horaInicio").val("").prop("disabled", false);
  $("#horaFin").val("").prop("disabled", false);
  $("#educador").val("").prop("disabled", false);
  $("#guardarCursoButton").val("").prop("disabled", false);
});

$("#participantesModal").on("hidden.bs.modal", function () {
  tablaParticipantes.clear().draw();
  $("#participante").val("");
  $("#cursoId").val("");
});
