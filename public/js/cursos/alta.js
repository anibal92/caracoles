//===============================================
//					Variables
//===============================================
var familiares = [];

//===============================================
//					Funciones
//===============================================
function listarFamiliares(familiares) {
	tablaFamiliares.clear().draw();

	$.each(familiares, function (indice, familiar) {
		var btn = '<div class="btn-group">';
		btn += '<button type="button" class="btn btn-primary editarFamiliarButton" value = "' + indice + '"><i class="fa fa-edit"></i></button>';
		btn += '<button type="button" class="btn btn-danger eliminarFamiliarButton" value="' + indice + '"><i class="fa fa-trash"></i></button>';
		btn += '</div>';
		tablaFamiliares.row.add([
			familiar.documento,
			familiar.apellido,
			familiar.nombres,
			familiar.vinculoTexto,
			btn
		]);
	});
	tablaFamiliares.draw();
}

function cargarContactoEmergencia(contacto) {
	$('#contactoDocumento').val(contacto.documento);
	$('#contactoNombres').val(contacto.nombres);
	$('#contactoApellido').val(contacto.apellido);
	$('#contactoTelefono').val(contacto.telefono);
	$('#contactoTelefonoAlternativo').val(contacto.telefonoAlternativo);
	$('#contactoCalle').val(contacto.domicilio.calle);
	$('#contactoNumero').val(contacto.domicilio.numero);
	$('#contactoPiso').val(contacto.domicilio.piso);
	$('#contactoDepartamento').val(contacto.domicilio.departamento);
	$('#contactoBarrio').val(contacto.domicilio.barrio);
	$('#contactoLocalidad').val(contacto.domicilio.localidad);

	$('#sinDocumentoContacto').prop('readonly', true);
	$('#buscarContactoButton').prop('readonly', true);
	$('#contactoDocumento').prop('readonly', true);
	$('#contactoNombres').prop('readonly', true);
	$('#contactoApellido').prop('readonly', true);
	$('#contactoTelefono').prop('readonly', true);
	$('#contactoTelefonoAlternativo').prop('readonly', true);
	$('#contactoCalle').prop('readonly', true);
	$('#contactoNumero').prop('readonly', true);
	$('#contactoPiso').prop('readonly', true);
	$('#contactoDepartamento').prop('readonly', true);
	$('#contactoBarrio').prop('disabled', true);
	$('#contactoLocalidad').prop('disabled', true);
}

function quitarContactos(familiares) {
	$.each(familiares, function (indice, familiar) {
		if (familiar.emergencia) {
			familiar.emergencia = 0;
		}
	});
}

function existeContactoFamiliar(familiares) {
	r = false;
	$.each(familiares, function (indice, familiar) {
		if (familiar.emergencia) {
			r = true;
		}
	});

	return r;
}

function reiniciarTodo() {
	limpiarForm($('#participanteForm'));
	deshabilitarForm($('#participanteForm'));
	$('#nuevoFamiliarButton').prop('disabled', true);
	$('#documento').prop('disabled', false);
	$('#sinDocumento').prop('disabled', false);
}

function reiniciarFamiliar() {
	limpiarForm($('#familiarForm'));
	deshabilitarForm($('#familiarForm'));
	$('#familiarDocumento').prop('disabled', false);
	$('#sinDocumentoFamiliar').prop('disabled', false);
}

function reiniciarContacto() {
	$('#contactoPersonaId').val('');
	$('#contactoDocumento').val('');
	$('#contactoNombres').val('');
	$('#contactoApellido').val('');
	$('#contactoTelefono').val('');
	$('#contactoTelefonoAlternativo').val('');
	$('#contactoCalle').val('');
	$('#contactoNumero').val('');
	$('#contactoPiso').val('');
	$('#contactoDepartamento').val('');
	$('#contactoBarrio').val('');
	$('#contactoLocalidad').val('');

	$('#sinDocumentoContacto').attr('checked', false);

	$('#contactoDocumento').prop('readonly', true);
	$('#contactoNombres').prop('readonly', true);
	$('#contactoApellido').prop('readonly', true);
	$('#contactoTelefono').prop('readonly', true);
	$('#contactoTelefonoAlternativo').prop('readonly', true);
	$('#contactoCalle').prop('readonly', true);
	$('#contactoNumero').prop('readonly', true);
	$('#contactoPiso').prop('readonly', true);
	$('#contactoDepartamento').prop('readonly', true);
	$('#contactoBarrio').prop('disabled', true);
	$('#contactoLocalidad').prop('disabled', true);

	$('#contactoDocumento').prop('readonly', false);
	$('#sinDocumentoContacto').prop('readonly', false);
}

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
	reiniciarTodo();
	reiniciarFamiliar();

	//Carga de los combos
	cargarComboGET($('#familiarCivil'), 'base/civil');
	cargarComboGET($('#escuela'), 'base/escuelas');
	cargarComboGET($('#grado'), 'base/grados');
	cargarComboGET($('#programaBeneficiario'), 'base/programas');
	cargarComboGET($('#sanguineo'), 'base/sanguineo');
	cargarComboGET($('#discapacidad'), 'base/discapacidades');
	cargarComboGET($('#familiarEscolaridad'), 'base/escolaridades');
	cargarComboGET($('#familiarVinculo'), 'base/vinculos');
	cargarCombosGET([$('#genero'), $('#familiarGenero')], 'base/generos');
	cargarCombosGET([$('#barrio'), $('#familiarBarrio'), $('#contactoBarrio')], 'base/barrios');
	cargarCombosGET([$('#localidad'), $('#familiarLocalidad'), $('#contactoLocalidad')], 'base/localidades/1');
	

	//Instancia la Datatable
	tablaFamiliares = $('#familiaresTable').DataTable({
		bLengthChange: false,
		info: false,
		language: {
			emptyTable: 'No hay resultados.',
			paginate: {
				next: 'Siguiente',
				previous: 'Anterior',
			}
		},
		responsive: true,
		ordering: false,
		searching: false,
	});

	//Instancia y validación del formulario de familiares
	$('#familiarForm').validate({
		submitHandler: function (form, event) {
			event.preventDefault();
			var familiar = {
				personaId: $('#familiarPersonaId').val(),
				vinculo: $('#familiarVinculo').val(),
				vinculoTexto: $('#familiarVinculo option:selected').text(),
				documento: $('#familiarDocumento').val(),
				nombres: $('#familiarNombres').val(),
				apellido: $('#familiarApellido').val(),
				genero: $('#familiarGenero').val(),
				civil: $('#familiarCivil').val(),
				nacimiento: $('#familiarNacimiento').val(),
				telefono: $('#familiarTelefono').val(),
				telefonoAlternativo: $('#familiarTelefonoAlternativo').val(),
				email: $('#familiarEmail').val(),
				domicilio: {
					calle: $('#familiarCalle').val(),
					piso: $('#familiarPiso').val(),
					numero: $('#familiarNumero').val(),
					departamento: $('#familiarDepartamento').val(),
					barrio: $('#familiarBarrio').val(),
					localidad: $('#familiarLocalidad').val(),
				},
				oficio: $('#familiarOficio').val(),
				escolaridad: $('#familiarEscolaridad').val(),
				interesaParticipar: $('#familiarInteresaParticipar').is(':checked') ? 1 : 0,
				interesaEstudios: $('#familiarInteresaEstudios').is(':checked') ? 1 : 0,
				emergencia: $('#familiarEmergencia').is(':checked') ? 1 : 0,
			}

			//Si está marcado como contacto de emergencia reemplaza el que ya estaba
			if (familiar.emergencia) {
				quitarContactos(familiares);
				cargarContactoEmergencia(familiar);
			}

			//Si no tiene valor es un nuevo familiar, sino usa el indice
			if ($('#guardarFamiliarButton').val() == '') {
				familiares.push(familiar);
			}
			else {
				familiares[$('#guardarFamiliarButton').val()] = familiar;
				$('#guardarFamiliarButton').val('')
			}

			//Si no hay ningun familiar marcado como contacto limpia el form
			if (!existeContactoFamiliar(familiares)) {
				if ($('#contactoDocumento').val() == null && !$('#sinDocumentoContacto').prop('checked')) {
					reiniciarContacto();
				}
			}

			listarFamiliares(familiares);
			$('#familiarModal').modal('hide');
		},
		rules: {
			familiarNombres: "required",
			familiarApellido: "required",
			familiarVinculo: "required",
			familiarGenero: "required",
			familiarEmail: "email",
			familiarCalle: {
				required: {
					depends: function (element) {
						return ($('#familiarNumero').val() != "" || $('#familiarLocalidad').val() != "");
					}
				}
			},
			familiarNumero: {
				required: {
					depends: function (element) {
						return ($('#familiarCalle').val() != "" || $('#familiarLocalidad').val() != "");
					}
				}
			},
			familiarLocalidad: {
				required: {
					depends: function (element) {
						return ($('#familiarCalle').val() != "" || $('#familiarNumero').val() != "");
					}
				}
			},
			familiarTelefono: {
				required: {
					depends: function (element) {
						return $('#familiarEmergencia').is(':checked');
					}
				}
			},
		},
		messages: {
			familiarNombres: "Campo requerido",
			familiarApellido: "Campo requerido",
			familiarVinculo: "Campo requerido",
			familiarGenero: "Campo requerido",
			familiarEmail: "E-mail inválido",
			familiarCalle: { required: "Campo requerido" },
			familiarNumero: { required: "Campo requerido" },
			familiarLocalidad: { required: "Campo requerido" },
			familiarTelefono: { required: "Campo requerido" },
		},
		errorElement: 'span',
		errorClass: 'help-block',
		highlight: function (element) {
			$(element).parent().addClass('has-error');
		},
		unhighlight: function (element) {
			$(element).parent().removeClass('has-error');
		},
		invalidHandler: function (event, validator) {
			toastr.error('Compruebe los campos');
		},
	});

	//Instancia de la validación y envío del form;
	$('#participanteForm').validate({
		submitHandler: function (form) {
			activarBotonCargando($('#guardarParticipanteButton'));
			$.ajax({
				type: 'post',
				url: path + 'participantes/alta',
				dataType: 'json',
				data: {
					personaId: $('#personaId').val(),
					documento: $('#documento').val(),
					nacimiento: $('#nacimiento').val(),
					genero: $('#genero').val(),
					nombres: $('#nombres').val(),
					apellido: $('#apellido').val(),
					domicilio: {
						calle: $('#calle').val(),
						numero: $('#numero').val(),
						piso: $('#piso').val(),
						departamento: $('#departamento').val(),
						barrio: $('#barrio').val(),
						localidad: $('#localidad').val()
					},
					escuela: $('#escuela').val(),
					grado: $('#grado').val(),
					programa: $('#programaBeneficiario').val(),
					salud: {
						sanguineo: $('#sanguineo').val(),
						enfermedad: $('#enfermedad').val(),
						alergia: $('#alergia').val(),
						medicacion: $('#medicacion').val(),
						discapacidad: $('#discapacidad').val(),
						cud: $('#cud').val(),
						observacion: $('#observacionSalud').val()
					},
					contactoEmergencia: {
						personaId: $('#contactoPersonaId').val(),
						documento: $('#contactoDocumento').val(),
						nombres: $('#contactoNombres').val(),
						apellido: $('#contactoApellido').val(),
						calle: $('#contactoCalle').val(),
						numero: $('#contactoNumero').val(),
						piso: $('#contactoPiso').val(),
						departamento: $('#contactoDepartamento').val(),
						barrio: $('#contactoBarrio').val(),
						localidad: $('#contactoLocalidad').val(),
						telefono: $('#contactoTelefono').val(),
						telefonoAlternativo: $('#contactoTelefonoAlternativo').val(),

					},
					familiares: familiares,
				},
				success: function (resultado) {
					desactivarBotonCargando($('#guardarParticipanteButton'));
					if (resultado.exito) {
						crearToastSession('Participante creado con éxito.');
						location = path + 'participantes/listado';
					}
					else {
						errorServidor(resultado.error);
					}
				},
				error: function (jqXHR, textStatus, thrownError) {
					desactivarBotonCargando($('#guardarParticipanteButton'));
					console.log(thrownError);
					toastr.error('Error ' + jqXHR.status, 'Error al dar de alta');
				},
			});
		},
		rules: {
			nombres: "required",
			apellido: "required",
			genero: "required",
			calle: {
				required: {
					depends: function (element) {
						return ($('#numero').val() != "" || $('#localidad').val() != "");
					}
				}
			},
			numero: {
				required: {
					depends: function (element) {
						return ($('#calle').val() != "" || $('#localidad').val() != "");
					}
				}
			},
			localidad: {
				required: {
					depends: function (element) {
						return ($('#calle').val() != "" || $('#numero').val() != "");
					}
				}
			},
			contactoNombres: "required",
			contactoApellido: "required",
			contactoTelefono: "required",
			contactoCalle: {
				required: {
					depends: function (element) {
						return ($('#contactoNumero').val() != "" || $('#contactoLocalidad').val() != "");
					}
				}
			},
			contactoNumero: {
				required: {
					depends: function (element) {
						return ($('#contactoCalle').val() != "" || $('#contactoLocalidad').val() != "");
					}
				}
			},
			contactoLocalidad: {
				required: {
					depends: function (element) {
						return ($('#contactoCalle').val() != "" || $('#contactoNumero').val() != "");
					}
				}
			},
		},
		messages: {
			nombres: "Campo requerido",
			apellido: "Campo requerido",
			genero: "Campo requerido",
			calle: { required: "Campo requerido" },
			numero: { required: "Campo requerido" },
			localidad: { required: "Campo requerido" },
			contactoNombres: "Campo requerido",
			contactoApellido: "Campo requerido",
			contactoTelefono: "Campo requerido",
			contactoCalle: { required: "Campo requerido" },
			contactoNumero: { required: "Campo requerido" },
			contactoLocalidad: { required: "Campo requerido" },
		},
		errorElement: 'span',
		errorClass: 'help-block',
		highlight: function (element) {
			$(element).parent().addClass('has-error');
		},
		unhighlight: function (element) {
			$(element).parent().removeClass('has-error');
		},
		invalidHandler: function (event, validator) {
			toastr.error('Compruebe los campos');
		},
	});
});

//===============================================
//					Eventos
//===============================================
$('#familiaresTable').on('click', '.eliminarFamiliarButton', function () {
	$('#confirmarEliminarFamiliarButton').val($(this).val());
	$('#eliminarFamiliarModal').modal('show');
});

$('#familiaresTable').on('click', '.editarFamiliarButton', function () {
	var familiar = familiares[$(this).val()];

	$('#familiarPersonaId').val(familiar.personaId);
	$('#familiarVinculo').val(familiar.vinculo);
	$('#familiarDocumento').val(familiar.documento);
	$('#familiarNombres').val(familiar.nombres);
	$('#familiarApellido').val(familiar.apellido);
	$('#familiarNacimiento').val(familiar.nacimiento);
	$('#familiarTelefono').val(familiar.telefono);
	$('#familiarTelefonoAlternativo').val(familiar.telefonoAlternativo);
	$('#familiarEmail').val(familiar.email);
	$('#familiarCalle').val(familiar.domicilio.calle);
	$('#familiarPiso').val(familiar.domicilio.piso);
	$('#familiarNumero').val(familiar.domicilio.numero);
	$('#familiarDepartamento').val(familiar.domicilio.departamento);
	$('#familiarOficio').val(familiar.oficio);
	$('#familiarInteresaParticipar').prop('checked', familiar.interesaParticipar);
	$('#familiarInteresaEstudios').prop('checked', familiar.interesaEstudios);
	$('#familiarEmergencia').prop('checked', familiar.emergencia);

	if (familiar.genero) {
		$('#familiarGenero').val(familiar.genero);
	}
	if (familiar.civil) {
		$('#familiarCivil').val(familiar.civil);
	}
	if (familiar.escolaridad) {
		$('#familiarEscolaridad').val(familiar.escolaridad);
	}
	if (familiar.domicilio.barrio) {
		$('#familiarBarrio').val(familiar.domicilio.barrio);
	}
	if (familiar.domicilio.localidad) {
		$('#familiarLocalidad').val(familiar.domicilio.localidad);
	}

	if (familiar.personaId) {
		$('#familiarVinculo').prop('disabled', false);
		$('#familiarOficio').prop('disabled', false);
		$('#familiarInteresaParticipar').prop('disabled', false);
		$('#familiarInteresEstudios').prop('disabled', false);
		$('#familiarEmergencia').prop('disabled', false);
	}
	else {
		habilitarForm($('#familiarForm'));
	}

	$('#guardarFamiliarButton').val($(this).val());
	$('#familiarModal').modal('show');
});

$('#confirmarEliminarFamiliarButton').click(function () {
	familiares.splice($(this).val(), 1);

	listarFamiliares(familiares);

	if (!existeContactoFamiliar(familiares)) {
		reiniciarContacto();
	}

	$(this).val('');
	$('#eliminarFamiliarModal').modal('hide');
});

$('#familiarModal').on('hidden.bs.modal', function () {
	//limpiarForm($('#familiarForm'));
	reiniciarFamiliar();
});

$('#buscarParticipanteButton').click(function () {
	$.ajax({
		type: 'post',
		url: path + 'participantes/existe',
		dataType: 'json',
		data: {
			documento: $('#documento').val()
		},
		success: function (resultado) {
			if (resultado.exito) {
				if (resultado.existe) {
					toastr.warning('Ya existe el participante');
					console.log('Existe participante');
				}
				else {
					habilitarForm($('#participanteForm'));
					reiniciarContacto();
					$('#nuevoFamiliarButton').prop('disabled', false);

					if (resultado.persona) {
						var persona = resultado.persona;
						$('#personaId').val(persona.id);
						$('#apellido').val(persona.apellido).prop('disabled', true);
						$('#nombres').val(persona.nombres).prop('disabled', true);
						$('#nacimiento').val(persona.nacimiento).prop('disabled', true);
						$('#genero').val(persona.genero).prop('disabled', true);
						$('#calle').val(persona.domicilio.calle).prop('disabled', true);
						$('#numero').val(persona.domicilio.numero).prop('disabled', true);
						$('#piso').val(persona.domicilio.piso).prop('disabled', true);
						$('#departamento').val(persona.domicilio.departamento).prop('disabled', true);
						$('#barrio').val(persona.domicilio.barrio).prop('disabled', true);
						$('#localidad').val(persona.domicilio.localidad).prop('disabled', true);
					} else {
						toastr.warning('Es posible que existan personas registradas sin documento.');
					}
				}
			}
			else {
				errorServidor(resultado.error);
			}
		},
		error: function (jqXHR, textStatus, thrownError) {
			console.log(thrownError);
			toastr.error('Error ' + jqXHR.status, 'Error al buscar');
		},
	});
});

$('#buscarFamiliarButton').click(function () {
	$.ajax({
		type: 'post',
		url: path + 'participantes/persona/existe',
		dataType: 'json',
		data: {
			documento: $('#familiarDocumento').val()
		},
		success: function (resultado) {
			if (resultado.exito) {
				if (resultado.existe) {
					var persona = resultado.persona;
					$('#familiarPersonaId').val(persona.id);
					$('#familiarApellido').val(persona.apellido).prop('disabled', true);
					$('#familiarNombres').val(persona.nombres).prop('disabled', true);
					$('#familiarNacimiento').val(persona.nacimiento).prop('disabled', true);
					$('#familiarGenero').val(persona.genero).prop('disabled', true);
					$('#familiarCivil').val(persona.civil).prop('disabled', true);
					$('#familiarEscolaridad').val(persona.escolaridad).prop('disabled', true);
					$('#familiarTelefono').val(persona.telefono).prop('disabled', true);
					$('#familiarTelefonoAlternativo').val(persona.telefonoAlternativo).prop('disabled', true);
					$('#familiarEscolaridad').val(persona.escolaridad).prop('disabled', true);

					$('#familiarCalle').val(persona.domicilio.calle).prop('disabled', true);
					$('#familiarNumero').val(persona.domicilio.numero).prop('disabled', true);
					$('#familiarPiso').val(persona.domicilio.piso).prop('disabled', true);
					$('#familiarDepartamento').val(persona.domicilio.departamento).prop('disabled', true);
					$('#familiarBarrio').val(persona.domicilio.barrio).prop('disabled', true);
					$('#familiarLocalidad').val(persona.domicilio.localidad).prop('disabled', true);

					$('#familiarVinculo').prop('disabled', false);
					$('#familiarOficio').prop('disabled', false);
					$('#familiarInteresaParticipar').prop('disabled', false);
					$('#familiarInteresEstudios').prop('disabled', false);
					$('#familiarEmergencia').prop('disabled', false);

				}
				else {
					toastr.warning('Es posible que existan personas registradas sin documento.')
					habilitarForm($('#familiarForm'));
				}
			}
			else {
				errorServidor(resultado.error);
			}
		},
		error: function (jqXHR, textStatus, thrownError) {
			console.log(thrownError);
			toastr.error('Error ' + jqXHR.status, 'Error al buscar');
		},
	});
});

$('#buscarContactoButton').click(function () {
	$.ajax({
		type: 'post',
		url: path + 'participantes/persona/existe',
		dataType: 'json',
		data: {
			documento: $('#contactoDocumento').val()
		},
		success: function (resultado) {
			if (resultado.exito) {
				if (resultado.existe) {
					var persona = resultado.persona;
					$('#contactoPersonaId').val(persona.id);
					$('#contactoApellido').val(persona.apellido).prop('readonly', true);
					$('#contactoNombres').val(persona.nombres).prop('readonly', true);
					$('#contactoTelefono').val(persona.telefono).prop('readonly', true);
					$('#contactoTelefonoAlternativo').val(persona.telefonoAlternativo).prop('readonly', true);

					$('#contactoCalle').val(persona.domicilio.calle).prop('readonly', true);
					$('#contactoNumero').val(persona.domicilio.numero).prop('readonly', true);
					$('#contactoPiso').val(persona.domicilio.piso).prop('readonly', true);
					$('#contactoDepartamento').val(persona.domicilio.departamento).prop('readonly', true);
					$('#contactoBarrio').val(persona.domicilio.barrio).prop('readonly', true);
					$('#contactoLocalidad').val(persona.domicilio.localidad).prop('readonly', true);
				}
				else {
					$('#contactoApellido').prop('readonly', false);
					$('#contactoNombres').prop('readonly', false);
					$('#contactoTelefono').prop('readonly', false);
					$('#contactoTelefonoAlternativo').prop('readonly', false);

					$('#contactoCalle').prop('readonly', false);
					$('#contactoNumero').prop('readonly', false);
					$('#contactoPiso').prop('readonly', false);
					$('#contactoDepartamento').prop('readonly', false);
					$('#contactoBarrio').prop('disabled', false);
					$('#contactoLocalidad').prop('disabled', false);
				}
			}
			else {
				errorServidor(resultado.error);
				toastr.warning('Es posible que existan personas registradas sin documento.')
			}
		},
		error: function (jqXHR, textStatus, thrownError) {
			console.log(thrownError);
			toastr.error('Error ' + jqXHR.status, 'Error al buscar');
		},
	});
});

$('#documento').keyup(function () {
	if ($('#personaId').val() != '') {
		reiniciarTodo();
	}
});

$('#familiarDocumento').keyup(function () {
	if ($('#familiarPersonaId').val() != '') {
		reiniciarFamiliar();
	}
});

$('#contactoDocumento').keyup(function () {
	if ($('#contactoPersonaId').val() != '') {
		reiniciarContacto();
	}
});

$("#sinDocumento").change(function () {
	let sinDocEstado = $('#sinDocumento').prop('checked');
	reiniciarTodo();
	$('#sinDocumento').prop('checked', sinDocEstado);
	if ($('#sinDocumento').prop('checked')) {
		habilitarForm($('#participanteForm'));
		$('#nuevoFamiliarButton').prop('disabled', false);
		reiniciarContacto();
		$('#documento').prop('disabled', true);
	}
});

$("#sinDocumentoFamiliar").change(function () {
	let sinDocEstado = $('#sinDocumentoFamiliar').prop('checked');
	reiniciarFamiliar();
	$('#sinDocumentoFamiliar').prop('checked', sinDocEstado);
	if ($('#sinDocumentoFamiliar').prop('checked')) {
		habilitarForm($('#familiarForm'));
		$('#familiarDocumento').prop('disabled', true);
	}
});

$("#sinDocumentoContacto").change(function () {
	let sinDocEstado = $('#sinDocumentoContacto').prop('checked');
	reiniciarContacto();
	$('#sinDocumentoContacto').prop('checked', sinDocEstado);
	if ($('#sinDocumentoContacto').prop('checked')) {
		$('#contactoApellido').prop('readonly', false);
		$('#contactoNombres').prop('readonly', false);
		$('#contactoTelefono').prop('readonly', false);
		$('#contactoTelefonoAlternativo').prop('readonly', false);

		$('#contactoCalle').prop('readonly', false);
		$('#contactoNumero').prop('readonly', false);
		$('#contactoPiso').prop('readonly', false);
		$('#contactoDepartamento').prop('readonly', false);
		$('#contactoBarrio').prop('disabled', false);
		$('#contactoLocalidad').prop('disabled', false);
		
		$('#contactoDocumento').prop('readonly', true);
	}
});

/**
 * TODO
 * ====
 *
 *
 *
 *
 */