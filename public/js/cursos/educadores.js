//===============================================
//					Variables
//===============================================
var educadores;
const date = new Date();
let fechaActual =
  date.getDate() +
  "-" +
  (date.getMonth() + 1) +
  "-" +
  date.getFullYear() +
  " " +
  date.getHours() +
  ":" +
  date.getMinutes() +
  ":" +
  date.getSeconds();

var tablaEducadores = $("#educadoresTable").DataTable({
  bLengthChange: false,
  info: false,
  language: {
    loadingRecords: '<div class="loader centrado"></div>',
    emptyTable: "No hay resultados.",
    paginate: {
      next: "Siguiente",
      previous: "Anterior",
    },
    search: "Buscar:",
  },
  dom: "Bftp",
  buttons: [
    {
      extend: "excelHtml5",
      text: '<i class="fa fa-file-excel-o"></i>',
      titleAttr: "Exportar a Excel",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2],
      },
      title: "Listado de educadores - CURSOS",
      filename: "Listado de educadores - CURSOS" + " " + fechaActual,
    },
    {
      extend: "pdfHtml5",
      text: '<i class="fa fa-file-pdf-o"></i> ',
      titleAttr: "Exportar a Pdf",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2],
      },
      title: "   ",
      filename: "Listado de educadores" + " " + fechaActual,
      customize: function (doc) {
        doc.content[1].table.widths = ["33%", "33%", "33%"];
        (doc.download = "open"), (doc.pageMargins = [30, 60, 20, 30]);
        doc.styles.tableHeader = {
          fontSize: "12",
          alignment: "left",
          fillColor: "#545454",
          color: "#FFFFFF",
        };

        doc.content.splice(1, 0, {
          alignment: "left",
          text: [
            "\n",
            "\n",
            "\n",
            {
              text: "Fecha: " + fechaActual,
              fontSize: 12,
              bold: false,
            },
            "\n",
            "\n",
          ],
        });

        doc["header"] = function () {
          return {
            columns: [
              {
                margin: [30, 5, 2, 5],
                alignment: "left",
                image: logoBase64,
                width: 160,
              },
              {
                margin: [0, 30, 100, 0],
                alignment: "right",
                text: "Listado de educadores", //TITULO DEL INFORME
                fontSize: 25,
                bold: false,
              },
            ],
          };
        };

        doc["footer"] = function (page, pages) {
          return [
            //para vertical x2: 580 ---- para horizontal x2:820
            {
              canvas: [
                { type: "line", x1: 20, y1: 6, x2: 580, y2: 6, lineWidth: 1 },
              ],
            },
            {
              columns: [
                {
                  text: "Centro Educativo Caracoles",
                  fontSize: 9,
                  bold: true,
                },
                {
                  alignment: "right",
                  text: [
                    {
                      text:
                        "Página " + page.toString() + " de " + pages.toString(),
                      fontSize: 9,
                      bold: true,
                    },
                  ],
                },
              ],
              margin: [20, 0],
            },
          ];
        };
      },
    },
  ],
  responsive: true,
  ordering: true,
  searching: true,
  order: [[0, "desc"]],
  ajax: {
    type: "post",
    url: path + "cursos/educadores/listado",
    dataType: "json",
    dataSrc: function (resultado) {
      if (resultado.exito) {
        educadores = resultado.educadores;
        $.each(resultado.educadores, function (indice, educador) {
          educador.nombreCompleto = educador.apellido + ", " + educador.nombres;

          educador.btn = '<div class="btn-group">';
          educador.btn +=
            '<button type="button" class="btn btn-primary verEducadorButton" title="Ver educador" value = "' +
            indice +
            '"><i class="fa fa-eye"></i></button>';
          educador.btn +=
            '<button type="button" class="btn btn-danger eliminarEducadorButton" value="' +
            educador.id +
            '"><i class="fa fa-trash"></i></button>';
          educador.btn += "</div>";
        });

        return resultado.educadores;
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al listar");
    },
  },
  columnDefs: [{ width: "12%", targets: [3] }],
  columns: [
    { data: "documento" },
    { data: "nombreCompleto" },
    { data: "telefono" },
    { data: "btn" },
  ],
});
//===============================================
//					Funciones
//===============================================

function reiniciarTodo() {
  limpiarForm($("#educadorForm"));
  deshabilitarForm($("#educadorForm"));
  $("#documento").prop("disabled", false);
  $("#sinDocumento").prop("disabled", false);
  $("#guardarEducadorButton").val("").prop("disabled", false);
}

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
  //Carga de los combos
  cargarComboGET($("#civil"), "base/civil");
  cargarComboGET($("#genero"), "base/generos");
  cargarComboGET($("#barrio"), "base/barrios");
  cargarComboGET($("#localidad"), "base/localidades/1");

  reiniciarTodo();

  //Instancia de la validación y envío del seguimiento;
  $("#educadorForm").validate({
    submitHandler: function (form) {
      activarBotonCargando($("#guardarEducadorButton"));
      $.ajax({
        type: "post",
        url: path + "cursos/educadores/alta",
        dataType: "json",
        data: {
          personaId: $("#personaId").val(),
          documento: $("#documento").val(),
          nacimiento: $("#nacimiento").val(),
          genero: $("#genero").val(),
          nombres: $("#nombres").val(),
          apellido: $("#apellido").val(),
          domicilio: {
            calle: $("#calle").val(),
            numero: $("#numero").val(),
            piso: $("#piso").val(),
            departamento: $("#departamento").val(),
            barrio: $("#barrio").val(),
            localidad: $("#localidad").val(),
          },
          telefono: $("#telefono").val(),
          email: $("#email").val(),
        },
        success: function (resultado) {
          desactivarBotonCargando($("#guardarEducadorButton"));
          if (resultado.exito) {
            tablaEducadores.ajax.reload();
            toastr.success("Educador guardado con éxito.");
            $("#educadorModal").modal("hide");
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($("#guardarEducadorButton"));
          console.log(thrownError);
          toastr.error("Error " + jqXHR.status, "Error guardar el educador");
        },
      });
    },
    rules: {
      nombres: "required",
      apellido: "required",
      genero: "required",
      calle: {
        required: {
          depends: function (element) {
            return $("#numero").val() != "" || $("#localidad").val() != "";
          },
        },
      },
      numero: {
        required: {
          depends: function (element) {
            return $("#calle").val() != "" || $("#localidad").val() != "";
          },
        },
      },
      localidad: {
        required: {
          depends: function (element) {
            return $("#calle").val() != "" || $("#numero").val() != "";
          },
        },
      },
    },
    messages: {
      nombres: "Campo requerido",
      apellido: "Campo requerido",
      genero: "Campo requerido",
      calle: { required: "Campo requerido" },
      numero: { required: "Campo requerido" },
      localidad: { required: "Campo requerido" },
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });
});

//===============================================
//					Eventos
//===============================================

$("#educadoresTable").on("click", ".verEducadorButton", function () {
  var educador = educadores[$(this).val()];

  $("#documento").val(educador.documento).prop("disabled", true);
  $("#apellido").val(educador.apellido).prop("disabled", true);
  $("#nombres").val(educador.nombres).prop("disabled", true);
  $("#nacimiento").val(educador.nacimiento).prop("disabled", true);
  $("#genero").val(educador.genero).prop("disabled", true);
  $("#civil").val(educador.civil).prop("disabled", true);
  $("#email").val(educador.email).prop("disabled", true);
  $("#telefono").val(educador.telefono).prop("disabled", true);
  $("#calle").val(educador.domicilio.calle).prop("disabled", true);
  $("#numero").val(educador.domicilio.numero).prop("disabled", true);
  $("#piso").val(educador.domicilio.piso).prop("disabled", true);
  $("#departamento")
    .val(educador.domicilio.departamento)
    .prop("disabled", true);
  $("#barrio").val(educador.domicilio.barrio).prop("disabled", true);
  $("#localidad").val(educador.domicilio.localidad).prop("disabled", true);

  $("#sinDocumento").prop("disabled", true);
  $("#buscarParticipanteButton").prop("disabled", true);
  $("#guardarEducadorButton").val(educador.id).prop("disabled", true);

  $("#educadorModal").modal("show");
});

$("#educadoresTable").on("click", ".eliminarEducadorButton", function () {
  $("#confirmarEliminarEducadorButton").val($(this).val());
  $("#eliminarEducadorModal").modal("show");
});

$("#confirmarEliminarEducadorButton").click(function () {
  activarBotonCargando($(this));
  $.ajax({
    type: "post",
    url: path + "cursos/educadores/eliminar",
    dataType: "json",
    data: {
      id: $(this).val(),
    },
    success: function (resultado) {
      desactivarBotonCargando($("#confirmarEliminarEducadorButton"));
      $(this).val("");
      $("#eliminarEducadorModal").modal("hide");

      if (resultado.exito) {
        toastr.success("Elemento eliminado.");
        tablaEducadores.ajax.reload();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      $(this).val("");
      $("#eliminarEducadorModal").modal("hide");
      desactivarBotonCargando($("#confirmarEliminarEducadorButton"));
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al eliminar");
    },
  });
});

$("#educadorModal").on("hidden.bs.modal", function () {
  reiniciarTodo();
});

$("#buscarEducadorButton").click(function () {
  $.ajax({
    type: "post",
    url: path + "cursos/educadores/existe",
    dataType: "json",
    data: {
      documento: $("#documento").val(),
    },
    success: function (resultado) {
      if (resultado.exito) {
        if (resultado.existe) {
          toastr.warning("Ya existe el educador");
          console.log("Existe educador");
        } else {
          habilitarForm($("#educadorForm"));

          if (resultado.persona) {
            var persona = resultado.persona;
            $("#personaId").val(persona.id);
            $("#apellido").val(persona.apellido).prop("disabled", true);
            $("#nombres").val(persona.nombres).prop("disabled", true);
            $("#nacimiento").val(persona.nacimiento).prop("disabled", true);
            $("#genero").val(persona.genero).prop("disabled", true);
            $("#civil").val(persona.civil).prop("disabled", true);
            $("#calle").val(persona.domicilio.calle).prop("disabled", true);
            $("#numero").val(persona.domicilio.numero).prop("disabled", true);
            $("#piso").val(persona.domicilio.piso).prop("disabled", true);
            $("#departamento")
              .val(persona.domicilio.departamento)
              .prop("disabled", true);
            $("#barrio").val(persona.domicilio.barrio).prop("disabled", true);
            $("#localidad")
              .val(persona.domicilio.localidad)
              .prop("disabled", true);
            $("#email").val(persona.email).prop("disabled", true);
            $("#telefono").val(persona.telefono).prop("disabled", true);
          }
        }
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al buscar");
    },
  });
});

$("#documento").keyup(function () {
  if ($("#personaId").val() != "") {
    reiniciarTodo();
  }
});

$("#sinDocumento").change(function () {
  let sinDocEstado = $("#sinDocumento").prop("checked");
  reiniciarTodo();
  $("#sinDocumento").prop("checked", sinDocEstado);
  if ($("#sinDocumento").prop("checked")) {
    habilitarForm($("#educadorForm"));
    $("#documento").prop("disabled", true);
  }
});
