//===============================================
//					Variables
//===============================================
var usuario = [];

//===============================================
//					Funciones
//===============================================


//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
	$('#separarEditarButton').show();
	//Instancia de la validación y envío del form;
	$('#altaForm').validate({
		submitHandler: function (form) {
			activarBotonCargando($('#altaButton'));
			$.ajax({
				type: 'post',
				url: path + 'usuarios/alta',
				dataType: 'json',
				data: {
					usuario: $('#usuario').val(),
					email: $('#email').val(),
					nombre: $('#nombre').val(),
					apellido: $('#apellido').val(),
					contrasenia: $('#contrasenia').val(),
					permisos: {
						moduloUsuarios: $('#moduloUsuarios').is(':checked') ? 1 : 0,
						moduloParticipantes: $('#moduloParticipantes').is(':checked') ? 1 : 0,
						moduloCursos: $('#moduloCursos').is(':checked') ? 1 : 0,
						moduloInventario: $('#moduloInventario').is(':checked') ? 1 : 0,
						moduloBiblioteca: $('#moduloBiblioteca').is(':checked') ? 1 : 0,
						moduloFinanzas: $('#moduloFinanzas').is(':checked') ? 1 : 0,
					}
				},
				success: function (resultado) {
					desactivarBotonCargando($('#altaButton'));
					if (resultado.exito) {
						crearToastSession('Usuario creado con éxito.');
						location = path + 'usuarios/listado';
					}
					else {
						errorServidor(resultado.error);
					}
				},
				error: function (jqXHR, textStatus, thrownError) {
					desactivarBotonCargando($('#altaButton'));
					console.log(thrownError);
					toastr.error('Error ' + jqXHR.status, 'Error al dar de alta');
				},
			});
		},
		rules: {
			usuario: 'required',
			email: {
				required: true,
				email: true,
			},
			contrasenia: {
				required: true,
				equalTo: '#contraseniaConfirmar'
			},
			contraseniaConfirmar: {
				required: true,
				equalTo: '#contrasenia'
			},
		},
		messages: {
			usuario: 'Complete este campo',
			email: {
				required: 'Complete este campo',
				email: 'E-mail inválido',
			},
			contrasenia: {
				required: 'Complete este campo',
				equalTo: 'Las contraseñas no coinciden'
			},
			contraseniaConfirmar: {
				required: 'Complete este campo',
				equalTo: 'Las contraseñas no coinciden'
			},
		},
		errorElement: 'span',
		errorClass: 'help-block',
		highlight: function (element) {
			$(element).parent().addClass('has-error');
		},
		unhighlight: function (element) {
			$(element).parent().removeClass('has-error');
		},
		invalidHandler: function (event, validator) {
			toastr.error('Compruebe los campos');
		},
	});
});

//===============================================
//					Eventos
//===============================================
