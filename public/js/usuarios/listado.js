//===============================================
//					Variables
//===============================================
const date = new Date();
let fechaActual =
  date.getDate() +
  "-" +
  (date.getMonth() + 1) +
  "-" +
  date.getFullYear() +
  " " +
  date.getHours() +
  ":" +
  date.getMinutes() +
  ":" +
  date.getSeconds();

var tablaUsuarios = $("#usuariosTable").DataTable({
  bLengthChange: false,
  info: false,
  language: {
    loadingRecords: '<div class="loader centrado"></div>',
    emptyTable: "No hay resultados.",
    paginate: {
      next: "Siguiente",
      previous: "Anterior",
    },
    search: "Buscar:",
  },
  responsive: true,
  dom: "Bftp",
  buttons: [
    {
      extend: "excelHtml5",
      text: '<i class="fa fa-file-excel-o"></i>',
      titleAttr: "Exportar a Excel",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2, 3],
      },
      title: "Listado de usuarios - ADMINISTRACIÓN",
      filename: "Listado de usuarios - ADMINISTRACIÓN" + " " + fechaActual,
    },
    {
      extend: "pdfHtml5",
      text: '<i class="fa fa-file-pdf-o"></i> ',
      titleAttr: "Exportar a Pdf",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2, 3],
      },
      title: "  ", //ESTO DEJAR ASÍ
      filename: "Listado de usuarios" + " " + fechaActual,
      customize: function (doc) {
        doc.content[1].table.widths = ["25%", "25%", "25%", "25%"];
        (doc.download = "open"), (doc.pageMargins = [30, 80, 20, 30]);
        doc.styles.tableHeader = {
          fontSize: "12",
          alignment: "left",
          fillColor: "#545454",
          color: "#FFFFFF",
        };

        doc.content.splice(1, 0, {
          alignment: "left",
          text: [
            "\n",
            "\n",
            "\n",
            {
              text: "Fecha: " + fechaActual,
              fontSize: 12,
              bold: false,
            },
            "\n",
            "\n",
          ],
        });

        doc["header"] = function () {
          return {
            columns: [
              {
                margin: [30, 10, 2, 5],
                alignment: "left",
                image: logoBase64,
                width: 100,
              },
              {
                margin: [0, 30, 100, 0],
                alignment: "right",
                text: "Listado de usuarios", //TITULO DEL INFORME
                fontSize: 20,
                bold: false,
              },
            ],
          };
        };

        doc["footer"] = function (page, pages) {
          return [
            //para vertical x2: 580 ---- para horizontal x2:820
            {
              canvas: [
                { type: "line", x1: 20, y1: 6, x2: 580, y2: 6, lineWidth: 1 },
              ],
            },
            {
              columns: [
                {
                  text: "Centro Educativo Caracoles",
                  fontSize: 9,
                  bold: true,
                },
                {
                  alignment: "right",
                  text: [
                    {
                      text:
                        "Página " + page.toString() + " de " + pages.toString(),
                      fontSize: 9,
                      bold: true,
                    },
                  ],
                },
              ],
              margin: [20, 0],
            },
          ];
        };
      },
    },
  ],
  ordering: true,
  searching: true,
  ajax: {
    type: "post",
    url: path + "usuarios/listado",
    dataType: "json",
    dataSrc: function (resultado) {
      if (resultado.exito) {
        $.each(resultado.usuarios, function (indice, usuario) {
          usuario.btn = '<div class="btn-group">';
          usuario.btn +=
            '<button type="button" class="btn btn-primary verButton" title="Ver usuario" value = "' +
            usuario.id +
            '"><i class="fa fa-eye"></i></button>';
          usuario.btn +=
            '<button type="button" class="btn btn-primary recuperarButton" value="' +
            usuario.id +
            '"><i class="fa fa-lock"></i></button>';
          usuario.btn +=
            '<button type="button" class="btn btn-danger eliminarButton" value="' +
            usuario.id +
            '"><i class="fa fa-trash"></i></button>';
          usuario.btn += "</div>";
        });

        return resultado.usuarios;
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al listar");
    },
  },
  columns: [
    { data: "usuario" },
    { data: "nombre" },
    { data: "email" },
    { data: "ultimoAcceso" },
    { data: "btn" },
  ],
});

//===============================================
//					Funciones
//===============================================

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
  $("#contraseniaForm").validate({
    submitHandler: function (form) {
      activarBotonCargando($("#confirmarRecuperarButton"));
      $.ajax({
        type: "post",
        url: path + "usuarios/contrasenia",
        dataType: "json",
        data: {
          id: $("#confirmarRecuperarButton").val(),
          contrasenia: $("#contrasenia").val(),
        },
        success: function (resultado) {
          desactivarBotonCargando($("#confirmarRecuperarButton"));
          $("#contraseniaModal").modal("hide");
          if (resultado.exito) {
            toastr.success("Contraseña cambiada");
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($("#confirmarRecuperarButton"));
          $("#contraseniaModal").modal("hide");
          console.log(thrownError);
          toastr.error(
            "Error " + jqXHR.status,
            "Error al cambiar la contraseña"
          );
        },
      });
    },
    rules: {
      contrasenia: {
        required: true,
        equalTo: "#contraseniaConfirmar",
      },
      contraseniaConfirmar: {
        required: true,
        equalTo: "#contrasenia",
      },
    },
    messages: {
      contrasenia: {
        required: "Complete este campo",
        equalTo: "Las contraseñas no coinciden",
      },
      contraseniaConfirmar: {
        required: "Complete este campo",
        equalTo: "Las contraseñas no coinciden",
      },
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });
});
//===============================================
//					Eventos
//===============================================
$("#usuariosTable").on("click", ".verButton", function () {
  window.location = path + "usuarios/ver/" + $(this).val();
});

$("#usuariosTable").on("click", ".recuperarButton", function () {
  $("#confirmarRecuperarButton").val($(this).val());
  $("#contraseniaModal").modal("show");
});

$("#usuariosTable").on("click", ".eliminarButton", function () {
  $("#confirmarEliminarButton").val($(this).val());
  $("#eliminarModal").modal("show");
});

$("#confirmarEliminarButton").click(function () {
  activarBotonCargando($(this));
  $.ajax({
    type: "post",
    url: path + "usuarios/eliminar",
    dataType: "json",
    data: {
      id: $(this).val(),
    },
    success: function (resultado) {
      desactivarBotonCargando($("#confirmarEliminarButton"));
      $(this).val("");
      $("#eliminarModal").modal("hide");

      if (resultado.exito) {
        toastr.success("Usuario desactivado.");
        tablaUsuarios.ajax.reload();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      $(this).val("");
      $("#eliminarModal").modal("hide");
      desactivarBotonCargando($("#confirmarEliminarButton"));
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al desactivar");
    },
  });
});

$("#contraseniaModal").on("hidden.bs.modal", function () {
  $("#contrasenia").val("").prop("disabled", false);
  $("#contraseniaConfirmar").val("").prop("disabled", false);
  $("#confirmarRecuperarButton").val("").prop("disabled", false);
});
