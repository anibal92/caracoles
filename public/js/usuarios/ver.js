//===============================================
//					Variables
//===============================================
var usuarioOriginal;
var usuarioEditado;
//===============================================
//					Funciones
//===============================================
function obtenerUsuario(id) {
	$.ajax({
		type: 'post',
		url: path + 'usuarios/ver',
		dataType: 'json',
		data: {
			id: id
		},
		success: function (resultado) {
			if (resultado.exito) {
				usuarioOriginal = resultado.usuario;
				cargarFormUsuario(usuarioOriginal);
				$('.loader').hide();
				$('#contenido').show();
			}
			else {
				errorServidor(resultado.error);
			}
		},
		error: function (jqXHR, textStatus, thrownError) {
			console.log(thrownError);
			toastr.error('Error ' + jqXHR.status, 'Error al listar');
		},
	});
}

function cargarFormUsuario(usuario) {
	$('#id').val(usuario.id),
	$('#usuario').val(usuario.usuario),
	$('#email').val(usuario.email),
	$('#nombre').val(usuario.nombre),
	$('#apellido').val(usuario.apellido),
	$('#moduloUsuarios').prop('checked', usuario.permisos.moduloUsuarios);
	$('#moduloParticipantes').prop('checked', usuario.permisos.moduloParticipantes);
	$('#moduloCursos').prop('checked', usuario.permisos.moduloCursos);
	$('#moduloInventario').prop('checked', usuario.permisos.moduloInventario);
	$('#moduloBiblioteca').prop('checked', usuario.permisos.moduloBiblioteca);
	$('#moduloFinanzas').prop('checked', usuario.permisos.moduloFinanzas);
}

function activarEdicion() {
	habilitarForm($('#editarForm'));
	$('#editarButton').show();
	$('#cancelarEditarBtn').show();
	$('#activarEditarBtn').hide();
	$('#separarEditarButton').show();
}


function cancelarEdicion(restaurar = false) {
	if (restaurar) {
		cargarFormUsuario(usuarioOriginal);
	}
	deshabilitarForm($('#editarForm'));
	$('#editarButton').hide();
	$('#activarEditarBtn').show();
	$('#cancelarEditarBtn').hide();
	$('#separarEditarButton').hide();
}
//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
	//Oculta elementos
	$('.loader').show();
	$('#contenido').hide();
	cancelarEdicion();

	obtenerUsuario(usuarioId);

	//Instancia de la validación y envío del form;
	$('#editarForm').validate({
		submitHandler: function (form) {
			activarBotonCargando($('#editarButton'));
			usuarioEditado = {
				id: $('#id').val(),
				usuario: $('#usuario').val(),
				email: $('#email').val(),
				nombre: $('#nombre').val(),
				apellido: $('#apellido').val(),
				permisos: {
					moduloUsuarios: $('#moduloUsuarios').is(':checked') ? 1 : 0,
					moduloParticipantes: $('#moduloParticipantes').is(':checked') ? 1 : 0,
					moduloCursos: $('#moduloCursos').is(':checked') ? 1 : 0,
					moduloInventario: $('#moduloInventario').is(':checked') ? 1 : 0,
					moduloBiblioteca: $('#moduloBiblioteca').is(':checked') ? 1 : 0,
					moduloFinanzas: $('#moduloFinanzas').is(':checked') ? 1 : 0,
				}
			};
			$.ajax({
				type: 'post',
				url: path + 'usuarios/editar',
				dataType: 'json',
				data: usuarioEditado,
				success: function (resultado) {
					desactivarBotonCargando($('#editarButton'));
					if (resultado.exito) {
						toastr.success('Usuario editado con éxito.');
						usuarioOriginal = usuarioEditado;
						cancelarEdicion();
					}
					else {
						errorServidor(resultado.error);
						cancelarEdicion(true);
					}
				},
				error: function (jqXHR, textStatus, thrownError) {
					desactivarBotonCargando($('#editarButton'));
					cancelarEdicion(true);
					console.log(thrownError);
					toastr.error('Error ' + jqXHR.status, 'Error al dar editar');
				},
			});
		},
		rules: {
			id: 'required',
			usuario: 'required',
			email: {
				required: true,
				email: true,
			},
		},
		messages: {
			usuario: 'Complete este campo',
			email: {
				required: 'Complete este campo',
				email: 'E-mail inválido',
			},
		},
		errorElement: 'span',
		errorClass: 'help-block',
		highlight: function (element) {
			$(element).parent().addClass('has-error');
		},
		unhighlight: function (element) {
			$(element).parent().removeClass('has-error');
		},
		invalidHandler: function (event, validator) {
			toastr.error('Compruebe los campos');
		},
	});
});

//===============================================
//					Eventos
//===============================================
$('#activarEditarBtn').click(function () {
	activarEdicion();
});

$('#cancelarEditarBtn').click(function () {
	cancelarEdicion(true);
});