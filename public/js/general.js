var path = "/caracoles/public/";

//Variables de session
if (!sessionStorage.iniciado) {
  sessionStorage.iniciado = true;

  sessionStorage.toastMensaje = "";
  sessionStorage.toastTipo = "";
}

//AJAX SETUP
//Agrega el encabezado del CSRF-TOKEN a todas las Requests
$.ajaxSetup({
  headers: {
    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
  },
});

//Boton regresar
$("#atrasLink").click(function () {
  window.history.back();
});

//Carga inicial
$(document).ready(function () {
  //Muesta los toast en sesion (local)
  mostrarToastSession();

  //Oculta todos los spinners
  var spinners = $(".fa-spin").hide();
});

//Muestra los errores del servidor
function errorServidor(error) {
  if (error.mostrarToast) {
    toastr.error(error.mensaje);
  }
  console.log(error.descripcion);
}

//Muestra un toast con la informacion en la sesión (cliente)
function mostrarToastSession() {
  if (sessionStorage.toastMensaje != "") {
    if (sessionStorage.toastTipo == "success") {
      toastr.success(sessionStorage.toastMensaje);
    } else if (sessionStorage.toastTipo == "error") {
      toastr.error(sessionStorage.toastMensaje);
    } else if (sessionStorage.toastTipo == "warning") {
      toastr.warning(sessionStorage.toastMensaje);
    } else {
      console.log("Tipo inválido de Toast");
    }

    sessionStorage.toastMensaje = "";
    sessionStorage.toastTipo = "";
  }
}

//Crea un toast de sesión
function crearToastSession(mensaje, tipo = "success") {
  sessionStorage.toastMensaje = mensaje;
  sessionStorage.toastTipo = tipo;
}

//Limpia el formulario que recibe como parámetro
function limpiarForm($formulario) {
  $formulario.find("input, select, textarea").val("");
  $formulario
    .find("input:radio, input:checkbox")
    .removeAttr("checked")
    .removeAttr("selected");
  $formulario.find("option:disabled").prop("selected", true);
  $formulario.find("input:radio, input:checkbox").prop("checked", false);
}

//Limpia toda la informacion de un modal con formulario
function limpiarModal($modal) {
  $modal.find("input, select, textarea").val("");
  $modal
    .find("input:radio, input:checkbox")
    .removeAttr("checked")
    .removeAttr("selected");
  $modal.find("option:disabled").prop("selected", true);
}

function deshabilitarForm($formulario) {
  $formulario.find("input, select, textarea").attr("disabled", true);
  $formulario.find("input:radio, input:checkbox").attr("disabled", true);
  $formulario.find("option:disabled").attr("disabled", true);
}

function habilitarForm($formulario) {
  $formulario.find("input, select, textarea").attr("disabled", false);
  $formulario.find("input:radio, input:checkbox").attr("disabled", false);
  $formulario.find("option:disabled").attr("disabled", false);
}

function activarBotonCargando($boton) {
  $boton.attr("disabled", true);
  $boton.children(".fa-spin").show();
}

function desactivarBotonCargando($boton) {
  $boton.attr("disabled", false);
  $boton.children(".fa-spin").hide();
}

function cargarCombo($combo, lista) {
  $.each(lista, function (indice, elemento) {
    $combo.append(
      "<option value=" + elemento.id + ">" + elemento.descripcion + "</option>"
    );
  });

  $combo.val($combo.val());
}

async function cargarComboGET($combo, url) {
  await $.ajax({
    type: "get",
    url: path + url,
    dataType: "json",
    success: function (resultado) {
      if (resultado.exito) {
        cargarCombo($combo, resultado.lista);
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al listar");
    },
  });
}

async function cargarCombosGET($combos, url) {
  await $.ajax({
    type: "get",
    url: path + url,
    dataType: "json",
    success: function (resultado) {
      if (resultado.exito) {
        $.each($combos, function (indice, $combo) {
          cargarCombo($combo, resultado.lista);
        });
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al listar");
    },
  });
}

$.fn.dataTable.render.ellipsis = function (cutoff) {
  return function (data, type, row) {
    return type === "display" && data.length > cutoff
      ? data.substr(0, cutoff) + "…"
      : data;
  };
};

function nl2br(str, is_xhtml) {
  var breakTag =
    is_xhtml || typeof is_xhtml === "undefined" ? "<br />" : "<br>";
  return (str + "").replace(
    /([^>\r\n]?)(\r\n|\n\r|\r|\n)/g,
    "$1" + breakTag + "$2"
  );
}

var logoBase64 =
  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAbwAAADsCAYAAAAPSROcAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAALiIAAC4iAari3ZIAACfCSURBVHhe7Z2/tu22cYdlp0iqqEqquEsVq0olq0ol6wFkvYDlB7BeQEmXJlaZIvYLWOrS2KqSxlKXxnaXyqmy3Dhd0jj87t7Q5eUBSWAw+Ef+vrV+i+fcezbJTQIYzGAAvCWEEEIIIYQQ4iJ863kU1+KHi/788eMr3n4e4Q+LPl/0X69+E0KImyCDNwd/s+iXjx9fGazvPn50A+P3u0VfL/of/mHFbxZhIIUQQgg3/n7Rr5/64wT6/iIhhBDiFIzGrxb94vnz7MJQr0OpQggxDApptoWw4c8Wffrqt+vyD88jhO8shBBdkcGrC+Ntnz1+fJVI8lePH2+Pyp0QojlqePz56SKSQEg0+QH/IHZR+RNCNEMNjg+Mw3256N1FSuSw8d6irx4/CiGEGA28OTIrYwkckk10HoQQwh15ePnQKIu6MBeQbE+VTyGEG2pQ0mA8jgnYoh8qq20gYrFm+7sQ06JG5BgZuvH4eJGmOfgTpo98uIiVfFjZh5V3AjJ8QlwUDN12bEkaS99bJHxgusxPFsWeM8LY8f9CiIsRq/DSuJLhK4Ox0iNjF4TRI0lLiGn59vMoXld6MRdkdeq92flk0XbB8D20w4aYGhm8x/qPjF/8+NVvYlbwQDSlwUZsqbuwO0fg6svhiRtwZ4OHgcMzYIBeS37NDw0y4c0QghNp7C32rQUUxOW4q8GjQdQg/LXhHWtpt3NYHSgFrYIjpuduBi94AOIeYPDYeknskzoux9J5LIAuxLTcaR4eOxdor7b7wW4VJGXgyXzAP4gXhE5gWOEmBobxO48fhZiTOxg8KjCZaHcbdKfxYtL8508d9eQZw2RC9x22MKIshC2bxGtI3vri8eOreai/XUQnIUw+18RzMT1XN3ihkl7N2LEiBkkFGCd63S3SxXmWXI9rXeF5apWhl9Dh2XpxocMoxPRcudJfYXfxEI4Dvsdo74tM17cfP771/qJZJoGzIzv3y5ZEV4PJ4XjrQoibwJjEzMKbwmDPBvfMvce+02gihMe9zj53j++AGKPefkfeh1aiEeKiEHJLWSZpVNFwXQU8jdh3HE0YPo4zEozd9jttFf5OCHERQsM1i37+PF59fcIwwX8GzQJlh3KT07njM/L2hLgAsQo+sq6eCbkHIbbRPfAZjELsvnMkhJgUQjWje3c09Bw1D/DB9vmMppHH9WL3myshxISkjF/0VDDGIg5hOVZCCR2CUYQXOuIKLbF7tUqI2zHztAQaJFL2R1wvMUwnUKJAGiM2wOwWQHgzTLsYAc/npHmIQkzCuqc6mkgPFzZiz3MEjYB31qsQYgJGDWNqLzYf8Nxjz7eXCLei3sTurUTqmAkxOLGK21sydHXg2Y6WjNQTbw9P5Vbcjpm2B6KSjgRjPIyDXHF5qhHg2bZYIzSHnmXQ+1mw8bEQt2IWgzdCSGkNjbG2mqkPz3i0LWlG63hZ0RQZcTtmMHg0ML0NHosNk3WJoVN2W1vwbHjmZL7yHkbgKkZPiFsxusELaf29V7/gPkZKT78jYYuaEYzeV4tmNnrsIqLyLMRAML+ORqWnRpx8fHfYBzD2rnqodaZjWH+1VCrXQgxGrKK2FLs+i3GJvbPWwgCxOHYriDSQXRm7lxzRaRBCDELPBYavvnvBlYi9v9bCCLX0mErrhpa6E2IgYpW0heiti/mIvcseapn1aJmcH+Y0CnFbRss4JBuz19Y5yr6cl1Ea8lZlKEQhyBxOCamSpEK2a0gCE0IMQI9Q5oiLT4t8Yu+2h1qCAUOx+wjCOCp6IcRgnFXcGhLXIvaOe6g1sXsIUvKVEIPR2thp4dzrEnvfLUXZUuKTEAMyysTzFuN2YS3Czxdp0u116T0WS/LKaGuACiEGIdZLriVxH2Lvv5VCFqUQQnzDtqGoqbvDWA5JQbkTl2dlhJV6NOdNCPEKQj+W+UQ5Ymyw5UoYPfHeL+1IPNcZGvNg5GPfoZWEECLaOHgKT+aqawbyvUiOYN5i7Lu3FMYvJB2NCIa5dVLUVkKIAeg1wF+7EWA1+98u+vjVb/ODcfvNoi9f/fbWW58+j6PBprhfP358ZWRGAe/3h48fu/DRIpKlhBAd6WXwaq6ogmH40SKM3qwEY/H+ot5bI1kJ2/gQVqTB701PT4sVUZQZLMQNWYd6amhWGGvCE+kdfquhEUKejOXWHjM+kuZ+CnFDYo2Bh3rvim6BBpgxJkv25MzqRexeWkmT0YXoTOuQJhW/Fr0nHKdAGHdGw1yDq44fnzFDORXikrRcaaXW9ikkSozeiOC90cOXsXsNIT6eS2t6l5WrZg4LIVaswzseYlxopEzALUx8ZpX62L1Lr9XjHcbuo6WEEBfGOxEjGLuWm26mErtf6Vytvb3YPbTSrJm3QkxNq5Am6fXeYPBI9x6FMAlc2MAItHx+PUOb2oNRiIvinX044pJWsfuU7GoFZalXdqy8PCEa08LD++7z6MVIE3hDaFX40iqx451Fvbby6ZGwI4SoiOdEXwwLxxEI4TcyDdf3KPkIz4tjC76/aHv9FlK2phCNqT2OQcX2ZITpByTK9Fg1g7UYWR8UmIqRu3QaY4zcO0dvr7smLd65dznNYYQyLYQoJHhkXhoBPDvv73Wk2l4Ay21xHcJrwasaUbXp5eUhJbAIcQFilduiUSZrt9hXjWv0hHmDGHTPULSXasM1QqZtawkhJidWsS1itf2esBxYba+u1s4RJcTus6daELtuCwkhJsbLQPTOZGMPtVrGbpZQ1iihTt5DbWLXbSUhRGVarqWZQ5hQ/t7z2APCW99Z5LXZKvvDkQJPkgKaZUNQUve5X94J3+Fni3pR2+j1SiCZee9GIW6NR6p+i978ETW8uqvAs+nl9THGyThjTWLXbSFNRBeiMjU8vNL1LfEkeiaqsKvBh48fXQge3VXA4PXy8sgqDVMzatHrXZEpKoSYiFjPNVc9d4ZmzNDDuxsls7Q2se/eQrWzWWPXrK27lBkhuuHt4XmMRfRaOowwHd6px5gdY393AG+oh7dXe9Hwz57HloRsYCHEBBAKjPVcc9WD2H1YdFfIOI09j5qqbRxi12whIcQExCpvrnrgkYChhYAfHkrs2dQUY3q1aLHQQExCiEqMMi2BEFKPZAG80i8eP5ohBNtz+sQohCkXLakZ/v7keWyNjJ4Qg7Ptpeaqdqp5DLyD0gQVEYf3GXteNVQztBm7XgsJIQYmVmlz1BoaSUKZ1jmDeIbimNhzq6HaC2zHrtlCQohBiVXYVLF8V0u4Xuw+zoSBJHWco0iD5xV7lt6quUxb7HotJIRwxmMMr7Rytk5rt04ZYA85xFJbIo1WafY19/ebae9AIURltj3THLWmZNmzmhmBV4Zn7rHc3JlqErtebfUY1xZCnBCrrKlqSez6qRJl8AxbhDdrUZrcZJUQwpHSkKY1ZMVOAS1T2Esaj15rK14JnuGIe/6l8punhBA3xjppu+W6gYTTrPfZe/PZK2FNFspRTVpPRKeOtE7oEuLSlHp4Xz+PubRaa5LsPdbHtCQe4JXUXpn/TrRITqqZJNPaQ6Xc3mVNViGaUBKuoxdqpVWY0HqPGMiexg5vIjSwNHwslsz9EFZjRZOZNwwlGaPWNAI2qK1l9ErKu5Wa30cIkUEIveSo9rYua2LXT1VtMGLhWttwa2pyBxOuaQyDZpkfGPsuXqptHGLXrCkZOyEGIVZBz9SqAseunaLai0DHrllLI48/xu7XQ1c0eDJ6QjhhHcOjMo5KSUNfYxFoGqywtmRLCH/iBY74rmqO59U0EK0zNT32ZhRCFBJ6oGdah+da9FS5xvr6OfKGe0EtJl2n6nuLRqHkXR2pdjmzZvyWSAjRkVilPFOLlSOsk5s907+DVzWyRljFg8SVGkav9lgmYe/YdWtKCNEJ60aftSlpPD3o0fO3imc1wo4Ptby8mlg7VaUSQhRiGcP7+HnMgbT6UfGYIkGDNNMiw4wNMb0Bg9MzueX953EmtOKKEDfC0ivnMzWx9ro9xrT4bttxOu6nlyeQq/A+e2EpT2eqPf0lds3aEkJ0IFYZz1ST2PVSVQqN9SyG7UgYiJZzJNcQWo3dU6lqErtebQkhGmPpjddOIohdM0WlxM45s3i3qAex+ylVTWLXayEhREPCfLJU1U6MiF0zVSXEzncV9TB6sfsoESHmmsSuWVu9OiNC3JZYRTxSzZVLrKFEPldCrRAczwrhEdO49Z6/1xLvqRyl7/iMFjs/bDXCVBIhbkWsIp6pFrFrpai0MYydM0drQ5ZDDwPYktzowZFqLxEHsevWlAyeEIXkTEuwbI9Sa1cEa3iH1edLtlzxGI9k4WieS+6zeXsRn2mVFv/LRS0TWTx3pxhpRRkvtFWVEIXkGLwrjCGUrk34xfNoJRitEt55Hmvz/edRjIPG8YRoBJUtFmo5Ug0s94FKG4vSJaW8ITTrGQbcU6tG1nulmtobtsauWVO8bxk8IQoo3fG8B71WkP/yebRQI7RLaJYwFyum1KTV8/b2XGtnCLdecaX1jutC3JpYr/NINYhdJ1WlWD282tDzj13XWy2IXdeqFt5Q7Lo1JQ9PiAJSPTzSxntTknlHskoJpKHnJEJwPfZ8q+HZbSlJwjlj7cWQbFOb0vckhBDFWLwIb0o8mdIFkulZx857pNbE7sFTLbwLz/l4LTJMe+yQ4bmVlRC3ItXDyx0/+Px59ILxGOsYBl5D65TuFp7dlh7X9MayE0dPyLptjQyeEEZqJa189Dx6UTsxwwvCmD0awcB7i7iHGrCVT+35bZ7vucWWVD0SSa44x1CIJsyQpVmabee131tKY0zvu+fef18tqtU5oKGdaW7eVbMaR95bUoihSTF4vRs5GnBrajzhTA9vE6OLMTmCBI8Rwoo1l6BqYUS8w+E1waNujQyeEEZSDF7PDE28s17z7tZgdM88p5J5ep7UHK+cafyoRVbpWSeoBi2+lxCXZPSQZuvJvUeceTefPI8jUPO5eYWI9/jB81jKVce6ZPCEMFLD4I2ULchUBi+OGtDR5o/VXG+zdifEy2saqbMkhJiE2FygI3kRO3eOvOeN7a1bOerqF7X27WsR4o5dN1c/XtSC2LVr604oK1W4MUOW5ijseR6k64/Iu8+jN7WyQL1pNfdylucxI3QmSZrTzh2iGbEe5pE8oICXrrpRw/PinOtrjL4p5/peveSxJ+AZsevmqhXbMtFCrbzXEbjTdxWVOfPwcsfAvHq7HmGMD5/HmoyeIv7Z8+iJPJr+3MnjqVGGxU05M3i5q3Z4rfJBRmRppa6xqDK9+XWCyh2Xefr6eRQPemRNai6eEAZGHMMjhOFhSL77PNYAo4dGX79yVoM8U4MugyfEJJwZvNxJ3x5hRK95WLVCbzXGBmvxy+fRkxarrZRGClpOCO8R4lVYWQgD3h6eR7hrlBVLrkCNhaRn8C5UhoQQ2cQyxNb6w+Z3D7bntGj07MlWkPwTez4l8pzMv0fsujlq6YX3yNKcKcogxDCUenje4xdUZI/Fg73CorND58Gbmjuse1FrDqIQYmJGS1rxSlgRD7z35hthua7RQqoqr0JMwpnBy5kD47GepBbG9cU7waRm5mvgbA5mShn54HlsQYskHiGEA2cGT+nPc0Not9YO6LUonX+pDEYhRJQ/eR73+LtF//480nDyMxlwHP9z0b+ujvBvz6MFeu7/t+h/F/01/1AAhvofHz/eGjwynsVfLOIdIX7+y0W5EM78s0W1n+t/L/r9IspcgOkVlLm/ffXba7inf17E/wX9x6KScpgLYeOtV8oz/9PHj+7wLDDqLb+jELdjbx807wSRECLiiBTmLCO8NxplPct6pOwTqOcvhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBCCCGEEEIIIYQQQgghhBBClPGt59GLsBPz+4vCVjLb9QNZ+HaGTUSFaAFrnLIebWxnEtbn/GTRDDtA8D3YG5B7ZoH5APUfffzqNyHe5OeLKN+U/2ArQl3gd8r/MPxiETf800Xsrv3HDPEZPjsC6z3NqLBbI70HG9a2JmWx7tDI7Inn/+tFPe6/hNytg0YpX2voFKJtfUjVrxb1hvpC/Y3d35l+t+gni0q3gfJiXfdHgPrZA65L24dqPhPKPm1PrGykqKj8Wz08LuwBWw7xcCn83t5mKsEr/fR5BLad+eLx4zdQIOjBUmHX//fuoq8fP35zLm9i91gLelu8k17vY0v47hhnen7bysi/hd7hXmMRNieu9X7OoBFhWx/KO9GPs01uz+A81JkekRKPuv/5ot8u4l3ybN5b5M32XX+4aFunuf7ae6bTnrPptSfb+6WtWZd17vOrRfxbafmBUM+30PbRnoXn4lFn6OAEr82jDaP84/mFc1bD2qs7U/AUKXCtoccQu6dcYQhrQaHjGcWuW0t8H67rUeBL4Pqx+8tVr+/BtS0RkFR5b8+1R+zaHqLe8268vfHcckM70LOsh2cQu7ceCvW/hJptFvfHsRrbC9ZQj0IXu48S1SC38nqL67d+LxCuG7unXPW4/1odxJhqgRfg1Sk8EmEuz3JmuWca6F54lXNP8QythPcZO6+nqjhJsQvVFA8q5m7XIHZ9q3jJNRipMpRUglw8v3dND3wL12pp7IK8weNo7XXwzj1CdrFzn0kG76UslIzRWURdc3t3sQu0VE1i1ytVDXo0nkdqlewSu3aJaFRqE7tuS3mB0Ymdv5VK31XsnGdqUT72aOFFW8QYaw6xc7TSaXj/28/jHpxE9GeUjLYAg9C5FUG0waPO0nC09ORjkNzg4emJMnISQ3rkYKw5HQc+Mnj04skK6k3PXpfY56PnUbxmhLJKRmppZyTMoe3NaB29mlTPODSyzmI9gjBmrykVaw47fEcG7+1FI0wQ5yHevac3QkHaQg98ts4Iqem14FmM8J54LyWdkdHeKdEEMT7bKR892c3/2DN4odC3Shw5Ak9zpp4e422inBqdnNqey0gT+UsMV4v5nilwH6N6Pt6M+j1TogWUNeYjj8Luqj5HHt4ohT4wopcTY6QXX5uaXt5MnZzRvCJr3eV7jFbP+C53yCWY3bBPUV9jBo8B6xE8uzUUeu811WqFSVNj3jncpZfbglqN52gdRCtWL5UyGso+44g16sGVGbWOz9j27HbEYwaPsA/L2pTAslQxleDdoPzT8+hNjbBZSQeEpZJogMLyWt7sFq5BGbEhjtWVICuUmZzxL95hSb3HM2TMn3vmXOFn9MGiUjRUkAYGystIpXRcKGMlbTP3GsrJVly7Vrv1DfSALcpZGij2+RR5Th6mUsau4SFvYtdIUQqkEqOSuX41DF7sOl7ypKQc5RgYy/vJmYxbsvRZajiLv7M+r9yU99g5zlSjHKdifS69KSn/qYRlxHL14n0ejeHlkpMZhvW2MFqo9QqQjYsIZVt7UyyILPJhEdxULN5XzriKdewOT46FsVPg76ye2ixj+HfDWvdzImFuswW2Bq/lwGPv0FLN5JIcb7cWqY1QAKNHRlbu58B7PHSkbMda5Hb6rJ3E2stl5dZj/p4V+cX84EVZ6z67ZeTgYi+2Bs9aOSyVsfccv5re4gi90bBlUQ6jTCafae7V6FGHlPGc1qE8S9kU42GNCFk61S72wiOkyd5Ws4Fhrzkna4QKbTW61nv3NFI5K/xY9lLzjGSUJnjlYNk1PKUnbU06KGnwLI1eq62QRF08kphSeNEZ9TB4ua5pCV7ZRzmNOuMsuRV75rFGazjRMwyZ05uzLH/XO+PPWo5zxvwCNUP3Vs+QTrKlY9WycyHm50U92xq83uNqZ1h6haXQq86t2C3HQvewNnTW0IHnTtG1Owy9F7m1eGpWUiIZFgNsMb6lXDkUaonI9HgHM3Fq8CyDySUZerkG1is5onaDOoKHZ31WVu/D+jkPciv+SOv+1SalLFo6kqN3ju9A73cwfXLZdh5DqqzEznUmD2Ln3VMgd76JpxcRO3+KrMTOdSav75szH2ztecf+/0hexM59phJi5zvTGYxpxz53pNLsz9z6FJRK7LNnsoZoPbC8g573C7F7SpGV2LmO9GLetscY3l3IDRH2ztRs3fvz+r4jZLjeDYt3XhrF0NzNN2mZC3EXXkR+7mjw2LcplXVDMMK4nHiTkoQMeoDCTs0s5zvSc0hgFoqXGett8HpMacgZv1l7G73nDd6FnPGkkvTm4sozCWpI50CRjTSot0GxtoLyjqL1u7fBm+kla5D+GK+GdYSEnyuRUsdmMYpXNt53KvfW+ZSMWa5Fh5dFT4KIOgSFv3mDOxq8nF2vrcs53RGvhb2tobLcVH+NIc3HlQ3enTy8WispMQ6KY7LrnPQ2eLX2pDui5djDnbY08ZgjlTO+ui3UuVMTWpYDT2o0+lc2JLMw4zuw3nM3497b4PWYSN6SO4VBPSpszjlKy86sPWrPCf6z0XvBgJrM2FZY8xqoez2cnRcGz7JMUwl4BanJAx4VvXTuUOq9jkDrFenZaaGUnErgMeYx0yLVa3LqaUrHwNJZKU04szSWs3rlKdzJw4NfPY9N2Rq81r3eMKgYGuejRpqH29vgKKFin9Zlx2OaiIeRbg115stFhHBTvIJay3Fp3piAkja5+dSgrcHrkXpPBX5nEQki4RhTNOsmk1KDles1zRo2s2DZtaAEj+vN+n6oByTpUF9jdWWt0joj2jBzWSyhaWizt4fXmtKQSG5YdUYPwkKPcIxH+F1zK4Uop8TLaxra7J20MjIeCTWE3Wbbw6vXCiQ9ntOdPHBv7tKZa8WMY3heNGtzZPD22VvFIzebaraBdktvzcNw/Px5LIEQnmhDlyw7MSQeYfMmRm9r8K7cy/BaC/PKqdFWPAxNj3R7DPX0W5x0QglcwpvqRu9OBm/mSeCWOTqWBome2qePH5vTq+xpHE+MgCVKMtJqQV7RlapG704hTa/dgXNDlCUr+gcsxivXgLA0WC9jBx7PyYI8vD7jcV5L0d0ZwsoYCBI/6KxuRSefuceIVYx45hz5v7D/HhGr8PelYPQ8po41CW8CD46L5WoGcjdY3OOHi2J/f6RSYudMUQo0+LHPpsiz0Yqdf09HRir292cqIXa+M40G46ex+zxTCbWvGfvsmTwafSssghC7p1by+u7BeMaukSt37uTheSWPeHmKLQgFB8PEMRTG8DuyrjZCT671yjyBOy+vVYM7ZwiOQu+MYS8v38twAu1TVXB3Q0OYoxmI3feezuaGxD5zpNIMxNg5eypEArzYnv9MR6yNeapKiJ3vTKNh7ZGXIA/vTQg/xu6plTyjNRC7hlVuaFpCHO9VQ662DBO9Uc8pAB5x/4CSUMSM5Hp43l6599q7rATkdU6MnosHvDV4bz+PV8O793J3WALuSmiqiZgN7xCotwENa716DT9QR3O2D4tyFw/v6tsQtYDxOrw6T88ukLMpbw16j58I0Zsa47gs3kGSnxfkYRTteHMXg5czjy3FDc9N1ujdoJdCb63m4tDeq9GogyNmo3enyzLXNwXvqCELiJjH9bYGTw1FWuObWzhLG/RahTGVMBVjBBgbOKP38xLnKDP0TWpt4zQC3lEh6rdp/uxdPDzvycV4O7mN6hUmOGP0vFc3z82MS5kWohClEHnUrjOeRo+FOJhOlR0uvYvBy3mZKRmD9E5zE2Fm3V17S1jdwYuc1V1SQ8mWyisjKe5Mi91KvD09MrKzFjG/g8HLbZxTPY7ckMxME9ZTKM6YMpA6vWNvp4sjlKkp7kyr8u9p9Ogs/8vjxzS2Bu+Kvdyc0GOOEcudW3e1MQvGJXsYPSFEHBLuiFCRi3GUfLf9P9qmLx4/NgGjx316zL+lHUp2au7g4eVMfswJU+YasJFWNvfCO7vyjJzlj67WwRAiBsYjiPmxRKiIcPDz+v/W2v4fmZStV5nheshr0Ykko3cHDy+n4auZ3dfaOGwJPaqgnI7AEZ7jeWfkrKKiMTlxda6QjexpaE87xHdJWkklJzXY0qBaJ2FaV4pZ9+JCjyoo9PJ6kpvIk/Mcei1sLdLQBrLlXCUvwKsdOp2ytDV4V5wLkpN9lBMy++R5zMG6zmNWJtKT1AY/GMQSrF5e7lSNnMnvlmdWusi3SEchZ7HGw+jRnhx6jFf38HLnjF0pDJabVNPb2/PGsohCi9Rs8UAhZ7HFow06nOZ0dYOXW6k+eh6vgKVBuZLRs0xNEEL0xaMNYqulKFc3eLmptrlhlty/n6FXq+XlhBA9KTV6u8k8vQ0eA9fEXBk7IYFhnVRBggJjQ0Hh30ciN5mEGPPog/WWsS9QEkIbqANhA949MT8y1JdadUZjcKImZJJbE88Ia0bLfW+DF7JqGDvBGHCj4bhtQA9jszvkJkXkYplq4DUd4AyrN2ld3TzX+Bdt83Fzzsod///u48dqWDOHhUgBg2XtfO8yQkhza8iOGuoco8cUgNohRPVy7bDNx4jsxv8HIqVc83w99yLbcoU5YCOhCMlLSkKbUVvR2+DV3CfOOgUgB4tBrW2Ee1Hb+LfqXNQ0Eh7keG5qROdB7yoOoU0rL8KavQ1ey/XbUliPgaD1uMhahOM4XpUWxiU3Pk9HYf3sEe+I3xn/xTPbvrMrkhtGz5lbmkOph6cG/k3cw3cXwXUMeoSQ5ojQqBw1LKOG49ZctUFZP3veEb8zVotnlmsMZiT3vdaaW1jLkN6Vq0Z+evKijF7Z4FmSXK5Eydb6LUK1quBCvEZjovtYx/JeOCa9Dd4VdxBIoUW4beT93cjwu4M3dmVajJHfiRk7gIQbw/BBGF7g31Bs6kxJZMwyzPLimfY2eIpb12PkDFKltM+PxuB8Gbm+7rGOooXhBf4Nxdr27kv3XTWk6TrQOSkjV6DRe7OpcwRn6JUrVDYHdwjxl5TFz57HHGgD36jLvQ3eVba3GJGRK9Do4cwr7RrSaqEDUcYdDF4JVifmjbrc2+Cp91mP1kblju9yhkZq1HscMQIhozM2lqlMb7A1eJbMRourGbBcLyUObDnv1WjdoFzJW08tP6cbTopdLB2kkknIKdReju0Idf7Pyc35ePFMPTy8GQdbRyBns1HLIqpKKqjPDMk3KZ5+jzl1Izbwyjy9FoTz3/DyPAxeSa/o8+cxh6uMSdw1Q5X05KtgKb+tSQnT9SiLlnbjypPdR55GNCs4Y284ZB4Gr/XcirOdvEvi8OwFR9iE8BzHtdb7xPE7hpcjf2sxwrVDgKOGSHhmOYS/50h5SfF2Pfb0SzHM1ugGc5ZGokc0wGJktVfjm8zWAWhdzmgDD5Nd1pMEc2TlaL3KPZ0R+8yZSsHIxs57pJyso/X6kakqCbfFznem1O/Dmpexzx+plNg5z8TE2RRinz1TybSZ2PnOdIalHiIrfP/Y+c6UQ+zzZ+oZora8g95eoaVdsnK2B2RMrLH7Br2zNK+y2gY9/VzPJSexxuJJjBoiydmNgGf6yePH5tT0kK0rDL2owE589DzmYt1ZQkllPpREszxoOX3HEhF40W5uDZ6lkhNisoRocpI2xD2hQpdkAZeQWsEsYTbrmFmtDY3Phgn26L5yhjM9E71K1r6dCTwvCy7twNbgWQbhqbyttvmxjpkcMepYVylX/V6tSG38rL1ca8XPpUadCVi++wwb7Ip65EbCAIfKpRxvDR7hI0sCBiGanPg3oRlL79CSnn8ED7FnKnLJWM6M5DbyXo11yc7JZ5SElXIiI1YDmeo5WJKoSJpIHesMWMOgLbKze3YSe4cnLViGTQhn53R6SIr8cpFLGDzWEJT2PDGaR+4nD8n6clMarpz7p5J7TR5mADc3Y5XeTorRo4DkNhQl381SBnjnZ+NtueelkXvn8WMxlu/EGHNKuI93aK2QGPUjo0TnsCSUmWrseT40+LlhPe6fd8/njsobhtEayqWeEDrO6fBa3ncgeCHrd8qz2XYK9v6fKRfbtoD7/+DxY5SSdjHA9emEYCCAdxIEoZyF33l3oVMZHBa+x7rMn7VPlucchgG49tH4cbi2pW6ltEffwJcoEeNzFHB6sJbso5hSDEPsc0dKOWcqsfOfKfX6PM/Y549UEjqKne9MKT392OfO5EXs3ClKgfcY+2xPWcq29XtQ1jDMsf+jXFjK71qW7xI7T28dgcGLfaanUp477513HPt8rngGXnUpeu97WZpYxxL3nnAlvTl6yF6ZmB8+j0cceZa1sVw79LTOKHkXrbj7KhWWsYnRsHoYRB9iXig5AZTdkuSWFqFMYYd3HDzKUih/HqFLIgHRcOuewcPV3LrvPaExSQltBfc8Fc8NaC3p86lhytzvBZbPlJBqvHPobURTQ2gWD6Qm1BeL8aK37mm4MXSlmZwkxI32fMVrvPMqPMAARx2QPYNHAavRgFlJ8e4gd+UB65iCaENvz3bmeaKWDtgMkQTRjlRvi3Z3lLJDR9/kyHjFUj2USuyzR/Jemid2jSMxvpnSe90bHzlS6zE8dIRlHMeb2DXOlEPs861FKKfk3UPsvL1kJXaunjrLyB1xDA+lEvtsL+2y5+EBDfEI4xI178E64XaP3HmMqfMXLeHJkh6XZdzk7Hq5nYsZvY0R7plQTmnm8Qj1HlqH5WtyVtdHDA3mUHPqTw6HZffI4AFGj7G8XhWZm0/xgCAlS3BNjQKWa0A9BmhrYFlI4GzMN/eco4whH/YYNzDm2KuRpq5g7FLryxGcg/rRy/BxbcLJd1l9BEbcZT+3LB9Nu6gNZZXrH5b/M4MH9BZ7jOflVt7c8bha43d3zSrz3jxzpDHkHGikqXwty0EwTJZxuz3eW0TmZQ+jR6TEO/rSk5Rn6JlA50Vu4hPz63qUF65JBMmyzN8uTExE23ipt5jAbcnsip3rSDVWRreMU6WstoHhj332SCU9fcaAYuc80xGxvz8S5cCb2HVSZIEyzDuwvLscEdnIjW7kwDVqf4e1SidfB2Ln7qmz+mgZp28hC6XzLnPEtXh2VQhJFrELl4rzpjT+e+QWGO+ElUDu4HOK4bVsw4FKiJ3vTHsNL8865/2cNQ5W8Fhi1ztTCZSHWgkJlJ2axm4N7wTV+C7h3J7ErtNTZ98v9pkRVEJ4r7HzlspUZlJCmmuYC8dFvMM1uKT07EqWkSLenOrScr1aIZPcCeiMVZ31anNj6Xy/1qEFxl32JqBy/94hTwsh3JhD6fg11wyhHs93wn0xZkjosQXUe95xbvk+gudB+Tc1XpPAe0p5797lw4PS++Gd0kGi3HjaDMog58suM6WZNXge68HW3CQMHigPxKMS7X15jEnMYNSqYIw9hSy57fMIWZz8DQY3NKaEEI8a1nCvnI8XTUNH757PIL6f93cMn2VyfBhP433xPMMz5f+27y52Tf4+Nr7EedeT7zlnOF+t98N51+9lLymLuZ8h0cbrXkLZKElWwnj2TA4Angfe8llHbQ/eMR2gmsY6vDOeOe+XKMO6kxvKdIxYXQpw32H8f6/sxKC+HiVibcvY+joQ6jucddbX9x/qK4Qjz4JzBR3hVfYBz4x7yyk33N/6O2A8zeXGO5X06OFQwHiJvCzrhpNCXAnG+c4mt1Nnehu4MxhHWTfCGHQaKhon/p3GiqSMHz3/TdwbygMdYMr2WXYqHfzc6V5CCCGEEEIIIYQQQggxP2+99f/GphpBfaXL3gAAAABJRU5ErkJggg==";
