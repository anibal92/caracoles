//===============================================
//					Variables
//===============================================


//===============================================
//					Funciones
//===============================================


//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
	//Instancia de la validación y envío del form;
	$('#loginForm').validate({
		submitHandler: function (form) {
			activarBotonCargando($('#loginButton'));
			$.ajax({
				type: 'post',
				url: path + 'usuario/login',
				dataType: 'json',
				data: {
					usuario: $('#usuario').val(),
					contrasenia: $('#contrasenia').val()
				},
				success: function (resultado) {
					desactivarBotonCargando($('#loginButton'));
					if (resultado.exito) {
						window.location.replace(path);
					}
					else {
						errorServidor(resultado.error);
					}
				},
				error: function (jqXHR, textStatus, thrownError) {
					desactivarBotonCargando($('#loginButton'));
					console.log(thrownError);
					toastr.error('Error ' + jqXHR.status, 'Inicio de sesión');
				},
			});
		},
		rules: {
			usuario: 'required',
			contrasenia: 'required'
		},
		messages: {
			usuario: 'Complete este campo',
			contrasenia: 'Complete este campo',
		},
		errorElement: 'span',
		errorClass: 'help-block',
		highlight: function (element) {
			$(element).parent().addClass('has-error');
		},
		unhighlight: function (element) {
			$(element).parent().removeClass('has-error');
		},
		invalidHandler: function (event, validator) {
			toastr.error('Compruebe los campos');
		},
	});
});
//===============================================
//					Eventos
//===============================================