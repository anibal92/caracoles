//===============================================
//					Variables
//===============================================

//===============================================
//					Funciones
//===============================================

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
  $("#contraseniaForm").validate({
    submitHandler: function (form) {
      activarBotonCargando($("#cambiarButton"));
      $.ajax({
        type: "post",
        url: path + "usuario/contrasenia",
        dataType: "json",
        data: {
          actual: $("#actual").val(),
          contrasenia: $("#contrasenia").val(),
        },
        success: function (resultado) {
          desactivarBotonCargando($("#cambiarButton"));
          if (resultado.exito) {
            toastr.success("Contraseña cambiada");
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($("#cambiarButton"));
          console.log(thrownError);
          toastr.error(
            "Error " + jqXHR.status,
            "Error al cambiar la contraseña"
          );
        },
      });
    },
    rules: {
      actual: "required",
      contrasenia: {
        required: true,
        equalTo: "#contraseniaConfirmar",
      },
      contraseniaConfirmar: {
        required: true,
        equalTo: "#contrasenia",
      },
    },
    messages: {
      actual: "Complete este campo",
      contrasenia: {
        required: "Complete este campo",
        equalTo: "Las contraseñas no coinciden",
      },
      contraseniaConfirmar: {
        required: "Complete este campo",
        equalTo: "Las contraseñas no coinciden",
      },
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });
});

//===============================================
//					Eventos
//===============================================
