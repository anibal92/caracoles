//===============================================
//					Variables
//===============================================
var meses = [
  "Enero",
  "Febrero",
  "Marzo",
  "Abril",
  "Mayo",
  "Junio",
  "Julio",
  "Agosto",
  "Septiembre",
  "Octubre",
  "Noviembre",
  "Diciembre",
];

var balanceData = {
  datasets: [
    {
      label: "Diferencia Ingreso / Egreso",
      backgroundColor: "rgba(60, 141, 188, .8)",
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      order: 1,
      type: "line",
    },
    {
      label: "Ingreso",
      backgroundColor: "rgb(0, 166, 90)",
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      order: 2,
    },
    {
      label: "Egreso",
      backgroundColor: "rgb(255, 40, 0)",
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      order: 2,
    },
  ],
  labels: meses,
};

var balanceData2 = {
  datasets: [
    {
      label: "Balance",
      backgroundColor: "rgba(230, 180, 0, .5)",
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      order: 1,
      type: "line",
    },
    {
      label: "Ingreso",
      backgroundColor: "rgb(0, 166, 90)",
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      order: 2,
    },
    {
      label: "Egreso",
      backgroundColor: "rgb(255, 40, 0)",
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      order: 2,
    },
  ],
  labels: meses,
};

var actualMonth = new Date().getMonth() + 1;
var cantidadMovimientos = 0;

const balanceTotal = document.querySelector("#balanceTotal");
const balanceAnual = document.querySelector("#balanceAnual");
//const balanceMensual = document.querySelector("#balanceMensual");
const ingresosAnual = document.querySelector("#ingresosAnual");
const egresosAnual = document.querySelector("#egresosAnual");

var balanceGrafico = null;

//===============================================
//				Carga de la página
//===============================================

$(document).ready(function () {
  balanceGrafico = new Chart($("#balanceMensualChart"), {
    type: "bar",
    data: balanceData,
    options: {
      responsive: true,
      legend: {
        display: true,
      },
      title: {
        display: true,
        text: "BALANCE ANUAL " + $("#selectAnio").val(),
      },
      scales: {
        xAxes: [
          {
            ticks: {
              beginAtZero: true,
            },
          },
        ],
      },
    },
  });
  balancePorAnio($("#selectAnio").val());
  resumenPorAnio($("#selectAnio").val());
});

//===============================================
//				Funciones
//===============================================
function balancePorAnio(anio) {
  balanceData.datasets[0].data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  balanceData.datasets[1].data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  balanceData.datasets[2].data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  $.ajax({
    type: "post",
    url: path + "finanzas/graficos",
    datatype: "json",
    data: { anio: anio },
    success: function (resultado) {
      $("#selectAnio").prop("disabled", false);
      if (resultado.exito) {
        balanceData.datasets[0].data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        balanceData.datasets[1].data = resultado.ingresos;
        balanceData.datasets[2].data = resultado.egresos;

        for (let index = 0; index < 12; index++) {
          balanceData.datasets[0].data[index] =
            balanceData.datasets[1].data[index] -
            balanceData.datasets[2].data[index];
        }

        balanceGrafico.update();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      $("#selectAnio").prop("disabled", false);
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al obtener los datos");
    },
  });
}

function resumenPorAnio(anio) {
  $.ajax({
    type: "post",
    url: path + "finanzas/resumen",
    datatype: "json",
    data: { anio: anio },
    success: function (resultado) {
      if (resultado.exito) {
        balanceAnual.querySelector(
          ".inner h3"
        ).textContent = `$ ${resultado.anual}`;
        balanceAnual.querySelector(
          ".inner h4"
        ).textContent = `Balance año ${resultado.anio}`;

        ingresosAnual.querySelector(
          ".inner h3"
        ).textContent = `$ ${resultado.anualIngresos}`;
        ingresosAnual.querySelector(
          ".inner h4"
        //).textContent = `Ingresos mes de ${meses[resultado.mes - 1]}`;
        ).textContent = `Ingresos año ${resultado.anio}`;

        egresosAnual.querySelector(
          ".inner h3"
        ).textContent = `$ ${resultado.anualEgresos}`;
        egresosAnual.querySelector(
          ".inner h4"
        ).textContent = `Egresos año ${resultado.anio}`;
      } else {
        errorServidor(resultado.error);
      }

      actualizarBalanceAnualWidget(resultado.anual);
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al obtener los datos");
    },
  });
}

function actualizarResultadosMensuales() {
  let resultado = [];
  for (let i = 0; i < balanceData.datasets[0].data.length; i++) {
    resultado[i] =
      balanceData.datasets[1].data[i] - balanceData.datasets[2].data[i];
  }
  balanceData.datasets[0].data = resultado;
}

function actualizarBalanceAnualWidget(balance) {
  
  if (balance < 0) {
    balanceAnual.classList.remove("bg-green", "bg-yellow");
    balanceAnual.classList.add("bg-red");
  } else if (balance < 1000) {
    balanceAnual.classList.remove("bg-green");
    balanceAnual.classList.add("bg-yellow");
  } else {
    balanceAnual.classList.remove("bg-yellow", "bg-red");
    balanceAnual.classList.add("bg-green");
  }
}

$("#selectAnio").change(function () {
  $("#selectAnio").prop("disabled", true);
  balancePorAnio($("#selectAnio").val());
  resumenPorAnio($("#selectAnio").val());
});
