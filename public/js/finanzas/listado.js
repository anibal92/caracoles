//===============================================
//					Variables
//===============================================
var movimientos;
const date = new Date();
let fechaActual =
  date.getDate() +
  "-" +
  (date.getMonth() + 1) +
  "-" +
  date.getFullYear() +
  " " +
  date.getHours() +
  ":" +
  date.getMinutes() +
  ":" +
  date.getSeconds();

var tablaMovimientos = $("#movimientosTable").DataTable({
  bLengthChange: false,
  info: false,
  language: {
    loadingRecords: '<div class="loader centrado"></div>',
    emptyTable: "No hay resultados.",
    paginate: {
      next: "Siguiente",
      previous: "Anterior",
    },
    search: "Buscar:",
  },
  responsive: true,
  dom: "Bftp",
  buttons: [
    {
      extend: "excelHtml5",
      text: '<i class="fa fa-file-excel-o"></i>',
      titleAttr: "Exportar a Excel",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2, 3],
      },
      title: "Listado de movimientos - FINANZAS",
      filename: "Listado de movimientos - FINANZAS" + " " + fechaActual,
    },
    {
      extend: "pdfHtml5",
      text: '<i class="fa fa-file-pdf-o"></i> ',
      titleAttr: "Exportar a Pdf",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2, 3],
      },
      title: "   ",
      filename: "Listado de movimientos" + " " + fechaActual,
      customize: function (doc) {
        doc["header"] = function () {
          return {
            columns: [
              {
                margin: [30, 10, 2, 5],
                alignment: "left",
                image: logoBase64,
                width: 100,
              },
              {
                margin: [0, 30, 100, 0],
                alignment: "right",
                text: "Listado de movimientos", //TITULO DEL INFORME
                fontSize: 20,
                bold: false,
              },
            ],
          };
        };
        
        doc.content[1].table.widths = ["25%", "25%", "25%", "25%"];
        (doc.download = "open"), (doc.pageMargins = [30, 80, 20, 30]);
        doc.styles.tableHeader = {
          fontSize: "12",
          alignment: "left",
          fillColor: "#545454",
          color: "#FFFFFF",
        };

        doc.content.splice(0, 1, {
          alignment: "left",
          text: [
            "\n",
            "\n",
            "\n",
            {
              text: "Fecha: " + fechaActual,
              fontSize: 12,
              bold: false,
            },
            "\n",
            "\n",
          ],
        });

        

        doc["footer"] = function (page, pages) {
          return [
            //para vertical x2: 580 ---- para horizontal x2:820
            {
              canvas: [
                { type: "line", x1: 20, y1: 6, x2: 580, y2: 6, lineWidth: 1 },
              ],
            },
            {
              columns: [
                {
                  text: "Centro Educativo Caracoles",
                  fontSize: 9,
                  bold: true,
                },
                {
                  alignment: "right",
                  text: [
                    {
                      text:
                        "Página " + page.toString() + " de " + pages.toString(),
                      fontSize: 9,
                      bold: true,
                    },
                  ],
                },
              ],
              margin: [20, 0],
            },
          ];
        };
      },
    },
  ],
  ordering: true,
  searching: true,
  order: [[0, "desc"]],
  ajax: {
    type: "post",
    url: path + "finanzas/listado",
    dataType: "json",
    dataSrc: function (resultado) {
      if (resultado.exito) {
        movimientos = resultado.movimientos;
        $.each(resultado.movimientos, function (indice, movimiento) {
          movimiento.btn = '<div class="btn-group">';
          movimiento.btn +=
            '<button type="button" class="btn btn-danger eliminarMovimientoButton" value="' +
            movimiento.id +
            '"><i class="fa fa-trash"></i></button>';
          movimiento.btn += "</div>";
        });

        return resultado.movimientos;
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al listar");
    },
  },
  columns: [
    { data: "tipo" },
    { data: "monto" },
    { data: "descripcion" },
    { data: "fecha" },
    { data: "btn" },
  ],
});

//===============================================
//				Carga de la página
//===============================================

$(document).ready(function () {
  $("#btnIn").children(".fa-spin").hide();
  $("#btnEg").children(".fa-spin").hide();

  var tipo;
  var submitBtn;
  // Evento que maneja el tipo del movimiento, dependiendo de cual botón dispare el submit del form
  $('#movimientoForm button[type="button"]').on("click", function (e) {
    if (e.target.getAttribute("id") === "btnIn") {
      tipo = "INGRESO";
      submitBtn = e.target;
      $("#movimientoForm").submit();
      console.log(tipo, submitBtn);
    } else {
      tipo = "EGRESO";
      submitBtn = e.target;
      $("#movimientoForm").submit();
      console.log(tipo, submitBtn);
    }
  });

  $("#movimientoForm").validate({
    submitHandler: function (form) {
      $(submitBtn).children(".fa-fw").hide();
      activarBotonCargando($(submitBtn));

      var url = path + "finanzas/alta";
      var data = {
        monto: $("#monto").val(),
        descripcion: $("#descripcion").val(),
        fecha: $("#fecha").val(),
        tipo: tipo,
      };

      $.ajax({
        type: "post",
        url: url,
        dataType: "json",
        data: data,
        success: function (resultado) {
          desactivarBotonCargando($(submitBtn));
          $(submitBtn).children(".fa-fw").show();
          if (resultado.exito) {
            tablaMovimientos.ajax.reload();
            toastr.success("Movimiento guardado con éxito.");
            document.querySelector("#movimientoForm").reset();
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($(submitBtn));
          $(submitBtn).children(".fa-fw").show();
          console.log(thrownError);
          toastr.error("Error " + jqXHR.status, "Error guardar el movimiento");
        },
      });
    },
    rules: {
      monto: "required",
      fecha: "required",
      usuario: "required",
      //tipo: "required",
      descripcion: "required",
    },
    messages: {
      monto: "Campo requerido",
      fecha: "Campo requerido",
      usuario: "Campo requerido",
      //tipo: "Campo requerido",
      descripcion: "Campo requerido",
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });
});

//===============================================
//					Eventos
//===============================================

$("#movimientosTable").on("click", ".eliminarMovimientoButton", function () {
  $("#confirmarEliminarMovimientoButton").val($(this).val());
  $("#eliminarMovimientoModal").modal("show");
});

$("#confirmarEliminarMovimientoButton").click(function () {
  activarBotonCargando($(this));
  $.ajax({
    type: "post",
    url: path + "finanzas/eliminar",
    dataType: "json",
    data: {
      id: $(this).val(),
    },
    success: function (resultado) {
      desactivarBotonCargando($("#confirmarEliminarMovimientoButton"));
      $(this).val("");
      $("#eliminarMovimientoModal").modal("hide");

      if (resultado.exito) {
        toastr.success("Elemento eliminado.");
        tablaMovimientos.ajax.reload();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      $(this).val("");
      $("#eliminarMovimientoModal").modal("hide");
      desactivarBotonCargando($("#confirmarEliminarMovimientoButton"));
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al eliminar");
    },
  });
});
