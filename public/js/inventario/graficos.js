//===============================================
//					Variables
//===============================================
var procedenciaData = {
  labels: ["COMPRA", "DONACIÓN", "OTRO"],
  datasets: [
    {
      label: "Procedencia",
      data: [],
      backgroundColor: ["#00BFFF", "#4682B4", "#9370DB"],
      hoverOffset: 4,
    },
  ],
};

var estadoData = {
  labels: [],
  datasets: [
    {
      label: "Estado",
      data: [],
      backgroundColor: ["#00BFFF", "#32CD32", "#9370DB", "#DC143C"],
      hoverOffset: 4,
    },
  ],
};

var participantesData = {
  labels: [],
  datasets: [
    {
      label: "Total",
      backgroundColor: "rgb(255, 205, 86)",
      borderColor: "rgb(255, 205, 86)",
      borderWidth: 1,
      data: [],
    },
  ],
};

var participantesGrafico = null;

//===============================================
//					Funciones
//===============================================

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
  procedenciaGrafico = new Chart($("#procedenciaCanvas"), {
    type: "pie",
    data: procedenciaData,
    options: {
      responsive: true,
      legend: {
        position: "left",
      },
      title: {
        display: false,
        text: "Distribución por procedencia",
      },
    },
  });

  estadoGrafico = new Chart($("#estadoCanvas"), {
    type: "pie",
    data: estadoData,
    options: {
      responsive: true,
      legend: {
        position: "left",
      },
      title: {
        display: false,
        text: "Distribución por estado",
      },
    },
  });

  $.ajax({
    type: "post",
    url: path + "inventario/graficos",
    dataType: "json",
    data: {},
    success: function (resultado) {
      //console.log(resultado);
      if (resultado.exito) {
        $(".loader").hide();
        $("#contenido").show();
        procedenciaData.datasets[0].data = resultado.procedencia;
        var totalProcedencia =
          procedenciaData.datasets[0].data[0] +
          procedenciaData.datasets[0].data[1] +
          procedenciaData.datasets[0].data[2];
        var porcentajeCompra =
          (procedenciaData.datasets[0].data[0] * 100) / totalProcedencia;
        var porcentajeDonacion =
          (procedenciaData.datasets[0].data[1] * 100) / totalProcedencia;
        var porcentajeOtros =
          (procedenciaData.datasets[0].data[2] * 100) / totalProcedencia;
        procedenciaData.labels[0] =
          procedenciaData.labels[0] + " " + porcentajeCompra.toFixed(2) + "%";
        procedenciaData.labels[1] =
          procedenciaData.labels[1] + " " + porcentajeDonacion.toFixed(2) + "%";
        procedenciaData.labels[2] =
          procedenciaData.labels[2] + " " + porcentajeOtros.toFixed(2) + "%";
        procedenciaGrafico.update();
        estadoData.datasets[0].data = resultado.estado.data;
        // Capitalize labels array
        const labels = resultado.estado.labels;
        estadoData.labels = resultado.estado.labels;
        estadoGrafico.update();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al obtener los datos.");
    },
  });
});

//===============================================
//					Eventos
//===============================================
