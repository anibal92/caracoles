//===============================================
//					Variables
//===============================================
var materiales;
var lote;
const date = new Date();
let fechaActual =
  date.getDate() +
  "-" +
  (date.getMonth() + 1) +
  "-" +
  date.getFullYear() +
  " " +
  date.getHours() +
  ":" +
  date.getMinutes() +
  ":" +
  date.getSeconds();

var tablaMateriales = $("#materialesTable").DataTable({
  bLengthChange: false,
  info: true,
  language: {
    loadingRecords: '<div class="loader centrado"></div>',
    emptyTable: "No hay resultados.",
    paginate: {
      next: "Siguiente",
      previous: "Anterior",
    },
    search: "Buscar:",
    info: "Total: _TOTAL_",
  },
  responsive: true,
  dom: "Blfrtip",
  buttons: [
    {
      extend: "excelHtml5",
      exportOptions: {
        columns: [0, 1, 2, 3],
      },
      text: '<i class="fa fa-file-excel-o"></i>',
      titleAttr: "Exportar a Excel",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2, 3],
      },
      title: "Listado de materiales - INVENTARIO",
      filename: "Listado de materiales - INVENTARIO" + " " + fechaActual,
    },
    {
      extend: "pdfHtml5",
      exportOptions: {
        columns: [0, 1, 2, 3],
      },
      text: '<i class="fa fa-file-pdf-o"></i> ',
      titleAttr: "Exportar a Pdf",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2, 3],
      },
      title: "   ",
      filename: "Listado de materiales" + " " + fechaActual,
      customize: function (doc) {
        doc.content[1].table.widths = ["25%", "25%", "25%", "25%"];
        (doc.download = "open"), (doc.pageMargins = [30, 80, 20, 30]);
        doc.styles.tableHeader = {
          fontSize: "12",
          alignment: "left",
          fillColor: "#545454",
          color: "#FFFFFF",
        };

        doc.content.splice(1, 0, {
          alignment: "left",
          text: [
            "\n",
            "\n",
            "\n",
            {
              text: "Fecha: " + fechaActual,
              fontSize: 12,
              bold: false,
            },
            "\n",
            "\n",
          ],
        });

        doc["header"] = function () {
          return {
            columns: [
              {
                margin: [30, 10, 2, 5],
                alignment: "left",
                image: logoBase64,
                width: 100,
              },
              {
                margin: [0, 30, 100, 0],
                alignment: "right",
                text: "Listado de materiales", //TITULO DEL INFORME
                fontSize: 20,
                bold: false,
              },
            ],
          };
        };

        doc["footer"] = function (page, pages) {
          return [
            //para vertical x2: 580 ---- para horizontal x2:820
            {
              canvas: [
                { type: "line", x1: 20, y1: 6, x2: 580, y2: 6, lineWidth: 1 },
              ],
            },
            {
              columns: [
                {
                  text: "Centro Educativo Caracoles",
                  fontSize: 9,
                  bold: true,
                },
                {
                  alignment: "right",
                  text: [
                    {
                      text:
                        "Página " + page.toString() + " de " + pages.toString(),
                      fontSize: 9,
                      bold: true,
                    },
                  ],
                },
              ],
              margin: [20, 0],
            },
          ];
        };
      },
    },
  ],
  ordering: true,
  searching: true,
  order: [[0, "desc"]],
  ajax: {
    type: "post",
    url: path + "inventario/material/listado",
    dataType: "json",
    dataSrc: function (resultado) {
      if (resultado.exito) {
        materiales = resultado.materiales;
        $.each(resultado.materiales, function (indice, material) {
          material.btn = '<div class="btn-group">';
          material.btn +=
            '<button type="button" class="btn btn-primary verMaterialButton" title="Ver material" value = "' +
            indice +
            '"><i class="fa fa-eye"></i></button>';
          //material.btn += '<button type="button" class="btn btn-primary asignarMaterialButton" value = "' + material.id + '"><i class="fa fa-users"></i></button>';
          material.btn +=
            '<button type="button" class="btn btn-danger eliminarMaterialButton" value="' +
            material.id +
            '"><i class="fa fa-trash"></i></button>';
          material.btn += "</div>";
        });

        return resultado.materiales;
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al listar");
    },
  },
  columnDefs: [{ width: "12%", targets: [4] }],
  columns: [
    { data: "codigo" },
    { data: "nombre" },
    { data: "categoria" },
    { data: "estado" },
    { data: "btn" },
  ],
});

function cursosPorMaterial(materialId) {
  $.ajax({
    type: "post",
    url: path + "material/cursos",
    dataType: "json",
    data: {
      materialId: materialId,
    },
    success: function (resultado) {
      if (resultado.exito) {
        tablaParticipantes.clear().draw();
        $.each(resultado.participantes, function (indice, participante) {
          var btn = '<div class="btn-group">';
          btn +=
            '<button type="button" class="btn btn-primary quitarParticipanteButton" value = "' +
            participante.id +
            '"><i class="fa fa-eye"></i></button>';
          btn += "</div>";

          tablaParticipantes.row.add([
            participante.documento,
            participante.apellido,
            participante.nombres,
            btn,
          ]);
        });

        tablaParticipantes.draw();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al listar");
    },
  });
}

//===============================================
//					Funciones
//===============================================

/*function participantesPorMaterial(cursoId) {
	$.ajax({
		type: 'post',
		url: path + 'cursos/participantes',
		dataType: 'json',
		data: {
			cursoId: cursoId
		},
		success: function (resultado) {
			if (resultado.exito) {
				tablaParticipantes.clear().draw();
				$.each(resultado.participantes, function (indice, participante) {
					var btn = '<div class="btn-group">';
					btn += '<button type="button" class="btn btn-primary quitarParticipanteButton" value = "' + participante.id + '"><i class="fa fa-eye"></i></button>';
					btn += '</div>';

					tablaParticipantes.row.add([
						participante.documento,
						participante.apellido,
						participante.nombres,
						'',
					]);
				});

				tablaParticipantes.draw();
			}
			else {
				errorServidor(resultado.error);
			}
		},
		error: function (jqXHR, textStatus, thrownError) {
			console.log(thrownError);
			toastr.error('Error ' + jqXHR.status, 'Error al listar');
		},
	});
}*/

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
  //Carga de los combos
  cargarComboGET($("#categoria"), "base/categorias");
  cargarComboGET($("#estado"), "base/estados/material");
  cargarComboGET($("#proyecto"), "base/proyectos");

  //Instancia de la validación y envío del material;
  $("#materialForm").validate({
    submitHandler: function (form) {
      activarBotonCargando($("#guardarMaterialButton"));

      var url = path + "inventario/material/alta";
      var data = {
        codigo: $("#codigo").val(),
        nombre: $("#nombre").val(),
        categoria: $("#categoria").val(),
        estado: $("#estado").val(),
        procedencia: $("#procedencia").val(),
        proyecto: $("#proyecto").val(),
        descripcion: $("#descripcion").val(),
      };

      if ($("#guardarMaterialButton").val() != "") {
        url = path + "inventario/material/editar";
        data.id = $("#guardarMaterialButton").val();
      }

      $.ajax({
        type: "post",
        url: url,
        dataType: "json",
        data: data,
        success: function (resultado) {
          desactivarBotonCargando($("#guardarMaterialButton"));
          if (resultado.exito) {
            tablaMateriales.ajax.reload();
            toastr.success("Material guardado con éxito.");
            $("#materialModal").modal("hide");
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($("#guardarMaterialButton"));
          console.log(thrownError);
          toastr.error("Error " + jqXHR.status, "Error guardar el material");
        },
      });
    },
    rules: {
      nombre: "required",
      categoria: "required",
      estado: "required",
      procedencia: "required",
    },
    messages: {
      nombre: "Campo requerido",
      categoria: "Campo requerido",
      estado: "Campo requerido",
      procedencia: "Campo requerido",
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });

  /*$('#csv').parse({
		config: {
			// base config to use for each file
		},
		before: function(file, inputElem)
		{
			// executed before parsing each file begins;
			// what you return here controls the flow
		},
		error: function(err, file, inputElem, reason)
		{
			// executed if an error occurs while loading the file,
			// or if before callback aborted for some reason
		},
		complete: function()
		{
			// executed after all files are complete
		}
	});*/
});

//===============================================
//					Eventos
//===============================================

$("#materialesTable").on("click", ".verMaterialButton", function () {
  var material = materiales[$(this).val()];

  $("#codigo").val(material.codigo);
  $("#nombre").val(material.nombre);
  $("#estado").val(material.estadoId);
  $("#categoria").val(material.categoriaId);
  $("#procedencia").val(material.procedencia);
  $("#proyecto").val(material.proyectoId);
  $("#descripcion").val(material.descripcion);
  $("#guardarMaterialButton").val(material.id);

  $("#materialModal").modal("show");
});

$("#materialesTable").on("click", ".eliminarMaterialButton", function () {
  $("#confirmarEliminarMaterialButton").val($(this).val());
  $("#eliminarMaterialModal").modal("show");
});

$("#confirmarEliminarMaterialButton").click(function () {
  activarBotonCargando($(this));
  $.ajax({
    type: "post",
    url: path + "inventario/material/eliminar",
    dataType: "json",
    data: {
      id: $(this).val(),
    },
    success: function (resultado) {
      desactivarBotonCargando($("#confirmarEliminarMaterialButton"));
      $(this).val("");
      $("#eliminarMaterialModal").modal("hide");

      if (resultado.exito) {
        toastr.success("Elemento eliminado.");
        tablaMateriales.ajax.reload();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      $(this).val("");
      $("#eliminarMaterialModal").modal("hide");
      desactivarBotonCargando($("#confirmarEliminarMaterialButton"));
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al eliminar");
    },
  });
});

$("#materialModal").on("hidden.bs.modal", function () {
  $("#codigo").val("").prop("disabled", false);
  $("#nombre").val("").prop("disabled", false);
  $("#estado").val("").prop("disabled", false);
  $("#categoria").val("").prop("disabled", false);
  $("#procedencia").val("COMPRA").prop("disabled", false);
  $("#proyecto").val("").prop("disabled", false);
  $("#descripcion").val("").prop("disabled", false);
  $("#guardarMaterialButton").val("").prop("disabled", false);
});

$("#participantesModal").on("hidden.bs.modal", function () {
  $("#participante").val("");
  $("#cursoId").val("");
});

$("#csvModal").on("hidden.bs.modal", function () {
  $("#csv").val("");
  $("#textoCSV").html("");
  lote = null;
});

$("#csv").change(function (evt) {
  Papa.parse(evt.target.files[0], {
    header: true,
    dynamicTyping: true,
    complete: function (results) {
      lote = results.data;
      $("#textoCSV").html(
        "<p>Se cargarán " + (lote.length - 1) + " elementos."
      );
      $("#cargaCsvButton").prop("disabled", false);
    },
  });
});

$("#cargaCsvButton").click(function () {
  activarBotonCargando($(this));
  console.log(lote);
  $.ajax({
    type: "post",
    url: path + "inventario/material/lote",
    dataType: "json",
    data: {
      lote: lote,
      total: lote.length - 1,
    },
    success: function (resultado) {
      desactivarBotonCargando($("#cargaCsvButton"));

      if (resultado.exito) {
        tablaMateriales.ajax.reload();

        if (resultado.fallo.length > 0) {
          $("#cargaCsvButton").prop("disabled", true);
          toastr.warning("Algunos elementos no se cargaron.");
          var msj =
            "<br><h5>Los siguientes elementos no se guardaron: </h5><br>";
          $.each(resultado.fallo, function (indice, elemento) {
            msj += "<p>" + indice + ": " + elemento.nombre + "</p>";
            msj += "<p>" + elemento.motivo + "<p>";
          });

          $("#textoCSV").html(msj);
        } else {
          toastr.success("Elementos cargados.");
          $("#csvModal").modal("hide");
        }
      } else {
        $("#csvModal").modal("hide");
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      $(this).val("");
      $("#csvModal").modal("hide");
      desactivarBotonCargando($("#cargaCsvButton"));
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al cargar");
    },
  });
});
