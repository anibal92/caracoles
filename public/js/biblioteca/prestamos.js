//===============================================
//					Variables
//===============================================
var prestamos;

const date = new Date();
let fechaActual =
  date.getDate() +
  "-" +
  (date.getMonth() + 1) +
  "-" +
  date.getFullYear() +
  " " +
  date.getHours() +
  ":" +
  date.getMinutes() +
  ":" +
  date.getSeconds();

var tablaPrestamos = $("#prestamosTable").DataTable({
  bLengthChange: false,
  info: false,
  language: {
    loadingRecords: '<div class="loader centrado"></div>',
    emptyTable: "No hay resultados.",
    paginate: {
      next: "Siguiente",
      previous: "Anterior",
    },
    search: "Buscar:",
  },
  responsive: true,
  dom: "Blfrtip",
  buttons: [
    {
      extend: "excelHtml5",
      text: '<i class="fa fa-file-excel-o"></i>',
      titleAttr: "Exportar a Excel",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2, 3, 4],
      },
      title: "Listado de préstamos y devoluciones - BIBLIOTECA",
      filename: "Listado de préstamos y devoluciones - BIBLIOTECA" + " " + fechaActual,
    },
    {
      extend: "pdfHtml5",
      text: '<i class="fa fa-file-pdf-o"></i> ',
      titleAttr: "Exportar a Pdf",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2, 3, 4],
      },
      title: "   ",
      filename: "Listado de préstamos y devoluciones" + " " + fechaActual,
      customize: function (doc) {
        doc.content[1].table.widths = ["15%", "30%", "30%", "12.5%", "12.5%"];
        (doc.download = "open"), (doc.pageMargins = [30, 80, 20, 30]);
        doc.styles.tableHeader = {
          fontSize: "12",
          alignment: "left",
          fillColor: "#545454",
          color: "#FFFFFF",
        };

        doc.content.splice(1, 0, {
          alignment: "left",
          text: [
            "\n",
            "\n",
            "\n",
            {
              text: "Fecha: " + fechaActual,
              fontSize: 12,
              bold: false,
            },
            "\n",
            "\n",
          ],
        });

        doc["header"] = function () {
          return {
            columns: [
              {
                margin: [30, 10, 2, 5],
                alignment: "left",
                image: logoBase64,
                width: 100,
              },
              {
                margin: [0, 30, 100, 0],
                alignment: "right",
                text: "Listado de préstamos y devoluciones", //TITULO DEL INFORME
                fontSize: 20,
                bold: false,
              },
            ],
          };
        };

        doc["footer"] = function (page, pages) {
          return [
            //para vertical x2: 580 ---- para horizontal x2:820
            {
              canvas: [
                { type: "line", x1: 20, y1: 6, x2: 580, y2: 6, lineWidth: 1 },
              ],
            },
            {
              columns: [
                {
                  text: "Centro Educativo Caracoles",
                  fontSize: 9,
                  bold: true,
                },
                {
                  alignment: "right",
                  text: [
                    {
                      text:
                        "Página " + page.toString() + " de " + pages.toString(),
                      fontSize: 9,
                      bold: true,
                    },
                  ],
                },
              ],
              margin: [20, 0],
            },
          ];
        };
      },
    },
  ],
  ordering: true,
  searching: true,
  order: [[4, "asc"]],
  ajax: {
    type: "post",
    url: path + "biblioteca/prestamo/listado",
    dataType: "json",
    dataSrc: function (resultado) {
      if (resultado.exito) {
        prestamos = resultado.prestamos;
        $.each(resultado.prestamos, function (indice, prestamo) {
          var devolucionDisabled =
            prestamo.devolucion != "" && prestamo.devolucion != null
              ? "disabled"
              : "";
          prestamo.btn = '<div class="btn-group">';
          prestamo.btn +=
            '<button type="button" class="btn btn-primary verPrestamoButton" title="Ver préstamo" value = "' +
            indice +
            '"><i class="fa fa-eye"></i></button>';
          prestamo.btn +=
            '<button type="button" class="btn btn-primary editarPrestamoButton" title="Editar préstamo" value = "' +
            indice +
            '"' +
            devolucionDisabled +
            '><i class="fa fa-pencil"></i></button>';
          prestamo.btn +=
            '<button type="button" class="btn btn-primary devolucionButton" title="Devolución" value = "' +
            indice +
            '"' +
            devolucionDisabled +
            '><i class="fa fa-mail-forward"></i></button>';
          prestamo.btn +=
            '<button type="button" class="btn btn-danger eliminarPrestamoButton" value="' +
            prestamo.id +
            '"><i class="fa fa-trash"></i></button>';
          prestamo.btn += "</div>";
        });

        return resultado.prestamos;
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al listar");
    },
  },
  columnDefs: [
    { width: "8%", targets: [0, 3, 4, 5] },
    { width: "15%", targets: [1] },
    { width: "15%", targets: [2] },
  ],
  columns: [
    { data: "ejemplar.codigo" },
    { data: "ejemplar.titulo" },
    { data: "participante.nombre" },
    { data: "fechaFormato" },
    { data: "devolucionFormato" },
    { data: "btn" },
  ],
});

//===============================================
//					Funciones
//===============================================

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
  //Carga de los combos
  cargarComboGET($("#participante"), "base/participantes");

  //Instancia de la validación y envío del prestamo
  $("#prestamoForm").validate({
    submitHandler: function (form) {
      activarBotonCargando($("#guardarPrestamoButton"));

      $.ajax({
        type: "post",
        url: path + "biblioteca/prestamo/editar",
        dataType: "json",
        data: {
          id: $("#guardarPrestamoButton").val(),
          ejemplar: $("#ejemplarId").val(),
          participante: $("#participante").val(),
          fecha: $("#fecha").val(),
        },
        success: function (resultado) {
          desactivarBotonCargando($("#guardarPrestamoButton"));
          if (resultado.exito) {
            tablaPrestamos.ajax.reload();
            toastr.success("Prestamos guardado con éxito.");
            $("#prestamoModal").modal("hide");
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($("#guardarPrestamoButton"));
          console.log(thrownError);
          toastr.error("Error " + jqXHR.status, "Error guardar el prestamo");
        },
      });
    },
    rules: {
      fecha: "required",
      participante: "required",
    },
    messages: {
      fecha: "Campo requerido",
      participante: "Campo requerido",
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });

  //Instancia de la validación y envío de la devolucion
  $("#devolucionForm").validate({
    submitHandler: function (form) {
      activarBotonCargando($("#guardarDevolucionButton"));

      $.ajax({
        type: "post",
        url: path + "biblioteca/prestamo/devolucion",
        dataType: "json",
        data: {
          id: $("#guardarDevolucionButton").val(),
          fecha: $("#fechaDevolucion").val(),
          observacion: $("#observacionDevolucion").val(),
          estado: $("#estadoDevolucion").val(),
        },
        success: function (resultado) {
          desactivarBotonCargando($("#guardarDevolucionButton"));
          if (resultado.exito) {
            tablaPrestamos.ajax.reload();
            toastr.success("Devolucion guardada con éxito.");
            $("#devolucionModal").modal("hide");
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($("#guardarDevolucionButton"));
          console.log(thrownError);
          toastr.error("Error " + jqXHR.status, "Error guardar la devolucion");
        },
      });
    },
    rules: {
      fechaDevolucion: "required",
      estadoDevolucion: "required",
    },
    messages: {
      fechaDevolucion: "Campo requerido",
      estadoDevolucion: "Campo requerido",
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });
});

//===============================================
//					Eventos
//===============================================
$("#prestamosTable").on("click", ".verPrestamoButton", function () {
  var prestamo = prestamos[$(this).val()];
  $("#ejemplarVer").val(
    prestamo.ejemplar.codigo + "-" + prestamo.ejemplar.titulo
  );
  $("#participanteVer").val(prestamo.participante.nombre);
  $("#estadoVer").val(prestamo.estado);
  $("#fechaPrestamoVer").val(prestamo.fecha);
  $("#fechaDevolucionVer").val(prestamo.devolucion);
  $("#observacionVer").val(prestamo.observacion);

  $("#verPrestamoModal").modal("show");
});

$("#prestamosTable").on("click", ".editarPrestamoButton", function () {
  var prestamo = prestamos[$(this).val()];
  $("#ejemplarId").val(prestamo.ejemplar.id);
  $("#ejemplar").val(prestamo.ejemplar.codigo + "-" + prestamo.ejemplar.titulo);
  $("#participante").val(prestamo.participante.id);
  $("#fecha").val(prestamo.fecha);
  $("#guardarPrestamoButton").val(prestamo.id);

  $("#prestamoModal").modal("show");
});

$("#prestamosTable").on("click", ".devolucionButton", function () {
  var prestamo = prestamos[$(this).val()];
  $("#ejemplarDevolucion").val(
    prestamo.ejemplar.codigo + "-" + prestamo.ejemplar.titulo
  );
  $("#participanteDevolucion").val(prestamo.participante.nombre);
  $("#estadoDevolucion").val(prestamo.estado);
  $("#guardarDevolucionButton").val(prestamo.id);

  $("#devolucionModal").modal("show");
});

$("#prestamosTable").on("click", ".eliminarPrestamoButton", function () {
  $("#confirmarEliminarPrestamoButton").val($(this).val());
  $("#eliminarPrestamoModal").modal("show");
});

$("#confirmarEliminarPrestamoButton").click(function () {
  activarBotonCargando($(this));
  $.ajax({
    type: "post",
    url: path + "biblioteca/prestamo/eliminar",
    dataType: "json",
    data: {
      id: $(this).val(),
    },
    success: function (resultado) {
      desactivarBotonCargando($("#confirmarEliminarPrestamoButton"));
      $(this).val("");
      $("#eliminarPrestamoModal").modal("hide");

      if (resultado.exito) {
        toastr.success("Elemento eliminado.");
        tablaPrestamos.ajax.reload();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      $(this).val("");
      $("#eliminarPrestamoModal").modal("hide");
      desactivarBotonCargando($("#confirmarEliminarPrestamoButton"));
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al eliminar");
    },
  });
});

$("#prestamoModal").on("hidden.bs.modal", function () {
  $("#ejemplarId").val("");
  $("#ejemplarId").val("");
  $("#participante").val("");
  $("#fecha").val("");
  $("#guardarPrestamoButton").val("");
});

$("#verPrestamoModal").on("hidden.bs.modal", function () {
  $("#ejemplarId").val("");
  $("#ejemplarId").val("");
  $("#participante").val("");
  $("#fecha").val("");
  $("#guardarPrestamoButton").val("");
});

$("#prestamoModal").on("hidden.bs.modal", function () {
  $("#ejemplarVer").val("");
  $("#participanteVer").val("");
  $("#estadoVer").val("");
  $("#fechaPrestamoVer").val("");
  $("#fechaDevolucionVer").val("");
  $("#observacionVer").val("");
});
