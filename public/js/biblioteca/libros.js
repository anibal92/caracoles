//===============================================
//					Variables
//===============================================
var libros;
var ejemplares;
var cantAutoresSelect;
const date = new Date();
let fechaActual =
  date.getDate() +
  "-" +
  (date.getMonth() + 1) +
  "-" +
  date.getFullYear() +
  " " +
  date.getHours() +
  ":" +
  date.getMinutes() +
  ":" +
  date.getSeconds();

var tablaLibros = $("#librosTable").DataTable({
  bLengthChange: false,
  info: true,
  language: {
    loadingRecords: '<div class="loader centrado"></div>',
    emptyTable: "No hay resultados.",
    paginate: {
      next: "Siguiente",
      previous: "Anterior",
    },
    search: "Buscar:",
    info: "Total: _TOTAL_",
  },
  responsive: true,
  dom: "Blfrtip",
  buttons: [
    {
      extend: "excelHtml5",
      text: '<i class="fa fa-file-excel-o"></i>',
      titleAttr: "Exportar a Excel",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2, 3, 4],
      },
      title: "Listado de libros - BIBLIOTECA",
      filename: "Listado de libros - BIBLIOTECA" + " " + fechaActual,
    },
    {
      extend: "pdfHtml5",
      text: '<i class="fa fa-file-pdf-o"></i> ',
      titleAttr: "Exportar a Pdf",
      className: "btn btn-default btn-sm",
      exportOptions: {
        columns: [0, 1, 2, 3, 4],
      },
      title: "   ",
      filename: "Listado de libros" + " " + fechaActual,
      customize: function (doc) {
        doc.content[1].table.widths = ["25%", "25%", "25%", "12.5%", "12.5%"];
        (doc.download = "open"), (doc.pageMargins = [30, 80, 20, 30]);
        doc.styles.tableHeader = {
          fontSize: "12",
          alignment: "left",
          fillColor: "#545454",
          color: "#FFFFFF",
        };

        doc.content.splice(1, 0, {
          alignment: "left",
          text: [
            "\n",
            "\n",
            "\n",
            {
              text: "Fecha: " + fechaActual,
              fontSize: 12,
              bold: false,
            },
            "\n",
            "\n",
          ],
        });

        doc["header"] = function () {
          return {
            columns: [
              {
                margin: [30, 10, 2, 5],
                alignment: "left",
                image: logoBase64,
                width: 100,
              },
              {
                margin: [0, 30, 100, 0],
                alignment: "right",
                text: "Listado de libros", //TITULO DEL INFORME
                fontSize: 20,
                bold: false,
              },
            ],
          };
        };

        doc["footer"] = function (page, pages) {
          return [
            //para vertical x2: 580 ---- para horizontal x2:820
            {
              canvas: [
                { type: "line", x1: 20, y1: 6, x2: 580, y2: 6, lineWidth: 1 },
              ],
            },
            {
              columns: [
                {
                  text: "Centro Educativo Caracoles",
                  fontSize: 9,
                  bold: true,
                },
                {
                  alignment: "right",
                  text: [
                    {
                      text:
                        "Página " + page.toString() + " de " + pages.toString(),
                      fontSize: 9,
                      bold: true,
                    },
                  ],
                },
              ],
              margin: [20, 0],
            },
          ];
        };
      },
    },
  ],
  ordering: true,
  searching: true,
  order: [[0, "desc"]],
  ajax: {
    type: "post",
    url: path + "biblioteca/libro/listado",
    dataType: "json",
    dataSrc: function (resultado) {
      if (resultado.exito) {
        libros = resultado.libros;
        $.each(resultado.libros, function (indice, libro) {
          libro.btn = '<div class="btn-group">';
          libro.btn +=
            '<button type="button" class="btn btn-primary verLibroButton" title="Ver libro" value = "' +
            indice +
            '"><i class="fa fa-eye"></i></button>';
          libro.btn +=
            '<button type="button" class="btn btn-primary nuevoEjemplarButton" title="Agregar ejemplar" value = "' +
            libro.id +
            '"><i class="fa fa-plus"></i></button>';
          libro.btn +=
            '<button type="button" class="btn btn-primary verEjemplaresButton" title="Ver ejemplares" value = "' +
            libro.id +
            '"><i class="fa fa-book"></i></button>';
          libro.btn +=
            '<button type="button" class="btn btn-danger eliminarLibroButton" value="' +
            libro.id +
            '"><i class="fa fa-trash"></i></button>';
          libro.btn += "</div>";
        });

        return resultado.libros;
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al listar");
    },
  },
  columnDefs: [
    { width: "12%", targets: [5] },
    { width: "5%", targets: [3, 4] },
  ],

  columns: [
    { data: "titulo" },
    { data: "autoresDescripcion" },
    { data: "literario" },
    { data: "ejemplares" },
    { data: "disponibles" },
    { data: "btn" },
  ],
});

var tablaEjemplares = $("#ejemplaresTable").DataTable({
  bLengthChange: false,
  info: false,
  language: {
    loadingRecords: '<div class="loader centrado"></div>',
    emptyTable: "No hay resultados.",
    paginate: {
      next: "Siguiente",
      previous: "Anterior",
    },
    search: "Buscar:",
  },
  responsive: true,
  ordering: true,
  searching: false,
  order: [[0, "desc"]],
});

//===============================================
//					Funciones
//===============================================
function obtenerAutoresSelect() {
  var autores = [];
  $.each(document.getElementsByName("autores"), function (indice, autor) {
    if (autor.value) {
      autores.push(autor.value);
    }
  });

  return autores;
}

function ejemplaresPorLibro(libroId) {
  $.ajax({
    type: "post",
    url: path + "biblioteca/ejemplar/listado",
    dataType: "json",
    data: {
      id: libroId,
    },
    success: function (resultado) {
      if (resultado.exito) {
        ejemplares = resultado.ejemplares;
        tablaEjemplares.clear().draw();
        $.each(resultado.ejemplares, function (indice, ejemplar) {
          var prestamoDisabled =
            ejemplar.prestamo == "-" && ejemplar.estado != "EXTRAVIADO"
              ? ""
              : "disabled";
          var eliminarDisabled = ejemplar.prestamo == "-" ? "" : "disabled";
          var btn = '<div class="btn-group">';
          btn +=
            '<button type="button" class="btn btn-primary verEjemplarButton" title="Ver ejemplar" value = "' +
            indice +
            '"><i class="fa fa-eye"></i></button>';
          btn +=
            '<button type="button" class="btn btn-primary nuevoPrestamoButton" title="Nuevo préstamo" value = "' +
            ejemplar.id +
            '" ' +
            prestamoDisabled +
            '><i class="fa fa-reply-all"></i></button>';
          btn +=
            '<button type="button" class="btn btn-danger eliminarEjemplarButton" value="' +
            ejemplar.id +
            '" ' +
            prestamoDisabled +
            '><i class="fa fa-trash"></i></button>';
          btn += "</div>";

          tablaEjemplares.row.add([
            ejemplar.codigo,
            ejemplar.editorial,
            ejemplar.estado,
            ejemplar.prestamo,
            btn,
          ]);
        });

        tablaEjemplares.draw();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al listar");
    },
  });
}
//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
  //Carga de los combos
  cargarComboGET($("#literario"), "base/literarios");
  cargarComboGET($("#autorSelectFijo"), "base/autores");
  cargarComboGET($("#editorial"), "base/editoriales");
  cargarComboGET($("#participante"), "base/participantes");

  //Autores Multiples
  cantAutoresSelect = 1;

  //Instancia de la validación y envío del libro;
  $("#libroForm").validate({
    submitHandler: function (form) {
      activarBotonCargando($("#guardarLibroButton"));

      var url = path + "biblioteca/libro/alta";
      var data = {
        titulo: $("#titulo").val(),
        literario: $("#literario").val(),
        autores: obtenerAutoresSelect(),
      };

      if ($("#guardarLibroButton").val() != "") {
        url = path + "biblioteca/libro/editar";
        data.id = $("#guardarLibroButton").val();
      }

      $.ajax({
        type: "post",
        url: url,
        dataType: "json",
        data: data,
        success: function (resultado) {
          desactivarBotonCargando($("#guardarLibroButton"));
          if (resultado.exito) {
            tablaLibros.ajax.reload();
            toastr.success("Libro guardado con éxito.");
            $("#libroModal").modal("hide");
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($("#guardarLibroButton"));
          console.log(thrownError);
          toastr.error("Error " + jqXHR.status, "Error guardar el libro");
        },
      });
    },
    rules: {
      titulo: "required",
      literario: "required",
      autores: "required",
    },
    messages: {
      titulo: "Campo requerido",
      literario: "Campo requerido",
      autores: "Campo requerido",
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });

  //Instancia de la validación y envío del ejemplar;
  $("#ejemplarForm").validate({
    submitHandler: function (form) {
      activarBotonCargando($("#guardarEjemplarButton"));

      var url = path + "biblioteca/ejemplar/alta";
      var data = {
        libro: $("#libroId").val(),
        codigo: $("#codigo").val(),
        isbn: $("#isbn").val(),
        editorial: $("#editorial").val(),
        edicion: $("#edicion").val(),
        anio: $("#anio").val(),
        estado: $("#estado").val(),
      };

      if ($("#guardarEjemplarButton").val() != "") {
        url = path + "biblioteca/ejemplar/editar";
        data.id = $("#guardarEjemplarButton").val();
      }

      $.ajax({
        type: "post",
        url: url,
        dataType: "json",
        data: data,
        success: function (resultado) {
          desactivarBotonCargando($("#guardarEjemplarButton"));
          if (resultado.exito) {
            tablaLibros.ajax.reload();
            toastr.success("Ejemplar guardado con éxito.");
            $("#ejemplarModal").modal("hide");
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($("#guardarEjemplarButton"));
          console.log(thrownError);
          toastr.error("Error " + jqXHR.status, "Error guardar el ejemplar");
        },
      });
    },
    rules: {
      codigo: "required",
      editorial: "required",
      estado: "required",
    },
    messages: {
      codigo: "Campo requerido",
      editorial: "Campo requerido",
      estado: "Campo requerido",
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });

  //Instancia de la validación y envío del autor;
  $("#autorForm").validate({
    submitHandler: function (form) {
      activarBotonCargando($("#guardarAutorButton"));

      $.ajax({
        type: "post",
        url: path + "biblioteca/autor/alta",
        dataType: "json",
        data: {
          nombre: $("#autorNombre").val(),
          apellido: $("#autorApellido").val(),
        },
        success: function (resultado) {
          desactivarBotonCargando($("#guardarAutorButton"));
          if (resultado.exito) {
            toastr.success("Autor guardado con éxito.");

            $("#autorSelectFijo").empty();
            $("#autorSelectFijo").append(
              '<option value="">Seleccione</option>'
            );
            cargarComboGET($("#autorSelectFijo"), "base/autores");

            $("#autorModal").modal("hide");
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($("#guardarAutorButton"));
          console.log(thrownError);
          toastr.error("Error " + jqXHR.status, "Error guardar el autor");
        },
      });
    },
    rules: {
      autorNombre: "required",
      autorApellido: "required",
    },
    messages: {
      autorNombre: "Campo requerido",
      autorApellido: "Campo requerido",
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });

  //Instancia de la validación y envío de la editorial;
  $("#editorialForm").validate({
    submitHandler: function (form) {
      activarBotonCargando($("#guardarEditorialButton"));

      $.ajax({
        type: "post",
        url: path + "biblioteca/editorial/alta",
        dataType: "json",
        data: {
          descripcion: $("#editorialDescripcion").val(),
          pais: $("#editorialPais").val(),
        },
        success: function (resultado) {
          desactivarBotonCargando($("#guardarEditorialButton"));
          if (resultado.exito) {
            toastr.success("Editorial guardada con éxito.");

            $("#editorial").empty();
            $("#editorial").append('<option value="">Seleccione</option>');
            cargarComboGET($("#editorial"), "base/editoriales");

            $("#editorialModal").modal("hide");
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($("#guardarEditorialButton"));
          console.log(thrownError);
          toastr.error("Error " + jqXHR.status, "Error guardar la editorial");
        },
      });
    },
    rules: {
      editorialDescripcion: "required",
      editorialPais: "required",
    },
    messages: {
      editorialDescripcion: "Campo requerido",
      editorialPais: "Campo requerido",
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });

  //Instancia de la validación y envío del genero;
  $("#literarioForm").validate({
    submitHandler: function (form) {
      activarBotonCargando($("#guardarLiterarioButton"));

      $.ajax({
        type: "post",
        url: path + "biblioteca/literario/alta",
        dataType: "json",
        data: {
          descripcion: $("#literarioDescripcion").val(),
        },
        success: function (resultado) {
          desactivarBotonCargando($("#guardarLiterarioButton"));
          if (resultado.exito) {
            toastr.success("Género guardado con éxito.");

            $("#literario").empty();
            $("#literario").append('<option value="">Seleccione</option>');
            cargarComboGET($("#literario"), "base/literarios");

            $("#literarioModal").modal("hide");
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($("#guardarLiterarioButton"));
          console.log(thrownError);
          toastr.error("Error " + jqXHR.status, "Error guardar el género");
        },
      });
    },
    rules: {
      literarioDescripcion: "required",
    },
    messages: {
      literarioDescripcion: "Campo requerido",
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });

  //Instancia de la validación y envío del prestamo;
  $("#prestamoForm").validate({
    submitHandler: function (form) {
      activarBotonCargando($("#guardarPrestamoButton"));

      $.ajax({
        type: "post",
        url: path + "biblioteca/prestamo/alta",
        dataType: "json",
        data: {
          ejemplar: $("#ejemplarId").val(),
          participante: $("#participante").val(),
          fecha: $("#fecha").val(),
        },
        success: function (resultado) {
          desactivarBotonCargando($("#guardarPrestamoButton"));
          if (resultado.exito) {
            tablaLibros.ajax.reload();
            toastr.success("Prestamos guardado con éxito.");
            $("#prestamoModal").modal("hide");
          } else {
            errorServidor(resultado.error);
          }
        },
        error: function (jqXHR, textStatus, thrownError) {
          desactivarBotonCargando($("#guardarPrestamoButton"));
          console.log(thrownError);
          toastr.error("Error " + jqXHR.status, "Error guardar el prestamo");
        },
      });
    },
    rules: {
      fecha: "required",
      participante: "required",
    },
    messages: {
      fecha: "Campo requerido",
      participante: "Campo requerido",
    },
    errorElement: "span",
    errorClass: "help-block",
    highlight: function (element) {
      $(element).parent().addClass("has-error");
    },
    unhighlight: function (element) {
      $(element).parent().removeClass("has-error");
    },
    invalidHandler: function (event, validator) {
      toastr.error("Compruebe los campos");
    },
  });
});

//===============================================
//					Eventos
//===============================================

$("#librosTable").on("click", ".verLibroButton", function () {
  var libro = libros[$(this).val()];

  $("#titulo").val(libro.titulo); //.prop('disabled', true);
  $("#literario").val(libro.literarioId); //.prop('disabled', true);
  $("#autor").val(libro.autorId); //.prop('disabled', true);
  $("#guardarLibroButton").val(libro.id); //.prop('disabled', true);

  for (let i = 0; i < libro.autores.length - 1; i++) {
    cantAutoresSelect++;
    var options = $("#autorSelectFijo").html();
    $("#autoresWrapper").append(
      '<div class="row my-2"><div class="input-group form-group col-lg-12 col-sm-12"><select class="form-control autorSelect" name="autores">' +
        options +
        '</select><div class="input-group-addon"><a href="#" class="quitarAutor">x</a></div></div></div>'
    );
  }

  for (let i = 0; i < libro.autores.length; i++) {
    $($("select[name=autores]")[i]).val(libro.autores[i].id);
  }

  $("#libroModal").modal("show");
});

$("#librosTable").on("click", ".nuevoEjemplarButton", function () {
  $("#libroId").val($(this).val());
  $("#ejemplarModal").modal("show");
});

$("#librosTable").on("click", ".verEjemplaresButton", function () {
  ejemplaresPorLibro($(this).val());
  $("#ejemplarLibroId").val($(this).val());
  $("#ejemplaresModal").modal("show");
});

$("#librosTable").on("click", ".eliminarLibroButton", function () {
  $("#confirmarEliminarLibroButton").val($(this).val());
  $("#eliminarLibroModal").modal("show");
});

$("#ejemplaresTable").on("click", ".verEjemplarButton", function () {
  var ejemplar = ejemplares[$(this).val()];

  $("#libroId").val(ejemplar.libroId);
  $("#codigo").val(ejemplar.codigo).prop("disabled", false);
  $("#isbn").val(ejemplar.isbn).prop("disabled", false);
  $("#editorial").val(ejemplar.editorialId).prop("disabled", false);
  $("#edicion").val(ejemplar.edicion).prop("disabled", false);
  $("#anio").val(ejemplar.anio).prop("disabled", false);
  $("#estado").val(ejemplar.estado).prop("disabled", false);
  $("#guardarEjemplarButton").val(ejemplar.id).prop("disabled", false);

  $("#ejemplaresModal").modal("hide");
  $("#ejemplarModal").modal("show");
});

$("#ejemplaresTable").on("click", ".nuevoPrestamoButton", function () {
  $("#ejemplarId").val($(this).val());

  $("#ejemplaresModal").modal("hide");
  $("#prestamoModal").modal("show");
});

$("#ejemplaresTable").on("click", ".eliminarEjemplarButton", function () {
  $("#confirmarEliminarEjemplarButton").val($(this).val());
  $("#eliminarEjemplarModal").modal("show");
});

$("#confirmarEliminarLibroButton").click(function () {
  activarBotonCargando($(this));
  $.ajax({
    type: "post",
    url: path + "biblioteca/libro/eliminar",
    dataType: "json",
    data: {
      id: $(this).val(),
    },
    success: function (resultado) {
      desactivarBotonCargando($("#confirmarEliminarLibroButton"));
      $(this).val("");
      $("#eliminarLibroModal").modal("hide");

      if (resultado.exito) {
        toastr.success("Elemento eliminado.");
        tablaLibros.ajax.reload();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      $(this).val("");
      $("#eliminarLibroModal").modal("hide");
      desactivarBotonCargando($("#confirmarEliminarLibroButton"));
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al eliminar");
    },
  });
});

$("#confirmarEliminarEjemplarButton").click(function () {
  activarBotonCargando($(this));
  $.ajax({
    type: "post",
    url: path + "biblioteca/ejemplar/eliminar",
    dataType: "json",
    data: {
      id: $(this).val(),
    },
    success: function (resultado) {
      desactivarBotonCargando($("#confirmarEliminarEjemplarButton"));
      $(this).val("");
      $("#eliminarEjemplarModal").modal("hide");

      if (resultado.exito) {
        toastr.success("Elemento eliminado.");
        ejemplaresPorLibro($("#ejemplarLibroId").val());
        tablaLibros.ajax.reload();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      $(this).val("");
      $("#eliminarEjemplarModal").modal("hide");
      desactivarBotonCargando($("#confirmarEliminarEjemplarButton"));
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al eliminar");
    },
  });
});

$("#libroModal").on("hidden.bs.modal", function () {
  $("#titulo").val("").prop("disabled", false);
  $("#literario").val("").prop("disabled", false);
  $("#autor").val("").prop("disabled", false);
  $("#autoresWrapper").empty();
  cantAutoresSelect = 1;
  $("#guardarLibroButton").val("").prop("disabled", false);
});

$("#ejemplarModal").on("hidden.bs.modal", function () {
  $("#libroId").val("");
  $("#codigo").val("").prop("disabled", false);
  $("#isbn").val("").prop("disabled", false);
  $("#editorial").val("").prop("disabled", false);
  $("#edicion").val("").prop("disabled", false);
  $("#anio").val("").prop("disabled", false);
  $("#estado").val("").prop("disabled", false);
  $("#guardarEjemplarButton").val("").prop("disabled", false);

  ejemplares = null;
});

$("#autorModal").on("hidden.bs.modal", function () {
  $("#autorNombre").val("").prop("disabled", false);
  $("#autorApellido").val("").prop("disabled", false);
});

$("#editorialModal").on("hidden.bs.modal", function () {
  $("#editorialDescripcion").val("").prop("disabled", false);
  $("#editorialPais").val("").prop("disabled", false);
});

$("#literarioModal").on("hidden.bs.modal", function () {
  $("#literarioDescripcion").val("").prop("disabled", false);
});

$("#ejemplaresModal").on("hidden.bs.modal", function () {
  $("#ejemplarLibroId").val("");
  tablaEjemplares.clear().draw();
});

$("#prestamoModal").on("hidden.bs.modal", function () {
  $("#ejemplarId").val("").prop("disabled", false);
  $("#fecha").val("").prop("disabled", false);
  $("#participante").val("").prop("disabled", false);
});

$("#nuevaAsigAutor").click(function (e) {
  e.preventDefault();

  if (cantAutoresSelect < 10) {
    cantAutoresSelect++;
    var options = $("#autorSelectFijo").html();
    $("#autoresWrapper").append(
      '<div class="row my-2"><div class="input-group form-group col-lg-12 col-sm-12"><select class="form-control autorSelect" name="autores">' +
        options +
        '</select><div class="input-group-addon"><a href="#" class="quitarAutor">x</a></div></div></div>'
    );
  }
});

$("#autoresWrapper").on("click", ".quitarAutor", function (e) {
  e.preventDefault();
  $(this).parent("div").parent("div").remove();
  cantAutoresSelect--;
});
