//===============================================
//					Variables
//===============================================
var estadoData = {
  labels: ["BUENO", "REGULAR", "DAÑADO", "EXTRAVIADO"],
  datasets: [
    {
      label: "Estado",
      data: [],
      backgroundColor: ["#00BFFF", "#32CD32", "#9370DB", "#DC143C"],
      hoverOffset: 4,
    },
  ],
};

var generoData = {
  labels: [],
  datasets: [
    {
      data: [],
      backgroundColor: "#32CD32",
      hoverOffset: 4,
    },
  ],
};

var popularidadData = {
  labels: [],
  datasets: [
    {
      data: [],
      backgroundColor: "#DC143C",
      hoverOffset: 4,
    },
  ],
};

var estadoGrafico = null;
var generoGrafico = null;
var popularidadGrafico = null;

//===============================================
//					Funciones
//===============================================

//===============================================
//				Carga de la página
//===============================================
$(document).ready(function () {
  estadoGrafico = new Chart($("#estadoCanvas"), {
    type: "pie",
    data: estadoData,
    options: {
      responsive: true,
      legend: {
        position: "left",
      },
      title: {
        display: false,
        text: "Distribución por estado",
      },
    },
  });

  generoGrafico = new Chart($("#generoCanvas"), {
    type: "bar",
    data: generoData,
    options: {
      responsive: true,
      legend: {
        display: false,
      },
      title: {
        display: false,
        text: "Distribución por género",
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
            },
          },
        ],
      },
    },
  });

  popularidadGrafico = new Chart($("#popularidadCanvas"), {
    type: "bar",
    data: popularidadData,
    options: {
      responsive: true,
      legend: {
        display: false,
      },
      title: {
        display: false,
        text: "Popularidad por género",
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
            },
          },
        ],
      },
    },
  });

  $.ajax({
    type: "post",
    url: path + "biblioteca/graficos",
    dataType: "json",
    data: {},
    success: function (resultado) {
      if (resultado.exito) {
        $(".loader").hide();
        $("#contenido").show();
        estadoData.datasets[0].data = resultado.estado;
        var totalEstado =
          estadoData.datasets[0].data[0] +
          estadoData.datasets[0].data[1] +
          estadoData.datasets[0].data[2] +
          estadoData.datasets[0].data[3];
        var porcentajeBueno =
          (estadoData.datasets[0].data[0] * 100) / totalEstado;
        var porcentajeRegular =
          (estadoData.datasets[0].data[1] * 100) / totalEstado;
        var porcentajeDaniado =
          (estadoData.datasets[0].data[2] * 100) / totalEstado;
        var porcentajeExtraviado =
          (estadoData.datasets[0].data[3] * 100) / totalEstado;
        estadoData.labels[0] =
          estadoData.labels[0] + " " + porcentajeBueno.toFixed(2) + "%";
        estadoData.labels[1] =
          estadoData.labels[1] + " " + porcentajeRegular.toFixed(2) + "%";
        estadoData.labels[2] =
          estadoData.labels[2] + " " + porcentajeDaniado.toFixed(2) + "%";
        estadoData.labels[3] =
          estadoData.labels[3] + " " + porcentajeExtraviado.toFixed(2) + "%";
        estadoGrafico.update();
        generoData.datasets[0].data = resultado.genero;
        generoData.labels = resultado.generos;
        generoGrafico.update();
        popularidadData.datasets[0].data = resultado.popularidad;
        popularidadData.labels = resultado.generos;
        popularidadGrafico.update();
      } else {
        errorServidor(resultado.error);
      }
    },
    error: function (jqXHR, textStatus, thrownError) {
      console.log(thrownError);
      toastr.error("Error " + jqXHR.status, "Error al obtener los datos.");
    },
  });
});

//===============================================
//					Eventos
//===============================================
