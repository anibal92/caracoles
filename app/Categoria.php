<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categorias';
    public $timestamps = false;

    public function materiales()
    {
        return $this->hasMany('App\Material', 'categoria_id');
    }
}
