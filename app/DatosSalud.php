<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosSalud extends Model
{
    protected $table = 'datos_salud';
    public $timestamps = false;

    public function participante()
    {
        return $this->belongsTo('App\Participante', 'participante_id');
    }

    public function grupo_sanguineo()
    {
        return $this->belongsTo('App\GrupoSanguineo', 'sanguineo_id');
    }

    public function discapacidad()
    {
        return $this->belongsTo('App\Discapacidad', 'discapacidad_id');
    }
}
