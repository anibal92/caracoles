<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seguimiento extends Model
{
    protected $table = 'seguimientos';
    public $timestamps = false;

    public function participante()
    {
        return $this->belongsTo('App\Participante', 'participante_id');
    }

    public function usuario()
    {
        return $this->belongsTo('App\Usuario', 'usuario_id');
    }
}
