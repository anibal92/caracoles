<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'usuarios';
    public $timestamps = false;

    public function permiso()
    {
        return $this->hasOne('App\Permiso');
    }

    public function seguimientos()
    {
        return $this->hasMany('App\Seguimiento', 'usuario_id');
    }

    public function movimientos()
    {
        return $this->hasMany('App\Movimiento', 'usuario_id');
    }
}
