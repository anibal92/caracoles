<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $table = 'materiales';
    public $timestamps = false;

    public function categoria()
    {
        return $this->belongsTo('App\Categoria', 'categoria_id');
    }

    public function estado()
    {
        return $this->belongsTo('App\EstadoMaterial', 'estado_id');
    }

    public function proyecto()
    {
        return $this->belongsTo('App\Proyecto', 'proyecto_id');
    }

    public function material_curso()
    {
        return $this->hasMany('App\MaterialCurso', 'material_id');
    }
}
