<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialCurso extends Model
{
    protected $table = 'material_curso';
    public $timestamps = false;

    public function material()
    {
        return $this->belongsTo('App\Material', 'material_id');
    }

    public function curso()
    {
        return $this->belongsTo('App\Curso', 'curso_id');
    }
}
