<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoSanguineo extends Model
{
    protected $table = 'grupo_sanguineo';
    public $timestamps = false;

    public function datos_salud()
    {
        return $this->hasMany('App\DatosSalud', 'sanguineo_id');
    }
}
