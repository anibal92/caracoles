<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Literario extends Model
{
    protected $table = 'literarios';
    public $timestamps = false;

    public function libros()
    {
        return $this->hasMany('App\Libro', 'literario_id');
    }
}
