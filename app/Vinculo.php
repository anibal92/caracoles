<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vinculo extends Model
{
    protected $table = 'vinculos';
    public $timestamps = false;

    public function parentescos()
    {
        return $this->hasMany('App\Parentesco', 'vinculo_id');
    }
}
