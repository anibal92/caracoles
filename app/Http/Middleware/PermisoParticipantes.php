<?php

namespace App\Http\Middleware;

use Closure;

class PermisoParticipantes
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tiene_permiso = (bool) session('permiso')->modulo_participantes;
        if (!$tiene_permiso)
            return redirect('/');

        return $next($request);
    }
}
