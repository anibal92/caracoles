<?php

namespace App\Http\Middleware;

use Closure;

class PermisoFinanzas
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tiene_permiso = (bool) session('permiso')->modulo_finanzas;
        if (!$tiene_permiso)
            return redirect('/');
            
        return $next($request);
    }
}
