<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use stdClass;

use App\Curso;
use App\Educador;
use App\EducadorCurso;
use App\Horario;
use App\ParticipanteCurso;

class CursoController extends Controller
{
    public function listado(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Todos los cursos
            $cursos = Curso::with(['horarios', 'educador_curso.educador.persona'])
                ->where('activo', 1)
                ->orderBy('nombre', 'desc')
                ->get();

            $resultado->cursos = array();
            foreach ($cursos as $curso) {
                $c = new \stdClass;
                $e = new \stdClass;
                $c->id = $curso->id;
                $c->nombre = $curso->nombre;
                $c->horarios = $curso->horarios;
                $c->horarioStr = '';
                switch ($curso->horarios[0]->dia) {
                    case 'L':
                        $c->horarioStr .= 'LUNES';
                        break;
                    case 'M':
                        $c->horarioStr .= 'MARTES';
                        break;
                    case 'X':
                        $c->horarioStr .= 'MIERCOLES';
                        break;
                    case 'J':
                        $c->horarioStr .= 'JUEVES';
                        break;
                    case 'V':
                        $c->horarioStr .= 'VIERNES';
                        break;
                    case 'S':
                        $c->horarioStr .= 'SABADO';
                        break;
                    case 'D':
                        $c->horarioStr .= 'DOMINGO';
                        break;
                }
                $c->horarioStr .= ' | ' . $curso->horarios[0]->hora_inicio . ' - ' . $curso->horarios[0]->hora_fin;
                $c->educadorId = $curso->educador_curso[0]->educador_id;
                $c->educador = $curso->educador_curso[0]->educador->persona->nombres . ' ' . $curso->educador_curso[0]->educador->persona->apellido;
                $c->inscriptos = ParticipanteCurso::where('curso_id', $curso->id)->count();
                $resultado->cursos[] = $c;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los cursos.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function alta(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'nombre' => 'required|string',
            'horaInicio' => 'required|string',
            'educador' => 'string|nullable',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Alta curso
            $curso = new Curso;
            $curso->nombre = $request->nombre;
            $curso->save();

            //Alta del horario
            $horario = new Horario;
            $horario->dia = $request->dia;
            $horario->periodicidad = 'SEMANAL';
            $horario->hora_inicio = $request->horaInicio;
            $horario->hora_fin = $request->horaFin;
            $horario->curso_id = $curso->id;
            $horario->save();

            //Relacion educador curso
            if ($request->educador != null) {
                $educadorCurso = new EducadorCurso();
                $educadorCurso->educador_id = $request->educador;
                $educadorCurso->curso_id = $curso->id;
                $educadorCurso->save();
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar el curso.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function editar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
            'nombre' => 'required|string',
            'horaInicio' => 'required|string',
            'educador' => 'string|nullable',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Alta curso
            $curso = Curso::find($request->id);
            $curso->nombre = $request->nombre;
            $curso->save();

            //Alta del horario
            $horario = Horario::where('curso_id', $curso->id)->first();
            $horario->dia = $request->dia;
            $horario->periodicidad = 'SEMANAL';
            $horario->hora_inicio = $request->horaInicio;
            $horario->hora_fin = $request->horaFin;
            $horario->curso_id = $curso->id;
            $horario->save();

            //Relacion educador curso
            if ($request->educador != null) {
                $educadorCurso = EducadorCurso::where('curso_id', $curso->id)->first();
                $educadorCurso->educador_id = $request->educador;
                $educadorCurso->save();
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar el curso.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function eliminar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Busca el curso
            $curso = Curso::find($request->id);

            if ($curso == null) {
                $error->mensaje = 'No existe el elemento solicitado.';
                throw new \Exception('No existe el elemento solicitado.');
            }

            $curso->activo = 0;
            $curso->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al eliminar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function verificarExistencia(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Validaciones
            if ($request->documento == '' or $request->documento == null) {
                $error->mensaje = 'Complete el documento.';
                throw new \Exception('Complete el documento.');
            }

            if (!ctype_digit($request->documento)) {
                $error->mensaje = 'Documento inválido.';
                throw new \Exception('Documento inválido.');
            }

            $cursos = Educador::whereHas('persona', function ($query) use ($request) {
                $query->where('documento', $request->documento);
            })
                ->where('activo', 1)
                ->get();

            $resultado->existe = count($cursos) > 0;

            if (!$resultado->existe) {
                $persona = Persona::with('domicilio')->where('documento', $request->documento)->first();
                if ($persona) {
                    $resultado->persona = new \stdClass;
                    $resultado->persona->id = $persona->id;
                    $resultado->persona->nombres = $persona->nombres;
                    $resultado->persona->apellido = $persona->apellido;
                    $resultado->persona->nacimiento = $persona->nacimiento;
                    $resultado->persona->genero = $persona->genero_id;

                    $resultado->persona->domicilio = new \stdClass;
                    if ($persona->domicilio) {
                        $resultado->persona->domicilio->calle = $persona->domicilio->calle;
                        $resultado->persona->domicilio->numero = $persona->domicilio->numero;
                        $resultado->persona->domicilio->piso = $persona->domicilio->piso;
                        $resultado->persona->domicilio->departamento = $persona->domicilio->departamento;
                        $resultado->persona->domicilio->barrio = $persona->domicilio->barrio_id;
                        $resultado->persona->domicilio->localidad = $persona->domicilio->localidad_id;
                    }
                } else {
                    $resultado->persona = null;
                }
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al verificar el documento.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function agregarParticipante(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'participanteId' => 'required|integer',
            'cursoId' => 'required|integer',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            $existe = ParticipanteCurso::where('participante_id', $request->participanteId)
                ->where('curso_id', $request->cursoId)
                ->get();

            if (count($existe) > 0) {
                $error->mensaje = 'El partincipante ya está en el curso.';
                throw new \Exception('El partincipante ya está en el curso.');
            }

            $participanteCurso = new ParticipanteCurso();
            $participanteCurso->participante_id = $request->participanteId;
            $participanteCurso->curso_id = $request->cursoId;
            $participanteCurso->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al agregar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function quitarParticipante(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'participanteId' => 'required|integer',
            'cursoId' => 'required|integer'
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Busca la relacion
            $participanteCurso = ParticipanteCurso::where('curso_id', $request->cursoId)->where('participante_id', $request->participanteId);

            if ($participanteCurso == null) {
                $error->mensaje = 'No existe el elemento solicitado.';
                throw new \Exception('No existe el elemento solicitado.');
            }

            $participanteCurso->delete();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al eliminar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function graficos(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Todos los cursos
            $desiertos = DB::table('cursos')
                ->select('nombre', DB::raw('0 as cantidad'))
                ->whereRaw('cursos.id not in (select participante_curso.curso_id from participante_curso where participante_curso.curso_id = cursos.id)')
                ->where('activo', 1)
                ->groupBy('nombre');

            $cursos = DB::table('cursos')
                ->select('nombre', DB::raw('count(*) as cantidad'))
                ->join('participante_curso', 'cursos.id', '=', 'participante_curso.curso_id')
                ->where('activo', 1)
                ->groupBy('nombre')
                ->union($desiertos)
                ->get();

            $resultado->cursos = array();
            $resultado->totales = array();
            foreach ($cursos as $curso) {
                $resultado->cursos[] = strtoupper($curso->nombre);
                $resultado->totales[] = $curso->cantidad;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al obtener los datos.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }
}
