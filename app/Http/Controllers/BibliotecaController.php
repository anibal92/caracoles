<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use stdClass;

use App\Autor;
use App\Libro;
use App\LibroAutor;
use App\Literario;
use App\Ejemplar;
use App\Editorial;
use App\Prestamo;
use App\Participante;

class BibliotecaController extends Controller
{
    public function listado(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Todos los libros
            $libros = Libro::with(['libro_autor.autor', 'literario'])
                ->withCount(['ejemplares' => function ($query) {
                    $query->where('activo', 1);
                }])
                ->where('activo', 1)
                ->orderBy('titulo', 'desc')
                ->get();

            $resultado->libros = array();
            foreach ($libros as $libro) {
                $l = new \stdClass;
                $l->id = $libro->id;
                $l->titulo = $libro->titulo;
                $l->literarioId = $libro->literario->id;
                $l->literario = $libro->literario->descripcion;
                $l->ejemplares = $libro->ejemplares_count;

                $prestamos = Prestamo::whereHas('ejemplar', function ($query) use ($libro) {
                    $query->where('libro_id', $libro->id);
                })
                    ->whereNull('fecha_devolucion')
                    ->get();
                $l->disponibles = $l->ejemplares - count($prestamos);

                $l->autoresDescripcion = '';
                foreach ($libro->libro_autor as $autor) {
                    $l->autores[] = $autor->autor;
                    $l->autoresDescripcion .= $autor->autor->descripcion . '; ';
                }

                $l->autoresDescripcion = rtrim($l->autoresDescripcion, '; ');

                $resultado->libros[] = $l;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los libros.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function alta(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'titulo' => 'required|string',
            'literario' => 'required|integer',

        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Alta libro
            $libro = new Libro;
            $libro->titulo = $request->titulo;
            $libro->literario_id = $request->literario;
            $libro->save();

            //Relación libro-autor
            foreach ($request->autores as $autor) {
                $libroAutor = new LibroAutor;
                $libroAutor->libro_id = $libro->id;
                $libroAutor->autor_id = $autor;
                $libroAutor->save();
            }


            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar el libro.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function editar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
            'titulo' => 'required|string',
            'literario' => 'required|integer',

        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Busca el libro
            $libro = Libro::find($request->id);

            if ($libro == null) {
                $error->mensaje = 'No existe el elemento solicitado.';
                throw new \Exception('No existe el elemento solicitado.');
            }
            $libro->titulo = $request->titulo;
            $libro->literario_id = $request->literario;
            $libro->save();

            //Relación libro-autor
            LibroAutor::where('libro_id', $libro->id)->delete();
            foreach ($request->autores as $autor) {
                $libroAutor = new LibroAutor;
                $libroAutor->libro_id = $libro->id;
                $libroAutor->autor_id = $autor;
                $libroAutor->save();
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al modificar el libro.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function eliminar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Busca el libro
            $libro = Libro::find($request->id);

            if ($libro == null) {
                $error->mensaje = 'No existe el elemento solicitado.';
                throw new \Exception('No existe el elemento solicitado.');
            }

            //Chequear que no tenga ejemplares prestados
            $prestados = Ejemplar::where('libro_id', $libro->id)
                ->whereHas('prestamos', function ($query) {
                    $query->where('fecha_devolucion', null);
                })
                ->count();

            if ($prestados > 0) {
                $error->mensaje = 'El libro se encuentra prestado.';
                throw new \Exception('El libro se encuentra prestado.');
            }


            $libro->activo = 0;
            $libro->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al eliminar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function listadoEjemplar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Todos los ejemplares
            $ejemplares = Ejemplar::whereHas('libro', function ($query) use ($request) {
                $query->where('libro_id', $request->id);
            })
                ->where('activo', 1)
                ->get();

            $resultado->ejemplares = array();
            foreach ($ejemplares as $ejemplar) {
                $e = new \stdClass;
                $e->id = $ejemplar->id;
                $e->libroId = $ejemplar->libro_id;
                $e->editorialId = $ejemplar->editorial_id;
                $editorial = Editorial::find($e->editorialId);
                $e->editorial = $editorial->descripcion;
                $e->codigo = $ejemplar->codigo;
                $e->isbn = $ejemplar->isbn;
                $e->anio = $ejemplar->anio;
                $e->estado = $ejemplar->estado;

                $prestamo = Prestamo::with('participante.persona')
                    ->where('ejemplar_id', $ejemplar->id)
                    ->whereNull('fecha_devolucion')
                    ->orderBy('fecha_prestamo', 'desc')
                    ->first();
                if ($prestamo)
                    $e->prestamo = $prestamo->participante->persona->apellido . ', ' . $prestamo->participante->persona->nombres . ' (' . date("d-m-Y h:m:s", strtotime($prestamo->fecha_prestamo)) . ')';
                else
                    $e->prestamo = '-';

                $resultado->ejemplares[] = $e;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los ejemplares.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function altaEjemplar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'codigo' => 'required|string',
            'editorial' => 'required|integer',
            'libro' => 'required|integer',
            'estado' => 'required|string',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Alta ejemplar
            $ejemplar = new Ejemplar;
            $ejemplar->codigo = $request->codigo;
            $ejemplar->isbn = $request->isbn;
            $ejemplar->edicion = $request->edicion;
            $ejemplar->anio = $request->anio;
            $ejemplar->estado = $request->estado;
            $ejemplar->editorial_id = $request->editorial;
            $ejemplar->libro_id = $request->libro;
            $ejemplar->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar el ejemplar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function editarEjemplar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
            'codigo' => 'required|string',
            'editorial' => 'required|integer',
            'libro' => 'required|integer',
            'estado' => 'required|string',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Busca el ejemplar
            $ejemplar = Ejemplar::find($request->id);
            $ejemplar->codigo = $request->codigo;
            $ejemplar->isbn = $request->isbn;
            $ejemplar->edicion = $request->edicion;
            $ejemplar->anio = $request->anio;
            $ejemplar->estado = $request->estado;
            $ejemplar->editorial_id = $request->editorial;
            $ejemplar->libro_id = $request->libro;
            $ejemplar->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar el ejemplar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function eliminarEjemplar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Busca el libro
            $ejemplar = Ejemplar::find($request->id);

            if ($ejemplar == null) {
                $error->mensaje = 'No existe el elemento solicitado.';
                throw new \Exception('No existe el elemento solicitado.');
            }

            $ejemplar->activo = 0;
            $ejemplar->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al eliminar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function altaAutor(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'nombre' => 'required|string',
            'apellido' => 'required|string',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Valida si existe el autor
            $nya = strtoupper($request->apellido . ', ' . $request->nombre);
            $existe = Autor::select('id')->where('descripcion', $nya)->get();
            if (count($existe) != 0) {
                $error->mensaje = 'Ya existe el autor.';
                throw new \Exception('Ya existe el autor.');
            }

            //Alta autor
            $autor = new Autor;
            $autor->descripcion = $nya;

            $autor->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar el autor.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function altaEditorial(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'descripcion' => 'required|string',
            'pais' => 'required|string',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Valida si existe la editorial
            $descripcion = strtoupper($request->descripcion);
            $pais = strtoupper($request->pais);
            $existe = Editorial::select('id')->where('descripcion', $descripcion)->where('pais', $pais)->get();
            if (count($existe) != 0) {
                $error->mensaje = 'Ya existe la editorial.';
                throw new \Exception('Ya existe la editorial.');
            }

            //Alta editorial
            $editorial = new Editorial;
            $editorial->descripcion = $descripcion;
            $editorial->pais = $pais;

            $editorial->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar la editorial.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function graficos(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Por estado
            $bueno = DB::table('ejemplares')
                ->select('libro_id')
                ->where('activo', 1)
                ->where('estado', 'BUENO')
                ->count();

            $regular = DB::table('ejemplares')
                ->select('libro_id')
                ->where('activo', 1)
                ->where('estado', 'REGULAR')
                ->count();

            $daniado = DB::table('ejemplares')
                ->select('libro_id')
                ->where('activo', 1)
                ->where('estado', 'DAÑADO')
                ->count();

            $extraviado = DB::table('ejemplares')
                ->select('libro_id')
                ->where('activo', 1)
                ->where('estado', 'EXTRAVIADO')
                ->count();

            $generos = DB::table('literarios')->where('activo', 1)->orderBy('descripcion', 'asc')->get();
            $generoData = array();
            $generoLabel = array();
            $popularidadData = array();

            foreach ($generos as $genero) {
                $generoData[] = DB::table('libros')->where('literario_id', $genero->id)->where('activo', 1)->count();
                $popularidadData[] = Prestamo::with(['ejemplar', 'ejemplar.libro'])
                    ->whereHas('ejemplar.libro', function ($query) use ($genero) {
                        $query->where('literario_id', $genero->id);
                    })
                    ->where('activo', 1)
                    ->count();
                $generoLabel[] = strtoupper($genero->descripcion);
            }

            $resultado->generos = $generoLabel;
            $resultado->estado = [$bueno, $regular, $daniado, $extraviado];
            $resultado->genero = $generoData;
            $resultado->popularidad = $popularidadData;

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al obtener los datos.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function altaLiterario(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'descripcion' => 'required|string',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Valida si existe el genero literario
            $descripcion = strtoupper($request->descripcion);
            $existe = Literario::select('id')->where('descripcion', $descripcion)->get();
            if (count($existe) != 0) {
                $error->mensaje = 'Ya existe el género.';
                throw new \Exception('Ya existe el género.');
            }

            //Alta literario
            $literario = new Literario;
            $literario->descripcion = $descripcion;

            $literario->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar el género.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }
}
