<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Seguimiento;
use App\Usuario;

class SeguimientoController extends Controller
{
    public function listado(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Valida el id
            if (!ctype_digit($request->participanteId)) {
                $error->mensaje = 'Error de validación.';
                throw new \Exception('Error de validación.');
            }

            //Busca los seguimientos del participante
            $seguimientos = Seguimiento::with('usuario')
                ->where('participante_id', $request->participanteId)
                ->where('activo', 1)
                ->orderBy('fecha_alta', 'desc')
                ->get();

            $resultado->seguimientos = array();
            foreach ($seguimientos as $seguimiento) {
                $s = new \stdClass;
                $s->id = $seguimiento->id;
                $s->fecha = date("d-m-Y h:m:s", strtotime($seguimiento->fecha_alta));
                $s->usuario = $seguimiento->usuario->nombre . ' ' . $seguimiento->usuario->apellido;
                $s->observacion = $seguimiento->observacion;
                $resultado->seguimientos[] = $s;
            }
            //dd($resultado);
            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los seguimientos.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function alta(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'participanteId' => 'required|integer',
            'observacion' => 'required|string',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Alta de la observación
            $seguimiento = new Seguimiento();
            $seguimiento->observacion = $request->observacion;
            $seguimiento->participante_id = $request->participanteId;
            $seguimiento->usuario_id = session('usuarioId');
            $seguimiento->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar el seguimiento.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function eliminar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Busca el usuario
            $seguimiento = Seguimiento::find($request->id);

            if ($seguimiento == null) {
                $error->mensaje = 'No existe el elemento solicitado.';
                throw new \Exception('No existe el elemento solicitado.');
            }

            $seguimiento->activo = 0;
            $seguimiento->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al eliminar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }
}
