<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use stdClass;

use App\Material;
use App\Categoria;
use App\Proyecto;
use App\EstadoMaterial;
use App\MaterialCurso;

class MaterialController extends Controller
{

    public function eliminarTildes($cadena)
    {

        //Codificamos la cadena en formato utf8 en caso de que nos de errores
        //$cadena = utf8_encode($cadena);

        //Ahora reemplazamos las letras
        $cadena = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $cadena
        );

        $cadena = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $cadena
        );

        $cadena = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $cadena
        );

        $cadena = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $cadena
        );

        $cadena = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $cadena
        );

        return $cadena;
    }

    public function listado(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Todos los materiales
            $materiales = Material::with(['categoria', 'estado', 'proyecto'])
                ->where('activo', 1)
                ->orderBy('nombre', 'desc')
                ->get();

            $resultado->materiales = array();
            foreach ($materiales as $material) {
                $m = new \stdClass;

                $m->id = $material->id;
                $m->codigo = $material->codigo;
                $m->nombre = $material->nombre;
                $m->categoriaId = $material->categoria->id;
                $m->categoria = $material->categoria->descripcion;
                $m->estadoId = $material->estado->id;
                $m->estado = $material->estado->descripcion;
                $m->procedencia = $material->procedencia;
                $m->proyectoId = isset($material->proyecto->id) ? $material->proyecto->id : null;
                $m->proyecto = isset($material->proyecto->descripcion) ? $material->proyecto->descripcion : null;
                $m->descripcion = $material->descripcion;

                $resultado->materiales[] = $m;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los materiales.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function alta(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'nombre' => 'required|string',
            'categoria' => 'required|integer',
            'estado' => 'required|integer',
            'procedencia' => 'required|string',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Alta material
            $material = new Material;
            $material->codigo = $request->codigo;
            $material->nombre = $request->nombre;
            $material->estado_id = $request->estado;
            $material->categoria_id = $request->categoria;
            $material->procedencia = $request->procedencia;
            $material->proyecto_id = $request->proyecto;
            $material->descripcion = $request->descripcion;
            $material->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar el material.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function editar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
            'nombre' => 'required|string',
            'categoria' => 'required|integer',
            'estado' => 'required|integer',
            'procedencia' => 'required|string',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Alta material
            $material = Material::find($request->id);

            if ($material == null) {
                $error->mensaje = 'No existe el elemento solicitado.';
                throw new \Exception('No existe el elemento solicitado.');
            }

            $material->codigo = $request->codigo;
            $material->nombre = $request->nombre;
            $material->estado_id = $request->estado;
            $material->categoria_id = $request->categoria;
            $material->procedencia = $request->procedencia;
            $material->proyecto_id = $request->proyecto;
            $material->descripcion = $request->descripcion;
            $material->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar el material.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function eliminar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Busca el material
            $material = Material::find($request->id);

            if ($material == null) {
                $error->mensaje = 'No existe el elemento solicitado.';
                throw new \Exception('No existe el elemento solicitado.');
            }

            $material->activo = 0;
            $material->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al eliminar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function verificarExistencia(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Validaciones
            if ($request->documento == '' or $request->documento == null) {
                $error->mensaje = 'Complete el documento.';
                throw new \Exception('Complete el documento.');
            }

            if (!ctype_digit($request->documento)) {
                $error->mensaje = 'Documento inválido.';
                throw new \Exception('Documento inválido.');
            }

            $materials = Educador::whereHas('persona', function ($query) use ($request) {
                $query->where('documento', $request->documento);
            })
                ->where('activo', 1)
                ->get();

            $resultado->existe = count($materials) > 0;

            if (!$resultado->existe) {
                $persona = Persona::with('domicilio')->where('documento', $request->documento)->first();
                if ($persona) {
                    $resultado->persona = new \stdClass;
                    $resultado->persona->id = $persona->id;
                    $resultado->persona->nombres = $persona->nombres;
                    $resultado->persona->apellido = $persona->apellido;
                    $resultado->persona->nacimiento = $persona->nacimiento;
                    $resultado->persona->genero = $persona->genero_id;

                    $resultado->persona->domicilio = new \stdClass;
                    if ($persona->domicilio) {
                        $resultado->persona->domicilio->calle = $persona->domicilio->calle;
                        $resultado->persona->domicilio->numero = $persona->domicilio->numero;
                        $resultado->persona->domicilio->piso = $persona->domicilio->piso;
                        $resultado->persona->domicilio->departamento = $persona->domicilio->departamento;
                        $resultado->persona->domicilio->barrio = $persona->domicilio->barrio_id;
                        $resultado->persona->domicilio->localidad = $persona->domicilio->localidad_id;
                    }
                } else {
                    $resultado->persona = null;
                }
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al verificar el documento.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function agregarParticipante(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'participanteId' => 'required|integer',
            'materialId' => 'required|integer',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Busca el material

            //validar que el participante no esté asignado al material

            //asignar participante al material

            $participanteCurso = new ParticipanteCurso();
            $participanteCurso->participante_id = $request->participanteId;
            $participanteCurso->material_id = $request->materialId;
            $participanteCurso->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al agregar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function lote(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        /*$reglas = [
            'nombre' => 'required|string',
            'categoria' => 'required|integer',
            'estado' => 'required|integer',
            'procedencia' => 'required|string',
        ];

        $validador = \Validator::make($request->all(), $reglas);*/

        try {
            DB::beginTransaction();
            //Valida los datos de entrada
            /*if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }*/

            //Alta materiales
            $fallo = array();
            for ($i = 0; $i < $request->total; $i++) {
                $elemento = (object)$request->lote[$i];
                //Validaciones
                //chequear campos requeridos
                /*if($elemento->nombre || $elemento->categoria || $elemento->procedencia || $elemento->estado) {
                    $elemento->motivo = 'Falta campo requerido';
                    $fallo[] = $elemento;
                    continue;
                }*/
                //chequear que el codigo no exista
                if ($elemento->codigo != '' and $elemento->codigo != null) {
                    $codigo = Material::where('codigo', $elemento->codigo)->get();
                    if (count($codigo) > 0) {
                        $elemento->motivo = 'Ya existe el código';
                        $fallo[] = $elemento;
                        continue;
                    }
                }

                //chequear que exista la categoria o crearla
                $categoria =  Categoria::where('descripcion', strtoupper($elemento->categoria))->first();
                if (!$categoria) {
                    $categoria = new Categoria;
                    $categoria->descripcion = strtoupper($elemento->categoria);
                    $categoria->save();
                }

                //chequear que exista la categoria o crearla
                $estado =  EstadoMaterial::where('descripcion', strtoupper($elemento->estado))->first();
                if (!$estado) {
                    $estado = new EstadoMaterial;
                    $estado->descripcion = $elemento->estado;
                    $estado->save();
                }

                //
                $material = new Material;
                $material->codigo = $elemento->codigo;
                $material->nombre = $elemento->nombre;
                $material->estado_id = $estado->id;
                $material->categoria_id = $categoria->id;
                $material->procedencia = strtoupper($this->eliminarTildes($elemento->procedencia));
                $material->descripcion = $elemento->descripcion;
                $material->save();
            }

            DB::commit();
            $resultado->fallo = $fallo;
            $resultado->exito = true;
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar el material.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function graficos(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Por procedencia
            $compra = DB::table('materiales')
                ->where('procedencia', 'COMPRA')
                ->where('activo', 1)
                ->count();

            $donacion = DB::table('materiales')
                ->where('procedencia', 'DONACION')
                ->where('activo', 1)
                ->count();

            $otro = DB::table('materiales')
                ->where('procedencia', 'OTRO')
                ->where('activo', 1)
                ->count();


            $resultado->procedencia = [$compra, $donacion, $otro];

            //por estado
            $estados = DB::table('estados_material')->orderBy('descripcion', 'asc')->get();
            $estadosData = array();
            $estadosLabel = array();
            $totalEstados = 0;

            foreach ($estados as $estado) {
                $valor = DB::table('materiales')->where('estado_id', $estado->id)->where('activo', 1)->count();
                $estadosData[] = $valor;
                $estadosLabel[] = strtoupper($estado->descripcion);
                $totalEstados += $valor;
            }

            for ($i = 0; $i < count($estadosData); $i++) {
                $valor = ($estadosData[$i] * 100) / $totalEstados;
                $estadosLabel[$i] = $estadosLabel[$i] . " " . round($valor, 2) . "%";
            }

            $resultado->estado = new \stdClass;
            $resultado->estado->data = $estadosData;
            $resultado->estado->labels = $estadosLabel;

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al obtener los datos.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }
}
