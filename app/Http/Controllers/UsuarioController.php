<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Usuario;
use App\Permiso;
use stdClass;

class UsuarioController extends Controller
{
    public function listado()
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Busca todos los usuario activos
            $usuarios = Usuario::where('activo', 1)->get();
            $resultado->usuarios = array();

            foreach ($usuarios as $usuario) {
                $u = new \stdClass;
                $u->id = $usuario->id;
                $u->usuario = $usuario->usuario;
                $u->nombre = $usuario->nombre . ' ' . $usuario->apellido;
                $u->email = $usuario->email;
                $u->ultimoAcceso = date("d-m-Y h:m:s", strtotime($usuario->ultimo_acceso));

                $resultado->usuarios[] = $u;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los usuarios.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function alta(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'usuario' => 'required|string',
            'email' => 'required|email',
            'nombre' => 'nullable|string',
            'apellido' => 'nullable|string',
            'contrasenia' => 'required|string',
            'permisos.moduloUsuarios' => 'required|boolean',
            'permisos.moduloParticipantes' => 'required|boolean',
            'permisos.moduloCursos' => 'required|boolean',
            'permisos.moduloInventario' => 'required|boolean',
            'permisos.moduloBiblioteca' => 'required|boolean',
            'permisos.moduloFinanzas' => 'required|boolean',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            DB::beginTransaction();

            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Cheque si existe el nombre de usuario
            $existeUsuario = Usuario::where('usuario', $request->usuario)->get();

            if (count($existeUsuario) > 0) {
                $error->mensaje = 'Nombre de usuario no disponible.';
                throw new \Exception('Nombre de usuario no disponible.');
            }

            //Cheque si existe la direccion de correo
            $existeEmail = Usuario::where('email', $request->email)->get();

            if (count($existeEmail) > 0) {
                $error->mensaje = 'E-mail no disponible.';
                throw new \Exception('E-mail no disponible.');
            }

            //Alta del usuario
            $usuario = new Usuario;
            $usuario->usuario = $request->usuario;
            $usuario->email = $request->email;
            $usuario->nombre = $request->nombre;
            $usuario->apellido = $request->apellido;
            $contrasenia = sha1($request->contrasenia . 'crcls');
            $usuario->contrasenia = $contrasenia;
            $usuario->email_verificado = 0;
            $usuario->activo = 1;
            $usuario->save();

            //Alta de permisos
            $permiso = new Permiso;
            $permiso->usuario_id = $usuario->id;
            $permiso->modulo_usuarios = $request->permisos['moduloUsuarios'];
            $permiso->modulo_participantes = $request->permisos['moduloParticipantes'];
            $permiso->modulo_cursos = $request->permisos['moduloCursos'];
            $permiso->modulo_inventario = $request->permisos['moduloInventario'];
            $permiso->modulo_biblioteca = $request->permisos['moduloBiblioteca'];
            $permiso->modulo_finanzas = $request->permisos['moduloFinanzas'];
            $permiso->save();

            DB::commit();

            $resultado->exito = true;
        } catch (\Exception $e) {
            DB::rollBack();

            if (!isset($error->mensaje))
                $error->mensaje = 'Error dar de alta el usuario.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function ver(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Valida el id
            if (!ctype_digit($request->id)) {
                $error->mensaje = 'Error de validación.';
                throw new \Exception('Error de validación.');
            }

            //Busca el usuario
            $usuario = Usuario::with('permiso')->where('id', $request->id)->first();

            if ($usuario == null) {
                $error->mensaje = 'No existe el usuario.';
                throw new \Exception('No existe el usuario.');
            }

            $p = new \stdClass;
            $p->moduloUsuarios = $usuario->permiso->modulo_usuarios;
            $p->moduloParticipantes = $usuario->permiso->modulo_participantes;
            $p->moduloCursos = $usuario->permiso->modulo_cursos;
            $p->moduloInventario = $usuario->permiso->modulo_inventario;
            $p->moduloBiblioteca = $usuario->permiso->modulo_biblioteca;
            $p->moduloFinanzas = $usuario->permiso->modulo_finanzas;

            $u = new \stdClass;
            $u->id = $usuario->id;
            $u->usuario = $usuario->usuario;
            $u->nombre = $usuario->nombre;
            $u->apellido = $usuario->apellido;
            $u->email = $usuario->email;
            $u->permisos = $p;

            $resultado->usuario = $u;
            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al obtener el usuario.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function editar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
            'usuario' => 'required|string',
            'email' => 'required|email',
            'nombre' => 'nullable|string',
            'apellido' => 'nullable|string',
            'permisos.moduloUsuarios' => 'required|boolean',
            'permisos.moduloParticipantes' => 'required|boolean',
            'permisos.moduloCursos' => 'required|boolean',
            'permisos.moduloInventario' => 'required|boolean',
            'permisos.moduloBiblioteca' => 'required|boolean',
            'permisos.moduloFinanzas' => 'required|boolean',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            DB::beginTransaction();

            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Obtiene el usuario
            $usuario = Usuario::where('id', $request->id)->first();

            //Cheque si existe usuario
            if ($usuario == null) {
                $error->mensaje = 'No existe el usuario.';
                throw new \Exception('No existe el usuario');
            }

            //Cheque si existe el nombre de usuario
            $existe_usuario = Usuario::where('usuario', $request->usuario)
                ->where('id', '!=', $request->id)
                ->get();

            if (count($existe_usuario) > 0) {
                $error->mensaje = 'Nombre de usuario no disponible.';
                throw new \Exception('Nombre de usuario no disponible.');
            }

            //Cheque si existe la direccion de correo
            $existe_email = Usuario::where('email', $request->email)
                ->where('id', '!=', $request->id)
                ->get();

            if (count($existe_email) > 0) {
                $error->mensaje = 'E-mail no disponible.';
                throw new \Exception('E-mail no disponible.');
            }

            //Actualiza el usuario
            $usuario->usuario = $request->usuario;
            $usuario->email_verificado = ($usuario->email == $request->email) ? $usuario->email_verificado : !$usuario->email_verificado; //Si el email actual y el nuevo son iguales, mantiene el verificado
            $usuario->email = $request->email;
            $usuario->nombre = $request->nombre;
            $usuario->apellido = $request->apellido;
            $usuario->save();

            //Alta de permisos
            $permiso = Permiso::where('usuario_id', $usuario->id)->first();
            $permiso->modulo_usuarios = $request->permisos['moduloUsuarios'];
            $permiso->modulo_participantes = $request->permisos['moduloParticipantes'];
            $permiso->modulo_cursos = $request->permisos['moduloCursos'];
            $permiso->modulo_inventario = $request->permisos['moduloInventario'];
            $permiso->modulo_biblioteca = $request->permisos['moduloBiblioteca'];
            $permiso->modulo_finanzas = $request->permisos['moduloFinanzas'];
            $permiso->save();

            DB::commit();

            $resultado->exito = true;
        } catch (\Exception $e) {
            DB::rollBack();

            if (!isset($error->mensaje))
                $error->mensaje = 'Error dar de alta el usuario.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function eliminar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Busca el usuario
            $usuario = Usuario::find($request->id);

            if ($usuario == null) {
                $error->mensaje = 'No existe el usuario.';
                throw new \Exception('No existe el usuario.');
            }

            $usuario->activo = 0;
            $usuario->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al eliminar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function auth(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'usuario' => 'required|string',
            'contrasenia' => 'required|string',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Busca el usuario
            $contrasenia = sha1($request->contrasenia . 'crcls');
            $usuario = Usuario::with('permiso')
                ->where('usuario', $request->usuario)
                ->where('contrasenia', $contrasenia)
                ->first();

            //Chequea si el usuario fue encontrado
            if ($usuario == null) {
                $error->mensaje = 'Usuario y/o contraseña incorrectos.';
                throw new \Exception('Usuario y/o contraseña incorrectos.');
            }

            //Chequea si es un usuario inactivo
            if ($usuario->activo == 0) {
                $error->mensaje = 'Usuario inactivo.';
                throw new \Exception('Usuario inactivo.');
            }

            //Actualiza la fecha y hora del ultimo acceso
            $usuario->ultimo_acceso = date('Y-m-d H:i:s');
            $usuario->save();

            //Guarda los datos en sessión
            session([
                'logueado' => true,
                'usuarioId' => $usuario->id,
                'usuario' => $usuario->usuario,
                'email' => $usuario->email,
                'nombre' => $usuario->nombre,
                'apellido' => $usuario->apellido,
                'emailVerificado' => $usuario->email_verificado,
                'permiso' => $usuario->permiso,
            ]);

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al iniciar sesión.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('login');
    }

    public function recuperarContrasenia(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
            'contrasenia' => 'required|string',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Obtiene el usuario
            $usuario = Usuario::where('id', $request->id)->first();

            //Cheque si existe usuario
            if ($usuario == null) {
                $error->mensaje = 'No existe el usuario.';
                throw new \Exception('No existe el usuario');
            }

            $usuario->contrasenia = sha1($request->contrasenia . 'crcls');
            $usuario->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error dar de alta el usuario.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function cambiarContrasenia(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'actual' => 'required|string',
            'contrasenia' => 'required|string',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Obtiene el usuario
            $usuario = Usuario::where('id', session('usuarioId'))->first();

            //Cheque si existe usuario
            if ($usuario == null) {
                $error->mensaje = 'No existe el usuario.';
                throw new \Exception('No existe el usuario');
            }

            //Cheque si las contraseñas coinciden
            if ($usuario->contrasenia != sha1($request->actual . 'crcls')) {
                $error->mensaje = 'Contraseña incorrecta.';
                throw new \Exception('Contraseña incorrecta');
            }

            $usuario->contrasenia = sha1($request->contrasenia . 'crcls');
            $usuario->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error dar de alta el usuario.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }
}
