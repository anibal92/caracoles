<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use stdClass;

use App\Libro;
use App\Ejemplar;
use App\Editorial;
use App\Prestamo;
use App\Participante;

class PrestamoController extends Controller
{
    public function listado(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Todos los prestamos
            $prestamos = Prestamo::with(['ejemplar.libro', 'participante.persona'])
                ->where('activo', 1)
                ->orderBy('fecha_devolucion', 'asc')
                ->get();

            $resultado->prestamos = array();
            foreach ($prestamos as $prestamo) {
                $ejemplar = new \stdClass;
                $participante = new \stdClass;
                $p = new \stdClass;

                $ejemplar->id = $prestamo->ejemplar->id;
                $ejemplar->titulo = $prestamo->ejemplar->libro->titulo;
                $ejemplar->codigo = $prestamo->ejemplar->codigo;
                $ejemplar->estado = $prestamo->ejemplar->id;

                $participante->id = $prestamo->participante->id;
                $participante->documento = $prestamo->participante->persona->documento;
                $participante->nombre = $prestamo->participante->persona->apellido . ', ' . $prestamo->participante->persona->nombres;

                $p->id = $prestamo->id;
                $p->ejemplar = $ejemplar;
                $p->participante = $participante;
                $p->fecha = $prestamo->fecha_prestamo;
                $p->devolucion = $prestamo->fecha_devolucion;
                $p->fechaFormato = $prestamo->fecha_prestamo ? date("d-m-Y", strtotime($prestamo->fecha_prestamo)) : null;
                $p->devolucionFormato = $prestamo->fecha_devolucion ? date("d-m-Y", strtotime($prestamo->fecha_devolucion)) : null;
                $p->estado = $prestamo->estado;
                $p->observacion = $prestamo->obs_devolucion;

                $resultado->prestamos[] = $p;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los prestamos.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function alta(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'ejemplar' => 'required|integer',
            'participante' => 'required|integer',
            'fecha' => 'required|date',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Valida que exista el ejemplar
            $ejemplar = Ejemplar::find($request->ejemplar);
            if ($ejemplar == null) {
                $error->mensaje = 'No existe el ejemplar.';
                throw new \Exception('No existe el ejemplar.');
            }

            //Alta prestamo
            $prestamo = new Prestamo;
            $prestamo->participante_id = $request->participante;
            $prestamo->ejemplar_id = $request->ejemplar;
            $prestamo->fecha_prestamo = $request->fecha;
            $prestamo->estado = $ejemplar->estado;
            $prestamo->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar el prestamo.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function editar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
            'ejemplar' => 'required|integer',
            'participante' => 'required|integer',
            'fecha' => 'required|date',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Valida que exista el ejemplar
            $ejemplar = Ejemplar::find($request->ejemplar);
            if ($ejemplar == null) {
                $error->mensaje = 'No existe el ejemplar.';
                throw new \Exception('No existe el ejemplar.');
            }

            //Busca el prestamo
            $prestamo = Prestamo::find($request->id);
            $prestamo->participante_id = $request->participante;
            $prestamo->ejemplar_id = $request->ejemplar;
            $prestamo->fecha_prestamo = $request->fecha;
            $prestamo->estado = $ejemplar->estado;
            $prestamo->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar el prestamo.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function eliminar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Busca el prestamo
            $prestamo = Prestamo::find($request->id);

            if ($prestamo == null) {
                $error->mensaje = 'No existe el elemento solicitado.';
                throw new \Exception('No existe el elemento solicitado.');
            }

            $prestamo->activo = 0;
            if ($prestamo->fecha_devolucion == '' or $prestamo->fecha_devolucion == '')
                $prestamo->fecha_devolucion = Date('Y-m-d');
            $prestamo->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al eliminar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function devolucion(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
            'fecha' => 'required|date',
            'estado' => 'required|string',
            'observacion' => 'nullable|string',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Valida que exista el prestamo
            $prestamo = Prestamo::with('ejemplar')->find($request->id);
            if ($prestamo == null) {
                $error->mensaje = 'No existe el prestamo.';
                throw new \Exception('No existe el prestamo.');
            }

            //Termina con el prestamo
            $prestamo->fecha_devolucion = $request->fecha;
            $prestamo->estado = $request->estado;
            $prestamo->obs_devolucion = $request->observacion;
            $prestamo->save();

            //Actualiza el estado del ejemplar
            $ejemplar = Ejemplar::find($prestamo->ejemplar_id);
            $ejemplar->estado = $request->estado;
            $ejemplar->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar la devolución.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }
}
