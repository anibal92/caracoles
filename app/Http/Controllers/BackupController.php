<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BackupController extends Controller
{
    public function listado(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {

            $backups = Storage::files(ENV('BACKUP_DIR'));
            $resultado->backups = array();
            foreach ($backups as $backup) {
                $bu = new \stdClass;
                $bu->archivo = str_replace(ENV('BACKUP_DIR') . '/', '', $backup);
                $fecha = substr($backup, strlen(ENV('BACKUP_DIR')) + 1, 10);
                $fecha = explode('-', $fecha);
                $bu->fecha = $fecha[2] . '-' . $fecha[1] . '-' . $fecha[0];
                $bu->hora = str_replace('-', ':', substr($backup, strlen(ENV('BACKUP_DIR')) + 12, 8));
                $resultado->backups[] = $bu;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los backups.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function descargar($archivo)
    {
        return Storage::download(ENV('BACKUP_DIR') . '/' . $archivo);
    }

    public function eliminar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'archivo' => 'required|string',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            Storage::delete(ENV('BACKUP_DIR') . '/' . $request->archivo);

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al eliminar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }
}
