<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\EstadoCivil;
use App\Barrio;
use App\Discapacidad;
use App\Escolaridad;
use App\Escuela;
use App\Genero;
use App\Grado;
use App\GrupoSanguineo;
use App\Localidad;
use App\Programa;
use App\Provincia;
use App\Vinculo;
use App\Educador;
use App\Categoria;
use App\EstadoMaterial;
use App\Proyecto;
use App\Literario;
use App\Autor;
use App\Editorial;
use App\Participante;

class DatosBaseController extends Controller
{
	public function obtenerEstadoCivil()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$civil = EstadoCivil::all();
			$resultado->lista = $civil;

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar los estados civiles.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerBarrios(Request $request)
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$barrios = Barrio::all();
			$resultado->lista = $barrios;

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar los barrios.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerDiscapacidades()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$discapacidades = Discapacidad::all();
			$resultado->lista = $discapacidades;

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar las discapacidades.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerEscolaridades()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$escolaridades = Escolaridad::all();
			$resultado->lista = $escolaridades;

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar las escolaridades.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerEscuelas()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$escuelas = Escuela::all();
			$resultado->lista = $escuelas;

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar las escuelas.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerGeneros()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$genero = Genero::all();
			$resultado->lista = $genero;

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar los genero.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerGrados()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$grados = Grado::all();
			$resultado->lista = $grados;

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar los grados.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerGrupoSanguineo()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$grupo_sanguineo = GrupoSanguineo::all();
			$resultado->lista = $grupo_sanguineo;

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar los grupos sanguineos.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerLocalidades($id)
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$localidades = Localidad::where('provincia_id', $id)->get();
			$resultado->lista = $localidades;

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar las localidades.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerProgramas()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$programas = Programa::all();
			$resultado->lista = $programas;

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar los programas.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerProvincias()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$provincias = Provincia::all();
			$resultado->lista = $provincias;

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar las provincias.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerVinculos()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$vinculos = Vinculo::all();
			$resultado->lista = $vinculos;

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar los vinculos.';
			$error->lista = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerEducadores()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$educadores = Educador::with('persona')->where('activo', 1)->get();
			$resultado->lista = array();

            foreach ($educadores as $educador) {
                $e = new \stdClass;
                $e->id = $educador->id;
                $e->descripcion = $educador->persona->apellido . ', ' . $educador->persona->nombres;
                
                $resultado->lista[] = $e;
            }

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar los educadoresS.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerCategorias()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$categorias = Categoria::all();
			$resultado->lista = $categorias;

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar las categorias.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerEstadosMaterial()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$estadosMaterial = EstadoMaterial::all();
			$resultado->lista = $estadosMaterial;

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar los estados.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerProyectos()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$proyectos = Proyecto::all();
			$resultado->lista = [];
			foreach($proyectos as $proyecto) {
				$p = new \stdClass;
				$p->id = $proyecto->id;
				$p->descripcion = $proyecto->nombre;

				$resultado->lista[] = $p;
			}

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar los proyectos.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerLiterarios()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$literarios = Literario::where('activo', 1)->orderBy('descripcion', 'asc')->get();
			$resultado->lista = [];
			foreach($literarios as $literario) {
				$l = new \stdClass;
				$l->id = $literario->id;
				$l->descripcion = $literario->descripcion;

				$resultado->lista[] = $l;
			}

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar los proyectos.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerAutores()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$autores = Autor::where('activo', 1)->orderBy('descripcion', 'asc')->get();
			$resultado->lista = [];
			foreach($autores as $autor) {
				$a = new \stdClass;
				$a->id = $autor->id;
				$a->descripcion = $autor->descripcion;

				$resultado->lista[] = $a;
			}

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar los autores.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerEditoriales()
	{
		$resultado = new \stdClass;
		$error = new \stdClass;

		try {
			$editoriales = Editorial::all();
			$resultado->lista = [];
			foreach($editoriales as $editorial) {
				$e = new \stdClass;
				$e->id = $editorial->id;
				$e->descripcion = $editorial->descripcion;

				$resultado->lista[] = $e;
			}

			$resultado->exito = true;
		} catch (\Exception $e) {
			if (!isset($error->mensaje))
				$error->mensaje = 'Error al listar las editoriales.';
			$error->descripcion = $e->getMessage();
			$error->mostrarToast = true;

			$resultado->exito = false;
			$resultado->error = $error;
		}

		return response()->json($resultado);
	}

	public function obtenerParticipantes()
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            $participantes = Participante::with('persona')->where('activo', 1)->get();
            $resultado->participantes = array();

            foreach ($participantes as $participante) {
				$p = new \stdClass;
				$p->id = $participante->id;
				$p->descripcion = 'DNI ' . $participante->persona->documento . ' - ' . $participante->persona->apellido . ', ' . $participante->persona->nombres;
                
                $resultado->lista[] = $p;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los participantes.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

}
