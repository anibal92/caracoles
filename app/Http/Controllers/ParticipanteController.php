<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Persona;
use App\Participante;
use App\Domicilio;
use App\DatosSalud;
use App\ContactoEmergencia;
use App\Parentesco;
use App\Seguimiento;
use stdClass;

class ParticipanteController extends Controller
{
    public function listado()
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            $participantes = Participante::with('persona')->where('activo', 1)->get();
            $resultado->participantes = array();

            foreach ($participantes as $participante) {
                $p = new \stdClass;
                $p->id = $participante->id;
                $p->documento = $participante->persona->documento;
                $p->nombres = $participante->persona->nombres;
                $p->apellido = $participante->persona->apellido;
                $p->nacimiento = $participante->persona->nacimiento ? date("d-m-Y", strtotime($participante->persona->nacimiento)) : null;

                $resultado->participantes[] = $p;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los participantes.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function alta(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'nacimiento' => 'date|nullable',
            'nombres' => 'required|string',
            'apellido' => 'required|string',
            'calle' => 'string|nullable',
            'numero' => 'string|nullable',
            'piso' => 'string|nullable',
            'departamento' => 'string|nullable',
            'barrio' => 'integer|nullable',
            'localidad' => 'integer|nullable',
            'escuela' => 'integer|nullable',
            'grupo_sanguineo' => 'integer|nullable',
            'enfermedad' => 'string|nullable',
            'alergia' => 'string|nullable',
            'medicacion' => 'string|nullable',
            'discapacidad' => 'integer|nullable',
            'cud' => 'string|nullable',
            'observacionSalud' => 'string|nullable',
        ];

        //TODO: validar grupo familiar

        $validador = \Validator::make($request->all(), $reglas);

        try {
            DB::beginTransaction();

            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Cheque si existe un participante con ese documento
            $existeParticipante = Participante::whereHas('persona', function ($query) use ($request) {
                $query->where('documento', $request->documento)->where('documento', '!=', null);
            })->get();

            if (count($existeParticipante) > 0) {
                $error->mensaje = 'Ya existe un participante registrado con ese DNI.';
                throw new \Exception('Ya existe un participante registrado con ese DNI.');
            }

            //Alta del participante
            if ($request->personaId == null) {
                $request->domicilio = (object) $request->domicilio;
                $domicilio = new Domicilio;

                if ($request->domicilio->calle != null and $request->domicilio->numero != null and $request->domicilio->localidad != null) {
                    $domicilio->calle = strtoupper($request->domicilio->calle);
                    $domicilio->numero = $request->domicilio->numero;
                    $domicilio->piso = $request->domicilio->piso;
                    $domicilio->departamento = strtoupper($request->domicilio->departamento);
                    $domicilio->barrio_id = $request->domicilio->barrio;
                    $domicilio->localidad_id = $request->domicilio->localidad;
                    $domicilio->save();
                }
                //TODO: Si el domicilio es inválido debería avisar que no lo guarda

                $persona = new Persona;
                $persona->documento = $request->documento;
                $persona->nombres = strtoupper($request->nombres);
                $persona->apellido = strtoupper($request->apellido);
                $persona->nacimiento = $request->nacimiento;
                $persona->genero_id = $request->genero;
                $persona->civil_id = 1;
                $persona->domicilio_id = $domicilio->id;
                $persona->save();
            } else {
                $persona = new stdClass;
                $persona->id = $request->personaId;
            }

            $participante = new Participante;
            $participante->activo = 1;
            $participante->persona_id = $persona->id;
            $participante->escuela_id = $request->escuela;
            $participante->grado_id = $request->grado;
            $participante->programa_id = $request->programa;
            $participante->save();

            $request->salud = (object) $request->salud;
            $salud = new DatosSalud;
            $salud->cud = $request->salud->cud;
            $salud->enfermedad = strtoupper($request->salud->enfermedad);
            $salud->alergia = strtoupper($request->salud->alergia);
            $salud->medicacion = strtoupper($request->salud->medicacion);
            $salud->observacion = $request->salud->observacion;
            $salud->sanguineo_id = $request->salud->sanguineo;
            $salud->discapacidad_id = $request->salud->discapacidad;
            $salud->participante_id = $participante->id;
            $salud->save();

            //Alta del grupo familiar
            if ($request->familiares) {
                foreach ($request->familiares as $familiar) {
                    $familiar = (object) $familiar;
                    $familiar->domicilio = (object) $familiar->domicilio;

                    if ($familiar->personaId == null) {
                        $domicilioFamiliar = new Domicilio;
                        if ($familiar->domicilio->calle != null and $familiar->domicilio->numero != null and $familiar->domicilio->localidad != null) {
                            $domicilioFamiliar->calle = strtoupper($familiar->domicilio->calle);
                            $domicilioFamiliar->numero = $familiar->domicilio->numero;
                            $domicilioFamiliar->piso = $familiar->domicilio->piso;
                            $domicilioFamiliar->departamento = strtoupper($familiar->domicilio->departamento);
                            $domicilioFamiliar->barrio_id = $familiar->domicilio->barrio;
                            $domicilioFamiliar->localidad_id = $familiar->domicilio->localidad;
                            $domicilioFamiliar->save();
                        }
                        //TODO: Si el domicilio es inválido debería avisar que no lo guarda

                        $personaFamiliar = new Persona;
                        $personaFamiliar->documento = $familiar->documento;
                        $personaFamiliar->nacimiento = $familiar->nacimiento;
                        $personaFamiliar->nombres = strtoupper($familiar->nombres);
                        $personaFamiliar->apellido = strtoupper($familiar->apellido);
                        $personaFamiliar->telefono = $familiar->telefono;
                        $personaFamiliar->telefono_alternativo = $familiar->telefonoAlternativo;
                        $personaFamiliar->email = $familiar->email;
                        $personaFamiliar->genero_id = $familiar->genero;
                        $personaFamiliar->civil_id = $familiar->civil;
                        $personaFamiliar->escolaridad_id = $familiar->escolaridad;
                        $personaFamiliar->domicilio_id = $domicilioFamiliar->id;
                        $personaFamiliar->save();
                    } else {
                        $personaFamiliar = new stdClass;
                        $personaFamiliar->id = $familiar->personaId;
                    }

                    $parentesco = new Parentesco;
                    $parentesco->oficio = strtoupper($familiar->oficio);
                    $parentesco->interesa_participar = $familiar->interesaParticipar;
                    $parentesco->interesa_terminar_estudios = $familiar->interesaEstudios;
                    $parentesco->vinculo_id = $familiar->vinculo;
                    $parentesco->participante_id = $participante->id;
                    $parentesco->persona_id = $personaFamiliar->id;
                    $parentesco->save();

                    if ($familiar->emergencia)
                        $personaEmergencia = $personaFamiliar;
                }
            }

            //Alta del contacto de emergencia
            if (!isset($personaEmergencia)) {
                $request->contactoEmergencia = (object) $request->contactoEmergencia;

                $domicilioEmergencia = new Domicilio;

                if ($request->contactoEmergencia->calle != null and $request->contactoEmergencia->numero != null and $request->contactoEmergencia->localidad != null) {
                    $domicilioEmergencia->calle = strtoupper($request->contactoEmergencia->calle);
                    $domicilioEmergencia->numero = $request->contactoEmergencia->numero;
                    $domicilioEmergencia->piso = $request->contactoEmergencia->piso;
                    $domicilioEmergencia->departamento = strtoupper($request->contactoEmergencia->departamento);
                    $domicilioEmergencia->barrio_id = $request->contactoEmergencia->barrio;
                    $domicilioEmergencia->localidad_id = $request->contactoEmergencia->localidad;
                    $domicilioEmergencia->save();
                }
                //TODO: no debería tirar una excepción acá? si el domicilio es obligatorio
                //TODO: Si el domicilio es inválido debería avisar que no lo guarda

                $personaEmergencia = new Persona;
                $personaEmergencia->documento = isset($request->contactoEmergencia->documento) ? $request->contactoEmergencia->documento : null;
                $personaEmergencia->nombres = strtoupper($request->contactoEmergencia->nombres);
                $personaEmergencia->apellido = strtoupper($request->contactoEmergencia->apellido);
                $personaEmergencia->telefono = $request->contactoEmergencia->telefono;
                $personaEmergencia->telefono_alternativo = $request->contactoEmergencia->telefonoAlternativo;
                $personaEmergencia->domicilio_id = $domicilioEmergencia->id;
                $personaEmergencia->save();
            }

            $contactoEmergencia = new ContactoEmergencia;
            $contactoEmergencia->participante_id = $participante->id;
            $contactoEmergencia->persona_id = $personaEmergencia->id;
            $contactoEmergencia->save();

            DB::commit();

            $resultado->exito = true;
        } catch (\Exception $e) {
            DB::rollBack();

            if (!isset($error->mensaje))
                $error->mensaje = 'Error dar de alta el participante.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function ver(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Valida el id
            if (!ctype_digit($request->id)) {
                $error->mensaje = 'Error de validación.';
                throw new \Exception('Error de validación.');
            }

            //Busca el participante
            $participante = Participante::with([
                'persona.domicilio',
                'parentescos.persona.domicilio',
                'parentescos.vinculo',
                'contacto_emergencia.persona.domicilio',
                'datos_salud'
            ])
                ->where('id', $request->id)
                ->first();

            if ($participante == null) {
                $error->mensaje = 'No existe el participante.';
                throw new \Exception('No existe el participante.');
            }

            $resultado->participante = new \stdClass;
            $resultado->participante->id = $participante->id;
            $resultado->participante->personaId = $participante->persona->id;
            $resultado->participante->documento = $participante->persona->documento;
            $resultado->participante->nombres = $participante->persona->nombres;
            $resultado->participante->apellido = $participante->persona->apellido;
            $resultado->participante->genero = $participante->persona->genero_id;
            $resultado->participante->nacimiento = $participante->persona->nacimiento;
            $resultado->participante->escuela = $participante->escuela_id;
            $resultado->participante->grado = $participante->grado_id;
            $resultado->participante->programa = $participante->programa_id;

            if ($participante->persona->domicilio != null) {
                $resultado->participante->domicilio = new \stdClass;
                $resultado->participante->domicilio->calle = $participante->persona->domicilio->calle;
                $resultado->participante->domicilio->numero = $participante->persona->domicilio->numero;
                $resultado->participante->domicilio->piso = $participante->persona->domicilio->piso;
                $resultado->participante->domicilio->departamento = $participante->persona->domicilio->departamento;
                $resultado->participante->domicilio->barrio = $participante->persona->domicilio->barrio_id;
                $resultado->participante->domicilio->localidad = $participante->persona->domicilio->localidad_id;
            } else {
                $resultado->participante->domicilio = null;
            }

            $resultado->participante->salud = new \stdClass;
            $resultado->participante->salud->sanguineo = $participante->datos_salud->sanguineo_id;
            $resultado->participante->salud->enfermedad = $participante->datos_salud->enfermedad;
            $resultado->participante->salud->alergia = $participante->datos_salud->alergia;
            $resultado->participante->salud->medicacion = $participante->datos_salud->medicacion;
            $resultado->participante->salud->discapacidad = $participante->datos_salud->discapacidad_id;
            $resultado->participante->salud->cud = $participante->datos_salud->cud;
            $resultado->participante->salud->observacion = $participante->datos_salud->observacion;

            $resultado->participante->emergencia = new \stdClass;
            $resultado->participante->emergencia->personaId = $participante->contacto_emergencia->persona_id;
            $resultado->participante->emergencia->documento = $participante->contacto_emergencia->persona->documento;
            $resultado->participante->emergencia->nombres = $participante->contacto_emergencia->persona->nombres;
            $resultado->participante->emergencia->apellido = $participante->contacto_emergencia->persona->apellido;
            $resultado->participante->emergencia->telefono = $participante->contacto_emergencia->persona->telefono;
            $resultado->participante->emergencia->telefonoAlternativo = $participante->contacto_emergencia->persona->telefono_alternativo;

            if ($participante->contacto_emergencia->persona->domicilio) {
                $resultado->participante->emergencia->domicilio = new \stdClass;
                $resultado->participante->emergencia->domicilio->calle = $participante->contacto_emergencia->persona->domicilio->calle;
                $resultado->participante->emergencia->domicilio->numero = $participante->contacto_emergencia->persona->domicilio->numero;
                $resultado->participante->emergencia->domicilio->piso = $participante->contacto_emergencia->persona->domicilio->piso;
                $resultado->participante->emergencia->domicilio->departamento = $participante->contacto_emergencia->persona->domicilio->departamento;
                $resultado->participante->emergencia->domicilio->barrio = $participante->contacto_emergencia->persona->domicilio->barrio_id;
                $resultado->participante->emergencia->domicilio->localidad = $participante->contacto_emergencia->persona->domicilio->localidad_id;
            } else {
                $resultado->participante->emergencia->domicilio = null;
            }


            $resultado->participante->familiares = array();
            foreach ($participante->parentescos as $familiar) {
                $f = new \stdClass;
                $f->personaId = $familiar->persona_id;
                $f->civil = $familiar->persona->civil_id;
                $f->vinculo = $familiar->vinculo_id;
                $f->vinculoTexto = $familiar->vinculo->descripcion;
                $f->documento = $familiar->persona->documento;
                $f->nombres = $familiar->persona->nombres;
                $f->apellido = $familiar->persona->apellido;
                $f->genero = $familiar->persona->genero_id;
                $f->nacimiento = $familiar->persona->nacimiento;
                $f->email = $familiar->persona->email;
                $f->telefono = $familiar->persona->telefono;
                $f->telefonoAlternativo = $familiar->persona->telefono_alternativo;
                $f->oficio = $familiar->oficio;
                $f->escolaridad = $familiar->persona->escolaridad_id;
                $f->interesaParticipar = $familiar->interesa_participar;
                $f->interesaEstudios = $familiar->interesa_terminar_estudios;
                $f->emergencia = ($resultado->participante->emergencia->personaId == $f->personaId) ? 1 : 0;

                if ($familiar->persona->domicilio) {
                    $f->domicilio = new \stdClass;
                    $f->domicilio->calle = $familiar->persona->domicilio['calle'];
                    $f->domicilio->numero = $familiar->persona->domicilio['numero'];
                    $f->domicilio->piso = $familiar->persona->domicilio['piso'];
                    $f->domicilio->departamento = $familiar->persona->domicilio['departamento'];
                    $f->domicilio->barrio = $familiar->persona->domicilio['barrio_id'];
                    $f->domicilio->localidad = $familiar->persona->domicilio['localidad_id'];
                } else {
                    $f->domicilio = null;
                }

                $resultado->participante->familiares[] = $f;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            /*dd($e);
            die;*/

            if (!isset($error->mensaje))
                $error->mensaje = 'Error al obtener el participante.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function editar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
            'nacimiento' => 'date|nullable',
            'nombres' => 'required|string',
            'apellido' => 'required|string',
            'calle' => 'string|nullable',
            'numero' => 'string|nullable',
            'piso' => 'string|nullable',
            'departamento' => 'string|nullable',
            'barrio' => 'integer|nullable',
            'localidad' => 'integer|nullable',
            'escuela' => 'integer|nullable',
            'grupo_sanguineo' => 'integer|nullable',
            'enfermedad' => 'string|nullable',
            'alergia' => 'string|nullable',
            'medicacion' => 'string|nullable',
            'discapacidad' => 'integer|nullable',
            'cud' => 'string|nullable',
            'observacionSalud' => 'string|nullable',
        ];

        //TODO: validar grupo familiar

        $validador = \Validator::make($request->all(), $reglas);

        try {

            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Cheque si existe una persona con ese documento
            $existePersona = Persona::where('documento', $request->documento)
                ->where('id', '!=', $request->personaId)
                ->where('documento', '!=', null)
                ->get();

            if (count($existePersona) > 0) {
                $error->mensaje = 'DNI en uso.';
                throw new \Exception('DNI en uso.');
            }

            //Busca el participante
            $participante = Participante::find($request->id);

            if ($participante == null) {
                $error->mensaje = 'No existe el elemento solicitado.';
                throw new \Exception('No existe el elemento solicitado.');
            }

            DB::beginTransaction();

            $persona = Persona::find($request->personaId);
            $domicilio = Domicilio::find($persona->domicilio_id);
            $request->domicilio = (object) $request->domicilio;

            //Si viene un domicilio lo actualiza
            if ($request->domicilio->calle != null and $request->domicilio->numero != null and $request->domicilio->localidad != null) {
                //Si no tenía domicilio se crea uno
                if (!$domicilio)
                    $domicilio = new Domicilio;

                $domicilio->calle = strtoupper($request->domicilio->calle);
                $domicilio->numero = $request->domicilio->numero;
                $domicilio->piso = $request->domicilio->piso;
                $domicilio->departamento = strtoupper($request->domicilio->departamento);
                $domicilio->barrio_id = $request->domicilio->barrio;
                $domicilio->localidad_id = $request->domicilio->localidad;
                $domicilio->save();

                $persona->domicilio_id = $domicilio->id;
            }

            //Actualiza los datos de la persona
            $persona->documento = $request->documento;
            $persona->nombres = strtoupper($request->nombres);
            $persona->apellido = strtoupper($request->apellido);
            $persona->nacimiento = $request->nacimiento;
            $persona->genero_id = $request->genero;
            $persona->civil_id = 1;
            $persona->save();

            //Actualiza los datos del participante
            $participante->activo = 1;
            $participante->persona_id = $persona->id;
            $participante->escuela_id = $request->escuela;
            $participante->grado_id = $request->grado;
            $participante->programa_id = $request->programa;
            $participante->save();

            //Actualiza los datos de salud
            $request->salud = (object) $request->salud;
            $salud = DatosSalud::where('participante_id', $request->id)->first();
            $salud->cud = $request->salud->cud;
            $salud->enfermedad = strtoupper($request->salud->enfermedad);
            $salud->alergia = strtoupper($request->salud->alergia);
            $salud->medicacion = strtoupper($request->salud->medicacion);
            $salud->observacion = $request->salud->observacion;
            $salud->sanguineo_id = $request->salud->sanguineo;
            $salud->discapacidad_id = $request->salud->discapacidad;
            $salud->save();

            //Editar el grupo familiar
            if ($request->familiares) {
                foreach ($request->familiares as $familiar) {
                    $familiar = (object) $familiar;

                    if ($familiar->domicilio)
                        $familiar->domicilio = (object) $familiar->domicilio;

                    //!! TODO: VER NO DEBERÍA VER PRIMERO SI ES UN FAMILIAR NUEVO O CON ID?
                    // Si el familiar viene sin ID de pesona, creo la persona, el domicilio y el paretensco
                    //Sino busco estos datos de la persona
                    if ($familiar->personaId == null) {
                        $domicilioFamiliar = new Domicilio;
                        $personaFamiliar = new Persona;
                        $parentesco = new Parentesco;
                    } else {
                        $personaFamiliar = Persona::find($familiar->personaId);
                        $domicilioFamiliar = Domicilio::find($personaFamiliar->domicilio_id);
                        $parentesco = Parentesco::where('participante_id', $participante->id)->where('persona_id', $familiar->personaId)->first();

                        //Si no estaban relacionados antes, creo una nueva relación
                        if (!$parentesco)
                            $parentesco = new Parentesco();
                    }

                    //Si el familiar viene con domicilio, lo guardo
                    if ($familiar->domicilio and $familiar->domicilio->calle != null and $familiar->domicilio->numero != null and $familiar->domicilio->localidad != null) {
                        if (!isset($domicilioFamiliar))
                            $domicilioFamiliar = new Domicilio;

                        $domicilioFamiliar->calle = strtoupper($familiar->domicilio->calle);
                        $domicilioFamiliar->numero = $familiar->domicilio->numero;
                        $domicilioFamiliar->piso = $familiar->domicilio->piso;
                        $domicilioFamiliar->departamento = strtoupper($familiar->domicilio->departamento);
                        $domicilioFamiliar->barrio_id = $familiar->domicilio->barrio;
                        $domicilioFamiliar->localidad_id = $familiar->domicilio->localidad;
                        $domicilioFamiliar->save();
                    }

                    //Guardo los datos de la persona
                    $personaFamiliar->documento = $familiar->documento;
                    $personaFamiliar->nacimiento = $familiar->nacimiento;
                    $personaFamiliar->nombres = strtoupper($familiar->nombres);
                    $personaFamiliar->apellido = strtoupper($familiar->apellido);
                    $personaFamiliar->telefono = $familiar->telefono;
                    $personaFamiliar->telefono_alternativo = $familiar->telefonoAlternativo;
                    $personaFamiliar->email = $familiar->email;
                    $personaFamiliar->genero_id = $familiar->genero;
                    $personaFamiliar->civil_id = $familiar->civil;
                    $personaFamiliar->escolaridad_id = $familiar->escolaridad;
                    $personaFamiliar->domicilio_id = $domicilioFamiliar ? $domicilioFamiliar->id : null;;
                    $personaFamiliar->save();

                    //Guardo el parentesco
                    $parentesco->participante_id = $request->id;
                    $parentesco->persona_id = $personaFamiliar->id;
                    $parentesco->oficio = strtoupper($familiar->oficio);
                    $parentesco->interesa_participar = $familiar->interesaParticipar;
                    $parentesco->interesa_terminar_estudios = $familiar->interesaEstudios;
                    $parentesco->vinculo_id = $familiar->vinculo;
                    $parentesco->save();

                    //Si el familiar estaba declarado como contacto de emergencia lo guardo
                    if ($familiar->emergencia)
                        $personaEmergencia = $personaFamiliar;
                }
            }

            //Quita los familiares eliminados
            if ($request->eliminados) {
                foreach ($request->eliminados as $eliminado) {
                    $eliminado = (object) $eliminado;

                    //Buscar la relación entre el participante y el familiar y borrarla
                    $parentesco = Parentesco::where('participante_id', $request->id)->where('persona_id', $eliminado->personaId);
                    $parentesco->delete();
                }
            }

            //Si no tiene ningu familiar como contacto de emergencia
            if (!isset($personaEmergencia)) {
                $request->contactoEmergencia = (object) $request->contactoEmergencia;

                //Si no es una persona registrada creo domicilio y persona
                if ($request->contactoEmergencia->personaId == null) {
                    $domicilioEmergencia = new Domicilio;
                    $personaEmergencia = new Persona;
                } else {
                    $personaEmergencia = Persona::find($request->contactoEmergencia->personaId);
                    $domicilioEmergencia = Domicilio::find($personaEmergencia->domicilio);
                }

                //Si tiene domicilio lo guardo
                if ($request->contactoEmergencia->calle != null and $request->contactoEmergencia->numero != null and $request->contactoEmergencia->localidad != null) {

                    if (!$domicilioEmergencia)
                        $domicilioEmergencia = new Domicilio;

                    $domicilioEmergencia->calle = strtoupper($request->contactoEmergencia->calle);
                    $domicilioEmergencia->numero = $request->contactoEmergencia->numero;
                    $domicilioEmergencia->piso = $request->contactoEmergencia->piso;
                    $domicilioEmergencia->departamento = strtoupper($request->contactoEmergencia->departamento);
                    $domicilioEmergencia->barrio_id = $request->contactoEmergencia->barrio;
                    $domicilioEmergencia->localidad_id = $request->contactoEmergencia->localidad;
                    $domicilioEmergencia->save();
                    $personaEmergencia->domicilio_id = $domicilioEmergencia->id;
                }

                //Guardo los datos de la persona
                $personaEmergencia->nombres = strtoupper($request->contactoEmergencia->nombres);
                $personaEmergencia->apellido = strtoupper($request->contactoEmergencia->apellido);
                $personaEmergencia->telefono = $request->contactoEmergencia->telefono;
                $personaEmergencia->telefono_alternativo = $request->contactoEmergencia->telefonoAlternativo;
                $personaEmergencia->save();
            } else {
                //Si viene la persona de familiares
                //ESTO CREO QUE NO SERÍA NECESARIO, PORQUE SI VIENE DEL FAMILIAR EL CONTACTO
                //SIMPLEMENTE LO SIGUE UTILIZANDO

            }

            //Si no viene con ID de persona, crea un contacto
            //Sino busca una relación entre el participante y la persona como contacto de emergencia
            //Esto está mal, porque para editar asumo que ya todos tienen contacto de emegencia (obligatorio)
            //Entonces solamente debería buscar el registro correspondiente a esa persona y actualizar el ID de persona         
            /*if ($request->contactoEmergencia->personaId == null) {
                $contactoEmergencia = new ContactoEmergencia();
            } else {
                $contactoEmergencia = ContactoEmergencia::where('persona_id', $request->contactoEmergencia->personaId)
                    ->where('participante_id', $request->id)
                    ->first();

                //Si no encuentra una relación previa, crea un nuevo contacto
                if (!$contactoEmergencia)
                    $contactoEmergencia = new ContactoEmergencia();
            }*/

            //Guarda la relación del contacto de emergencia
            $contactoEmergencia = ContactoEmergencia::where('participante_id', $request->id)->first();
            $contactoEmergencia->persona_id = $personaEmergencia->id;
            $contactoEmergencia->save();

            DB::commit();

            $resultado->exito = true;
        } catch (\Exception $e) {
            DB::rollBack();
            dd($e);
            if (!isset($error->mensaje))
                $error->mensaje = 'Error dar de alta el participante.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function eliminar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Busca el curso
            $participante = Participante::find($request->id);

            if ($participante == null) {
                $error->mensaje = 'No existe el elemento solicitado.';
                throw new \Exception('No existe el elemento solicitado.');
            }

            //Desactiva el participante
            $participante->activo = 0;
            $participante->save();

            //Elimina las relaciones con el grupo familiar y el contacto de emergencia
            Parentesco::where('participante_id', $request->id)->delete();
            ContactoEmergencia::where('participante_id', $request->id)->delete();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al eliminar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function verificarExistencia(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Validaciones
            if ($request->documento == '' or $request->documento == null) {
                $error->mensaje = 'Complete el documento.';
                throw new \Exception('Complete el documento.');
            }

            if (!ctype_digit($request->documento)) {
                $error->mensaje = 'Documento inválido.';
                throw new \Exception('Documento inválido.');
            }

            $participantes = Participante::whereHas('persona', function ($query) use ($request) {
                $query->where('documento', $request->documento);
            })
                ->where('activo', 1)
                ->get();

            $resultado->existe = count($participantes) > 0;

            if (!$resultado->existe) {
                $persona = Persona::with('domicilio')->where('documento', $request->documento)->first();
                if ($persona) {
                    $resultado->persona = new \stdClass;
                    $resultado->persona->id = $persona->id;
                    $resultado->persona->nombres = $persona->nombres;
                    $resultado->persona->apellido = $persona->apellido;
                    $resultado->persona->nacimiento = $persona->nacimiento;
                    $resultado->persona->genero = $persona->genero_id;

                    $resultado->persona->domicilio = new \stdClass;
                    if ($persona->domicilio) {
                        $resultado->persona->domicilio->calle = $persona->domicilio->calle;
                        $resultado->persona->domicilio->numero = $persona->domicilio->numero;
                        $resultado->persona->domicilio->piso = $persona->domicilio->piso;
                        $resultado->persona->domicilio->departamento = $persona->domicilio->departamento;
                        $resultado->persona->domicilio->barrio = $persona->domicilio->barrio_id;
                        $resultado->persona->domicilio->localidad = $persona->domicilio->localidad_id;
                    }
                } else {
                    $resultado->persona = null;
                }
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al verificar el documento.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function verificarExistenciaPersona(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Validaciones
            if ($request->documento == '' or $request->documento == null) {
                $error->mensaje = 'Complete el DNI.';
                throw new \Exception('Complete el DNI.');
            }

            if (!ctype_digit($request->documento)) {
                $error->mensaje = 'DNI inválido.';
                throw new \Exception('DNI inválido.');
            }

            $persona = Persona::with('domicilio')->where('documento', $request->documento)->first();
            $resultado->existe = $persona ? true : false;

            if ($resultado->existe) {
                $resultado->persona = new \stdClass;
                $resultado->persona->id = $persona->id;
                $resultado->persona->nombres = $persona->nombres;
                $resultado->persona->apellido = $persona->apellido;
                $resultado->persona->nacimiento = $persona->nacimiento;
                $resultado->persona->genero = $persona->genero_id;
                $resultado->persona->civil = $persona->civil_id;
                $resultado->persona->telefono = $persona->telefono;
                $resultado->persona->telefonoAlternativo = $persona->telefono_alternativo;
                $resultado->persona->email = $persona->email;
                $resultado->persona->escolaridad = $persona->escolaridad_id;

                $resultado->persona->domicilio = new \stdClass;
                if ($persona->domicilio) {
                    $resultado->persona->domicilio->calle = $persona->domicilio->calle;
                    $resultado->persona->domicilio->numero = $persona->domicilio->numero;
                    $resultado->persona->domicilio->piso = $persona->domicilio->piso;
                    $resultado->persona->domicilio->departamento = $persona->domicilio->departamento;
                    $resultado->persona->domicilio->barrio = $persona->domicilio->barrio_id;
                    $resultado->persona->domicilio->localidad = $persona->domicilio->localidad_id;
                }
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al verificar DNI.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function participantesPorCurso(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            $participantes = Participante::whereHas('participante_curso', function ($query) use ($request) {
                $query->where('curso_id', $request->cursoId);
            })
                //->where('participante_curso.curso_id', $request->cursoId)
                ->where('activo', 1)
                ->get();
            $resultado->participantes = array();
            //$resultado->participantes = $participantes;

            foreach ($participantes as $participante) {
                $p = new \stdClass;
                $p->id = $participante->id;
                $p->documento = $participante->persona->documento;
                $p->nombres = $participante->persona->nombres;
                $p->apellido = $participante->persona->apellido;

                $resultado->participantes[] = $p;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los participantes.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function autocomplete()
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            $participantes = Participante::with('persona')->where('activo', 1)->get();
            $resultado->participantes = array();

            foreach ($participantes as $participante) {
                $p = new \stdClass;
                $str = 'DNI ' . $participante->persona->documento . ' - ' . $participante->persona->apellido . ', ' . $participante->persona->nombres;

                $p->value = $participante->id;
                $p->label = $str;

                $resultado->participantes[] = $p;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los participantes.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function graficos(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            $_0_4 = Participante::whereHas('persona', function ($query) {
                $query->whereRaw('TIMESTAMPDIFF(YEAR, nacimiento, CURDATE()) <= 4');
            })
                ->where('activo', 1)
                ->count();

            $_5_9 = Participante::whereHas('persona', function ($query) {
                $query->whereRaw('TIMESTAMPDIFF(YEAR, nacimiento, CURDATE()) > 4')->whereRaw('TIMESTAMPDIFF(YEAR, nacimiento, CURDATE()) <= 9');
            })
                ->where('activo', 1)
                ->count();

            $_10_14 = Participante::whereHas('persona', function ($query) {
                $query->whereRaw('TIMESTAMPDIFF(YEAR, nacimiento, CURDATE()) > 9')->whereRaw('TIMESTAMPDIFF(YEAR, nacimiento, CURDATE()) <= 14');
            })
                ->where('activo', 1)
                ->count();

            $_15_17 = Participante::whereHas('persona', function ($query) {
                $query->whereRaw('TIMESTAMPDIFF(YEAR, nacimiento, CURDATE()) > 14')->whereRaw('TIMESTAMPDIFF(YEAR, nacimiento, CURDATE()) <= 17');
            })
                ->where('activo', 1)
                ->count();

            $_18_ = Participante::whereHas('persona', function ($query) {
                $query->whereRaw('TIMESTAMPDIFF(YEAR, nacimiento, CURDATE()) >= 18');
            })
                ->where('activo', 1)
                ->count();

            $desconocida = Participante::whereHas('persona', function ($query) {
                $query->whereNull('nacimiento');
            })
                ->where('activo', 1)
                ->count();

            $resultado->edades = [$_0_4, $_5_9, $_10_14, $_15_17, $_18_, $desconocida];

            $inicial = Participante::whereHas('grado', function ($query) {
                $query->where('nivel', 'like', 'INICIAL');
            })
                ->where('activo', 1)
                ->count();

            $primario = Participante::whereHas('grado', function ($query) {
                $query->where('nivel', 'like', 'PRIMARIO');
            })
                ->where('activo', 1)
                ->count();

            $secundario = Participante::whereHas('grado', function ($query) {
                $query->where('nivel', 'like', 'SECUNDARIO');
            })
                ->where('activo', 1)
                ->count();

            $ninguno = Participante::doesntHave('grado')
                ->where('activo', 1)
                ->count();

            $resultado->escolaridades = [$inicial, $primario, $secundario, $ninguno];

            $mujeres = Participante::whereHas('persona', function ($query) {
                $query->where('genero_id', 1);
            })
                ->where('activo', 1)
                ->count();

            $hombres = Participante::whereHas('persona', function ($query) {
                $query->where('genero_id', 2);
            })
                ->where('activo', 1)
                ->count();

            $otres = Participante::whereHas('persona', function ($query) {
                $query->where('genero_id', 3);
            })
                ->where('activo', 1)
                ->count();

            $resultado->generos = [$hombres, $mujeres, $otres];

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al obtener los datos.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }
}
