<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Educador;
use App\Persona;
use App\Domicilio;
use stdClass;

class EducadorController extends Controller
{
    public function listado(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Todos los educadores
            $educadores = Educador::with('persona')
                ->where('activo', 1)
                ->orderBy('fecha_alta', 'desc')
                ->get();

            $resultado->educadores = array();
            foreach ($educadores as $educador) {
                $e = new \stdClass;
                $e->id = $educador->id;
                $e->documento = $educador->persona->documento;
                $e->apellido = $educador->persona->apellido;
                $e->nombres = $educador->persona->nombres;
                $e->nacimiento = $educador->persona->nacimiento;
                $e->telefono = $educador->persona->telefono;
                $e->email = $educador->persona->email;
                $e->genero = $educador->persona->genero_id;
                $e->civil = $educador->persona->civil_id;

                if ($educador->persona->domicilio) {
                    $e->domicilio = new \stdClass;
                    $e->domicilio->calle = $educador->persona->domicilio->calle;
                    $e->domicilio->numero = $educador->persona->domicilio->numero;
                    $e->domicilio->piso = $educador->persona->domicilio->piso;
                    $e->domicilio->departamento = $educador->persona->domicilio->departamento;
                    $e->domicilio->barrio = $educador->persona->domicilio->barrio_id;
                    $e->domicilio->localidad = $educador->persona->domicilio->localidad_id;
                } else {
                    $e->domicilio = null;
                }

                $resultado->educadores[] = $e;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los educadores.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function alta(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'nacimiento' => 'date|nullable',
            'nombres' => 'required|string',
            'apellido' => 'required|string',
            'calle' => 'string|nullable',
            'numero' => 'string|nullable',
            'piso' => 'string|nullable',
            'departamento' => 'string|nullable',
            'barrio' => 'integer|nullable',
            'localidad' => 'integer|nullable',
            'email' => 'email|nullable',
            'telefono' => 'string|nullable',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Alta del educador
            if ($request->personaId == null) {
                $request->domicilio = (object) $request->domicilio;
                $domicilio = new Domicilio;

                if ($request->domicilio->calle != null and $request->domicilio->numero != null and $request->domicilio->localidad != null) {
                    $domicilio->calle = strtoupper($request->domicilio->calle);
                    $domicilio->numero = $request->domicilio->numero;
                    $domicilio->piso = $request->domicilio->piso;
                    $domicilio->departamento = strtoupper($request->domicilio->departamento);
                    $domicilio->barrio_id = $request->domicilio->barrio;
                    $domicilio->localidad_id = $request->domicilio->localidad;
                    $domicilio->save();
                }
                //TODO: Si el domicilio es inválido debería avisar que no lo guarda

                $persona = new Persona;
                $persona->documento = $request->documento;
                $persona->nombres = strtoupper($request->nombres);
                $persona->apellido = strtoupper($request->apellido);
                $persona->nacimiento = $request->nacimiento;
                $persona->telefono = $request->telefono;
                $persona->email = $request->email;
                $persona->genero_id = $request->genero;
                $persona->civil_id = 1;
                $persona->domicilio_id = $domicilio->id;
                $persona->save();
            } else {
                $persona = new stdClass;
                $persona->id = $request->personaId;
            }

            $educador = new Educador;
            $educador->persona_id = $persona->id;
            $educador->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar el educador.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function eliminar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Busca el educador
            $educador = Educador::find($request->id);

            if ($educador == null) {
                $error->mensaje = 'No existe el elemento solicitado.';
                throw new \Exception('No existe el elemento solicitado.');
            }

            $educador->activo = 0;
            $educador->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al eliminar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function verificarExistencia(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Validaciones
            if ($request->documento == '' or $request->documento == null) {
                $error->mensaje = 'Complete el documento.';
                throw new \Exception('Complete el documento.');
            }

            if (!ctype_digit($request->documento)) {
                $error->mensaje = 'Documento inválido.';
                throw new \Exception('Documento inválido.');
            }

            $educadores = Educador::whereHas('persona', function ($query) use ($request) {
                $query->where('documento', $request->documento);
            })
                ->where('activo', 1)
                ->get();

            $resultado->existe = count($educadores) > 0;

            if (!$resultado->existe) {
                $persona = Persona::with('domicilio')->where('documento', $request->documento)->first();
                if ($persona) {
                    $resultado->persona = new \stdClass;
                    $resultado->persona->id = $persona->id;
                    $resultado->persona->nombres = $persona->nombres;
                    $resultado->persona->apellido = $persona->apellido;
                    $resultado->persona->nacimiento = $persona->nacimiento;
                    $resultado->persona->genero = $persona->genero_id;

                    $resultado->persona->domicilio = new \stdClass;
                    if ($persona->domicilio) {
                        $resultado->persona->domicilio->calle = $persona->domicilio->calle;
                        $resultado->persona->domicilio->numero = $persona->domicilio->numero;
                        $resultado->persona->domicilio->piso = $persona->domicilio->piso;
                        $resultado->persona->domicilio->departamento = $persona->domicilio->departamento;
                        $resultado->persona->domicilio->barrio = $persona->domicilio->barrio_id;
                        $resultado->persona->domicilio->localidad = $persona->domicilio->localidad_id;
                    }
                } else {
                    $resultado->persona = null;
                }
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al verificar el documento.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }
}
