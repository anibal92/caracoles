<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Movimiento;
use App\Usuario;

class FinanzasController extends Controller
{
    public function listado(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Busca los movimientos
            $movimientos = Movimiento::with('usuario')
                ->where('activo', 1)
                ->orderBy('fecha', 'desc')
                ->get();

            $resultado->movimientos = array();
            foreach ($movimientos as $movimiento) {
                $m = new \stdClass;
                $m->id = $movimiento->id;
                $m->monto = $movimiento->monto;
                $m->tipo = $movimiento->tipo;
                $m->fecha = $movimiento->fecha ? date("d-m-Y", strtotime($movimiento->fecha)) : null;
                $m->descripcion = $movimiento->descripcion;
                $resultado->movimientos[] = $m;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los movimientos.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function alta(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'monto' => 'required|numeric',
            'tipo' => 'required|string',
            'fecha' => 'required|date',
            'descripcion' => 'required|string'
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Alta del movimiento
            $movimiento = new Movimiento();
            $movimiento->monto = $request->monto;
            $movimiento->tipo = $request->tipo;
            $movimiento->fecha = $request->fecha;
            $movimiento->descripcion = $request->descripcion;
            $movimiento->usuario_id = session('usuarioId');
            $movimiento->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al guardar el movimiento.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function eliminar(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        //Validación
        $reglas = [
            'id' => 'required|integer',
        ];

        $validador = \Validator::make($request->all(), $reglas);

        try {
            //Valida los datos de entrada
            if ($validador->fails()) {
                $error->mensaje = 'Error de validación. Compruebe los campos.';
                throw new \Exception(implode('\n', $validador->errors()->all()));
            }

            //Busca el usuario
            $movimiento = Movimiento::find($request->id);

            if ($movimiento == null) {
                $error->mensaje = 'No existe el elemento solicitado.';
                throw new \Exception('No existe el elemento solicitado.');
            }

            $movimiento->activo = 0;
            $movimiento->save();

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al eliminar.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function resumen(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            $anio = $request->anio;
            $mes = (int)date('m');

            $totalIngresos = Movimiento::where('activo', 1)->where('tipo', 'INGRESO')->sum('monto');
            $totalEgresos = Movimiento::where('activo', 1)->where('tipo', 'EGRESO')->sum('monto');
            $anualIngresos = Movimiento::where('activo', 1)->where('tipo', 'INGRESO')->whereYear('fecha', $anio)->sum('monto');
            $anualEgresos = Movimiento::where('activo', 1)->where('tipo', 'EGRESO')->whereYear('fecha', $anio)->sum('monto');
            $mensualIngresos = Movimiento::where('activo', 1)->where('tipo', 'INGRESO')->whereMonth('fecha', $mes)->whereYear('fecha', $anio)->sum('monto');
            $mensualEgresos = Movimiento::where('activo', 1)->where('tipo', 'EGRESO')->whereMonth('fecha', $mes)->whereYear('fecha', $anio)->sum('monto');

            $resultado->anio = $anio;
            $resultado->mes = $mes;
            $resultado->total = $totalIngresos - $totalEgresos;
            $resultado->anual =  $anualIngresos - $anualEgresos;
            $resultado->anualIngresos =  $anualIngresos;
            $resultado->anualEgresos =  $anualEgresos;
            //$resultado->mensual = $mensualIngresos - $mensualEgresos;

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los movimientos.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function balanceAnual(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Busca los movimientos
            $movimientos = Movimiento::where('activo', 1)
                ->whereYear('fecha', $request->anio)
                ->sum('monto');

            $resultado->movimientos = array();
            foreach ($movimientos as $movimiento) {
                $m = new \stdClass;
                $m->id = $movimiento->id;
                $m->monto = $movimiento->monto;
                $m->tipo = $movimiento->tipo;
                $m->fecha = $movimiento->fecha;
                $m->descripcion = $movimiento->descripcion;
                $resultado->movimientos[] = $m;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los movimientos.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }

    public function grafico(Request $request)
    {
        $resultado = new \stdClass;
        $error = new \stdClass;

        try {
            //Busca los movimientos
            /*$movimientos = Movimiento::with('usuario')
                ->where('activo', 1)
                ->whereYear('fecha', $request->anio)
                ->orderBy('fecha', 'desc')
                ->get();

            $resultado->movimientos = array();
            foreach ($movimientos as $movimiento) {
                $m = new \stdClass;
                $m->id = $movimiento->id;
                $m->monto = $movimiento->monto;
                $m->tipo = $movimiento->tipo;
                $m->fecha = $movimiento->fecha;
                $m->descripcion = $movimiento->descripcion;
                $resultado->movimientos[] = $m;
            }*/

            //Busca los ingresos
            $ingresos = Movimiento::select(DB::raw('SUM(monto) as total'), DB::raw('MONTH(fecha) as mes'))
                ->where('activo', 1)
                ->where('tipo', 'INGRESO')
                ->whereYear('fecha', $request->anio)
                ->orderBy('mes', 'asc')
                ->groupBy('mes')
                ->get();

            $egresos = Movimiento::select(DB::raw('SUM(monto) as total'), DB::raw('MONTH(fecha) as mes'))
                ->where('activo', 1)
                ->where('tipo', 'EGRESO')
                ->whereYear('fecha', $request->anio)
                ->orderBy('mes', 'asc')
                ->groupBy('mes')
                ->get();

            $resultado->ingresos = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            $resultado->egresos = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

            foreach ($ingresos as $ingreso) {
                $resultado->ingresos[$ingreso->mes - 1] += $ingreso->total;
            }

            foreach ($egresos as $egreso) {
                $resultado->egresos[$egreso->mes - 1] += $egreso->total;
            }

            $resultado->exito = true;
        } catch (\Exception $e) {
            if (!isset($error->mensaje))
                $error->mensaje = 'Error al listar los movimientos.';
            $error->descripcion = $e->getMessage();
            $error->mostrarToast = true;

            $resultado->exito = false;
            $resultado->error = $error;
        }

        return response()->json($resultado);
    }
}
