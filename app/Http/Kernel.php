<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            //\App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],

        'PermisoUsuarios' => [
            \App\Http\Middleware\Logueado::class,
            \App\Http\Middleware\PermisoUsuarios::class,
        ],

        'PermisoParticipantes' => [
            \App\Http\Middleware\Logueado::class,
            \App\Http\Middleware\PermisoParticipantes::class,
        ],
        'PermisoCursos' => [
            \App\Http\Middleware\Logueado::class,
            \App\Http\Middleware\PermisoCursos::class,
        ],
        'PermisoInventario' => [
            \App\Http\Middleware\Logueado::class,
            \App\Http\Middleware\PermisoInventario::class,
        ],
        'PermisoFinanzas' => [
            \App\Http\Middleware\Logueado::class,
            \App\Http\Middleware\PermisoFinanzas::class,
        ],
        'PermisoBiblioteca' => [
            \App\Http\Middleware\Logueado::class,
            \App\Http\Middleware\PermisoBiblioteca::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,

        'Logueado' => \App\Http\Middleware\Logueado::class,
        'NoLogueado' => \App\Http\Middleware\NoLogueado::class,
        'PermisoUsuarios' => \App\Http\Middleware\PermisoUsuarios::class,
        'PermisoCursos' => \App\Http\Middleware\PermisoCursos::class,
        'PermisoInventario' => \App\Http\Middleware\PermisoInventario::class,
        'PermisoFinanzas' => \App\Http\Middleware\PermisoFinanzas::class,
        'PermisoBiblioteca' => \App\Http\Middleware\PermisoBiblioteca::class,

    ];
}
