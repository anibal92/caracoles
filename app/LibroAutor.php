<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibroAutor extends Model
{
    protected $table = 'libro_autor';
    public $timestamps = false;

    public function libro()
    {
        return $this->belongsTo('App\Libro', 'libro_id');
    }

    public function autor()
    {
        return $this->belongsTo('App\Autor', 'autor_id');
    }
}
