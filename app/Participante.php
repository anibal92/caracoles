<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participante extends Model
{
    protected $table = 'participantes';
    public $timestamps = false;

    public function persona()
    {
        return $this->belongsTo('App\Persona', 'persona_id');
    }

    public function escuela()
    {
        return $this->belongsTo('App\Escuela', 'escuela_id');
    }

    public function grado()
    {
        return $this->belongsTo('App\Grado', 'grado_id');
    }

    public function programa()
    {
        return $this->belongsTo('App\Programa', 'programa_id');
    }

    public function datos_salud()
    {
        return $this->hasOne('App\DatosSalud', 'participante_id');
    }

    public function contacto_emergencia()
    {
        return $this->hasOne('App\ContactoEmergencia', 'participante_id');
    }

    public function participante_curso()
    {
        return $this->hasOne('App\ParticipanteCurso', 'participante_id');
    }

    public function seguimientos()
    {
        return $this->hasMany('App\Seguimiento', 'participante_id');
    }

    public function parentescos()
    {
        return $this->hasMany('App\Parentesco', 'participante_id');
    }

    public function prestamos()
    {
        return $this->hasMany('App\Prestamo', 'participante_id');
    }
}
