<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discapacidad extends Model
{
    protected $table = 'discapacidades';
    public $timestamps = false;

    public function datos_salud()
    {
        return $this->hasMany('App\DatosSalud', 'discapacidad_id');
    }
}
