<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoCivil extends Model
{
    protected $table = 'estado_civil';
    public $timestamps = false;

    public function personas()
    {
        return $this->hasMany('App\Persona', 'civil_id');
    }
}
