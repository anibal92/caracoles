<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    protected $table = 'horarios';
    public $timestamps = false;

    public function curso()
    {
        return $this->belongsTo('App\Curso', 'curso_id');
    }
}
