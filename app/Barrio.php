<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barrio extends Model
{
    protected $table = 'barrios';
    public $timestamps = false;

    public function domicilios()
    {
        return $this->hasMany('App\Domicilio', 'barrio_id');
    }
}
