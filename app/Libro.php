<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    protected $table = 'libros';
    public $timestamps = false;

    public function libro_autor()
    {
        return $this->hasMany('App\LibroAutor', 'libro_id');
    }

    public function literario()
    {
        return $this->belongsTo('App\Literario', 'literario_id');
    }

    public function ejemplares()
    {
        return $this->hasMany('App\Ejemplar', 'libro_id');
    }
}
