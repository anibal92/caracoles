<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactoEmergencia extends Model
{
    protected $table = 'contactos_emergencia';
    public $timestamps = false;

    public function participante()
    {
        return $this->belongsTo('App\Participante', 'participante_id');
    }

    public function persona()
    {
        return $this->belongsTo('App\Persona', 'persona_id');
    }
}
