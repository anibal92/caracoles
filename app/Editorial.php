<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Editorial extends Model
{
    protected $table = 'editoriales';
    public $timestamps = false;

    public function ejemplares()
    {
        return $this->hasMany('App\Ejemplar', 'editorial_id');
    }
}
