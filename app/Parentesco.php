<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parentesco extends Model
{
    protected $table = 'parentescos';
    public $timestamps = false;

    public function participante()
    {
        return $this->belongsTo('App\Participante', 'participante_id');
    }

    public function persona()
    {
        return $this->belongsTo('App\Persona', 'persona_id');
    }

    public function vinculo()
    {
        return $this->belongsTo('App\Vinculo', 'vinculo_id');
    }
}
