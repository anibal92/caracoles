<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $table = 'cursos';
    public $timestamps = false;

    public function participante_curso()
    {
        return $this->hasMany('App\ParticipanteCurso', 'curso_id');
    }

    public function educador_curso()
    {
        return $this->hasMany('App\EducadorCurso', 'curso_id');
    }

    public function material_curso()
    {
        return $this->hasMany('App\MaterialCurso', 'curso_id');
    }

    public function horarios()
    {
        return $this->hasMany('App\Horario', 'curso_id');
    }
}
