<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParticipanteCurso extends Model
{
    protected $table = 'participante_curso';
    public $timestamps = false;

    public function participante()
    {
        return $this->belongsTo('App\Participante', 'participante_id');
    }

    public function curso()
    {
        return $this->belongsTo('App\Curso', 'curso_id');
    }
}
