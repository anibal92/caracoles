<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ejemplar extends Model
{
    protected $table = 'ejemplares';
    public $timestamps = false;

    public function prestamos()
    {
        return $this->hasMany('App\Prestamo', 'ejemplar_id');
    }

    public function libro()
    {
        return $this->belongsTo('App\Libro', 'libro_id');
    }

    public function editorial()
    {
        return $this->belongsTo('App\Editorial', 'libro_id');
    }
}
