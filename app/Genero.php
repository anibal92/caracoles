<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genero extends Model
{
    protected $table = 'generos';
    public $timestamps = false;

    public function personas()
    {
        return $this->hasMany('App\Persona', 'genero_id');
    }
}
