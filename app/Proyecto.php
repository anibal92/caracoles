<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    protected $table = 'proyectos';
    public $timestamps = false;

    public function materiales()
    {
        return $this->hasMany('App\Material', 'proyecto_id');
    }
}
