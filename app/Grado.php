<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grado extends Model
{
    protected $table = 'grados';
    public $timestamps = false;

    public function participantes()
    {
        return $this->hasMany('App\Participante', 'grado_id');
    }
}
