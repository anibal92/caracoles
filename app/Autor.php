<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    protected $table = 'autores';
    public $timestamps = false;

    public function libro_autor()
    {
        return $this->hasMany('App\LibroAutor', 'autor_id');
    }
}
