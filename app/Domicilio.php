<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domicilio extends Model
{
    protected $table = 'domicilios';
    public $timestamps = false;

    public function barrio()
    {
        return $this->belongsTo('App\Barrio', 'barrio_id');
    }

    public function localidad()
    {
        return $this->belongsTo('App\Localidad', 'localidad_id');
    }

    public function personas()
    {
        return $this->hasMany('App\Persona', 'domicilio_id');
    }
}
