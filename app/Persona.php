<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'personas';
    public $timestamps = false;

    public function genero()
    {
        return $this->belongsTo('App\Genero', 'genero_id');
    }

    public function estado_civil()
    {
        return $this->belongsTo('App\EstadoCivil', 'civil_id');
    }

    public function escolaridad()
    {
        return $this->belongsTo('App\Escolaridad', 'escolaridad_id');
    }

    public function domicilio()
    {
        return $this->belongsTo('App\Domicilio', 'domicilio_id');
    }

    public function participante()
    {
        return $this->hasOne('App\Participante', 'persona_id');
    }

    public function educador()
    {
        return $this->hasOne('App\Educador', 'persona_id');
    }

    public function parentescos()
    {
        return $this->hasMany('App\Parentesco', 'persona_id');
    }

    public function contacto_emergencia()
    {
        return $this->hasMany('App\ContactoEmergencia', 'persona_id');
    }
}
