<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoMaterial extends Model
{
    protected $table = 'estados_material';
    public $timestamps = false;

    public function materiales()
    {
        return $this->hasMany('App\Material', 'estado_id');
    }
}
