<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programa extends Model
{
    protected $table = 'programas';
    public $timestamps = false;

    public function participantes()
    {
        return $this->hasMany('App\Participante', 'programa_id');
    }
}
