<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escuela extends Model
{
    protected $table = 'escuelas';
    public $timestamps = false;

    public function participantes()
    {
        return $this->hasMany('App\Participante', 'escuela_id');
    }
}
