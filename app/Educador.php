<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Educador extends Model
{
    protected $table = 'educadores';
    public $timestamps = false;

    public function persona()
    {
        return $this->belongsTo('App\Persona', 'persona_id');
    }

    public function educador_curso()
    {
        return $this->hasMany('App\EducadorCurso', 'educador_id');
    }
}
