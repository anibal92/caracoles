<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducadorCurso extends Model
{
    protected $table = 'educador_curso';
    public $timestamps = false;

    public function educador()
    {
        return $this->belongsTo('App\Educador', 'educador_id');
    }

    public function curso()
    {
        return $this->belongsTo('App\Curso', 'curso_id');
    }
}
