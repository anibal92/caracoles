<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escolaridad extends Model
{
    protected $table = 'escolaridades';
    public $timestamps = false;

    public function personas()
    {
        return $this->hasMany('App\Persona', 'escolaridad_id');
    }
}
