<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Localidad extends Model
{
    protected $table = 'localidades';
    public $timestamps = false;

    public function provincia()
    {
        return $this->belongsTo('App\Provincia', 'provincia_id');
    }

    public function domicilios()
    {
        return $this->hasMany('App\Domicilio', 'domicilio_id');
    }
}
