<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    protected $table = 'provincias';
    public $timestamps = false;

    public function localidades()
    {
        return $this->hasMany('App\Localidad', 'provincia_id');
    }
}
