<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Generales
Route::get('/', function () {
    return view('inicio');
})->middleware('Logueado');

Route::get('error', function () {
    return view('error');
})->middleware('Logueado');

Route::post('participantes/autocomplete', 'ParticipanteController@autocomplete')->middleware('Logueado');

Route::prefix('base')->middleware('PermisoUsuarios')->group(function () {
    Route::get('civil', 'DatosBaseController@obtenerEstadoCivil');
    Route::get('barrios', 'DatosBaseController@obtenerBarrios');
    Route::get('discapacidades', 'DatosBaseController@obtenerDiscapacidades');
    Route::get('escolaridades', 'DatosBaseController@obtenerEscolaridades');
    Route::get('escuelas', 'DatosBaseController@obtenerEscuelas');
    Route::get('generos', 'DatosBaseController@obtenerGeneros');
    Route::get('grados', 'DatosBaseController@obtenerGrados');
    Route::get('sanguineo', 'DatosBaseController@obtenerGrupoSanguineo');
    Route::get('localidades/{id}', 'DatosBaseController@obtenerLocalidades');
    Route::get('programas', 'DatosBaseController@obtenerProgramas');
    Route::get('provincias', 'DatosBaseController@obtenerProvincias');
    Route::get('vinculos', 'DatosBaseController@obtenerVinculos');
    Route::get('educadores', 'DatosBaseController@obtenerEducadores');
    Route::get('categorias', 'DatosBaseController@obtenerCategorias');
    Route::get('estados/material', 'DatosBaseController@obtenerEstadosMaterial');
    Route::get('proyectos', 'DatosBaseController@obtenerProyectos');
    Route::get('literarios', 'DatosBaseController@obtenerLiterarios');
    Route::get('autores', 'DatosBaseController@obtenerAutores');
    Route::get('editoriales', 'DatosBaseController@obtenerEditoriales');
    Route::get('participantes', 'DatosBaseController@obtenerParticipantes');
});

//Usuario
Route::get('login', function () {
    return view('Usuario.login');
})->middleware('NoLogueado');

Route::get('logout', 'UsuarioController@logout')->middleware('Logueado');

Route::prefix('usuario')->middleware('Logueado')->group(function () {
});

Route::post('usuario/login', 'UsuarioController@auth');

Route::get('consulta', function () {
    return view('Biblioteca.consulta');
})->middleware('NoLogueado');

Route::post('consulta', 'BibliotecaController@listado')->middleware('NoLogueado');

//GET
Route::get('usuario/ajustes', function () {
    return view('Usuario.ajustes');
});
Route::post('usuario/contrasenia', 'UsuarioController@cambiarContrasenia');

//Usuarios
Route::prefix('usuarios')->middleware('PermisoUsuarios')->group(function () {
    //GET
    Route::get('alta', function () {
        return view('Usuarios.alta');
    });
    Route::get('listado', function () {
        return view('Usuarios.listado');
    });
    Route::get('ver/{id}', function ($id) {
        return view('Usuarios.ver', ['id' => $id]);
    });

    //POST
    Route::post('listado', 'UsuarioController@listado');
    Route::post('alta', 'UsuarioController@alta');
    Route::post('ver', 'UsuarioController@ver');
    Route::post('editar', 'UsuarioController@editar');
    Route::post('eliminar', 'UsuarioController@eliminar');
    Route::post('contrasenia', 'UsuarioController@recuperarContrasenia');
});

Route::prefix('backup')->middleware('PermisoUsuarios')->group(function () {
    //GET
    Route::get('listado', function () {
        return view('Backup.listado');
    });
    Route::get('descargar/{archivo}', 'BackupController@descargar');

    //POST
    Route::post('listado', 'BackupController@listado');
    Route::post('eliminar', 'BackupController@eliminar');
    Route::post('/crear', function () {
        $params = array();
        $params['--only-db'] = true;
        $exitCode = Artisan::call('backup:run', $params);

        return $exitCode;
    });
});

//Participantes
Route::prefix('participantes')->middleware('PermisoParticipantes')->group(function () {
    //GET
    Route::get('alta', function () {
        return view('Participantes.alta');
    });
    Route::get('listado', function () {
        return view('Participantes.listado');
    });

    Route::get('ver/{id}', function ($id) {
        return view('Participantes.ver', ['id' => $id]);
    });

    Route::get('graficos', function () {
        return view('Participantes.graficos');
    });

    Route::get('editar/{id}', function ($id) {
        return view('Participantes.editar', ['id' => $id]);
    });

    //POST
    Route::post('listado', 'ParticipanteController@listado');
    Route::post('alta', 'ParticipanteController@alta');
    Route::post('ver', 'ParticipanteController@ver');
    Route::post('eliminar', 'ParticipanteController@eliminar');
    Route::post('editar', 'ParticipanteController@editar');
    Route::post('graficos', 'ParticipanteController@graficos');

    Route::post('seguimiento/alta', 'SeguimientoController@alta');
    Route::post('seguimiento/listado', 'SeguimientoController@listado');
    Route::post('seguimiento/eliminar', 'SeguimientoController@eliminar');
    Route::post('existe', 'ParticipanteController@verificarExistencia');
    Route::post('persona/existe', 'ParticipanteController@verificarExistenciaPersona');
});

//Cursos
Route::prefix('cursos')->middleware('PermisoCursos')->group(function () {
    //GET
    Route::get('listado', function () {
        return view('Cursos.listado');
    });
    Route::get('educadores/listado', function () {
        return view('Cursos.educadores');
    });

    Route::get('graficos', function () {
        return view('Cursos.graficos');
    });

    //POST
    Route::post('listado', 'CursoController@listado');
    Route::post('alta', 'CursoController@alta');
    Route::post('editar', 'CursoController@editar');
    Route::post('eliminar', 'CursoController@eliminar');
    Route::post('graficos', 'CursoController@graficos');

    Route::post('participantes', 'ParticipanteController@participantesPorCurso');
    Route::post('participantes/agregar', 'CursoController@agregarParticipante');
    Route::post('participantes/quitar', 'CursoController@quitarParticipante');

    Route::post('educadores/listado', 'EducadorController@listado');
    Route::post('educadores/alta', 'EducadorController@alta');
    Route::post('educadores/editar', 'EducadorController@editar');
    Route::post('educadores/existe', 'EducadorController@verificarExistencia');
    Route::post('educadores/eliminar', 'EducadorController@eliminar');
});

//Inventario
Route::prefix('inventario')->middleware('PermisoInventario')->group(function () {
    //GET
    Route::get('listado', function () {
        return view('Inventario.listado');
    });
    Route::get('graficos', function () {
        return view('Inventario.graficos');
    });

    //POST
    Route::post('material/listado', 'MaterialController@listado');
    Route::post('material/alta', 'MaterialController@alta');
    Route::post('material/editar', 'MaterialController@editar');
    Route::post('material/eliminar', 'MaterialController@eliminar');
    Route::post('material/lote', 'MaterialController@lote');
    Route::post('graficos', 'MaterialController@graficos');
});

//Finanzas
Route::prefix('finanzas')->middleware('PermisoFinanzas')->group(function () {
    //GET
    Route::get('listado', function () {
        return view('Finanzas.listado');
    });
    Route::get('graficos', function () {
        return view('Finanzas.graficos');
    });

    //POST
    Route::post('listado', 'FinanzasController@listado');
    Route::post('alta', 'FinanzasController@alta');
    Route::post('eliminar', 'FinanzasController@eliminar');
    Route::post('balance/anual', 'FinanzasController@balanceAnual');
    Route::post('graficos', 'FinanzasController@grafico');
    Route::post('resumen', 'FinanzasController@resumen');
});

//Biblioteca
Route::prefix('biblioteca')->middleware('PermisoBiblioteca')->group(function () {
    //GET
    Route::get('libro/listado', function () {
        return view('Biblioteca.libros');
    });
    Route::get('prestamo/listado', function () {
        return view('Biblioteca.prestamos');
    });
    Route::get('graficos', function () {
        return view('Biblioteca.graficos');
    });

    //POST
    Route::post('libro/listado', 'BibliotecaController@listado');
    Route::post('libro/alta', 'BibliotecaController@alta');
    Route::post('libro/editar', 'BibliotecaController@editar');
    Route::post('libro/eliminar', 'BibliotecaController@eliminar');

    Route::post('ejemplar/alta', 'BibliotecaController@altaEjemplar');
    Route::post('ejemplar/listado', 'BibliotecaController@listadoEjemplar');
    Route::post('ejemplar/editar', 'BibliotecaController@editarEjemplar');
    Route::post('ejemplar/eliminar', 'BibliotecaController@eliminarEjemplar');

    Route::post('prestamo/alta', 'PrestamoController@alta');
    Route::post('prestamo/listado', 'PrestamoController@listado');
    Route::post('prestamo/editar', 'PrestamoController@editar');
    Route::post('prestamo/eliminar', 'PrestamoController@eliminar');
    Route::post('prestamo/devolucion', 'PrestamoController@devolucion');

    Route::post('autor/alta', 'BibliotecaController@altaAutor');
    Route::post('editorial/alta', 'BibliotecaController@altaEditorial');
    Route::post('literario/alta', 'BibliotecaController@altaLiterario');

    Route::post('graficos', 'BibliotecaController@graficos');
});

Route::get('prueba', function () {
    echo sha1('123456crcls');
});

Route::get('backups', function () {
    //    dd(Illuminate\Support\Facades\Storage::files('http---localhost'));
    echo now();
    $backups = Illuminate\Support\Facades\Storage::files('backups');
    $resultado = array();
    foreach ($backups as $backup) {
        $bu = new \stdClass;
        $bu->archivo = str_replace('backups/', '', $backup);;
        $bu->fecha = substr($backup, strlen(ENV('APP_URL')) + 1, 10);
        $bu->hora = str_replace('-', ':', substr($backup, strlen(ENV('APP_URL')) + 12, 8));
        $resultado[] = $bu;
    }
    dd($resultado);
    //return Illuminate\Support\Facades\Storage::download('http---localhost/2020-11-18-05-55-59.zip');
});
